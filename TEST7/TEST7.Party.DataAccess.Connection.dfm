object TEST6_Party_DataAccess_Connection: TTEST6_Party_DataAccess_Connection
  OldCreateOrder = True
  Height = 198
  Width = 282
  object Connection: TFDConnection
    Params.Strings = (
      'ConnectionDef=TEST6')
    BeforeCommit = ConnectionBeforeCommit
    AfterCommit = ConnectionAfterCommit
    Left = 40
    Top = 32
  end
  object FDQuery: TFDQuery
    BeforeEdit = FDQueryBeforeEdit
    AfterPost = FDQueryAfterPost
    OnUpdateRecord = FDQueryUpdateRecord
    Connection = Connection
    UpdateObject = FDUpdateSQL_Person
    SQL.Strings = (
      
      '/* ------------------------------------------------------------'
      'PURPOSE:'
      'All Families'
      '(Lists all families and the centers they are registered to or'
      'associated with)'
      'CREATED: 2017-05-12'
      '*/'
      ''
      'SELECT'
      '  pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, '
      '  g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, '
      '  pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, pg2.NAME AS CENTER'
      'FROM PARTY_PERSON pp'
      'JOIN GENDER g ON g.ID = pp.GENDER_ID'
      'JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID'
      'JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID'
      'JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID'
      'JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID'
      'ORDER BY pg2.NAME, pg.NAME, ph.RELATIONSHIP, pp.FIRST_NAME')
    Left = 128
    Top = 32
  end
  object FDUpdateSQL_Party1Person: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'VALUES (:PERSON_ID, 'PARTY')')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID')
    Left = 128
    Top = 80
  end
  object FDUpdateSQL_Party2Person: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO PARTY (ID, PARTY_TYPE_ID)'
      'SELECT :PERSON_ID, t.ID FROM PARTY_TYPE t'
      'WHERE t.NAME = ''PERSON''')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY p WHERE p.ID = :OLD_ID')
    Left = 128
    Top = 130
  end
  object FDUpdateSQL_Party3Person: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO PARTY_PERSON (ID, FIRST_NAME, LAST_NAME, '
      'GENDER_ID)'
      'SELECT :PERSON_ID, :NEW_FIRST_NAME, :NEW_LAST_NAME, g.ID'
      'FROM RDB$DATABASE'
      'LEFT JOIN GENDER g ON g.NAME = :NEW_GENDER')
    ModifySQL.Strings = (
      'UPDATE PARTY_PERSON pp SET pp.ID = :NEW_ID,'
      'pp.FIRST_NAME = :NEW_FIRST_NAME,'
      'pp.LAST_NAME = :NEW_LAST_NAME,'
      'pp.GENDER_ID = (SELECT g.ID FROM GENDER g '
      '  WHERE g.NAME = :NEW_GENDER)'
      'WHERE pp.ID = :OLD_ID')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY_PERSON pp WHERE pp.ID = :OLD_ID')
    FetchRowSQL.Strings = (
      'SELECT'
      '  pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, '
      '  g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, '
      '  pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, '
      '  pg2.NAME AS CENTER'
      'FROM PARTY_PERSON pp'
      'JOIN GENDER g ON g.ID = pp.GENDER_ID'
      'JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID'
      'JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID'
      'JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID'
      'JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID'
      'WHERE pp.ID = :OLD_ID;')
    Left = 128
    Top = 180
  end
  object FDUpdateSQL_Party4Group: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'SELECT :FAMILY_ID, ''PARTY'' FROM PARTY_TYPE t'
      'LEFT JOIN PARTY_GROUP pg ON pg.NAME = :NEW_FAMILY_NAME'
      'WHERE t.NAME = ''GROUP'' AND pg.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_FAMILY_ID'
      'AND NOT EXISTS (SELECT 1 FROM PARTY_HIERARCHY ph'
      'WHERE ph.PARENT_PARTY_ID = eb.ID AND'
      'ph.CHILD_PARTY_ID <> :OLD_ID)')
    Left = 150
    Top = 80
  end
  object FDUpdateSQL_Party5Group: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO PARTY (ID, PARTY_TYPE_ID)'
      'SELECT :FAMILY_ID, t.ID FROM PARTY_TYPE t'
      'LEFT JOIN PARTY_GROUP pg ON pg.NAME = :NEW_FAMILY_NAME'
      'WHERE t.NAME = ''GROUP'' AND pg.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY p WHERE p.ID = :OLD_FAMILY_ID'
      'AND NOT EXISTS (SELECT 1 FROM PARTY_HIERARCHY ph'
      'WHERE ph.PARENT_PARTY_ID = p.ID AND'
      'ph.CHILD_PARTY_ID <> :OLD_ID)')
    Left = 150
    Top = 130
  end
  object FDUpdateSQL_Party6Group: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO PARTY_GROUP (ID, NAME)'
      'SELECT :FAMILY_ID, :NEW_FAMILY_NAME FROM PARTY_TYPE t'
      'LEFT JOIN PARTY_GROUP pg ON pg.NAME = :NEW_FAMILY_NAME'
      'WHERE t.NAME = ''GROUP'' AND pg.ID IS NULL')
    ModifySQL.Strings = (
      'UPDATE PARTY_GROUP pg SET pg.ID = :NEW_FAMILY_ID, '
      '  pg.NAME = :NEW_FAMILY_NAME'
      'WHERE pg.ID = :OLD_FAMILY_ID')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY_GROUP pg WHERE pg.ID = :OLD_FAMILY_ID'
      'AND NOT EXISTS (SELECT 1 FROM PARTY_HIERARCHY ph'
      'WHERE ph.PARENT_PARTY_ID = pg.ID AND'
      'ph.CHILD_PARTY_ID <> :OLD_ID)')
    Left = 150
    Top = 180
  end
  object FDUpdateSQL_Party7Hierarchy: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) '
      'SELECT :HIERARCHY_ID, ''PARTY_HIERARCHY'' '
      'FROM RDB$DATABASE LEFT JOIN PARTY_HIERARCHY ph '
      '  ON ph.CHILD_PARTY_ID = :NEW_ID '
      '  AND ph.PARENT_PARTY_ID = :NEW_FAMILY_ID'
      'WHERE ph.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE EXISTS ('
      'SELECT 1 FROM PARTY_HIERARCHY ph WHERE ph.ID = eb.ID '
      'AND ph.CHILD_PARTY_ID = :OLD_ID '
      'AND ph.PARENT_PARTY_ID = :OLD_FAMILY_ID)')
    Left = 175
    Top = 80
  end
  object FDUpdateSQL_Party8Hierarchy: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO PARTY_HIERARCHY (ID, PARENT_PARTY_ID, '
      'CHILD_PARTY_ID, RELATIONSHIP)'
      'SELECT :HIERARCHY_ID, :NEW_FAMILY_ID, :NEW_ID, :NEW_RELATIONSHIP'
      'FROM RDB$DATABASE LEFT JOIN PARTY_HIERARCHY ph '
      '  ON ph.CHILD_PARTY_ID = :NEW_ID '
      '  AND ph.PARENT_PARTY_ID = :NEW_FAMILY_ID'
      'WHERE ph.ID IS NULL')
    ModifySQL.Strings = (
      'UPDATE PARTY_HIERARCHY ph SET ph.RELATIONSHIP = :NEW_RELATIONSHIP'
      'WHERE ph.CHILD_PARTY_ID = :NEW_ID '
      'AND ph.PARENT_PARTY_ID = :NEW_FAMILY_ID)')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY_HIERARCHY ph '
      'WHERE ph.CHILD_PARTY_ID = :OLD_ID '
      'AND ph.PARENT_PARTY_ID = :OLD_FAMILY_ID)')
    Left = 175
    Top = 130
  end
  object FDUpdateSQL_Party9Hierarchy: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) '
      'SELECT :HIERARCHY_ID, ''PARTY_HIERARCHY'' '
      'FROM RDB$DATABASE '
      'JOIN PARTY_GROUP pg ON pg.NAME = :NEW_CENTER'
      'LEFT JOIN PARTY_HIERARCHY ph '
      '  ON ph.CHILD_PARTY_ID = :NEW_FAMILY_ID '
      '  AND ph.PARENT_PARTY_ID = pg.ID'
      'WHERE ph.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE EXISTS ('
      'SELECT 1 FROM PARTY_HIERARCHY ph '
      'JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID '
      'WHERE ph.ID = eb.ID AND pg.NAME = :OLD_CENTER'
      'AND ph.CHILD_PARTY_ID = :OLD_FAMILY_ID) AND NOT EXISTS ('
      'SELECT 1 FROM PARTY_HIERARCHY ph WHERE '
      'ph.PARENT_PARTY_ID = :OLD_FAMILY_ID '
      'AND ph.CHILD_PARTY_ID <> :OLD_ID)')
    Left = 190
    Top = 80
  end
  object FDUpdateSQL_Party10Hierarchy: TFDUpdateSQL
    InsertSQL.Strings = (
      'INSERT INTO PARTY_HIERARCHY (ID, PARENT_PARTY_ID, '
      'CHILD_PARTY_ID, RELATIONSHIP)'
      'SELECT :HIERARCHY_ID, pg.ID, :NEW_FAMILY_ID, ''Member'' '
      'FROM RDB$DATABASE '
      'JOIN PARTY_GROUP pg ON pg.NAME = :NEW_CENTER'
      'LEFT JOIN PARTY_HIERARCHY ph '
      '  ON ph.CHILD_PARTY_ID = :NEW_FAMILY_ID '
      '  AND ph.PARENT_PARTY_ID = pg.ID'
      'WHERE ph.ID IS NULL')
    ModifySQL.Strings = (
      'UPDATE PARTY_HIERARCHY ph SET ph.PARENT_PARTY_ID = ('
      'SELECT pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = :NEW_CENTER)'
      'WHERE ph.CHILD_PARTY_ID = :OLD_FAMILY_ID ')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY_HIERARCHY ph '
      'WHERE ph.CHILD_PARTY_ID = :OLD_FAMILY_ID '
      'AND EXISTS (SELECT 1 FROM PARTY_GROUP pg WHERE '
      'pg.ID = ph.PARENT_PARTY_ID AND pg.NAME = :OLD_CENTER)'
      'AND NOT EXISTS ('
      'SELECT 1 FROM PARTY_HIERARCHY ph2 WHERE '
      'ph2.PARENT_PARTY_ID = ph.CHILD_PARTY_ID '
      'AND ph2.CHILD_PARTY_ID <> :OLD_ID)'
      )
    Left = 190
    Top = 130
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Tracing = True
    Left = 40
    Top = 80
  end
end