unit TEST7.BusinessObject.Entity_RAW;

interface

uses
  SysUtils, 
  Generics.Collections, 
  Aurelius.Mapping.Attributes, 
  Aurelius.Types.Blob, 
  Aurelius.Types.DynamicProperties, 
  Aurelius.Types.Nullable, 
  Aurelius.Types.Proxy, 
  Aurelius.Criteria.Dictionary;

type
  TEntityBase = class;
  TActivity = class;
  TActivityHierarchy = class;
  TActivityType = class;
  TAddress = class;
  TAddressHierarchy = class;
  TAddressType = class;
  TGender = class;
  TGrade = class;
  TLocator = class;
  TParticipant = class;
  TParticipantStatus = class;
  TParty = class;
  TPartyHierarchy = class;
  TPartyRole = class;
  TPartyRoleThing = class;
  TPartyRoleType = class;
  TPartyType = class;
  TThing = class;
  TThingHierarchy = class;
  TThingType = class;
  TPartyGroup = class;
  TPartyPerson = class;
  TPartyRoleCustomer = class;
  TPartyRoleGuardian = class;
  TPartyRoleLead = class;
  TPartyRoleLearningCenter = class;
  TPartyRoleStaff = class;
  TPartyRoleStudent = class;
  TActivityTableDictionary = class;
  TActivityHierarchyTableDictionary = class;
  TActivityTypeTableDictionary = class;
  TAddressTableDictionary = class;
  TAddressHierarchyTableDictionary = class;
  TAddressTypeTableDictionary = class;
  TEntityBaseTableDictionary = class;
  TGenderTableDictionary = class;
  TGradeTableDictionary = class;
  TLocatorTableDictionary = class;
  TParticipantTableDictionary = class;
  TParticipantStatusTableDictionary = class;
  TPartyTableDictionary = class;
  TPartyGroupTableDictionary = class;
  TPartyHierarchyTableDictionary = class;
  TPartyPersonTableDictionary = class;
  TPartyRoleTableDictionary = class;
  TPartyRoleCustomerTableDictionary = class;
  TPartyRoleGuardianTableDictionary = class;
  TPartyRoleLeadTableDictionary = class;
  TPartyRoleLearningCenterTableDictionary = class;
  TPartyRoleStaffTableDictionary = class;
  TPartyRoleStudentTableDictionary = class;
  TPartyRoleThingTableDictionary = class;
  TPartyRoleTypeTableDictionary = class;
  TPartyTypeTableDictionary = class;
  TThingTableDictionary = class;
  TThingHierarchyTableDictionary = class;
  TThingTypeTableDictionary = class;
  
  [Entity]
  [Table('ENTITY_BASE')]
  [Description('Common table which all other tables reference, no record can exist in any other table without first having an entry here. It stores standard information including the generated GUID/UUID primary key, created and updated timestamps, etc.')]
  [Inheritance(TInheritanceStrategy.JoinedTables)]
  [Id('FId', TIdGenerator.None)]
  TEntityBase = class
  private
    [Column('ID', [TColumnProp.Required], 16)]
    [Description('GUID Primary Key')]
    FId: TGuid;
    
    [Column('IS_ACTIVE', [TColumnProp.Required], 1)]
    [Description('Boolean indicator of whether or not the record is active')]
    FIsActive: Boolean;
    
    [Column('CREATED_BY', [TColumnProp.Required], 128)]
    [Description('User who created the record')]
    FCreatedBy: string;
    
    [Column('CREATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of creation')]
    FCreatedAt: TDateTime;
    
    [Column('UPDATED_BY', [TColumnProp.Required], 128)]
    [Description('User who last updated the record')]
    FUpdatedBy: string;
    
    [Column('UPDATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of last update')]
    FUpdatedAt: TDateTime;
    
    [Column('POS', [])]
    [Description('Integer index to track the order that records were created in a given day (for when records are creating at the same instanct and the timestamp is not enough)')]
    FPos: Nullable<Integer>;
    
    [Column('EXTERNAL_SOURCE', [], 35)]
    [Description('String field to store the name of the external source the data came from, e.g. CDS, M2, CYBERSOURCE, etc.')]
    FExternalSource: Nullable<string>;
    
    [Column('EXTERNAL_ID', [], 50)]
    [Description('String field to store an identifier from the external source the data came from.')]
    FExternalId: Nullable<string>;
    
    [Column('TABLE_NAME', [], 35)]
    [Description('String field to store the name of the TABLE that will indicate the subtype that this record is parent to. NOTE: This field does not fill in automatically, though a query can be written to search the entire database and update this field.')]
    FTableName: Nullable<string>;
  public
    property Id: TGuid read FId write FId;
    property IsActive: Boolean read FIsActive write FIsActive;
    property CreatedBy: string read FCreatedBy write FCreatedBy;
    property CreatedAt: TDateTime read FCreatedAt write FCreatedAt;
    property UpdatedBy: string read FUpdatedBy write FUpdatedBy;
    property UpdatedAt: TDateTime read FUpdatedAt write FUpdatedAt;
    property Pos: Nullable<Integer> read FPos write FPos;
    property ExternalSource: Nullable<string> read FExternalSource write FExternalSource;
    property ExternalId: Nullable<string> read FExternalId write FExternalId;
    property TableName: Nullable<string> read FTableName write FTableName;
  end;
  
  [Entity]
  [Table('ACTIVITY')]
  [Description('An event that typically is relevant within a time window. This table is hierarchical, so a record here can either be a single attribute of an activity (e.g. START_AT) or a container defining the master event record (e.g. TRIMATHLON).')]
  [PrimaryJoinColumn('ID')]
  TActivity = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('String value of the activity, one of 6 options to store the activity content.')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('Datetime value of the activity, one of 6 options to store the activity content.')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('Numeric/Decimal value of the activity, one of 6 options to store the activity content.')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('Boolean value of the activity, one of 6 options to store the activity content.')]
    FContentBoolean: Nullable<Boolean>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80, 0)]
    [Description('Blob value of the activity, one of 6 options to store the activity content.')]
    FContentBlob: TBlob;
    
    [Column('START_AT', [])]
    [Description('')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('')]
    FEndAt: Nullable<TDateTime>;
    
    [Column('TITLE', [], 255)]
    [Description('')]
    FTitle: Nullable<string>;
    
    [Column('ALIAS', [], 255)]
    [Description('')]
    FAlias: Nullable<string>;
    
    [Column('NOTE', [], 255)]
    [Description('')]
    FNote: Nullable<string>;
    
    [Column('IS_COMPLETE', [], 1)]
    [Description('')]
    FIsComplete: Nullable<Boolean>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ACTIVITY_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FActivityType: TActivityType;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<Boolean> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
    property Title: Nullable<string> read FTitle write FTitle;
    property Alias: Nullable<string> read FAlias write FAlias;
    property Note: Nullable<string> read FNote write FNote;
    property IsComplete: Nullable<Boolean> read FIsComplete write FIsComplete;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
    property ActivityType: TActivityType read FActivityType write FActivityType;
  end;
  
  [Entity]
  [Table('ACTIVITY_HIERARCHY')]
  [Description('Intermediate table between ACTIVITY and itself, allowing a many to many relationship; each activity can be made up of or related to other activites. e.g. the TriMathlon consists of 1 main event and is made up of sub-events for each grade level.')]
  [UniqueKey('PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID')]
  [PrimaryJoinColumn('ID')]
  TActivityHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('Integer value indicating the order that the child ACTIVITY record should appear among the other child ACTIVITY records with the same parent. This is also used for defining an enumeration lookup value.')]
    FPos: Nullable<Integer>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the relationship record represents a templated collection or not')]
    FIsTemplate: Boolean;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildActivity: TActivity;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentActivity: TActivity;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsTemplate: Boolean read FIsTemplate write FIsTemplate;
    property ChildActivity: TActivity read FChildActivity write FChildActivity;
    property ParentActivity: TActivity read FParentActivity write FParentActivity;
  end;
  
  [Entity]
  [Table('ACTIVITY_TYPE')]
  [Description('This table describes the subtypes that an ACTIVITY can be, which could be a top-level or container type (with its own KIND integer enumeration), or a lower-level detail type (with its own ATTRIBUTE integer enumeration).')]
  [PrimaryJoinColumn('ID')]
  TActivityType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the event type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the event type.')]
    FDescription: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('A character string that can be used to define how data in the ACTIVITY record is to be displayed or printed')]
    FFormatMask: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('A character string to describe what a valid entry is for the ACTIVITY record. This most likely will be a RegEx, or a Regular Expression.')]
    FValidation: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property Validation: Nullable<string> read FValidation write FValidation;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
    property DataType: Nullable<string> read FDataType write FDataType;
  end;
  
  [Entity]
  [Table('ADDRESS')]
  [Description('An address is a way to contact a person or place; this table does hold address information and simply acts as a single point for outside tables to reference its subtypes.')]
  [PrimaryJoinColumn('ID')]
  TAddress = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('')]
    FContentBoolean: Nullable<Boolean>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80)]
    [Description('')]
    [DBTypeMemo]
    FContentBlob: TBlob;
    
    [Column('CONTENT', [], 255)]
    [Description('Text value of the address. E.G. an email address, digits of a phone number, street address, country abbreviation, etc')]
    FContent: Nullable<string>;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the address, for abbreviated content like countries or states, this can hold the full name. E.g. the ADDRESS records with content of "NJ" can have a description of "New Jersey".')]
    FDescription: Nullable<string>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ADDRESS_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddressType: TAddressType;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<Boolean> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property Content: Nullable<string> read FContent write FContent;
    property Description: Nullable<string> read FDescription write FDescription;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
    property AddressType: TAddressType read FAddressType write FAddressType;
  end;
  
  [Entity]
  [Table('ADDRESS_HIERARCHY')]
  [Description('This table is an intermediate or junction table between ADDRESS and itself, allowing a many to many relationship. That is, each address can contain or be related to other address. For example the city "Chatham" can belong to the state "NJ".')]
  [UniqueKey('PARENT_ADDRESS_ID, CHILD_ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TAddressHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('Position of the child ADDRESS in reference to the other child ADDRESS that make up the associated parent ADDRESS, to preserve a desired order.')]
    FPos: Nullable<Integer>;
    
    [Column('RELATIONSHIP', [], 35)]
    [Description('Indicates the type of relationship between the parent and child ADDRESS.')]
    FRelationship: Nullable<string>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('')]
    FIsTemplate: Boolean;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildAddress: TAddress;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentAddress: TAddress;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property IsTemplate: Boolean read FIsTemplate write FIsTemplate;
    property ChildAddress: TAddress read FChildAddress write FChildAddress;
    property ParentAddress: TAddress read FParentAddress write FParentAddress;
  end;
  
  [Entity]
  [Table('ADDRESS_TYPE')]
  [Description('This table describes the types of ADDRESS. Each record in this table should correspond to a subtype table beginning with "ADDRESS_".')]
  [PrimaryJoinColumn('ID')]
  TAddressType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the address type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the address type.')]
    FDescription: Nullable<string>;
    
    [Column('KIND', [TColumnProp.Required])]
    [Description('Integer value to identify the kind of address, to be used as an Enumeration in Delphi for quick searching')]
    FKind: Integer;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('A character string that can be used to define how data in the CONTENT field of related ADDRESS records is to be displayed or printed')]
    FFormatMask: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('A character string to describe what a valid entry is for the CONTENT of related ADDRESS records. This most likely will be a RegEx, or a Regular Expression.')]
    FValidation: Nullable<string>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property Kind: Integer read FKind write FKind;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property Validation: Nullable<string> read FValidation write FValidation;
    property DataType: Nullable<string> read FDataType write FDataType;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
  end;
  
  [Entity]
  [Table('GENDER')]
  [Description('Stores the gender a Contact can be listed as. Useful mostly for the other information stored in this table which will help with email/mail merge fields (various gender-specific pronouns like she/he, her/him/ hers/his, herself/himself, Ms./Mr. etc.)')]
  [PrimaryJoinColumn('ID')]
  TGender = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Gender name: female, male, or neutral.')]
    FName: string;
    
    [Column('SUBJECT_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Subject pronoun: she, he, one.')]
    FSubjectPronoun: string;
    
    [Column('OBJECT_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Object pronoun: her, him, one.')]
    FObjectPronoun: string;
    
    [Column('POSSESSIVE_DET_PRONOUN', [], 10)]
    [Description('Possessive determiner pronoun: hers, his, ones.')]
    FPossessiveDetPronoun: Nullable<string>;
    
    [Column('POSSESSIVE_PRONOUN', [], 10)]
    [Description('Possessive pronoun: hers, his.')]
    FPossessivePronoun: Nullable<string>;
    
    [Column('REFLEXIVE_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Reflexive pronoun: herself, himself, oneself.')]
    FReflexivePronoun: string;
    
    [Column('TITLE', [], 5)]
    [Description('Title or prefix: Ms. and Mr.')]
    FTitle: Nullable<string>;
  public
    property Name: string read FName write FName;
    property SubjectPronoun: string read FSubjectPronoun write FSubjectPronoun;
    property ObjectPronoun: string read FObjectPronoun write FObjectPronoun;
    property PossessiveDetPronoun: Nullable<string> read FPossessiveDetPronoun write FPossessiveDetPronoun;
    property PossessivePronoun: Nullable<string> read FPossessivePronoun write FPossessivePronoun;
    property ReflexivePronoun: string read FReflexivePronoun write FReflexivePronoun;
    property Title: Nullable<string> read FTitle write FTitle;
  end;
  
  [Entity]
  [Table('GRADE')]
  [Description('A table to store the grade a student can be listed as. For all grades above highschool, e.g. college or otherwise, the grade will simply be listed as "Adult" and its numeric value will be 13.')]
  [PrimaryJoinColumn('ID')]
  TGrade = class(TEntityBase)
  private
    [Column('SERVICE_LEVEL_ID', [], 16)]
    [Description('Foreign Key to the service level that the grade belongs to.')]
    FServiceLevelId: Nullable<TGuid>;
    
    [Column('GRADE', [])]
    [Description('')]
    FGrade: Nullable<Integer>;
    
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the grade, e.g. 0 is Kindergarten, 13 is Adult.')]
    FName: string;
  public
    property ServiceLevelId: Nullable<TGuid> read FServiceLevelId write FServiceLevelId;
    property Grade: Nullable<Integer> read FGrade write FGrade;
    property Name: string read FName write FName;
  end;
  
  [Entity]
  [Table('LOCATOR')]
  [Description('This table is the intermediate or junction table between Parties and Addresses allowing a many to many relationship.')]
  [UniqueKey('PARTY_ID, ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TLocator = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Address: TAddress read FAddress write FAddress;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PARTICIPANT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TParticipant = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('')]
    FRelationship: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FActivity: TActivity;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTICIPANT_STATUS_ID', [], 'ID')]
    [Description('')]
    FParticipantStatus: TParticipantStatus;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_ID', [], 'ID')]
    [Description('')]
    FPartyRole: TPartyRole;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property Activity: TActivity read FActivity write FActivity;
    property ParticipantStatus: TParticipantStatus read FParticipantStatus write FParticipantStatus;
    property PartyRole: TPartyRole read FPartyRole write FPartyRole;
  end;
  
  [Entity]
  [Table('PARTICIPANT_STATUS')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TParticipantStatus = class(TEntityBase)
  private
    [Column('STATUS', [TColumnProp.Required], 35)]
    [Description('')]
    FStatus: string;
    
    [Column('DESCRIPTION', [], 1000)]
    [Description('')]
    FDescription: Nullable<string>;
  public
    property Status: string read FStatus write FStatus;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('PARTY')]
  [Description('A party is either a person or account that is associated with the business in some way, This table is the supertype for the entity types PERSON and GROUP.')]
  [PrimaryJoinColumn('ID')]
  TParty = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyType: TPartyType;
  public
    property PartyType: TPartyType read FPartyType write FPartyType;
  end;
  
  [Entity]
  [Table('PARTY_HIERARCHY')]
  [Description('This table is the intermediate or junction table between Parties and itself, allowing a many to many relationship. e.g. one party could represent a family and the parties related to it are the members of that family.')]
  [UniqueKey('PARENT_PARTY_ID, CHILD_PARTY_ID')]
  [PrimaryJoinColumn('ID')]
  TPartyHierarchy = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('The type of relationship (e.g. Member, Assistant, Representative, etc), can be null to show loosely related parties. The type of relationship should be thought of as {Child} is a {Relationship} of {Parent}.')]
    FRelationship: Nullable<string>;
    
    [Column('IS_PRIMARY', [TColumnProp.Required], 1)]
    [Description('Indicates that given a series of other parties related to the parent party, this relationship should be considered the primary one. For exampe, each family account must have at least one primary contact.')]
    FIsPrimary: Boolean;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the relationship between parties.')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentParty: TParty;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property IsPrimary: Boolean read FIsPrimary write FIsPrimary;
    property Note: Nullable<string> read FNote write FNote;
    property ChildParty: TParty read FChildParty write FChildParty;
    property ParentParty: TParty read FParentParty write FParentParty;
  end;
  
  [Entity]
  [Table('PARTY_ROLE')]
  [Description('This table is a supertype for all party roles. This table acts as the intermediary or joint table between parties and the roles that they can play.')]
  [PrimaryJoinColumn('ID')]
  TPartyRole = class(TEntityBase)
  private
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the the role this party is playing.')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRoleType: TPartyRoleType;
  public
    property Note: Nullable<string> read FNote write FNote;
    property Party: TParty read FParty write FParty;
    property PartyRoleType: TPartyRoleType read FPartyRoleType write FPartyRoleType;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_THING')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleThing = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('')]
    FRelationship: Nullable<string>;
    
    [Column('NOTE', [TColumnProp.Lazy], 80, 0)]
    [Description('')]
    FNote: TBlob;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRole: TPartyRole;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('THING_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FThing: TThing;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property Note: TBlob read FNote write FNote;
    property PartyRole: TPartyRole read FPartyRole write FPartyRole;
    property Thing: TThing read FThing write FThing;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_TYPE')]
  [Description('This table describes the various roles that a PARTY (either a person or account) can play. Each record in this table should correspond to a subtype table beginning with "PARTY_ROLE_".')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Primay Key and Name of the party role.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the party role.')]
    FDescription: Nullable<string>;
    
    [Column('IS_PERSON_ROLE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the entity role can be played by a person type party.')]
    FIsPersonRole: Boolean;
    
    [Column('IS_GROUP_ROLE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the entity role can be played by an account type party.')]
    FIsGroupRole: string;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property IsPersonRole: Boolean read FIsPersonRole write FIsPersonRole;
    property IsGroupRole: string read FIsGroupRole write FIsGroupRole;
  end;
  
  [Entity]
  [Table('PARTY_TYPE')]
  [Description('This table describes the subtypes that a PARTY can be. Each record in this table should correspond to a subtype table beginning with "PARTIES_".')]
  [PrimaryJoinColumn('ID')]
  TPartyType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the party type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the party type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('THING')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TThing = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('')]
    FContentBoolean: Nullable<Boolean>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80, 0)]
    [Description('')]
    FContentBlob: TBlob;
    
    [Column('TITLE', [], 255)]
    [Description('')]
    FTitle: Nullable<string>;
    
    [Column('NOTE', [TColumnProp.Lazy], 80, 0)]
    [Description('')]
    FNote: TBlob;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('THING_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FThingType: TThingType;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<Boolean> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property Title: Nullable<string> read FTitle write FTitle;
    property Note: TBlob read FNote write FNote;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
    property ThingType: TThingType read FThingType write FThingType;
  end;
  
  [Entity]
  [Table('THING_HIERARCHY')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TThingHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('')]
    FPos: Nullable<Integer>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('')]
    FIsTemplate: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_THING_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildThing: TThing;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_THING_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentThing: TThing;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsTemplate: string read FIsTemplate write FIsTemplate;
    property ChildThing: TThing read FChildThing write FChildThing;
    property ParentThing: TThing read FParentThing write FParentThing;
  end;
  
  [Entity]
  [Table('THING_TYPE')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TThingType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('')]
    FDescription: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('')]
    FFormatMask: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('')]
    FValidation: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property Validation: Nullable<string> read FValidation write FValidation;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
    property DataType: Nullable<string> read FDataType write FDataType;
  end;
  
  [Entity]
  [Table('PARTY_GROUP')]
  [Description('A group is a party that, like a person, can be associated with multiple addresses as well as other parties. A group can be a Family, a Mathnasium learning center, an Organization, etc.')]
  [PrimaryJoinColumn('ID')]
  TPartyGroup = class(TParty)
  private
    [Column('NAME', [], 75)]
    [Description('Name of the account.')]
    FName: Nullable<string>;
    
    [Column('ALIAS', [], 35)]
    [Description('Alias or alternative name for the account.')]
    FAlias: Nullable<string>;
    
    [Column('GROUP_TYPE', [], 50)]
    [Description('Account type, e.g. Organization, Family, Learning Center, Office, etc.')]
    FGroupType: Nullable<string>;
    
    [Column('IS_FOR_PROFIT', [], 1)]
    [Description('Boolean flag to indicate whether the organization is a for profit (true) or a not-for-profirt (false).')]
    FIsForProfit: Nullable<Boolean>;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes concerning the account.')]
    FNote: Nullable<string>;
  public
    property Name: Nullable<string> read FName write FName;
    property Alias: Nullable<string> read FAlias write FAlias;
    property GroupType: Nullable<string> read FGroupType write FGroupType;
    property IsForProfit: Nullable<Boolean> read FIsForProfit write FIsForProfit;
    property Note: Nullable<string> read FNote write FNote;
  end;
  
  [Entity]
  [Table('PARTY_PERSON')]
  [Description('This table simply stores the names of a person (first, last, middle, nickname, prefix, suffix), gender, birthday, and medical info.')]
  [PrimaryJoinColumn('ID')]
  TPartyPerson = class(TParty)
  private
    [Column('PREFIX', [], 10)]
    [Description('Prefix of the person, currently these are only Mr. and Ms., for which only Adults have values--however this should likely be changed to options like Dr. or Sir., since the values of Mr. and Ms. can be implied from the gender table.')]
    FPrefix: Nullable<string>;
    
    [Column('FIRST_NAME', [], 35)]
    [Description('First name of the person (Nicknames do not belong here, as there is a separate field for that).')]
    FFirstName: Nullable<string>;
    
    [Column('MIDDLE_NAMES', [], 35)]
    [Description('Middle names of the person, this field can represent a single middle initial, or simply all names between the first name and last name of the person, which, when combined, should result in the persons full name.')]
    FMiddleNames: Nullable<string>;
    
    [Column('LAST_NAME', [], 35)]
    [Description('Last name, surname, or family name of the person. Should not include any suffix like Jr. or Sr., as there is a suffix field for that.')]
    FLastName: Nullable<string>;
    
    [Column('SUFFIX', [], 10)]
    [Description('Suffix of the person, like Sr., Jr., III, etc. TO DO: Standardize these options, if a father is the Sr and the son is Jr, should this field say Jr an Sr, or should it say I and II? Or First and Second?')]
    FSuffix: Nullable<string>;
    
    [Column('NICK_NAME', [], 35)]
    [Description('Nick name of the person, should default to First Name but allow changes made to it. This should be the name that the person prefers to be called (e.g. Nina instead of Cristina, Apple instead of Appolonia, Billy instead of William, etc.)')]
    FNickName: Nullable<string>;
    
    [Column('MEDICAL_INFO', [], 255)]
    [Description('Any imporant medical information regarding the person, like severe allergies, heart conditions, anything requiring special care or medication, etc.')]
    FMedicalInfo: Nullable<string>;
    
    [Column('DO_NOT_CONTACT', [TColumnProp.Required], 1)]
    [Description('A boolean flag indicating that the person does not wish to be contacted. If a single entity in an account is marked as DO_NOT_CONTACT, then all members of the account should be considered DO_NOT_CONTACT.')]
    FDoNotContact: Boolean;
    
    [Column('BIRTH_DATE', [])]
    [Description('')]
    FBirthDate: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('GENDER_ID', [], 'ID')]
    [Description('')]
    FGender: TGender;
  public
    property Prefix: Nullable<string> read FPrefix write FPrefix;
    property FirstName: Nullable<string> read FFirstName write FFirstName;
    property MiddleNames: Nullable<string> read FMiddleNames write FMiddleNames;
    property LastName: Nullable<string> read FLastName write FLastName;
    property Suffix: Nullable<string> read FSuffix write FSuffix;
    property NickName: Nullable<string> read FNickName write FNickName;
    property MedicalInfo: Nullable<string> read FMedicalInfo write FMedicalInfo;
    property DoNotContact: Boolean read FDoNotContact write FDoNotContact;
    property BirthDate: Nullable<TDateTime> read FBirthDate write FBirthDate;
    property Gender: TGender read FGender write FGender;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_CUSTOMER')]
  [Description('The role of a person who is receiving a service. Customers make payments on services being received, but a person can still play the role of a customer even if they are not the bill payer.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleCustomer = class(TPartyRole)
  end;
  
  [Entity]
  [Table('PARTY_ROLE_GUARDIAN')]
  [Description('The the role of a person who is partially or fully responsible for another person. Parents, Legal Guardians, Family Members, Nannies, Emergency Contacts, and so on are all examples of guardians.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleGuardian = class(TPartyRole)
  private
    [Column('RELATIONSHIP', [TColumnProp.Required], 35)]
    [Description('How this Guardian is related to the Party who has them as their guardian. For example, if the referenced party is the granddaughter of this guardian, then this relationship field should read "Grandmother".')]
    FRelationship: string;
    
    [Column('IS_EMERGENCY_CONTACT', [TColumnProp.Required], 1)]
    [Description('')]
    FIsEmergencyContact: Boolean;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Relationship: string read FRelationship write FRelationship;
    property IsEmergencyContact: Boolean read FIsEmergencyContact write FIsEmergencyContact;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_LEAD')]
  [Description('A Lead is the role of a person who is a prospective customer. All members of a family (guardians & students) have the role of being a lead.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleLead = class(TPartyRole)
  private
    [Column('CONVERT_AT', [])]
    [Description('Datetime that the lead/prospective was converted')]
    FConvertAt: Nullable<TDateTime>;
    
    [Column('INITIAL_CONTACT_AT', [])]
    [Description('')]
    FInitialContactAt: Nullable<TDateTime>;
  public
    property ConvertAt: Nullable<TDateTime> read FConvertAt write FConvertAt;
    property InitialContactAt: Nullable<TDateTime> read FInitialContactAt write FInitialContactAt;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_LEARNING_CENTER')]
  [Description('A Learning Center is a role of a venue that acts as a space for instructors to use Mathnasium methods to instruct students.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleLearningCenter = class(TPartyRole)
  private
    [Column('STUDENT_NUMBER_PREFIX', [TColumnProp.Required], 1)]
    [Description('')]
    FStudentNumberPrefix: string;
    
    [Column('NEXT_STUDENT_NUMBER', [TColumnProp.Required])]
    [Description('')]
    FNextStudentNumber: Integer;
  public
    property StudentNumberPrefix: string read FStudentNumberPrefix write FStudentNumberPrefix;
    property NextStudentNumber: Integer read FNextStudentNumber write FNextStudentNumber;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_STAFF')]
  [Description('A staff member is the role of a person who receives wages/salary. For example, all instructors and administrators have the role of staff')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleStaff = class(TPartyRole)
  private
    [Column('HIRE_AT', [])]
    [Description('Datetime that the staff member was hired')]
    FHireAt: Nullable<TDateTime>;
  public
    property HireAt: Nullable<TDateTime> read FHireAt write FHireAt;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_STUDENT')]
  [Description('A Student is the role of a person who is receiving educational services, that is, one who "enrolls" in a "program offering".')]
  [UniqueKey('STUDENT_NUMBER')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleStudent = class(TPartyRole)
  private
    [Column('STUDENT_NUMBER', [], 12)]
    [Description('Legacy field representing the student number, which begins with a letter indicating the center location the student attends followed by a 5 digit number which increments as students enroll.')]
    FStudentNumber: Nullable<string>;
    
    [Column('LIVES_WITH', [], 35)]
    [Description('Indicates any notable living situations, like if the student lives with only one parent or switches between different parent homes.')]
    FLivesWith: Nullable<string>;
    
    [Column('CONSENT_CONTACT_SCHOOL', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to contact the students school or teacher.')]
    FConsentContactSchool: Nullable<Boolean>;
    
    [Column('CONSENT_LEAVE', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to allow the student to leave the center unattended.')]
    FConsentLeave: Nullable<Boolean>;
    
    [Column('CONSENT_PHOTOGRAPH', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to photograph or film the student while they are in the center.')]
    FConsentPhotograph: Nullable<Boolean>;
    
    [Column('SCHOOL_ENTRY_DATE', [])]
    [Description('')]
    FSchoolEntryDate: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('GRADE_ID', [], 'ID')]
    [Description('')]
    FGrade: TGrade;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_TEACHER_ID', [], 'ID')]
    [Description('')]
    FPartyRoleTeacher: TPartyRoleTeacher;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_SCHOOL_ID', [], 'ID')]
    [Description('')]
    FPartyRoleSchool: TPartyRoleSchool;
  public
    property StudentNumber: Nullable<string> read FStudentNumber write FStudentNumber;
    property LivesWith: Nullable<string> read FLivesWith write FLivesWith;
    property ConsentContactSchool: Nullable<Boolean> read FConsentContactSchool write FConsentContactSchool;
    property ConsentLeave: Nullable<Boolean> read FConsentLeave write FConsentLeave;
    property ConsentPhotograph: Nullable<Boolean> read FConsentPhotograph write FConsentPhotograph;
    property SchoolEntryDate: Nullable<TDateTime> read FSchoolEntryDate write FSchoolEntryDate;
    property Grade: TGrade read FGrade write FGrade;
    property PartyRoleTeacher: TPartyRoleTeacher read FPartyRoleTeacher write FPartyRoleTeacher;
    property PartyRoleSchool: TPartyRoleSchool read FPartyRoleSchool write FPartyRoleSchool;
  end;
  
  TDicDictionary = class
  private
    FActivity: TActivityTableDictionary;
    FActivityHierarchy: TActivityHierarchyTableDictionary;
    FActivityType: TActivityTypeTableDictionary;
    FAddress: TAddressTableDictionary;
    FAddressHierarchy: TAddressHierarchyTableDictionary;
    FAddressType: TAddressTypeTableDictionary;
    FEntityBase: TEntityBaseTableDictionary;
    FGender: TGenderTableDictionary;
    FGrade: TGradeTableDictionary;
    FLocator: TLocatorTableDictionary;
    FParticipant: TParticipantTableDictionary;
    FParticipantStatus: TParticipantStatusTableDictionary;
    FParty: TPartyTableDictionary;
    FPartyGroup: TPartyGroupTableDictionary;
    FPartyHierarchy: TPartyHierarchyTableDictionary;
    FPartyPerson: TPartyPersonTableDictionary;
    FPartyRole: TPartyRoleTableDictionary;
    FPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
    FPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
    FPartyRoleLead: TPartyRoleLeadTableDictionary;
    FPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
    FPartyRoleStaff: TPartyRoleStaffTableDictionary;
    FPartyRoleStudent: TPartyRoleStudentTableDictionary;
    FPartyRoleThing: TPartyRoleThingTableDictionary;
    FPartyRoleType: TPartyRoleTypeTableDictionary;
    FPartyType: TPartyTypeTableDictionary;
    FThing: TThingTableDictionary;
    FThingHierarchy: TThingHierarchyTableDictionary;
    FThingType: TThingTypeTableDictionary;
    function GetActivity: TActivityTableDictionary;
    function GetActivityHierarchy: TActivityHierarchyTableDictionary;
    function GetActivityType: TActivityTypeTableDictionary;
    function GetAddress: TAddressTableDictionary;
    function GetAddressHierarchy: TAddressHierarchyTableDictionary;
    function GetAddressType: TAddressTypeTableDictionary;
    function GetEntityBase: TEntityBaseTableDictionary;
    function GetGender: TGenderTableDictionary;
    function GetGrade: TGradeTableDictionary;
    function GetLocator: TLocatorTableDictionary;
    function GetParticipant: TParticipantTableDictionary;
    function GetParticipantStatus: TParticipantStatusTableDictionary;
    function GetParty: TPartyTableDictionary;
    function GetPartyGroup: TPartyGroupTableDictionary;
    function GetPartyHierarchy: TPartyHierarchyTableDictionary;
    function GetPartyPerson: TPartyPersonTableDictionary;
    function GetPartyRole: TPartyRoleTableDictionary;
    function GetPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
    function GetPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
    function GetPartyRoleLead: TPartyRoleLeadTableDictionary;
    function GetPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
    function GetPartyRoleStaff: TPartyRoleStaffTableDictionary;
    function GetPartyRoleStudent: TPartyRoleStudentTableDictionary;
    function GetPartyRoleThing: TPartyRoleThingTableDictionary;
    function GetPartyRoleType: TPartyRoleTypeTableDictionary;
    function GetPartyType: TPartyTypeTableDictionary;
    function GetThing: TThingTableDictionary;
    function GetThingHierarchy: TThingHierarchyTableDictionary;
    function GetThingType: TThingTypeTableDictionary;
  public
    destructor Destroy; override;
    property Activity: TActivityTableDictionary read GetActivity;
    property ActivityHierarchy: TActivityHierarchyTableDictionary read GetActivityHierarchy;
    property ActivityType: TActivityTypeTableDictionary read GetActivityType;
    property Address: TAddressTableDictionary read GetAddress;
    property AddressHierarchy: TAddressHierarchyTableDictionary read GetAddressHierarchy;
    property AddressType: TAddressTypeTableDictionary read GetAddressType;
    property EntityBase: TEntityBaseTableDictionary read GetEntityBase;
    property Gender: TGenderTableDictionary read GetGender;
    property Grade: TGradeTableDictionary read GetGrade;
    property Locator: TLocatorTableDictionary read GetLocator;
    property Participant: TParticipantTableDictionary read GetParticipant;
    property ParticipantStatus: TParticipantStatusTableDictionary read GetParticipantStatus;
    property Party: TPartyTableDictionary read GetParty;
    property PartyGroup: TPartyGroupTableDictionary read GetPartyGroup;
    property PartyHierarchy: TPartyHierarchyTableDictionary read GetPartyHierarchy;
    property PartyPerson: TPartyPersonTableDictionary read GetPartyPerson;
    property PartyRole: TPartyRoleTableDictionary read GetPartyRole;
    property PartyRoleCustomer: TPartyRoleCustomerTableDictionary read GetPartyRoleCustomer;
    property PartyRoleGuardian: TPartyRoleGuardianTableDictionary read GetPartyRoleGuardian;
    property PartyRoleLead: TPartyRoleLeadTableDictionary read GetPartyRoleLead;
    property PartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary read GetPartyRoleLearningCenter;
    property PartyRoleStaff: TPartyRoleStaffTableDictionary read GetPartyRoleStaff;
    property PartyRoleStudent: TPartyRoleStudentTableDictionary read GetPartyRoleStudent;
    property PartyRoleThing: TPartyRoleThingTableDictionary read GetPartyRoleThing;
    property PartyRoleType: TPartyRoleTypeTableDictionary read GetPartyRoleType;
    property PartyType: TPartyTypeTableDictionary read GetPartyType;
    property Thing: TThingTableDictionary read GetThing;
    property ThingHierarchy: TThingHierarchyTableDictionary read GetThingHierarchy;
    property ThingType: TThingTypeTableDictionary read GetThingType;
  end;
  
  TActivityTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
    FTitle: TDictionaryAttribute;
    FAlias: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FIsComplete: TDictionaryAttribute;
    FEntityBase: TDictionaryAssociation;
    FActivityType: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
    property Title: TDictionaryAttribute read FTitle;
    property Alias: TDictionaryAttribute read FAlias;
    property Note: TDictionaryAttribute read FNote;
    property IsComplete: TDictionaryAttribute read FIsComplete;
    property EntityBase: TDictionaryAssociation read FEntityBase;
    property ActivityType: TDictionaryAssociation read FActivityType;
  end;
  
  TActivityHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FChildActivity: TDictionaryAssociation;
    FParentActivity: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property ChildActivity: TDictionaryAssociation read FChildActivity;
    property ParentActivity: TDictionaryAssociation read FParentActivity;
  end;
  
  TActivityTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property Validation: TDictionaryAttribute read FValidation;
    property DataSize: TDictionaryAttribute read FDataSize;
    property DataType: TDictionaryAttribute read FDataType;
  end;
  
  TAddressTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FContent: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FEntityBase: TDictionaryAssociation;
    FAddressType: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property Content: TDictionaryAttribute read FContent;
    property Description: TDictionaryAttribute read FDescription;
    property EntityBase: TDictionaryAssociation read FEntityBase;
    property AddressType: TDictionaryAssociation read FAddressType;
  end;
  
  TAddressHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FRelationship: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FChildAddress: TDictionaryAssociation;
    FParentAddress: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property ChildAddress: TDictionaryAssociation read FChildAddress;
    property ParentAddress: TDictionaryAssociation read FParentAddress;
  end;
  
  TAddressTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FKind: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property Kind: TDictionaryAttribute read FKind;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property Validation: TDictionaryAttribute read FValidation;
    property DataType: TDictionaryAttribute read FDataType;
    property DataSize: TDictionaryAttribute read FDataSize;
  end;
  
  TEntityBaseTableDictionary = class
  private
    FId: TDictionaryAttribute;
    FIsActive: TDictionaryAttribute;
    FCreatedBy: TDictionaryAttribute;
    FCreatedAt: TDictionaryAttribute;
    FUpdatedBy: TDictionaryAttribute;
    FUpdatedAt: TDictionaryAttribute;
    FPos: TDictionaryAttribute;
    FExternalSource: TDictionaryAttribute;
    FExternalId: TDictionaryAttribute;
    FTableName: TDictionaryAttribute;
  public
    constructor Create;
    property Id: TDictionaryAttribute read FId;
    property IsActive: TDictionaryAttribute read FIsActive;
    property CreatedBy: TDictionaryAttribute read FCreatedBy;
    property CreatedAt: TDictionaryAttribute read FCreatedAt;
    property UpdatedBy: TDictionaryAttribute read FUpdatedBy;
    property UpdatedAt: TDictionaryAttribute read FUpdatedAt;
    property Pos: TDictionaryAttribute read FPos;
    property ExternalSource: TDictionaryAttribute read FExternalSource;
    property ExternalId: TDictionaryAttribute read FExternalId;
    property TableName: TDictionaryAttribute read FTableName;
  end;
  
  TGenderTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FSubjectPronoun: TDictionaryAttribute;
    FObjectPronoun: TDictionaryAttribute;
    FPossessiveDetPronoun: TDictionaryAttribute;
    FPossessivePronoun: TDictionaryAttribute;
    FReflexivePronoun: TDictionaryAttribute;
    FTitle: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property SubjectPronoun: TDictionaryAttribute read FSubjectPronoun;
    property ObjectPronoun: TDictionaryAttribute read FObjectPronoun;
    property PossessiveDetPronoun: TDictionaryAttribute read FPossessiveDetPronoun;
    property PossessivePronoun: TDictionaryAttribute read FPossessivePronoun;
    property ReflexivePronoun: TDictionaryAttribute read FReflexivePronoun;
    property Title: TDictionaryAttribute read FTitle;
  end;
  
  TGradeTableDictionary = class
  private
    FServiceLevelId: TDictionaryAttribute;
    FGrade: TDictionaryAttribute;
    FName: TDictionaryAttribute;
  public
    constructor Create;
    property ServiceLevelId: TDictionaryAttribute read FServiceLevelId;
    property Grade: TDictionaryAttribute read FGrade;
    property Name: TDictionaryAttribute read FName;
  end;
  
  TLocatorTableDictionary = class
  private
    FAddress: TDictionaryAssociation;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Address: TDictionaryAssociation read FAddress;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TParticipantTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FActivity: TDictionaryAssociation;
    FParticipantStatus: TDictionaryAssociation;
    FPartyRole: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property Activity: TDictionaryAssociation read FActivity;
    property ParticipantStatus: TDictionaryAssociation read FParticipantStatus;
    property PartyRole: TDictionaryAssociation read FPartyRole;
  end;
  
  TParticipantStatusTableDictionary = class
  private
    FStatus: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Status: TDictionaryAttribute read FStatus;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TPartyTableDictionary = class
  private
    FPartyType: TDictionaryAssociation;
  public
    constructor Create;
    property PartyType: TDictionaryAssociation read FPartyType;
  end;
  
  TPartyGroupTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FAlias: TDictionaryAttribute;
    FGroupType: TDictionaryAttribute;
    FIsForProfit: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Alias: TDictionaryAttribute read FAlias;
    property GroupType: TDictionaryAttribute read FGroupType;
    property IsForProfit: TDictionaryAttribute read FIsForProfit;
    property Note: TDictionaryAttribute read FNote;
  end;
  
  TPartyHierarchyTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FIsPrimary: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FChildParty: TDictionaryAssociation;
    FParentParty: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsPrimary: TDictionaryAttribute read FIsPrimary;
    property Note: TDictionaryAttribute read FNote;
    property ChildParty: TDictionaryAssociation read FChildParty;
    property ParentParty: TDictionaryAssociation read FParentParty;
  end;
  
  TPartyPersonTableDictionary = class
  private
    FPrefix: TDictionaryAttribute;
    FFirstName: TDictionaryAttribute;
    FMiddleNames: TDictionaryAttribute;
    FLastName: TDictionaryAttribute;
    FSuffix: TDictionaryAttribute;
    FNickName: TDictionaryAttribute;
    FMedicalInfo: TDictionaryAttribute;
    FDoNotContact: TDictionaryAttribute;
    FBirthDate: TDictionaryAttribute;
    FGender: TDictionaryAssociation;
  public
    constructor Create;
    property Prefix: TDictionaryAttribute read FPrefix;
    property FirstName: TDictionaryAttribute read FFirstName;
    property MiddleNames: TDictionaryAttribute read FMiddleNames;
    property LastName: TDictionaryAttribute read FLastName;
    property Suffix: TDictionaryAttribute read FSuffix;
    property NickName: TDictionaryAttribute read FNickName;
    property MedicalInfo: TDictionaryAttribute read FMedicalInfo;
    property DoNotContact: TDictionaryAttribute read FDoNotContact;
    property BirthDate: TDictionaryAttribute read FBirthDate;
    property Gender: TDictionaryAssociation read FGender;
  end;
  
  TPartyRoleTableDictionary = class
  private
    FNote: TDictionaryAttribute;
    FParty: TDictionaryAssociation;
    FPartyRoleType: TDictionaryAssociation;
  public
    constructor Create;
    property Note: TDictionaryAttribute read FNote;
    property Party: TDictionaryAssociation read FParty;
    property PartyRoleType: TDictionaryAssociation read FPartyRoleType;
  end;
  
  TPartyRoleCustomerTableDictionary = class
  end;
  
  TPartyRoleGuardianTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FIsEmergencyContact: TDictionaryAttribute;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsEmergencyContact: TDictionaryAttribute read FIsEmergencyContact;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TPartyRoleLeadTableDictionary = class
  private
    FConvertAt: TDictionaryAttribute;
    FInitialContactAt: TDictionaryAttribute;
  public
    constructor Create;
    property ConvertAt: TDictionaryAttribute read FConvertAt;
    property InitialContactAt: TDictionaryAttribute read FInitialContactAt;
  end;
  
  TPartyRoleLearningCenterTableDictionary = class
  private
    FStudentNumberPrefix: TDictionaryAttribute;
    FNextStudentNumber: TDictionaryAttribute;
  public
    constructor Create;
    property StudentNumberPrefix: TDictionaryAttribute read FStudentNumberPrefix;
    property NextStudentNumber: TDictionaryAttribute read FNextStudentNumber;
  end;
  
  TPartyRoleStaffTableDictionary = class
  private
    FHireAt: TDictionaryAttribute;
  public
    constructor Create;
    property HireAt: TDictionaryAttribute read FHireAt;
  end;
  
  TPartyRoleStudentTableDictionary = class
  private
    FStudentNumber: TDictionaryAttribute;
    FLivesWith: TDictionaryAttribute;
    FConsentContactSchool: TDictionaryAttribute;
    FConsentLeave: TDictionaryAttribute;
    FConsentPhotograph: TDictionaryAttribute;
    FSchoolEntryDate: TDictionaryAttribute;
    FGrade: TDictionaryAssociation;
    FPartyRoleTeacher: TDictionaryAssociation;
    FPartyRoleSchool: TDictionaryAssociation;
  public
    constructor Create;
    property StudentNumber: TDictionaryAttribute read FStudentNumber;
    property LivesWith: TDictionaryAttribute read FLivesWith;
    property ConsentContactSchool: TDictionaryAttribute read FConsentContactSchool;
    property ConsentLeave: TDictionaryAttribute read FConsentLeave;
    property ConsentPhotograph: TDictionaryAttribute read FConsentPhotograph;
    property SchoolEntryDate: TDictionaryAttribute read FSchoolEntryDate;
    property Grade: TDictionaryAssociation read FGrade;
    property PartyRoleTeacher: TDictionaryAssociation read FPartyRoleTeacher;
    property PartyRoleSchool: TDictionaryAssociation read FPartyRoleSchool;
  end;
  
  TPartyRoleThingTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FPartyRole: TDictionaryAssociation;
    FThing: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property Note: TDictionaryAttribute read FNote;
    property PartyRole: TDictionaryAssociation read FPartyRole;
    property Thing: TDictionaryAssociation read FThing;
  end;
  
  TPartyRoleTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FIsPersonRole: TDictionaryAttribute;
    FIsGroupRole: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property IsPersonRole: TDictionaryAttribute read FIsPersonRole;
    property IsGroupRole: TDictionaryAttribute read FIsGroupRole;
  end;
  
  TPartyTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TThingTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FTitle: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FEntityBase: TDictionaryAssociation;
    FThingType: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property Title: TDictionaryAttribute read FTitle;
    property Note: TDictionaryAttribute read FNote;
    property EntityBase: TDictionaryAssociation read FEntityBase;
    property ThingType: TDictionaryAssociation read FThingType;
  end;
  
  TThingHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FChildThing: TDictionaryAssociation;
    FParentThing: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property ChildThing: TDictionaryAssociation read FChildThing;
    property ParentThing: TDictionaryAssociation read FParentThing;
  end;
  
  TThingTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property Validation: TDictionaryAttribute read FValidation;
    property DataSize: TDictionaryAttribute read FDataSize;
    property DataType: TDictionaryAttribute read FDataType;
  end;
  
function Dic: TDicDictionary;

implementation

var
  __Dic: TDicDictionary;

function Dic: TDicDictionary;
begin
  if __Dic = nil then __Dic := TDicDictionary.Create;
  result := __Dic
end;

{ TDicDictionary }

destructor TDicDictionary.Destroy;
begin
  if FThingType <> nil then FThingType.Free;
  if FThingHierarchy <> nil then FThingHierarchy.Free;
  if FThing <> nil then FThing.Free;
  if FPartyType <> nil then FPartyType.Free;
  if FPartyRoleType <> nil then FPartyRoleType.Free;
  if FPartyRoleThing <> nil then FPartyRoleThing.Free;
  if FPartyRoleStudent <> nil then FPartyRoleStudent.Free;
  if FPartyRoleStaff <> nil then FPartyRoleStaff.Free;
  if FPartyRoleLearningCenter <> nil then FPartyRoleLearningCenter.Free;
  if FPartyRoleLead <> nil then FPartyRoleLead.Free;
  if FPartyRoleGuardian <> nil then FPartyRoleGuardian.Free;
  if FPartyRoleCustomer <> nil then FPartyRoleCustomer.Free;
  if FPartyRole <> nil then FPartyRole.Free;
  if FPartyPerson <> nil then FPartyPerson.Free;
  if FPartyHierarchy <> nil then FPartyHierarchy.Free;
  if FPartyGroup <> nil then FPartyGroup.Free;
  if FParty <> nil then FParty.Free;
  if FParticipantStatus <> nil then FParticipantStatus.Free;
  if FParticipant <> nil then FParticipant.Free;
  if FLocator <> nil then FLocator.Free;
  if FGrade <> nil then FGrade.Free;
  if FGender <> nil then FGender.Free;
  if FEntityBase <> nil then FEntityBase.Free;
  if FAddressType <> nil then FAddressType.Free;
  if FAddressHierarchy <> nil then FAddressHierarchy.Free;
  if FAddress <> nil then FAddress.Free;
  if FActivityType <> nil then FActivityType.Free;
  if FActivityHierarchy <> nil then FActivityHierarchy.Free;
  if FActivity <> nil then FActivity.Free;
  inherited;
end;

function TDicDictionary.GetActivity: TActivityTableDictionary;
begin
  if FActivity = nil then FActivity := TActivityTableDictionary.Create;
  result := FActivity;
end;

function TDicDictionary.GetActivityHierarchy: TActivityHierarchyTableDictionary;
begin
  if FActivityHierarchy = nil then FActivityHierarchy := TActivityHierarchyTableDictionary.Create;
  result := FActivityHierarchy;
end;

function TDicDictionary.GetActivityType: TActivityTypeTableDictionary;
begin
  if FActivityType = nil then FActivityType := TActivityTypeTableDictionary.Create;
  result := FActivityType;
end;

function TDicDictionary.GetAddress: TAddressTableDictionary;
begin
  if FAddress = nil then FAddress := TAddressTableDictionary.Create;
  result := FAddress;
end;

function TDicDictionary.GetAddressHierarchy: TAddressHierarchyTableDictionary;
begin
  if FAddressHierarchy = nil then FAddressHierarchy := TAddressHierarchyTableDictionary.Create;
  result := FAddressHierarchy;
end;

function TDicDictionary.GetAddressType: TAddressTypeTableDictionary;
begin
  if FAddressType = nil then FAddressType := TAddressTypeTableDictionary.Create;
  result := FAddressType;
end;

function TDicDictionary.GetEntityBase: TEntityBaseTableDictionary;
begin
  if FEntityBase = nil then FEntityBase := TEntityBaseTableDictionary.Create;
  result := FEntityBase;
end;

function TDicDictionary.GetGender: TGenderTableDictionary;
begin
  if FGender = nil then FGender := TGenderTableDictionary.Create;
  result := FGender;
end;

function TDicDictionary.GetGrade: TGradeTableDictionary;
begin
  if FGrade = nil then FGrade := TGradeTableDictionary.Create;
  result := FGrade;
end;

function TDicDictionary.GetLocator: TLocatorTableDictionary;
begin
  if FLocator = nil then FLocator := TLocatorTableDictionary.Create;
  result := FLocator;
end;

function TDicDictionary.GetParticipant: TParticipantTableDictionary;
begin
  if FParticipant = nil then FParticipant := TParticipantTableDictionary.Create;
  result := FParticipant;
end;

function TDicDictionary.GetParticipantStatus: TParticipantStatusTableDictionary;
begin
  if FParticipantStatus = nil then FParticipantStatus := TParticipantStatusTableDictionary.Create;
  result := FParticipantStatus;
end;

function TDicDictionary.GetParty: TPartyTableDictionary;
begin
  if FParty = nil then FParty := TPartyTableDictionary.Create;
  result := FParty;
end;

function TDicDictionary.GetPartyGroup: TPartyGroupTableDictionary;
begin
  if FPartyGroup = nil then FPartyGroup := TPartyGroupTableDictionary.Create;
  result := FPartyGroup;
end;

function TDicDictionary.GetPartyHierarchy: TPartyHierarchyTableDictionary;
begin
  if FPartyHierarchy = nil then FPartyHierarchy := TPartyHierarchyTableDictionary.Create;
  result := FPartyHierarchy;
end;

function TDicDictionary.GetPartyPerson: TPartyPersonTableDictionary;
begin
  if FPartyPerson = nil then FPartyPerson := TPartyPersonTableDictionary.Create;
  result := FPartyPerson;
end;

function TDicDictionary.GetPartyRole: TPartyRoleTableDictionary;
begin
  if FPartyRole = nil then FPartyRole := TPartyRoleTableDictionary.Create;
  result := FPartyRole;
end;

function TDicDictionary.GetPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
begin
  if FPartyRoleCustomer = nil then FPartyRoleCustomer := TPartyRoleCustomerTableDictionary.Create;
  result := FPartyRoleCustomer;
end;

function TDicDictionary.GetPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
begin
  if FPartyRoleGuardian = nil then FPartyRoleGuardian := TPartyRoleGuardianTableDictionary.Create;
  result := FPartyRoleGuardian;
end;

function TDicDictionary.GetPartyRoleLead: TPartyRoleLeadTableDictionary;
begin
  if FPartyRoleLead = nil then FPartyRoleLead := TPartyRoleLeadTableDictionary.Create;
  result := FPartyRoleLead;
end;

function TDicDictionary.GetPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
begin
  if FPartyRoleLearningCenter = nil then FPartyRoleLearningCenter := TPartyRoleLearningCenterTableDictionary.Create;
  result := FPartyRoleLearningCenter;
end;

function TDicDictionary.GetPartyRoleStaff: TPartyRoleStaffTableDictionary;
begin
  if FPartyRoleStaff = nil then FPartyRoleStaff := TPartyRoleStaffTableDictionary.Create;
  result := FPartyRoleStaff;
end;

function TDicDictionary.GetPartyRoleStudent: TPartyRoleStudentTableDictionary;
begin
  if FPartyRoleStudent = nil then FPartyRoleStudent := TPartyRoleStudentTableDictionary.Create;
  result := FPartyRoleStudent;
end;

function TDicDictionary.GetPartyRoleThing: TPartyRoleThingTableDictionary;
begin
  if FPartyRoleThing = nil then FPartyRoleThing := TPartyRoleThingTableDictionary.Create;
  result := FPartyRoleThing;
end;

function TDicDictionary.GetPartyRoleType: TPartyRoleTypeTableDictionary;
begin
  if FPartyRoleType = nil then FPartyRoleType := TPartyRoleTypeTableDictionary.Create;
  result := FPartyRoleType;
end;

function TDicDictionary.GetPartyType: TPartyTypeTableDictionary;
begin
  if FPartyType = nil then FPartyType := TPartyTypeTableDictionary.Create;
  result := FPartyType;
end;

function TDicDictionary.GetThing: TThingTableDictionary;
begin
  if FThing = nil then FThing := TThingTableDictionary.Create;
  result := FThing;
end;

function TDicDictionary.GetThingHierarchy: TThingHierarchyTableDictionary;
begin
  if FThingHierarchy = nil then FThingHierarchy := TThingHierarchyTableDictionary.Create;
  result := FThingHierarchy;
end;

function TDicDictionary.GetThingType: TThingTypeTableDictionary;
begin
  if FThingType = nil then FThingType := TThingTypeTableDictionary.Create;
  result := FThingType;
end;

{ TActivityTableDictionary }

constructor TActivityTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
  FTitle := TDictionaryAttribute.Create('Title');
  FAlias := TDictionaryAttribute.Create('Alias');
  FNote := TDictionaryAttribute.Create('Note');
  FIsComplete := TDictionaryAttribute.Create('IsComplete');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
  FActivityType := TDictionaryAssociation.Create('ActivityType');
end;

{ TActivityHierarchyTableDictionary }

constructor TActivityHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FChildActivity := TDictionaryAssociation.Create('ChildActivity');
  FParentActivity := TDictionaryAssociation.Create('ParentActivity');
end;

{ TActivityTypeTableDictionary }

constructor TActivityTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FValidation := TDictionaryAttribute.Create('Validation');
  FDataSize := TDictionaryAttribute.Create('DataSize');
  FDataType := TDictionaryAttribute.Create('DataType');
end;

{ TAddressTableDictionary }

constructor TAddressTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FContent := TDictionaryAttribute.Create('Content');
  FDescription := TDictionaryAttribute.Create('Description');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
  FAddressType := TDictionaryAssociation.Create('AddressType');
end;

{ TAddressHierarchyTableDictionary }

constructor TAddressHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FChildAddress := TDictionaryAssociation.Create('ChildAddress');
  FParentAddress := TDictionaryAssociation.Create('ParentAddress');
end;

{ TAddressTypeTableDictionary }

constructor TAddressTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FKind := TDictionaryAttribute.Create('Kind');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FValidation := TDictionaryAttribute.Create('Validation');
  FDataType := TDictionaryAttribute.Create('DataType');
  FDataSize := TDictionaryAttribute.Create('DataSize');
end;

{ TEntityBaseTableDictionary }

constructor TEntityBaseTableDictionary.Create;
begin
  inherited;
  FId := TDictionaryAttribute.Create('Id');
  FIsActive := TDictionaryAttribute.Create('IsActive');
  FCreatedBy := TDictionaryAttribute.Create('CreatedBy');
  FCreatedAt := TDictionaryAttribute.Create('CreatedAt');
  FUpdatedBy := TDictionaryAttribute.Create('UpdatedBy');
  FUpdatedAt := TDictionaryAttribute.Create('UpdatedAt');
  FPos := TDictionaryAttribute.Create('Pos');
  FExternalSource := TDictionaryAttribute.Create('ExternalSource');
  FExternalId := TDictionaryAttribute.Create('ExternalId');
  FTableName := TDictionaryAttribute.Create('TableName');
end;

{ TGenderTableDictionary }

constructor TGenderTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FSubjectPronoun := TDictionaryAttribute.Create('SubjectPronoun');
  FObjectPronoun := TDictionaryAttribute.Create('ObjectPronoun');
  FPossessiveDetPronoun := TDictionaryAttribute.Create('PossessiveDetPronoun');
  FPossessivePronoun := TDictionaryAttribute.Create('PossessivePronoun');
  FReflexivePronoun := TDictionaryAttribute.Create('ReflexivePronoun');
  FTitle := TDictionaryAttribute.Create('Title');
end;

{ TGradeTableDictionary }

constructor TGradeTableDictionary.Create;
begin
  inherited;
  FServiceLevelId := TDictionaryAttribute.Create('ServiceLevelId');
  FGrade := TDictionaryAttribute.Create('Grade');
  FName := TDictionaryAttribute.Create('Name');
end;

{ TLocatorTableDictionary }

constructor TLocatorTableDictionary.Create;
begin
  inherited;
  FAddress := TDictionaryAssociation.Create('Address');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TParticipantTableDictionary }

constructor TParticipantTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FActivity := TDictionaryAssociation.Create('Activity');
  FParticipantStatus := TDictionaryAssociation.Create('ParticipantStatus');
  FPartyRole := TDictionaryAssociation.Create('PartyRole');
end;

{ TParticipantStatusTableDictionary }

constructor TParticipantStatusTableDictionary.Create;
begin
  inherited;
  FStatus := TDictionaryAttribute.Create('Status');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TPartyTableDictionary }

constructor TPartyTableDictionary.Create;
begin
  inherited;
  FPartyType := TDictionaryAssociation.Create('PartyType');
end;

{ TPartyGroupTableDictionary }

constructor TPartyGroupTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FAlias := TDictionaryAttribute.Create('Alias');
  FGroupType := TDictionaryAttribute.Create('GroupType');
  FIsForProfit := TDictionaryAttribute.Create('IsForProfit');
  FNote := TDictionaryAttribute.Create('Note');
end;

{ TPartyHierarchyTableDictionary }

constructor TPartyHierarchyTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsPrimary := TDictionaryAttribute.Create('IsPrimary');
  FNote := TDictionaryAttribute.Create('Note');
  FChildParty := TDictionaryAssociation.Create('ChildParty');
  FParentParty := TDictionaryAssociation.Create('ParentParty');
end;

{ TPartyPersonTableDictionary }

constructor TPartyPersonTableDictionary.Create;
begin
  inherited;
  FPrefix := TDictionaryAttribute.Create('Prefix');
  FFirstName := TDictionaryAttribute.Create('FirstName');
  FMiddleNames := TDictionaryAttribute.Create('MiddleNames');
  FLastName := TDictionaryAttribute.Create('LastName');
  FSuffix := TDictionaryAttribute.Create('Suffix');
  FNickName := TDictionaryAttribute.Create('NickName');
  FMedicalInfo := TDictionaryAttribute.Create('MedicalInfo');
  FDoNotContact := TDictionaryAttribute.Create('DoNotContact');
  FBirthDate := TDictionaryAttribute.Create('BirthDate');
  FGender := TDictionaryAssociation.Create('Gender');
end;

{ TPartyRoleTableDictionary }

constructor TPartyRoleTableDictionary.Create;
begin
  inherited;
  FNote := TDictionaryAttribute.Create('Note');
  FParty := TDictionaryAssociation.Create('Party');
  FPartyRoleType := TDictionaryAssociation.Create('PartyRoleType');
end;

{ TPartyRoleGuardianTableDictionary }

constructor TPartyRoleGuardianTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsEmergencyContact := TDictionaryAttribute.Create('IsEmergencyContact');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TPartyRoleLeadTableDictionary }

constructor TPartyRoleLeadTableDictionary.Create;
begin
  inherited;
  FConvertAt := TDictionaryAttribute.Create('ConvertAt');
  FInitialContactAt := TDictionaryAttribute.Create('InitialContactAt');
end;

{ TPartyRoleLearningCenterTableDictionary }

constructor TPartyRoleLearningCenterTableDictionary.Create;
begin
  inherited;
  FStudentNumberPrefix := TDictionaryAttribute.Create('StudentNumberPrefix');
  FNextStudentNumber := TDictionaryAttribute.Create('NextStudentNumber');
end;

{ TPartyRoleStaffTableDictionary }

constructor TPartyRoleStaffTableDictionary.Create;
begin
  inherited;
  FHireAt := TDictionaryAttribute.Create('HireAt');
end;

{ TPartyRoleStudentTableDictionary }

constructor TPartyRoleStudentTableDictionary.Create;
begin
  inherited;
  FStudentNumber := TDictionaryAttribute.Create('StudentNumber');
  FLivesWith := TDictionaryAttribute.Create('LivesWith');
  FConsentContactSchool := TDictionaryAttribute.Create('ConsentContactSchool');
  FConsentLeave := TDictionaryAttribute.Create('ConsentLeave');
  FConsentPhotograph := TDictionaryAttribute.Create('ConsentPhotograph');
  FSchoolEntryDate := TDictionaryAttribute.Create('SchoolEntryDate');
  FGrade := TDictionaryAssociation.Create('Grade');
  FPartyRoleTeacher := TDictionaryAssociation.Create('PartyRoleTeacher');
  FPartyRoleSchool := TDictionaryAssociation.Create('PartyRoleSchool');
end;

{ TPartyRoleThingTableDictionary }

constructor TPartyRoleThingTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FNote := TDictionaryAttribute.Create('Note');
  FPartyRole := TDictionaryAssociation.Create('PartyRole');
  FThing := TDictionaryAssociation.Create('Thing');
end;

{ TPartyRoleTypeTableDictionary }

constructor TPartyRoleTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FIsPersonRole := TDictionaryAttribute.Create('IsPersonRole');
  FIsGroupRole := TDictionaryAttribute.Create('IsGroupRole');
end;

{ TPartyTypeTableDictionary }

constructor TPartyTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TThingTableDictionary }

constructor TThingTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FTitle := TDictionaryAttribute.Create('Title');
  FNote := TDictionaryAttribute.Create('Note');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
  FThingType := TDictionaryAssociation.Create('ThingType');
end;

{ TThingHierarchyTableDictionary }

constructor TThingHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FChildThing := TDictionaryAssociation.Create('ChildThing');
  FParentThing := TDictionaryAssociation.Create('ParentThing');
end;

{ TThingTypeTableDictionary }

constructor TThingTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FValidation := TDictionaryAttribute.Create('Validation');
  FDataSize := TDictionaryAttribute.Create('DataSize');
  FDataType := TDictionaryAttribute.Create('DataType');
end;

initialization
  RegisterEntity(TActivity);
  RegisterEntity(TActivityHierarchy);
  RegisterEntity(TActivityType);
  RegisterEntity(TAddress);
  RegisterEntity(TAddressHierarchy);
  RegisterEntity(TAddressType);
  RegisterEntity(TEntityBase);
  RegisterEntity(TGender);
  RegisterEntity(TGrade);
  RegisterEntity(TLocator);
  RegisterEntity(TParticipant);
  RegisterEntity(TParticipantStatus);
  RegisterEntity(TParty);
  RegisterEntity(TPartyGroup);
  RegisterEntity(TPartyHierarchy);
  RegisterEntity(TPartyPerson);
  RegisterEntity(TPartyRole);
  RegisterEntity(TPartyRoleCustomer);
  RegisterEntity(TPartyRoleGuardian);
  RegisterEntity(TPartyRoleLead);
  RegisterEntity(TPartyRoleLearningCenter);
  RegisterEntity(TPartyRoleStaff);
  RegisterEntity(TPartyRoleStudent);
  RegisterEntity(TPartyRoleThing);
  RegisterEntity(TPartyRoleType);
  RegisterEntity(TPartyType);
  RegisterEntity(TThing);
  RegisterEntity(TThingHierarchy);
  RegisterEntity(TThingType);

finalization
  if __Dic <> nil then __Dic.Free

end.
