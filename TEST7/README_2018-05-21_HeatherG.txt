TEST7 Leads Database

------------------------------------------------------------

2018-05-24 Update: In case you aren't getting my text messages, here is what I have texted you:

2018-05-22
    I'm sorry, I got caught up with some bookkeeping related tasks and documentation, so I didn't get to do much with the lead data yet.

    I did, however, take a brief five minutes to go over a couple getting started points with Stephanie for LO development

    Hopefully tomorrow I will be able to just focus on lead data.

2018-05-23
    I went through the items that David found on his last day that we video recorded, and that he wrote up at the bottom of his file, and fixed the issues that had to do with incorrect grades/genders and other basic issues.
    
    Next I need to sort through the records where the database has more lead records than the M2 and determine where they came from (TriMathlong?), and perhaps a way to sort them out of the comparison so that they stop showing up as false negatives
    
    On other news, Stephanie just created her first LO extension! I will be back tomorrow to continue vetting data. Text me if you have any specific DB questions.
    
    Also, I think I can make a query comparision of the number of activities per lead pre and post ETL, as well as a date comparison to see that all the activities were imported

2018-05-24
    So the remaining leads that David found with issues on importing lead students were either TriMathlon leads or duplicates in the M2 but not the database: as in the M2_LEADS flat table has multiple records that correlate to only one PARTY_PERSON record.

    This turned out to be 6 cases, and to fix them I manually modified the "Student" field for the 12 total records so that one was null, and one had the necessary information to create all the students with no duplicates.

    Also, the check of comparing activities by date was already done, it was in the guantlet of tests that David had done already.

    So apart from me running KAMI Tools related tasks against the new schema/data update (specifically importing monthly attendance) to make sure it works, the TEST7 database has all leads, lead students, and lead activities imported from the M2.

    Finally, I am still working on importing the private tutoring sessions, they have all been mapped to an enrollment, and now they need to be imported and mapped to their separate sales order details.

2018-05-28
    Friday and today I needed to spend some time on bookkeeping needs, hopefully tomorrow I can return to importing/mapping private tutoring sessions (at this point in the ETL, I am farther than we had gotten with David, the SQL to import and map the TH sessions was incomplete in the work we did in December)

------------------------------------------------------------


You should find the following files pushed to the TMS repo folder TEST7:
- TMS Data Modeler File TEST7.dgp
	- I haven't finished re-building the diagrams yet, but hopefully will have time for more tomorrow
- TMS Aurelius Exports:
	- TEST7.BusinessObject.Entity.pas - regular TMS Aurelius export with "Association Fetch Mode" set to "Eager" 
	- TEST7.BusinessObject.Entity.Lazy.pas - regular TMS Aurelius export with "Association Fetch Mode" set to "Lazy" 
- Enumerations: I'm not sure if these enumerations are what you need, but I just copied them from our previous work with hopes that it is? It's been a long time since I've looked at pascal files and they are still a bit opaque since I don't usually work with it.
	- TEST7.BusinessObject.Kind.pas
	- TEST7.BusinessObject.Kind.Sets.pas
- CRUD methods: Similar to above, I'm not sure if these files are what you need, but I just copied them from our previous work.
	- TEST7.Party.DataAccess.Connection.dfm
	- TEST7.Party.DataAccess.Connection.pas
- Other queries:
	- ViewAllLeads.sql
	- ViewLeadFamilyMembers.sql
	- ViewAllActivitiesByLead.sql


For me: As I'm sure the CRUD and Enumerations aren't exactly what you need, please let me know what about them works and what doesn't e.g. what do I need to do to get you want you need.


Also, here is how you can move a copy of the database from the WINSERVER2012 to be hosted on another machine:
- Open FlameRobin
- Register TEST7 database
- Create a backup file of TEST7 database
- Setup your new server in FlameRobin (if just using your local computer, use "localhost")
	- Right click on your new server and select "Manage Users" and create the following users: 
		- MN_KennedyR
		- MNTraining
		- If you decide to create additional users that did not previously exist, you will need to assign them roles. See the code after these instructions for how to do so.
- Right click on your new server and select "Restore backup into new database"
	- First indicate your new database parameters. NOTE: YOU MUST initially save your username and password in this first window so that the server can create the new database. After the file has been created, you can change your FlameRobin settings to not save the password. Don't forget to set Charset to UTF8.
	- In the next window, indicate where the backup file is. NOTE: careful of where the backup was saved to, it's likely on a network drive and may need to be copied locally before you can restore from it. In the restore window, be sure to check the box "Replace existing database file".
- Once the database backup has been restored into the new database file, it should be good to go



IF YOU CREATED A NEW USER AND THEY NEED TO BE ASSIGNED ROLES:
- Create user MN_JohnD
- run the following code to grant John Doe different user roles:
	GRANT MNR_ADMIN TO MN_JohnD;
	GRANT MNR_EXEC TO MN_JohnD;
	GRANT MNR_USER TO MN_JohnD;
	GRANT MNR_READONLY TO MN_JohnD;
- Be sure to minimally grant MNR_READONLY, or MN_JohnD will be a fairly useless account.