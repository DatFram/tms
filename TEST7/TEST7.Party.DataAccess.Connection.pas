// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. For proper use of the key component TFDUpdateSQL (used in tandem with
//     FDQuery for updated JOINED tables, respecting their data hierarchy, see:
//     - http://docwiki.embarcadero.com/CodeExamples/Tokyo/en/FireDAC.TFDQuery.OnUpdateRecord_Sample
//
//  2. For the challennges of not understanding #1, see:
//     - http://stackoverflow.com/questions/28175328/tfdupdatesql-not-posting-updates-on-datasnap-server
//     As its unanswered, you've got StackOverflow points waiting.
//
//  3. For ...
//     - http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Executing_Commands_(FireDAC)
//
// -----------------------------------------------------------------------------
unit TEST6.Party.DataAccess.Connection;

interface

uses
  Aurelius.Drivers.Interfaces,
  Aurelius.SQL.Firebird,
  Aurelius.Schema.Firebird,
  Aurelius.Drivers.FireDac
//
, System.SysUtils
, System.Classes
//
, Data.DB
//
, FireDAC.Stan.Intf
, FireDAC.Stan.Option
, FireDAC.Stan.Error
, FireDAC.UI.Intf
, FireDAC.Phys.Intf
, FireDAC.Stan.Def
, FireDAC.Stan.Pool
, FireDAC.Stan.Async
, FireDAC.Phys
, FireDAC.Phys.FB
, FireDAC.Phys.FBDef
, FireDAC.VCLUI.Wait
, FireDAC.Comp.Client
, FireDAC.Stan.Param
, FireDAC.DatS
, FireDAC.DApt.Intf
, FireDAC.DApt
, FireDAC.Comp.DataSet
, FireDAC.Moni.Base
, FireDAC.Moni.RemoteClient
;

type

  TTEST6_Party_DataAccess_Connection = class(TDataModule)
    Connection: TFDConnection;
    FDQuery: TFDQuery;
    FDUpdateSQL_Person: TFDUpdateSQL;
    FDMoniRemoteClientLink1: TFDMoniRemoteClientLink;
    procedure ConnectionAfterCommit(Sender: TObject);
    procedure ConnectionBeforeCommit(Sender: TObject);
    procedure FDQueryAfterPost(DataSet: TDataSet);
    procedure FDQueryBeforeEdit(DataSet: TDataSet);
    procedure FDQueryUpdateRecord(ASender: TDataSet; ARequest: TFDUpdateRequest;
        var AAction: TFDErrorAction; AOptions: TFDUpdateRowOptions);
  private
  public
    class function CreateConnection: IDBConnection;
    class function CreateFactory: IDBConnectionFactory;

  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
//
  Aurelius.Drivers.Base
//
, TEST6.Party.View.Lister.SQL
//
, CodeSiteLogging
;

{$R *.dfm}

{ TTEST6_Party_DataAccess_Connection }

procedure TTEST6_Party_DataAccess_Connection.ConnectionBeforeCommit(Sender: TObject);
var
  lIndex: Integer;
//  lCommand: String;
begin
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'BEFORE COMMIT CALLED' );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
//  lCommand := Connection.Command;
//  lIndex := TTEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'BEFORE COMMIT CALLED' );
//  TTEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.ConnectionAfterCommit(Sender: TObject);
var
  lIndex: Integer;
begin
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'AFTER COMMIT CALLED' );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

class function TTEST6_Party_DataAccess_Connection.CreateConnection: IDBConnection;
var
  DataModule: TTEST6_Party_DataAccess_Connection;
begin
  DataModule := TTEST6_Party_DataAccess_Connection.Create( nil );
  Result := TFireDacConnectionAdapter.Create(DataModule.Connection, 'Firebird', DataModule);
end;

class function TTEST6_Party_DataAccess_Connection.CreateFactory: IDBConnectionFactory;
begin
  Result := TDBConnectionFactory.Create(
    function: IDBConnection
    begin
      Result := CreateConnection;
    end
  );
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryBeforeEdit( DataSet: TDataSet );
var
  lIndex: Integer;
  lSQL: String;
  lValue: String;
begin
  lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lSQL );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
  lValue := DataSet.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryAfterPost( DataSet: TDataSet );
var
  lIndex: Integer;
  lSQL: String;
  lValue: String;
begin
  lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lSQL );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
  lValue := DataSet.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryUpdateRecord( aSender: TDataSet;
  aRequest: TFDUpdateRequest; var aAction: TFDErrorAction; aOptions:
    TFDUpdateRowOptions );
var
  lIndex: Integer;
  lRequest: String;
  lValue: String;
  lGUIDPerson: TGUID;
  lGUIDGroup: TGUID;
  lGUIDHierarchy: TGUID;
  lGUIDHierarchy2: TGUID;
begin
  if aRequest = TFDActionRequest.arNone then
    lRequest := 'Request: arNone'
  else
  if aRequest = TFDActionRequest.arFromRow then
      lRequest := 'Request: arFromRow'
  else
  if aRequest = TFDActionRequest.arSelect then
      lRequest := 'Request: arSelect'
  else
  if aRequest = TFDActionRequest.arInsert then
      lRequest := 'Request: arInsert (aka, Create)'
  else
  if aRequest = TFDActionRequest.arUpdate then
      lRequest := 'Request: arUpdate'
  else
  if aRequest = TFDActionRequest.arDelete then
      lRequest := 'Request: arDelete'
  else
  if aRequest = TFDActionRequest.arLock then
      lRequest := 'Request: arLock'
  else
  if aRequest = TFDActionRequest.arUnlock then
      lRequest := 'Request: arUnlock'
  else
  if aRequest = TFDActionRequest.arFetchRow then
      lRequest := 'Request: arFetchRow (aka, Read)'
  else
  if aRequest = TFDActionRequest.arUpdateHBlobs then
      lRequest := 'Request: arUpdateHBlobs'
  else
  if aRequest = TFDActionRequest.arDeleteAll then
      lRequest := 'Request: arDeleteAll'
  else
  if aRequest = TFDActionRequest.arFetchGenerators then
    lRequest := 'Request: arFetchGenerators'
  else
      lRequest := 'Request: Undefined!';

  // Assume SOME_DETAIL and SOME_MASTER tables are related one-to-many.
  // FDQuery.SQL is a join of these two tables. FDUpdateSQL_SomeDetail posts
  // Insert|Fetch|Modify|Delete (aka CRUD) changes to SOME_DETAIL table while
  // FDUpdateSQL_SomeMaster posts to SOME_MASTER table. At first, we
  // post changes to SOME_DETAIL (or sub type) table, at second to SOME_MASTER
  // (master) one. Note, however, in the case of Insert, a record in the master
  // table SOME_MASTER must be created BEFORE the detail(s) in the SOME_DETAIL.

  try
    if NOT ( aRequest in [arLock, arUnlock] ) then begin
      // Party1Person, Party2Person and Party3Person are used to CREATE/INSERT
      // or DELETE in the ENTITY_BASE, PARTY, and PARTY_PERSON tables,
      // respectively. Party3Person is additionally used to UPDATE/MODIFY
      // value in the PARTY_PERSON table.
      // 
      // If CREATING/INSERTING then pass in a new GUID for the new party
      // creation: first in the ENTITY_BASE table.
      FDUpdateSQL_Party1Person.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_Party1Person.DataSet := FDQuery;
      if aRequest = arInsert then begin
        CreateGUID( lGUIDPerson );
        FDUpdateSQL_Party1Person.Commands[aRequest].ParamByName( 'PERSON_ID' ).Value := lGUIDPerson.ToByteArray;
      end;
      FDUpdateSQL_Party1Person.Apply( aRequest, aAction, aOptions );
      
      if aAction = eaApplied then begin
        // If CREATING/INSERTING then pass in a new GUID for the new party
        // creation: next in the PARTY table.
        FDUpdateSQL_Party2Person.ConnectionName := FDQuery.ConnectionName;
        FDUpdateSQL_Party2Person.DataSet := FDQuery;
        if aRequest = arInsert then begin
          FDUpdateSQL_Party2Person.Commands[aRequest].ParamByName( 'PERSON_ID' ).Value := lGUIDPerson.ToByteArray;
        end;
        FDUpdateSQL_Party2Person.Apply( aRequest, aAction, aOptions );
        
        if aAction = eaApplied then begin
          // If CREATING/INSERTING then pass in a new GUID for the new party
          // creation: next in the PARTY_PERSON table.
          FDUpdateSQL_Party3Person.ConnectionName := FDQuery.ConnectionName;
          FDUpdateSQL_Party3Person.DataSet := FDQuery;
          if aRequest = arInsert then begin
            FDUpdateSQL_Party3Person.Commands[aRequest].ParamByName( 'PERSON_ID' ).Value := lGUIDPerson.ToByteArray;
          end;
          FDUpdateSQL_Party3Person.Apply( aRequest, aAction, aOptions );
          
          if aAction = eaApplied then begin
            // Party4Group, Party5Group and Party6Group are used to CREATE/INSERT
            // new family records (IFF the new FAMILY_NAME specified does not
            // already exists) into the ENTITY_BASE, PARTY, and PARTY_GROUP tables,
            // DELETE family records (IFF the family record is not referenced by
            // any other person records, that is, all family members must be deleted
            // first.). Party6Group is additionally used to UPDATE/MODIFY values in the 
            // PARTY_GROUP table.
            // 
            
            // If CREATING/INSERTING then pass in a new GUID for the new party
            // creation: first in the ENTITY_BASE table.
            FDUpdateSQL_Party4Group.ConnectionName := FDQuery.ConnectionName;
            FDUpdateSQL_Party4Group.DataSet := FDQuery;
            if aRequest = arInsert then begin
              CreateGUID( lGUIDGroup );
              FDUpdateSQL_Party4Group.Commands[aRequest].ParamByName( 'FAMILY_ID' ).Value := lGUIDGroup.ToByteArray;
            end;
            FDUpdateSQL_Party4Group.Apply( aRequest, aAction, aOptions );
            
            if aAction = eaApplied then begin
              // If CREATING/INSERTING then pass in a new GUID for the new party
              // creation: next in the PARTY table.
              FDUpdateSQL_Party5Group.ConnectionName := FDQuery.ConnectionName;
              FDUpdateSQL_Party5Group.DataSet := FDQuery;
              if aRequest = arInsert then begin
                FDUpdateSQL_Party5Group.Commands[aRequest].ParamByName( 'FAMILY_ID' ).Value := lGUIDGroup.ToByteArray;
              end;
              FDUpdateSQL_Party5Group.Apply( aRequest, aAction, aOptions );
              
              if aAction = eaApplied then begin
                // If CREATING/INSERTING then pass in a new GUID for the new party
                // creation: next in the PARTY_GROUP table.
                FDUpdateSQL_Party6Group.ConnectionName := FDQuery.ConnectionName;
                FDUpdateSQL_Party6Group.DataSet := FDQuery;
                if aRequest = arInsert then begin
                  FDUpdateSQL_Party6Group.Commands[aRequest].ParamByName( 'FAMILY_ID' ).Value := lGUIDGroup.ToByteArray;
                end;
                FDUpdateSQL_Party6Group.Apply( aRequest, aAction, aOptions );
                
                if aAction = eaApplied then begin
                  // Party7Hierarchy and Party8Hierarchy are used to
                  // CREATE/INSERT new party hierarchy records to connect the
                  // specified family and person party records together (IFF the
                  // relationship does not already exist) into the ENTITY_BASE and
                  // PARTY_HIERARCHY tables, or DELETE or UDATE party hierarchy records
                  //
                  // If CREATING/INSERTING then pass in a new GUID for the new hierarchy
                  // creation: first in the ENTITY_BASE table.
                  FDUpdateSQL_Party7Hierarchy.ConnectionName := FDQuery.ConnectionName;
                  FDUpdateSQL_Party7Hierarchy.DataSet := FDQuery;
                  if aRequest = arInsert then begin
                    CreateGUID( lGUIDHierarchy );
                    FDUpdateSQL_Party7Hierarchy.Commands[aRequest].ParamByName( 'HEIRARCHY_ID' ).Value := lGUIDHierarchy.ToByteArray;
                  end;
                  FDUpdateSQL_Party7Hierarchy.Apply( aRequest, aAction, aOptions );
                  
                  if aAction = eaApplied then begin
                    // If CREATING/INSERTING then pass in a new GUID for the new hierarchy
                    // creation: next in the PARTY_HIERARCHY table.
                    FDUpdateSQL_Party8Hierarchy.ConnectionName := FDQuery.ConnectionName;
                    FDUpdateSQL_Party8Hierarchy.DataSet := FDQuery;
                    if aRequest = arInsert then begin
                      FDUpdateSQL_Party8Hierarchy.Commands[aRequest].ParamByName( 'HEIRARCHY_ID' ).Value := lGUIDHierarchy.ToByteArray;
                    end;
                    FDUpdateSQL_Party8Hierarchy.Apply( aRequest, aAction, aOptions );
                    
                    if aAction = eaApplied then begin
                      // Party9Hierarchy and Party10Hierarchy are used to
                      // CREATE/INSERT new party hierarchy records to connect the
                      // specified family and center records together (IFF the
                      // relationship does not already exist) into the ENTITY_BASE and
                      // PARTY_HIERARCHY tables, or DELETE party hierarchy records
                      // (IFF the family record is not referenced by any other person
                      // records, that is, all family members must be deleted first.)
                      // 
                      // If CREATING/INSERTING then pass in a new GUID for the new hierarchy
                      // creation: first in the ENTITY_BASE table.
                      FDUpdateSQL_Party9Hierarchy.ConnectionName := FDQuery.ConnectionName;
                      FDUpdateSQL_Party9Hierarchy.DataSet := FDQuery;
                      if aRequest = arInsert then begin
                        CreateGUID( lGUIDHierarchy2 );
                        FDUpdateSQL_Party9Hierarchy.Commands[aRequest].ParamByName( 'HEIRARCHY_ID' ).Value := lGUIDHierarchy2.ToByteArray;
                      end;
                      FDUpdateSQL_Party9Hierarchy.Apply( aRequest, aAction, aOptions );
                      
                      if aAction = eaApplied then begin
                        // If CREATING/INSERTING then pass in a new GUID for the new hierarchy
                        // creation: next in the PARTY_HIERARCHY table.
                        FDUpdateSQL_Party10Hierarchy.ConnectionName := FDQuery.ConnectionName;
                        FDUpdateSQL_Party10Hierarchy.DataSet := FDQuery;
                        if aRequest = arInsert then begin
                          FDUpdateSQL_Party10Hierarchy.Commands[aRequest].ParamByName( 'HEIRARCHY_ID' ).Value := lGUIDHierarchy2.ToByteArray;
                        end;
                        FDUpdateSQL_Party10Hierarchy.Apply( aRequest, aAction, aOptions );
                        
                        // Commit only if all previous queries ran successfully
                        //if aAction = eaApplied then begin
                        //  Connection.Commit;
                        //end;
                        
                      end;
                    end;
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    aAction := eaApplied;
  except
    on E: Exception do
      CodeSite.SendException( E );
  end;

  //lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lRequest );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

  lValue := aSender.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;


end.