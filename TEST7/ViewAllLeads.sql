/* View all lead records */
SELECT
    UUID_TO_CHAR(pg.ID) AS PARTY_GROUP_ID, pg.NAME AS ACCOUNT_NAME, UUID_TO_CHAR(pp.ID) AS PARTY_PERSON_ID, pp.FIRST_NAME, pp.LAST_NAME, UUID_TO_CHAR(prl.ID) AS PARTY_ROLE_LEAD_ID, prl.INITIAL_CONTACT_AT
FROM PARTY_PERSON pp
JOIN PARTY_ROLE pr ON pr.PARTY_ID = pp.ID
JOIN PARTY_ROLE_LEAD prl ON prl.ID = pr.ID
JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID
JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID
WHERE 
    /* Makes sure the person is not listed as Do Not Contact */
    pp.DO_NOT_CONTACT = '0'
    /* Makes sure the person does not have the role "Customer" */
    AND NOT EXISTS (
        SELECT 1 FROM PARTY_ROLE_CUSTOMER prc JOIN PARTY_ROLE pr2 ON pr2.ID = prc.ID WHERE pr2.PARTY_ID = pp.ID
    )
    /* Makes sure the person does not have the role "Student" */
    AND NOT EXISTS (
        SELECT 1 FROM PARTY_ROLE_STUDENT prs JOIN PARTY_ROLE pr2 ON pr2.ID = prs.ID WHERE pr2.PARTY_ID = pp.ID
    )
ORDER BY prl.INITIAL_CONTACT_AT DESC