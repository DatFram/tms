unit TEST6.BusinessObject.Kind.Sets;
// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. For more about "Delphi Record Helpers For Sets (And Other Simple Types)",
//     see:
//     - https://www.thoughtco.com/record-helpers-for-sets-1058204
//
// -----------------------------------------------------------------------------
interface

uses
// TEST6 dependencies
  TEST6.BusinessObject.Kind
;

type

// =============================================================================
//  TSet< T > = set of T;
//
//  TRFI_Types_Generics = class( TInterfacedObject, IInterface )
//    public
//      function GetCount< T {: Set} >( const aElementSet: TSet< T > ): Integer;
//  end;

// =============================================================================
// TAddressKindSet

  TAddressTypeKindSet = Set of TAddressTypeKind;

  TAddressTypeKindSetHelper = record helper for TAddressTypeKindSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindSet

  TActivityTypeKindSet = Set of TActivityTypeKind;

  TActivityTypeKindSetHelper = record helper for TActivityTypeKindSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindCallSet

  TActivityTypeKindCallSet = Set of TActivityTypeKindCall;

  TActivityTypeKindCallSetHelper = record helper for TActivityTypeKindCallSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindVisitSet

  TActivityTypeKindVisitSet = Set of TActivityTypeKindVisit;

  TActivityTypeKindVisitSetHelper = record helper for TActivityTypeKindVisitSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindAssessmentSet

  TActivityTypeKindAssessmentSet = Set of TActivityTypeKindAssessment;

  TActivityTypeKindAssessmentSetHelper = record helper for TActivityTypeKindAssessmentSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindTrialSessionSet

  TActivityTypeKindTrialSessionSet = Set of TActivityTypeKindTrialSession;

  TActivityTypeKindTrialSessionSetHelper = record helper for TActivityTypeKindTrialSessionSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindEmailSet

  TActivityTypeKindEmailSet = Set of TActivityTypeKindEmail;

  TActivityTypeKindEmailSetHelper = record helper for TActivityTypeKindEmailSet
    public
      function Count: Integer;
  end;

// =============================================================================
// TActivityTypeKindCampaignSet

  TActivityTypeKindCampaignSet = Set of TActivityTypeKindCampaign;

  TActivityTypeKindCampaignSetHelper = record helper for TActivityTypeKindCampaignSet
    public
      function Count: Integer;
  end;


implementation

// =============================================================================
// TAddressTypeKindSet

{class} function TAddressTypeKindSetHelper.Count{( const aSubSet: TAddressTypeKindSet )}: Integer;
var
  lElement: TAddressTypeKind;
begin
  Result:= 0;
  for lElement := Low( TAddressTypeKind ) to High( TAddressTypeKind ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindSet

{class} function TActivityTypeKindSetHelper.Count{( const aSubSet: TActivityTypeKindSet )}: Integer;
var
  lElement: TActivityTypeKind;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKind ) to High( TActivityTypeKind ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindCallSet

{class} function TActivityTypeKindCallSetHelper.Count{( const aSubSet: TActivityTypeKindCallSet )}: Integer;
var
  lElement: TActivityTypeKindCall;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindCall ) to High( TActivityTypeKindCall ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindVisitSet

{class} function TActivityTypeKindVisitSetHelper.Count{( const aSubSet: TActivityTypeKindVisitSet )}: Integer;
var
  lElement: TActivityTypeKindVisit;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindVisit ) to High( TActivityTypeKindVisit ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindAssessentSet

{class} function TActivityTypeKindAssessentSetHelper.Count{( const aSubSet: TActivityTypeKindAssessentSet )}: Integer;
var
  lElement: TActivityTypeKindAssessent;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindAssessent ) to High( TActivityTypeKindAssessent ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindTrialSessionSet

{class} function TActivityTypeKindTrialSessionSetHelper.Count{( const aSubSet: TActivityTypeKindTrialSessionSet )}: Integer;
var
  lElement: TActivityTypeKindTrialSession;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindTrialSession ) to High( TActivityTypeKindTrialSession ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindEmailSet

{class} function TActivityTypeKindEmailSetHelper.Count{( const aSubSet: TActivityTypeKindEmailSet )}: Integer;
var
  lElement: TActivityTypeKindEmail;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindEmail ) to High( TActivityTypeKindEmail ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;

// =============================================================================
// TActivityTypeKindCampaignSet

{class} function TActivityTypeKindCampaignSetHelper.Count{( const aSubSet: TActivityTypeKindCampaignSet )}: Integer;
var
  lElement: TActivityTypeKindCampaign;
begin
  Result:= 0;
  for lElement := Low( TActivityTypeKindCampaign ) to High( TActivityTypeKindCampaign ) do begin
    if lElement in Self then
      Inc( Result );
  end;
end;



end.
