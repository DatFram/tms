procedure TTEST6_Address_DataAccess_Connection.FDQueryUpdateRecord( aSender: TDataSet;
  aRequest: TFDUpdateRequest; var aAction: TFDErrorAction; aOptions:
    TFDUpdateRowOptions );
var
  lIndex: Integer;
  lValue: String;
  lGUIDDetail: TGUID;
  lGUIDDetailHierarchy: TGUID;
begin

  // Assume SOME_DETAIL and SOME_MASTER tables are related one-to-many.
  // FDQuery.SQL is a join of these two tables. FDUpdateSQL_SomeDetail posts
  // Insert|Fetch|Modfy|Delete (aka CRUD) changes to SOME_DETAIL table while
  // FDUpdateSQL_SomeMaster posts to SOME_MASTER table. At first, we
  // post changes to SOME_DETAIL (or sub type) table, at second to SOME_MASTER
  // (master) one. Note, however, in the case of Insert, a record in the master
  // table SOME_MASTER must be created BEFORE the detail(s) in the SOME_DETAIL.

  try
  // *************************************************************************
  // Each item in the ADDRESS table is related to one-or-one other ADDRESS items
  // (principally, "content") via the ADDRESS_HIERARCHY table where HEATHER TO DESCRIBE.
  // The "ADDRESS" table has a foreign key to "ADDRESS_TYPE" table to indicate
  // the type of each record within the ADDRESS table. The CRUD process for an
  // item in the ADDRESS table (embodied hereunder in code) flows as follows:
  //
  //   1. Update the Master (parent) address record
  //   2. Update the Detail (child) address record
  //   3. Udate the Hierarchy (connector) record
  //
  // *************************************************************************
    Log( aRequest );

    if not (aRequest in [arLock, arUnlock]) then begin
      FDUpdateSQL_address1Master.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_address1Master.DataSet := FDQuery;
      FDUpdateSQL_address1Master.Apply( aRequest, aAction, aOptions);

      if aAction = eaApplied then begin
        FDUpdateSQL_address2Master.ConnectionName := FDQuery.ConnectionName;
        FDUpdateSQL_address2Master.DataSet := FDQuery;
        FDUpdateSQL_address2Master.Apply( aRequest, aAction, aOptions );

        if aAction = eaApplied then begin
          FDUpdateSQL_address3Detail.ConnectionName := FDQuery.ConnectionName;
          FDUpdateSQL_address3Detail.DataSet := FDQuery;
		    // PSEUDOCODE: Create guid var called lGUIDDetail
          CreateGUID( lGUIDDetail );
          FDUpdateSQL_address3Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToString; // lGUIDDetail.ToByteArray;
          FDUpdateSQL_address3Detail.Apply( aRequest, aAction, aOptions);

          if aAction = eaApplied then begin
            FDUpdateSQL_address4Detail.ConnectionName := FDQuery.ConnectionName;
            FDUpdateSQL_address4Detail.DataSet := FDQuery;
			    // PSEUDOCODE: use existing lGUIDDetail var
            FDUpdateSQL_address4Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToString; // lGUIDDetail.ToByteArray;
            FDUpdateSQL_address4Detail.Apply( aRequest, aAction, aOptions);

            if aAction = eaApplied then begin
              FDUpdateSQL_address5DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
              FDUpdateSQL_address5DetailHierarchy.DataSet := FDQuery;
			      // PSEUDOCODE: Create guid var called lGUIDDetailHierarchy
              CreateGUID( lGUIDDetailHierarchy );
			        FDUpdateSQL_address5DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToString; // lGUIDDetailHierarchy.ToByteArray;
              FDUpdateSQL_address5DetailHierarchy.Apply( aRequest, aAction, aOptions );

              if aAction = eaApplied then begin
                FDUpdateSQL_address6DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                FDUpdateSQL_address6DetailHierarchy.DataSet := FDQuery;
				      // PSEUDOCODE: use existing lGUIDDetailHierarchy var
                FDUpdateSQL_address6DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToString; // lGUIDDetailHierarchy.ToByteArray;
                FDUpdateSQL_address6DetailHierarchy.Apply( aRequest, aAction, aOptions );

                if aAction = eaApplied then begin
                  FDUpdateSQL_address7DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                  FDUpdateSQL_address7DetailHierarchy.DataSet := FDQuery;
                  FDUpdateSQL_address7DetailHierarchy.Apply( aRequest, aAction, aOptions );

                // Commit only if all previous queries ran successfully
				          if aAction = eaApplied then begin
                    Connection.Commit;
				          end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
	// Can this line be entirely removed? If it is successful, then aAction will already be set to eaApplied
    aAction := eaApplied;
  except
    on E: Exception do begin
      CodeSite.SendException( E );
      Connection.Rollback;
      raise E;
    end;
  end;

  //lSQL := FDUpdateSQL_Person.ModifySQl.Text;
//  lIndex := TEST6_Address_View_Lister_SQL.lbxCommands.Items.Add( lRequest );
//  TEST6_Address_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

  lValue := aSender.FieldByName( 'CONTENT' ).Value;
  lIndex := TEST6_Address_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Address_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

end;
