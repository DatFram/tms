// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. For proper use of the key component TFDUpdateSQL (used in tandem with
//     FDQuery for updated JOINED tables, respecting their data hierarchy, see:
//     - http://docwiki.embarcadero.com/CodeExamples/Tokyo/en/FireDAC.TFDQuery.OnUpdateRecord_Sample
//
//  2. For the challennges of not understanding #1, see:
//     - http://stackoverflow.com/questions/28175328/tfdupdatesql-not-posting-updates-on-datasnap-server
//     As its unanswered, you've got StackOverflow points waiting.
//
//  3. For ...
//     - http://docwiki.embarcadero.com/RADStudio/Tokyo/en/Executing_Commands_(FireDAC)
//
// -----------------------------------------------------------------------------
unit TEST6.Party.DataAccess.Connection;

interface

uses
  Aurelius.Drivers.Interfaces,
  Aurelius.SQL.Firebird,
  Aurelius.Schema.Firebird,
  Aurelius.Drivers.FireDac
//
, System.SysUtils
, System.Classes
//
, Data.DB
//
, FireDAC.Stan.Intf
, FireDAC.Stan.Option
, FireDAC.Stan.Error
, FireDAC.UI.Intf
, FireDAC.Phys.Intf
, FireDAC.Stan.Def
, FireDAC.Stan.Pool
, FireDAC.Stan.Async
, FireDAC.Phys
, FireDAC.Phys.FB
, FireDAC.Phys.FBDef
, FireDAC.VCLUI.Wait
, FireDAC.Comp.Client
, FireDAC.Stan.Param
, FireDAC.DatS
, FireDAC.DApt.Intf
, FireDAC.DApt
, FireDAC.Comp.DataSet
, FireDAC.Moni.Base
, FireDAC.Moni.RemoteClient
;

type

  TTEST6_Party_DataAccess_Connection = class(TDataModule)
    Connection: TFDConnection;
    FDQuery: TFDQuery;
    FDUpdateSQL_Person: TFDUpdateSQL;
    FDMoniRemoteClientLink1: TFDMoniRemoteClientLink;
    procedure ConnectionAfterCommit(Sender: TObject);
    procedure ConnectionBeforeCommit(Sender: TObject);
    procedure FDQueryAfterPost(DataSet: TDataSet);
    procedure FDQueryBeforeEdit(DataSet: TDataSet);
    procedure FDQueryUpdateRecord(ASender: TDataSet; ARequest: TFDUpdateRequest;
        var AAction: TFDErrorAction; AOptions: TFDUpdateRowOptions);
  private
  public
    class function CreateConnection: IDBConnection;
    class function CreateFactory: IDBConnectionFactory;

  end;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
//
  Aurelius.Drivers.Base
//
, TEST6.Party.View.Lister.SQL
//
, CodeSiteLogging
;

{$R *.dfm}

{ TTEST6_Party_DataAccess_Connection }

procedure TTEST6_Party_DataAccess_Connection.ConnectionBeforeCommit(Sender: TObject);
var
  lIndex: Integer;
//  lCommand: String;
begin
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'BEFORE COMMIT CALLED' );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
//  lCommand := Connection.Command;
//  lIndex := TTEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'BEFORE COMMIT CALLED' );
//  TTEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.ConnectionAfterCommit(Sender: TObject);
var
  lIndex: Integer;
begin
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( 'AFTER COMMIT CALLED' );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

class function TTEST6_Party_DataAccess_Connection.CreateConnection: IDBConnection;
var
  DataModule: TTEST6_Party_DataAccess_Connection;
begin
  DataModule := TTEST6_Party_DataAccess_Connection.Create( nil );
  Result := TFireDacConnectionAdapter.Create(DataModule.Connection, 'Firebird', DataModule);
end;

class function TTEST6_Party_DataAccess_Connection.CreateFactory: IDBConnectionFactory;
begin
  Result := TDBConnectionFactory.Create(
    function: IDBConnection
    begin
      Result := CreateConnection;
    end
  );
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryBeforeEdit( DataSet: TDataSet );
var
  lIndex: Integer;
  lSQL: String;
  lValue: String;
begin
  lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lSQL );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
  lValue := DataSet.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryAfterPost( DataSet: TDataSet );
var
  lIndex: Integer;
  lSQL: String;
  lValue: String;
begin
  lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lSQL );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
  lValue := DataSet.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;

procedure TTEST6_Party_DataAccess_Connection.FDQueryUpdateRecord( aSender: TDataSet;
  aRequest: TFDUpdateRequest; var aAction: TFDErrorAction; aOptions:
    TFDUpdateRowOptions );
var
  lIndex: Integer;
  lRequest: String;
  lValue: String;
begin
  if aRequest = TFDActionRequest.arNone then
    lRequest := 'Request: arNone'
  else
  if aRequest = TFDActionRequest.arFromRow then
      lRequest := 'Request: arFromRow'
  else
  if aRequest = TFDActionRequest.arSelect then
      lRequest := 'Request: arSelect'
  else
  if aRequest = TFDActionRequest.arInsert then
      lRequest := 'Request: arInsert (aka, Create)'
  else
  if aRequest = TFDActionRequest.arUpdate then
      lRequest := 'Request: arUpdate'
  else
  if aRequest = TFDActionRequest.arDelete then
      lRequest := 'Request: arDelete'
  else
  if aRequest = TFDActionRequest.arLock then
      lRequest := 'Request: arLock'
  else
  if aRequest = TFDActionRequest.arUnlock then
      lRequest := 'Request: arUnlock'
  else
  if aRequest = TFDActionRequest.arFetchRow then
      lRequest := 'Request: arFetchRow (aka, Read)'
  else
  if aRequest = TFDActionRequest.arUpdateHBlobs then
      lRequest := 'Request: arUpdateHBlobs'
  else
  if aRequest = TFDActionRequest.arDeleteAll then
      lRequest := 'Request: arDeleteAll'
  else
  if aRequest = TFDActionRequest.arFetchGenerators then
    lRequest := 'Request: arFetchGenerators'
  else
      lRequest := 'Request: Undefined!';

  // Assume SOME_DETAIL and SOME_MASTER tables are related one-to-many.
  // FDQuery.SQL is a join of these two tables. FDUpdateSQL_SomeDetail posts
  // Insert|Fetch|Modfy|Delete (aka CRUD) changes to SOME_DETAIL table while
  // FDUpdateSQL_SomeMaster posts to SOME_MASTER table. At first, we
  // post changes to SOME_DETAIL (or sub type) table, at second to SOME_MASTER
  // (master) one. Note, however, in the case of Insert, a record in the master
  // table SOME_MASTER must be created BEFORE the detail(s) in the SOME_DETAIL.

  try
  // NOTE: PARTY_PERSON table is a subtype (detail) of PARTY which, in turn, is
    //       a subtype (like all other tables) on ENTITY_BASE
    if NOT ( aRequest in [arLock, arUnlock] ) then begin
      FDUpdateSQL_Person.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_Person.DataSet := FDQuery;
      FDUpdateSQL_Person.Apply( aRequest, aAction, aOptions );

  //    if aAction = eaApplied then begin
  //      FDUpdateSQL_<NextTableToUpdate>.ConnectionName := FDQuery.ConnectionName;
  //      FDUpdateSQL_<NextTableToUpdate>.DataSet := FDQuery;
  //      FDUpdateSQL_<NextTableToUpdate>.Apply( aRequest, aAction, aOptions );
  //    end;

    end;
    aAction := eaApplied;
  except
    on E: Exception do
      CodeSite.SendException( E );
  end;

  //lSQL := FDUpdateSQL_Person.ModifySQl.Text;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lRequest );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

  lValue := aSender.FieldByName( 'LAST_NAME' ).Value;
  lIndex := TEST6_Party_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Party_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;
end;


end.