object TEST6_Party_DataAccess_Connection: TTEST6_Party_DataAccess_Connection
  OldCreateOrder = True
  Height = 198
  Width = 282
  object Connection: TFDConnection
    Params.Strings = (
      'ConnectionDef=TEST6')
    BeforeCommit = ConnectionBeforeCommit
    AfterCommit = ConnectionAfterCommit
    Left = 40
    Top = 32
  end
  object FDQuery: TFDQuery
    BeforeEdit = FDQueryBeforeEdit
    AfterPost = FDQueryAfterPost
    OnUpdateRecord = FDQueryUpdateRecord
    Connection = Connection
    UpdateObject = FDUpdateSQL_Person
    SQL.Strings = (
      
        '/* -------------------------------------------------------------' +
        '-'
      'PURPOSE:'
      #9
      #9'All Families'
      #9'(Lists all families and the centers they are registered to or'
      #9'associated with)'
      #9
      'CREATED: 2017-05-12'
      '*/'
      ''
      'SELECT'
      
        '    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSS' +
        'ESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME' +
        ' AS FAMILY_NAME, pg2.NAME AS CENTER'
      'FROM PARTY_PERSON pp'
      'JOIN GENDER g ON g.ID = pp.GENDER_ID'
      'JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID'
      'JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID'
      'JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID'
      'JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID'
      'ORDER BY pg2.NAME, pg.NAME, ph.RELATIONSHIP, pp.FIRST_NAME')
    Left = 128
    Top = 32
  end
  object FDUpdateSQL_Person: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO PARTY_PERSON (ID, FIRST_NAME, LAST_NAME, GENDER_ID)'
      'SELECT :NEW_ID, :NEW_FIRST_NAME, :NEW_LAST_NAME, g.ID'
      'FROM GENDER g WHERE g.NAME = :NEW_GENDER;')
    ModifySQL.Strings = (
      'UPDATE PARTY_PERSON pp SET pp.ID = :NEW_ID,'
      'pp.FIRST_NAME = :NEW_FIRST_NAME,'
      'pp.LAST_NAME = :NEW_LAST_NAME,'
      
        'pp.GENDER_ID = (SELECT g.ID FROM GENDER g WHERE g.NAME = :NEW_GE' +
        'NDER)'
      'WHERE pp.ID = :OLD_ID;')
    DeleteSQL.Strings = (
      'DELETE FROM PARTY_PERSON pp WHERE pp.ID = :OLD_ID;')
    FetchRowSQL.Strings = (
      'SELECT'
      
        '    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSS' +
        'ESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME' +
        ' AS FAMILY_NAME, pg2.NAME AS CENTER'
      'FROM PARTY_PERSON pp'
      'JOIN GENDER g ON g.ID = pp.GENDER_ID'
      'JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID'
      'JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID'
      'JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID'
      'JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID'
      'WHERE pp.ID = :OLD_ID;')
    Left = 128
    Top = 80
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Tracing = True
    Left = 128
    Top = 136
  end
end