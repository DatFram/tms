/* --------------------------------------------------------------
Requires TEST6 database.

UpdateSQL statements can be found after the query SQL
NOTE: 20 TFDUpdateSQL instances are required to fully support this query

PURPOSE:
	All Addresses
	Returns all postal addresses
	
NOTES:
    Outer query fetches all ADDRESS records marked as
	US_POSTAL_ADDRESS, and each inner query fetches the detail
	address that makes up one part of the postal address, e.g.
	Street1, Street2, City, etc.
	
PERFORMANCE:
	- 0.1 seconds in FlameRobin
	
CREATED: 2017-05-16

*/

	SELECT
		ah.ID AS ID,
		a2.CONTENT,
		t2.NAME AS ADDRESS_TYPE,
		ah.POSITION_INDEX,
		a.ID AS PARENT_ID,
		a2.ID AS CHILD_ID
	FROM ADDRESS a
	JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
	JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID 
	JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID
	JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
	WHERE t.NAME = 'US_POSTAL_ADDRESS'
	ORDER BY a.ID, ah.POSITION_INDEX

  