/*  */

--INSERT: CHAR_TO_UUID('C751CD8B-AD03-4736-B697-B07C58394412')
--MODIFY: CHAR_TO_UUID('41C8783F-7702-4D97-B32A-C610D31CF114')

--INSERT: CHAR_TO_UUID('FC73568A-5377-444C-B0F5-90A48D49027E')
--MODIFY: CHAR_TO_UUID('9DDD02F8-6EB4-4110-9D85-449C38101403')

  try

    // Addresses are related to other addresses via the address
    // hierarchy table. Address Type indicates what kind of address
    // each record is.
    // First update the Master (parent) address record,
    // Then update the Detail (child) address record,
    // Finally update the Hierarchy (connector) record
    if not (aRequest in [arLock, arUnlock]) then begin
      FDUpdateSQL_address1Master.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_address1Master.DataSet := FDQuery;
      FDUpdateSQL_address1Master.Apply( aRequest, aAction, aOptions);

      if aAction = eaApplied then begin
        FDUpdateSQL_address2Master.ConnectionName := FDQuery.ConnectionName;
        FDUpdateSQL_address2Master.DataSet := FDQuery;
        FDUpdateSQL_address2Master.Apply( aRequest, aAction, aOptions );

        if aAction = eaApplied then begin
          FDUpdateSQL_address3Detail.ConnectionName := FDQuery.ConnectionName;
          FDUpdateSQL_address3Detail.DataSet := FDQuery;
        // Explicitly set the type to string to ensure it's escaped
        // e.g. binary types are not escaped
          //FDUpdateSQL_address3Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUID.ToString; // lGUID.ToByteArray;
          FDUpdateSQL_address3Detail.Apply( aRequest, aAction, aOptions);

          if aAction = eaApplied then begin
            FDUpdateSQL_address4Detail.ConnectionName := FDQuery.ConnectionName;
            FDUpdateSQL_address4Detail.DataSet := FDQuery;
            FDUpdateSQL_address4Detail.Apply( aRequest, aAction, aOptions);

            if aAction = eaApplied then begin
              FDUpdateSQL_address5DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
              FDUpdateSQL_address5DetailHierarchy.DataSet := FDQuery;
              FDUpdateSQL_address5DetailHierarchy.Apply( aRequest, aAction, aOptions );

              if aAction = eaApplied then begin
                FDUpdateSQL_address6DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                FDUpdateSQL_address6DetailHierarchy.DataSet := FDQuery;
                FDUpdateSQL_address6DetailHierarchy.Apply( aRequest, aAction, aOptions );
                Connection.Commit;
              end;
            end;
          end;
        end;
      end;
    end;
    aAction := eaApplied;
  except
    on E: Exception do begin
      CodeSite.SendException( E );
      Connection.Rollback;
      raise E;
    end;
  end;


/* Copy into .dfm file: */


  object FDUpdateSQL_address1Master: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_PARENT_ID'
	  'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARENT_ADDRESS_ID = eb.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
	  'SELECT :NEW_PARENT_ID, ''ADDRESS'''
	  'FROM ADDRESS_TYPE t'
	  'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
	  'WHERE t.NAME = ''US_POSTAL_ADDRESS'' AND a.ID IS NULL')
    Left = 70
    Top = 10
  end
  object FDUpdateSQL_address2Master: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
	DeleteSQL.Strings = (
      'DELETE FROM ADDRESS a WHERE a.ID = :OLD_PARENT_ID'
	  'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARENT_ADDRESS_ID = a.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID) '
	  'SELECT :NEW_PARENT_ID, t.ID'
	  'FROM ADDRESS_TYPE t'
	  'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
	  'WHERE t.NAME = ''US_POSTAL_ADDRESS'' AND a.ID IS NULL')
    Left = 70
    Top = 20
  end
  object FDUpdateSQL_address3Detail: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
	  'SELECT :CHILD_ID, ''ADDRESS'''
	  'FROM ADDRESS_TYPE t'
	  'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = :NEW_CONTENT'
	  'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
	  'SELECT :CHILD_ID, ''ADDRESS'''
	  'FROM ADDRESS_TYPE t'
	  'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = :NEW_CONTENT'
	  'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    Left = 70
    Top = 30
  end
  object FDUpdateSQL_address4Detail: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
	DeleteSQL.Strings = (
		'DELETE FROM ADDRESS a WHERE a.ID = :NEW_CHILD_ID'
		'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.CHILD_ADDRESS_ID = a.ID AND ah.PARENT_ADDRESS_ID <> :OLD_PARENT_ID)')
	FetchRowSQL.Strings = (
		'SELECT'
		'	a2.ID,'
		'	a2.CONTENT,'
		'	t2.NAME AS ADDRESS_TYPE,'
		'	ah.POSITION_INDEX,'
		'	a.ID AS PARENT_ID'
		'FROM ADDRESS a'
		'JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID'
		'JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID '
		'JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID'
		'JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID'
		'WHERE t.NAME = ''US_POSTAL_ADDRESS'''
		'AND a2.ID = :NEW_CHILD_ID')
    InsertSQL.Strings = (
	  'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT)'
	  'SELECT :CHILD_ID, t.ID, :NEW_CONTENT'
	  'FROM ADDRESS_TYPE t'
	  'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = :NEW_CONTENT'
	  'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL'
      )
    ModifySQL.Strings = (
      'UPDATE ADDRESS a SET a.ID = :NEW_CHILD_ID,'
	  'a.ADDRESS_TYPE_ID = (SELECT t.ID FROM ADDRESS_TYPE t WHERE t.NAME = :NEW_ADDRESS_TYPE),'
	  'a.CONTENT = :NEW_CONTENT'
	  'WHERE a.ID = :OLD_CHILD_ID'
	  'AND NOT EXISTS (SELECT 1 FROM ADDRESS a2 JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID WHERE a2.CONTENT = :NEW_CONTENT AND t2.NAME = :NEW_ADDRESS_TYPE AND a2.ID <> a.ID)')
    Left = 70
    Top = 40
  end
  object FDUpdateSQL_address5DetailHierarchy: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:NEW_ID, ''ADDRESS_HIERARCHY'')')
    ModifySQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:NEW_ID, ''ADDRESS_HIERARCHY'')')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID')
    Left = 70
    Top = 50
  end
  object FDUpdateSQL_address6DetailHierarchy: TFDUpdateSQL
    ConnectionName = 'TEST6_COPY'
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, POSITION_INDEX)'
	  'SELECT :NEW_ID, :NEW_PARENT_ID, a.ID, :NEW_POSITION_INDEX'
	  'FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID '
	  'LEFT JOIN ADDRESS_HIERARCHY ah ON ah.CHILD_ADDRESS_ID = a.ID AND ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID'
	  'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.CONTENT = :NEW_CONTENT AND ah.ID IS NULL')
    ModifySQL.Strings = (
		'UPDATE ADDRESS_HIERARCHY ah SET ah.ID = :NEW_ID,'
		'	ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID,'
		'	ah.CHILD_ADDRESS_ID = (SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.CONTENT = :NEW_CONTENT),'
		'	ah.POSITION_INDEX = :NEW_POSITION_INDEX'
		'WHERE ah.ID = :OLD_ID')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS_HIERARCHY ah WHERE ah.ID = :OLD_ID')
    Left = 70
    Top = 60
  end
