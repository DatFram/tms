

  try

    // Addresses are related to other addresses via the address
    // hierarchy table. Address Type indicates what kind of address
    // each record is.
    // First update the Master (parent) address record,
    // Then update the Detail (child) address record,
    // Finally update the Hierarchy (connector) record
    if not (aRequest in [arLock, arUnlock]) then begin
      FDUpdateSQL_address1Master.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_address1Master.DataSet := FDQuery;
      FDUpdateSQL_address1Master.Apply( aRequest, aAction, aOptions);

      if aAction = eaApplied then begin
        FDUpdateSQL_address2Master.ConnectionName := FDQuery.ConnectionName;
        FDUpdateSQL_address2Master.DataSet := FDQuery;
        FDUpdateSQL_address2Master.Apply( aRequest, aAction, aOptions );

        if aAction = eaApplied then begin
          FDUpdateSQL_address3Detail.ConnectionName := FDQuery.ConnectionName;
          FDUpdateSQL_address3Detail.DataSet := FDQuery;
        // Explicitly set the type to string to ensure it's escaped
        // e.g. binary types are not escaped
		  // PSEUDOCODE: Create guid var called lGUIDDetail
          //FDUpdateSQL_address3Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToString; // lGUIDDetail.ToByteArray;
          FDUpdateSQL_address3Detail.Apply( aRequest, aAction, aOptions);

          if aAction = eaApplied then begin
            FDUpdateSQL_address4Detail.ConnectionName := FDQuery.ConnectionName;
            FDUpdateSQL_address4Detail.DataSet := FDQuery;
			// PSEUDOCODE: use existing lGUIDDetail var
            //FDUpdateSQL_address4Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToString; // lGUIDDetail.ToByteArray;
            FDUpdateSQL_address4Detail.Apply( aRequest, aAction, aOptions);

            if aAction = eaApplied then begin
              FDUpdateSQL_address5DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
              FDUpdateSQL_address5DetailHierarchy.DataSet := FDQuery;
			  // PSEUDOCODE: Create guid var called lGUIDDetailHierarchy
			  //FDUpdateSQL_address5DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToString; // lGUIDDetailHierarchy.ToByteArray;
              FDUpdateSQL_address5DetailHierarchy.Apply( aRequest, aAction, aOptions );

              if aAction = eaApplied then begin
                FDUpdateSQL_address6DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                FDUpdateSQL_address6DetailHierarchy.DataSet := FDQuery;
				// PSEUDOCODE: use existing lGUIDDetailHierarchy var
                //FDUpdateSQL_address6DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToString; // lGUIDDetailHierarchy.ToByteArray;
                FDUpdateSQL_address6DetailHierarchy.Apply( aRequest, aAction, aOptions );
				
				if aAction = eaApplied then begin
                  FDUpdateSQL_address7DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                  FDUpdateSQL_address7DetailHierarchy.DataSet := FDQuery;
				  FDUpdateSQL_address7DetailHierarchy.Apply( aRequest, aAction, aOptions );
				  
				  // Commit only if all previous queries ran successfully
				  if aAction = eaApplied then begin
                    Connection.Commit;
				  end;
				end;
              end;
            end;
          end;
        end;
      end;
    end;
	// Can this line be entirely removed? If it is successful, then aAction will already be set to eaApplied
    aAction := eaApplied;
  except
    on E: Exception do begin
      CodeSite.SendException( E );
      Connection.Rollback;
      raise E;
    end;
  end;

