object TEST6_Address_DataAccess_Connection: TTEST6_Address_DataAccess_Connection
  OldCreateOrder = True
  Height = 265
  Width = 543
  object Connection: TFDConnection
    Params.Strings = (
      'ConnectionDef=TEST6')
    Connected = True
    BeforeCommit = ConnectionBeforeCommit
    AfterCommit = ConnectionAfterCommit
    Left = 40
    Top = 32
  end
  object FDQuery: TFDQuery
    BeforeEdit = FDQueryBeforeEdit
    AfterPost = FDQueryAfterPost
    OnUpdateRecord = FDQueryUpdateRecord
    Connection = Connection
    UpdateObject = FDUpdateSQL_Address1Master
    SQL.Strings = (

        '/* -------------------------------------------------------------' +
        '-'
      'Requires TEST6 database.'
      ''
      'UpdateSQL statements can be found after the query SQL'
      
        'NOTE: 20 TFDUpdateSQL instances are required to fully support th' +
        'is query'
      ''
      'PURPOSE:'
      #9'All Addresses'
      #9'Returns all postal addresses'
      #9
      'NOTES:'
      '    Outer query fetches all ADDRESS records marked as'
      #9'US_POSTAL_ADDRESS, and each inner query fetches the detail'
      #9'address that makes up one part of the postal address, e.g.'
      #9'Street1, Street2, City, etc.'
      #9
      'PERFORMANCE:'
      #9'- 0.1 seconds in FlameRobin'
      #9
      'CREATED: 2017-05-16'
      ''
      '*/'
      ''
      #9'SELECT'
      #9#9'ah.ID AS ID,'
      #9#9'a2.CONTENT,'
      #9#9't2.NAME AS ADDRESS_TYPE,'
      #9#9'ah.POSITION_INDEX,'
      #9#9'a.ID AS PARENT_ID,'
      #9#9'a2.ID AS CHILD_ID'
      #9'FROM ADDRESS a'
      #9'JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID'
      #9'JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID '
      #9'JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID'
      #9'JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID'
      #9'WHERE t.NAME = '#39'US_POSTAL_ADDRESS'#39
      #9'ORDER BY a.ID, ah.POSITION_INDEX'
      ''
      '')
    Left = 128
    Top = 32
  end
  object FDUpdateSQL_Address1Master: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'SELECT :NEW_PARENT_ID, '#39'ADDRESS'#39
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
      'WHERE t.NAME = '#39'US_POSTAL_ADDRESS'#39' AND a.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_PARENT_ID'

        'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARE' +
        'NT_ADDRESS_ID = eb.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    Left = 96
    Top = 120
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Tracing = True
    Left = 232
    Top = 32
  end
  object FDUpdateSQL_Address2Master: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID) '
      'SELECT :NEW_PARENT_ID, t.ID'
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
      'WHERE t.NAME = '#39'US_POSTAL_ADDRESS'#39' AND a.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS a WHERE a.ID = :OLD_PARENT_ID'
      
        'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARE' +
        'NT_ADDRESS_ID = a.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    Left = 96
    Top = 184
  end
  object FDUpdateSQL_Address3Detail: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      
        'SELECT CHAR_TO_UUID('#39'C751CD8B-AD03-4736-B697-B07C58394412'#39'), '#39'AD' +
        'DRESS'#39
      'FROM ADDRESS_TYPE t'
      
        'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = ' +
        ':NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      
        'SELECT CHAR_TO_UUID('#39'41C8783F-7702-4D97-B32A-C610D31CF114'#39'), '#39'AD' +
        'DRESS'#39
      'FROM ADDRESS_TYPE t'

        'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = ' +
        ':NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    Left = 264
    Top = 120
  end
  object FDUpdateSQL_Address4Detail: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT)'
      
        'SELECT CHAR_TO_UUID('#39'C751CD8B-AD03-4736-B697-B07C58394412'#39'), t.I' +
        'D, :NEW_CONTENT'
      'FROM ADDRESS_TYPE t'

        'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = ' +
        ':NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT)'
      
        'SELECT CHAR_TO_UUID('#39'41C8783F-7702-4D97-B32A-C610D31CF114'#39'), t.I' +
        'D, :NEW_CONTENT'
      'FROM ADDRESS_TYPE t'
      
        'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = ' +
        ':NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL'
      '')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS a WHERE a.ID = :NEW_CHILD_ID'
      
        'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.CHIL' +
        'D_ADDRESS_ID = a.ID AND ah.PARENT_ADDRESS_ID <> :OLD_PARENT_ID)')
    FetchRowSQL.Strings = (
      'SELECT'
      #9'a2.ID,'
      #9'a2.CONTENT,'
      #9't2.NAME AS ADDRESS_TYPE,'
      #9'ah.POSITION_INDEX,'
      #9'a.ID AS PARENT_ID'
      'FROM ADDRESS a'
      'JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID'
      'JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID '
      'JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID'
      'JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID'
      'WHERE t.NAME = '#39'US_POSTAL_ADDRESS'#39
      'AND a2.ID = :NEW_CHILD_ID')
    Left = 264
    Top = 184
  end
  object FDUpdateSQL_Address5DetailHierarchy: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      
        'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (CHAR_TO_UUID('#39'F' +
        'C73568A-5377-444C-B0F5-90A48D49027E'#39'), '#39'ADDRESS_HIERARCHY'#39')')
    ModifySQL.Strings = (
      
        'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (CHAR_TO_UUID('#39'9' +
        'DDD02F8-6EB4-4110-9D85-449C38101403'#39'), '#39'ADDRESS_HIERARCHY'#39')')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID')
    Left = 416
    Top = 120
  end
  object FDUpdateSQL_Address6DetailHierarchy: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      
        'INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDR' +
        'ESS_ID, POSITION_INDEX)'
      
        'SELECT CHAR_TO_UUID('#39'FC73568A-5377-444C-B0F5-90A48D49027E'#39'), :NE' +
        'W_PARENT_ID, a.ID, :NEW_POSITION_INDEX'
      'FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID '
      
        'LEFT JOIN ADDRESS_HIERARCHY ah ON ah.CHILD_ADDRESS_ID = a.ID AND' +
        ' ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID'
      
        'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.CONTENT = :NEW_CONTENT AN' +
        'D ah.ID IS NULL')
    ModifySQL.Strings = (
      
        'INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDR' +
        'ESS_ID, POSITION_INDEX)'
      
        'SELECT CHAR_TO_UUID('#39'9DDD02F8-6EB4-4110-9D85-449C38101403'#39'), :NE' +
        'W_PARENT_ID, a.ID, :NEW_POSITION_INDEX'
      'FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID '
      
        'LEFT JOIN ADDRESS_HIERARCHY ah ON ah.CHILD_ADDRESS_ID = a.ID AND' +
        ' ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID'
      
        'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.CONTENT = :NEW_CONTENT AN' +
        'D ah.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS_HIERARCHY ah WHERE ah.ID = :OLD_ID')
    Left = 416
    Top = 184
  end
end