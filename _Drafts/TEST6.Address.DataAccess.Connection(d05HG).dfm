object TEST6_Address_DataAccess_Connection: TTEST6_Address_DataAccess_Connection
  OldCreateOrder = True
  Height = 265
  Width = 738
  object Connection: TFDConnection
    Params.Strings = (
      'ConnectionDef=TEST6')
    Connected = True
    BeforeCommit = ConnectionBeforeCommit
    AfterCommit = ConnectionAfterCommit
    Left = 40
    Top = 32
  end
  object FDQuery: TFDQuery
    BeforeEdit = FDQueryBeforeEdit
    AfterPost = FDQueryAfterPost
    OnUpdateRecord = FDQueryUpdateRecord
    Connection = Connection
    UpdateObject = FDUpdateSQL_Address1Master
    SQL.Strings = (

        '/* -------------------------------------------------------------' +
        '-'
      'Requires TEST6 database.'
      ''
      'UpdateSQL statements can be found after the query SQL'
      
        'NOTE: 20 TFDUpdateSQL instances are required to fully support th' +
        'is query'
      ''
      'PURPOSE:'
        'All Addresses'
        'Returns all postal addresses'
      
      'NOTES:'
      '    Outer query fetches all ADDRESS records marked as'
        'US_POSTAL_ADDRESS, and each inner query fetches the detail'
        'address that makes up one part of the postal address, e.g.'
        'Street1, Street2, City, etc.'
      
      'PERFORMANCE:'
        '- 0.1 seconds in FlameRobin'
      
      'CREATED: 2017-05-16'
      ''
      '*/'
      ''
        'SELECT'
          'ah.ID AS ID,'
          'a2.CONTENT,'
          't2.NAME AS ADDRESS_TYPE,'
          'ah.POSITION_INDEX,'
          'a.ID AS PARENT_ID,'
          'a2.ID AS CHILD_ID'
        'FROM ADDRESS a'
        'JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID'
        'JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID '
		'JOIN ENTITY_BASE eb ON eb.ID = ah.ID '
        'JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID'
        'JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID'
        'WHERE t.NAME = ''US_POSTAL_ADDRESS'''
		'AND eb.IS_ACTIVE = ''1'''
        'ORDER BY a.ID, ah.POSITION_INDEX')
    Left = 128
    Top = 32
  end
  object FDUpdateSQL_Address1Master: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'SELECT :NEW_PARENT_ID, ''ADDRESS'''
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
      'WHERE t.NAME = ''US_POSTAL_ADDRESS'' AND a.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_PARENT_ID'
      'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARE' +
      'NT_ADDRESS_ID = eb.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    Left = 96
    Top = 120
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Tracing = True
    Left = 232
    Top = 32
  end
  object FDUpdateSQL_Address2Master: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID) '
      'SELECT :NEW_PARENT_ID, t.ID'
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ID = :NEW_PARENT_ID'
      'WHERE t.NAME = ''US_POSTAL_ADDRESS'' AND a.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS a WHERE a.ID = :OLD_PARENT_ID'
      'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.PARE' +
      'NT_ADDRESS_ID = a.ID AND ah.CHILD_ADDRESS_ID <> :OLD_CHILD_ID)')
    Left = 96
    Top = 184
  end
  object FDUpdateSQL_Address3Detail: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'SELECT :CHILD_ID, ''ADDRESS'''
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = :NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'SELECT :CHILD_ID, ''ADDRESS'''
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = :NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    Left = 264
    Top = 120
  end
  object FDUpdateSQL_Address4Detail: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT)'
      'SELECT :CHILD_ID, '
      '  t.ID, :NEW_CONTENT'
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID '
      '  AND a.CONTENT = :NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT)'
      'SELECT :CHILD_ID, t.ID, :NEW_CONTENT'
      'FROM ADDRESS_TYPE t'
      'LEFT JOIN ADDRESS a ON a.ADDRESS_TYPE_ID = t.ID AND a.CONTENT = ' +
      ':NEW_CONTENT'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE AND a.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS a WHERE a.ID = :NEW_CHILD_ID'
      'AND NOT EXISTS (SELECT 1 FROM ADDRESS_HIERARCHY ah WHERE ah.CHIL' +
      'D_ADDRESS_ID = a.ID AND ah.PARENT_ADDRESS_ID <> :OLD_PARENT_ID)')
    FetchRowSQL.Strings = (
      'SELECT'
      'a2.ID,'
      'a2.CONTENT,'
      't2.NAME AS ADDRESS_TYPE,'
      'ah.POSITION_INDEX,'
      'a.ID AS PARENT_ID'
      'FROM ADDRESS a'
      'JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID'
      'JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID '
	  'JOIN ENTITY_BASE eb ON eb.ID = ah.ID '
      'JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID'
      'JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID'
      'WHERE t.NAME = ''US_POSTAL_ADDRESS'''
	  'AND eb.IS_ACTIVE = ''1'''
      'AND a2.ID = :NEW_CHILD_ID')
    Left = 264
    Top = 184
  end
  object FDUpdateSQL_Address5DetailHierarchy: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) '
	  'VALUES (:HIERARCHY_ID, '
	  ' ''ADDRESS_HIERARCHY'')')
    ModifySQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME) '
      'VALUES (:HIERARCHY_ID, '
	  ' ''ADDRESS_HIERARCHY'')')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID')
    Left = 416
    Top = 120
  end
  object FDUpdateSQL_Address6DetailHierarchy: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, '
      'CHILD_ADDRESS_ID, POSITION_INDEX)'
      'SELECT :HIERARCHY_ID, '
      ':NEW_PARENT_ID, a.ID, :NEW_POSITION_INDEX'
      'FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID '
      'LEFT JOIN ADDRESS_HIERARCHY ah ON ah.CHILD_ADDRESS_ID = a.ID '
      '  AND ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE '
      'AND a.CONTENT = :NEW_CONTENT AND ah.ID IS NULL')
    ModifySQL.Strings = (
      'INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, '
      'CHILD_ADDRESS_ID, POSITION_INDEX)'
      'SELECT :HIERARCHY_ID, '
      ':NEW_PARENT_ID, a.ID, :NEW_POSITION_INDEX'
      'FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID '
      'LEFT JOIN ADDRESS_HIERARCHY ah ON ah.CHILD_ADDRESS_ID = a.ID '
      '  AND ah.PARENT_ADDRESS_ID = :NEW_PARENT_ID'
      'WHERE t.NAME = :NEW_ADDRESS_TYPE '
      'AND a.CONTENT = :NEW_CONTENT AND ah.ID IS NULL')
    DeleteSQL.Strings = (
      'DELETE FROM ADDRESS_HIERARCHY ah WHERE ah.ID = :OLD_ID')
    Left = 416
    Top = 184
  end
  object FDUpdateSQL_Address7DetailHierarchy: TFDUpdateSQL
    Connection = Connection
    ModifySQL.Strings = (
	  'UPDATE ENTITY_BASE eb SET eb.IS_ACTIVE = ''0'''
	  'WHERE eb.ID = :OLD_ID')
    Left = 516
    Top = 184
  end
end