unit Aurelius.Sql.Firebird;
// -----------------------------------------------------------------------------
// PURPOSE
//
//  Modified TMS original source file found under $(ThirdParty) component dir
//  in \TMS\Business\Aurelius\source\core\ to support use of GUID and BOOLEAN
//  domains in Firebird 2.5. The ** starred ** modification herein fix Aurelius
//  GUID conversion that was previously failing because Aurelius.SQL.Firebird
//  lacked "domain-based" support for GUID and BOOLEAN in Firebird.
//
// -----------------------------------------------------------------------------
// TODO (Last updated 2017-05-26)
//
// x1. Add GUID and BOOLEAN domains within Firebird database per
//     "How can I force FireDAC to recognize some field as GUID?" and
//     "How can I force FireDAC to recognize some field as Boolean?"
//     - http://docwiki.embarcadero.com/RADStudio/en/Firebird_and_Interbase_Servers_Questions_(FireDAC)
//
// x2. Next, add GUID and BOOLEAN domains as supported datatypes witin Aurelius'
//     Firbird SQL generator (hereunder), sprecifically method DefineColumnType
//     and override GetSupportedFieldTypes to include ftGuid and ftBoolean.
//     Again, for the modified Aurelius.SQL.Firebird to work through FireDAC
//     reading/writing database records having GUID|UUID keys or boolean values,
//     then use:
//
//     In Firebird:
//       - "Char(16)" character set octets" for column containing UUID
//       - "Char(1)" for column containing Boolean
//
//     In MySQL:
//        - "Binary(16)" column containing UUID
//
// -----------------------------------------------------------------------------
{$I Aurelius.inc}

interface

uses
  Aurelius.Sql.AnsiSqlGenerator,
  Aurelius.Sql.BaseTypes,
  Aurelius.Sql.Commands,
  Aurelius.Sql.Interfaces,
  Aurelius.Sql.Metadata,
  Aurelius.Sql.Register;

type
  TFirebirdSQLGenerator = class(TAnsiSQLGenerator)
  private
    FWideStringCharSet: string;
    FUseBoolean: Boolean; // JamesR: Added 2017-05-26 for FB Boolean support
  // `--- per
  protected
    function GetMaxConstraintNameLength: Integer; override;
    procedure DefineColumnType(Column: TColumnMetadata); override;

    function GetGeneratorName: string; override;
    function GetSqlDialect: string; override;

    function GenerateInsert(Command: TInsertCommand): string; override;
    function GenerateGetNextSequenceValue(Command: TGetNextSequenceValueCommand): string; override;
    function GenerateCreateSequence(Sequence: TSequenceMetadata): string; override;
    function GenerateDropSequence(Sequence: TSequenceMetadata): string; override;
    function GenerateLimitedSelect(SelectSql: TSelectSql; Command: TSelectCommand): string; override;

    function GetSupportedFeatures: TDBFeatures; override;
    function GetSupportedFieldTypes: TFieldTypeSet; override;
  public
    constructor Create;
    property UseBoolean: Boolean read FUseBoolean write FUseBoolean;
    procedure AfterConstruction; override;
    property WideStringCharSet: string read FWideStringCharSet write FWideStringCharSet;
  end;

implementation

uses
  DB, SysUtils,
  Aurelius.Sql.Functions;

{ TFirebirdSQLGenerator }

procedure TFirebirdSQLGenerator.AfterConstruction;
begin
  inherited;
  FWideStringCharSet := 'ISO8859_1';
end;

constructor TFirebirdSQLGenerator.Create;
begin
  inherited Create;

//  RegisterFunction('length', TSimpleSQLFunction.Create('character_length', TypeInfo(integer)));
//  RegisterFunction('position', TSimpleSQLFunction.Create('position', TypeInfo(integer)));
end;

procedure TFirebirdSQLGenerator.DefineColumnType(Column: TColumnMetadata);
begin
  DefineNumericColumnType(Column);
  if Column.DataType <> '' then
    Exit;

  case Column.FieldType of
    ftLargeInt:
      begin
        Column.DataType := 'NUMERIC($pre)';
        Column.Precision := 18;
      end;

    ftWideString:
      Column.DataType := Format('VARCHAR($len) CHARACTER SET %s', [WideStringCharSet]);

    ftFixedWideChar:
      Column.DataType := Format('CHAR($len) CHARACTER SET %s', [WideStringCharSet]);

    ftCurrency:
      begin
        Column.DataType := 'NUMERIC($pre, $sca)';
        Column.Precision := 18;
        Column.Scale := 4;
      end;
//    ftFMTBcd:
//      Column.DataType := 'NUMERIC(18, 9)';

// *****************************************************************************
// Added 2017-05-26 to support use of domain definition in Firebird database
// *****************************************************************************
    ftGuid:
      Column.DataType := 'GUID'; // 'GUID' is a Firebird domain created in database
    ftBoolean:
      Column.DataType := 'BOOLEAN'; // 'GUID' is a Firebird domain created in database
// *****************************************************************************
    ftMemo:
      Column.DataType := 'BLOB SUB_TYPE TEXT';
    ftWideMemo:
      Column.DataType := 'BLOB SUB_TYPE TEXT';
    ftBlob:
      Column.DataType := 'BLOB';
  else
    inherited DefineColumnType(Column);
  end;
end;

function TFirebirdSQLGenerator.GenerateCreateSequence(Sequence: TSequenceMetadata): string;
begin
  Result := 'CREATE GENERATOR ' + IdName(Sequence.Name);
end;

function TFirebirdSQLGenerator.GenerateDropSequence(Sequence: TSequenceMetadata): string;
begin
  Result := 'DROP GENERATOR ' + IdName(Sequence.Name);
end;

function TFirebirdSQLGenerator.GenerateGetNextSequenceValue(
  Command: TGetNextSequenceValueCommand): string;
begin
  Result := 'SELECT GEN_ID(' + IdName(Command.SequenceName) + ', ' +
    IntToStr(Command.Increment) + ') FROM RDB$DATABASE';
end;

function TFirebirdSQLGenerator.GenerateInsert(Command: TInsertCommand): string;
begin
  Result := inherited GenerateInsert(Command);
  if Command.OutputColumn <> '' then
    Result := Result + ' RETURNING ' + Command.OutputColumn;
end;

function TFirebirdSQLGenerator.GenerateLimitedSelect(SelectSql: TSelectSql; Command: TSelectCommand): string;
begin
  Result := GenerateRegularSelect(SelectSql) + sqlLineBreak;
  if Command.HasFirstRow then
    Result := Result + Format('ROWS %d To %d', [Command.FirstRow + 1, Command.LastRow + 1])
  else
    Result := Result + Format('ROWS %d', [Command.MaxRows]);
end;

function TFirebirdSQLGenerator.GetSqlDialect: string;
begin
  Result := 'Firebird';
end;

function TFirebirdSQLGenerator.GetGeneratorName: string;
begin
  Result := 'Firebird SQL Generator';
end;

function TFirebirdSQLGenerator.GetMaxConstraintNameLength: Integer;
begin
  Result := 31;
end;

function TFirebirdSQLGenerator.GetSupportedFeatures: TDBFeatures;
begin
  Result := AllDBFeatures - [TDBFeature.AutoGenerated, TDBFeature.RetrieveIdOnInsert,
    TDBFeature.Schemas];
end;

// *****************************************************************************
// Added 2017-05-26 to support use of domain definition in Firebird database
// *****************************************************************************
function TFirebirdSQLGenerator.GetSupportedFieldTypes: TFieldTypeSet;
begin
  Result := inherited GetSupportedFieldTypes + [ftGuid];
  if UseBoolean then
    Result := Result + [ftBoolean];
end;
// *****************************************************************************


initialization
  TSQLGeneratorRegister.GetInstance.RegisterGenerator(TFirebirdSQLGenerator.Create);

end.
