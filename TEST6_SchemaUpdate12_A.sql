/* ***************************************************

The following SQL script is to perform schema changes
via DDL statements to modify tables and columns to
prepare the database for data import from the
M2_ACTIVITIES and M2_LEADS tables.

Schema change summary:
- Modify ACTIVITY tables and content within for a more
normalized organization (in particular of ACTIVITY_TYPE)
- Prep COMPONENT tables in the same hierarchical 
fashion as the ACTIVITY tables

Prerequisites:
- Run the EntityMethod ACTIVITY.sql from the 
HierarchyTestSuite ___ODMS repo branch

2018-05-18 Runtime: ~? min

Written for the TEST6 database.

-------- Change Log ---------
- Created 2017-MM_DD HeatherG
- Updated 2018-05-18 HeatherG
    Move changes to ADDRESS table to another schema update file, as we are not ready for it yet
    Add minor changes to address dependencies added since creation of this file (e.g. new stored procedures)

*************************************************** */

DROP PROCEDURE PROC_FAMILY_INVITE_LIST_GUID;
DROP PROCEDURE PROC_FAMILY_INVITE_LIST_STRING;

ALTER TABLE ACTIVITY ADD IS_COMPLETE BOOLEAN;
ALTER TABLE ACTIVITY ALTER NAME TYPE Varchar(255);
ALTER TABLE ACTIVITY ALTER COLUMN NAME TO TITLE;
ALTER TABLE ACTIVITY DROP PARTY_ROLE_TYPE_ID;

/* Drop constraints */
ALTER TABLE COMPONENT_QUERY DROP CONSTRAINT SUBTYPE_COMPONENT_QUERY;

/* Create new tables */
CREATE TABLE COMPONENT_HIERARCHY (
  ID GUID NOT NULL,
  PARENT_COMPONENT_ID GUID NOT NULL,
  CHILD_COMPONENT_ID GUID NOT NULL,
  POS INTEGER,
  IS_TEMPLATE BOOLEAN NOT NULL,
  CONSTRAINT PK_COMPONENT_HIERARCHY PRIMARY KEY (ID)
);

/* Alter existing tables */
ALTER TABLE ACTIVITY_TYPE ALTER TYPE_SIZE TO DATA_SIZE;

ALTER TABLE ACTIVITY_HIERARCHY ADD IS_TEMPLATE BOOLEAN DEFAULT '0' NOT NULL;

ALTER TABLE COMPONENT_TYPE ADD DATA_TYPE
  VARCHAR(35);

ALTER TABLE COMPONENT_TYPE ADD DATA_SIZE
  INTEGER;

ALTER TABLE COMPONENT_TYPE ADD FORMAT_MASK
  VARCHAR(255);

ALTER TABLE COMPONENT ADD CONTENT_NUMERIC
  DECIMAL(10,8);

ALTER TABLE COMPONENT ADD CONTENT_DATETIME
  TIMESTAMP;

ALTER TABLE COMPONENT ADD CONTENT_STRING
  VARCHAR(255);

ALTER TABLE COMPONENT ADD CONTENT_BOOLEAN
  BOOLEAN;

ALTER TABLE COMPONENT_TYPE ADD VALIDATION
  VARCHAR(255);

ALTER TABLE COMPONENT ADD CONTENT_GUID
  GUID;

ALTER TABLE COMPONENT ADD CONTENT_BLOB
  BLOB SUB_TYPE TEXT SEGMENT SIZE 80;

ALTER TABLE ACTIVITY_TYPE ADD DATA_TYPE
  VARCHAR(35);

ALTER TABLE ACTIVITY ADD CONTENT_BOOLEAN
  BOOLEAN;

ALTER TABLE ACTIVITY ADD CONTENT_GUID
  GUID;


/* Add new constraints */
ALTER TABLE COMPONENT_HIERARCHY ADD CONSTRAINT UNIQUE_COMPONENT_HIERARCHY UNIQUE (PARENT_COMPONENT_ID,CHILD_COMPONENT_ID);

ALTER TABLE ACTIVITY ADD CONSTRAINT FK_ACTIVITY_ENTITY_BASE
  FOREIGN KEY (CONTENT_GUID)
  REFERENCES ENTITY_BASE (ID);

ALTER TABLE COMPONENT_HIERARCHY ADD CONSTRAINT FK_COMPONENT_HIERARCHY_CHILD
  FOREIGN KEY (CHILD_COMPONENT_ID)
  REFERENCES COMPONENT (ID);

ALTER TABLE COMPONENT ADD CONSTRAINT FK_COMPONENT_ENTITY_BASE
  FOREIGN KEY (CONTENT_GUID)
  REFERENCES ENTITY_BASE (ID);

ALTER TABLE COMPONENT_HIERARCHY ADD CONSTRAINT FK_COMPONENT_HIERARCHY_PARENT
  FOREIGN KEY (PARENT_COMPONENT_ID)
  REFERENCES COMPONENT (ID);

ALTER TABLE COMPONENT_HIERARCHY ADD CONSTRAINT FK_COMPONENT_HIERARCHY
  FOREIGN KEY (ID)
  REFERENCES ENTITY_BASE (ID);

ALTER TABLE ACTIVITY ALTER ID POSITION 1;
ALTER TABLE ACTIVITY ALTER ACTIVITY_TYPE_ID POSITION 2;
ALTER TABLE ACTIVITY ALTER CONTENT_STRING POSITION 3;
ALTER TABLE ACTIVITY ALTER CONTENT_DATETIME POSITION 4;
ALTER TABLE ACTIVITY ALTER CONTENT_NUMERIC POSITION 5;
ALTER TABLE ACTIVITY ALTER CONTENT_BOOLEAN POSITION 6;
ALTER TABLE ACTIVITY ALTER CONTENT_BLOB POSITION 7;
ALTER TABLE ACTIVITY ALTER CONTENT_GUID POSITION 8;

ALTER TABLE COMPONENT ALTER ID POSITION 1;
ALTER TABLE COMPONENT ALTER COMPONENT_TYPE_ID POSITION 2;
ALTER TABLE COMPONENT ALTER CONTENT_STRING POSITION 3;
ALTER TABLE COMPONENT ALTER CONTENT_DATETIME POSITION 4;
ALTER TABLE COMPONENT ALTER CONTENT_NUMERIC POSITION 5;
ALTER TABLE COMPONENT ALTER CONTENT_BOOLEAN POSITION 6;
ALTER TABLE COMPONENT ALTER CONTENT_BLOB POSITION 7;
ALTER TABLE COMPONENT ALTER CONTENT_GUID POSITION 8;
ALTER TABLE COMPONENT ALTER VERSION_MAJOR POSITION 9;
ALTER TABLE COMPONENT ALTER VERSION_MINOR POSITION 10;
ALTER TABLE COMPONENT ALTER VERSION_BUILD POSITION 11;
ALTER TABLE COMPONENT ALTER NOTE POSITION 12;
ALTER TABLE COMPONENT ALTER DESCRIPTION POSITION 13;
ALTER TABLE COMPONENT ALTER ALIAS POSITION 14;
ALTER TABLE COMPONENT ALTER NAME POSITION 15;



/* ETL data before dropping tables/colums */
	/* Update DATA_TYPE values */
	UPDATE ACTIVITY_TYPE t SET t.DATA_TYPE = 'STRING' WHERE t.ATTRIBUTE <> 0;
	UPDATE ACTIVITY_TYPE t SET t.DATA_TYPE = 'DATETIME' WHERE t.NAME IN ('START_AT','END_AT','DUE_DATE');
	UPDATE ACTIVITY_TYPE t SET t.DATA_TYPE = 'NUMERIC' WHERE t.NAME IN ('GRADE_RANGE_START','GRADE_RANGE_END','MAXIMUM_SCORE','MINIMUM_AGE','POS','AGGREGATE_COUNT');
	UPDATE ACTIVITY_TYPE t SET t.DATA_TYPE = 'BOOLEAN' WHERE t.NAME IN ('IS_RECURRING','IS_CLOSURE','IS_COMPLETE','CHANGE_IN_CONTACT_STATUS');
	UPDATE ACTIVITY_TYPE t SET t.NAME = 'IS_SPECIAL_REQUEST', t.DATA_TYPE = 'BOOLEAN' WHERE t.NAME = 'SPECIAL_REQUEST';
	
	/* Consolidate repeated ACTIVITY_TYPE records */
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE id GUID;
    DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
	DECLARE VARIABLE kind Integer;
	DECLARE VARIABLE attribute Integer;
	DECLARE VARIABLE pos Integer;
    DECLARE VARIABLE start_at Timestamp;
    DECLARE VARIABLE end_at Timestamp;
    DECLARE VARIABLE title Varchar(255);
    DECLARE VARIABLE is_complete BOOLEAN;
    DECLARE VARIABLE note BLOB;
	BEGIN
		UPDATE ACTIVITY_TYPE t SET t.ATTRIBUTE = -1, t.KIND = -1 WHERE t.ATTRIBUTE = 0;
		
		/* get unique list of attributes */
		FOR
		SELECT MIN(t.KIND), t.ATTRIBUTE
		FROM ACTIVITY_TYPE t
		WHERE t.ATTRIBUTE <> -1
		GROUP BY t.ATTRIBUTE
		INTO :kind, :attribute
		DO BEGIN
			SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.KIND = :kind AND t.ATTRIBUTE = :attribute
			INTO :child_id;
			
			UPDATE ACTIVITY_TYPE t SET t.KIND = -1 WHERE t.ID = :child_id;
		END
		
		FOR
		SELECT t.ID, t2.ID
		FROM ACTIVITY_TYPE t 
		JOIN ACTIVITY_TYPE t2 ON t2.ATTRIBUTE = t.ATTRIBUTE
			AND t2.KIND = -1
		WHERE t.KIND <> -1
		INTO :id, :child_id
		DO BEGIN
			UPDATE ACTIVITY a SET a.ACTIVITY_TYPE_ID = :child_id WHERE a.ACTIVITY_TYPE_ID = :id;
			
			DELETE FROM ACTIVITY_TYPE t WHERE t.ID = :id;
			DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
		END
        
        /* Move attributes values replaced by columns into column values*/
        FOR
        SELECT a.ID, a.CONTENT_BOOLEAN
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = 'IS_COMPLETE'
        AND a.CONTENT_BOOLEAN IS NOT NULL
        INTO :child_id, :is_complete
        DO BEGIN
            FOR
            SELECT ah.ID, a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
            WHERE ah.CHILD_ACTIVITY_ID = :child_id
            INTO :id, :parent_id
            DO BEGIN
                UPDATE ACTIVITY a SET a.IS_COMPLETE = :is_complete WHERE a.ID = :parent_id;
                
                DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
                DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            END
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
        FOR
        SELECT a.ID, a.CONTENT_DATETIME
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = 'START_AT'
        AND a.CONTENT_DATETIME IS NOT NULL
        INTO :child_id, :start_at
        DO BEGIN
            FOR
            SELECT ah.ID, a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
            WHERE ah.CHILD_ACTIVITY_ID = :child_id
            INTO :id, :parent_id
            DO BEGIN
                UPDATE ACTIVITY a SET a.START_AT = :start_at WHERE a.ID = :parent_id;
                
                DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
                DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            END
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
        FOR
        SELECT a.ID, a.CONTENT_DATETIME
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = 'END_AT'
        AND a.CONTENT_DATETIME IS NOT NULL
        INTO :child_id, :start_at
        DO BEGIN
            FOR
            SELECT ah.ID, a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
            WHERE ah.CHILD_ACTIVITY_ID = :child_id
            INTO :id, :parent_id
            DO BEGIN
                UPDATE ACTIVITY a SET a.START_AT = :start_at WHERE a.ID = :parent_id;
                
                DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
                DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            END
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
        FOR
        SELECT a.ID, a.CONTENT_BLOB
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = 'NOTE'
        AND a.CONTENT_BLOB IS NOT NULL
        INTO :child_id, :note
        DO BEGIN
            FOR
            SELECT ah.ID, a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
            WHERE ah.CHILD_ACTIVITY_ID = :child_id
            INTO :id, :parent_id
            DO BEGIN
                UPDATE ACTIVITY a SET a.NOTE = :note WHERE a.ID = :parent_id;
                
                DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
                DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            END
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
        FOR
        SELECT a.ID, a.CONTENT_STRING
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = 'NAME'
        AND a.CONTENT_STRING IS NOT NULL
        INTO :child_id, :title
        DO BEGIN
            FOR
            SELECT ah.ID, a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
            WHERE ah.CHILD_ACTIVITY_ID = :child_id
            INTO :id, :parent_id
            DO BEGIN
                UPDATE ACTIVITY a SET a.TITLE = :note WHERE a.ID = :parent_id;
                
                DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
                DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            END
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
	END^
	SET TERM ; ^
    
	/* Setup hierarchy template for ACTIVITY_TYPE */
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
	DECLARE VARIABLE id GUID;
	DECLARE VARIABLE pos Integer;
	DECLARE VARIABLE unassigned_type_id GUID;
	DECLARE VARIABLE unassigned_id GUID;
	BEGIN
		/* Setup top-level type container */
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = 'UNASSIGNED'
		INTO :unassigned_type_id;
		
		unassigned_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:unassigned_id, 'ACTIVITY');
		INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID, CONTENT_GUID) VALUES (:unassigned_id, :unassigned_type_id, :unassigned_type_id);
		
		/* Add top-level types to container */
		pos = 0;
		FOR
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.ATTRIBUTE = -1
		INTO :type_id
		DO BEGIN
			child_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:child_id, 'ACTIVITY');
			INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID) VALUES (:child_id, :type_id);
			
			pos = pos + 1;
			id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ACTIVITY_HIERARCHY');
			INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, IS_TEMPLATE, POS)
			VALUES (:id, :unassigned_id, :child_id, 1, :pos);
		END
		
	END^
	SET TERM ; ^
	
	/* Create extra ACTIVITY types and attributes */
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE stmt_addContainerType BLOB;
	DECLARE VARIABLE stmt_addAttributeType BLOB;
	DECLARE VARIABLE stmt_addAttributeToActivityType BLOB;
	DECLARE VARIABLE result BOOLEAN;
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE parent_id GUID;
    DECLARE VARIABLE type_name Varchar(35);
	BEGIN
		SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'addContainerType'
		INTO :stmt_addContainerType;
		SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'addAttributeType'
		INTO :stmt_addAttributeType;
		SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'addAttributeToActivityType'
		INTO :stmt_addAttributeToActivityType;
		
		/* Create new top-level types */
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'MASS_MESSAGE',DESCRIPTION := 'Container: scheduled mass message, including mass email, postal mail, and call campaign', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'STAFF_MEETING', DESCRIPTION := 'Container: staff meeting', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'RECEIVE_PAYMENT', DESCRIPTION := 'Container: event where staff member receives physical payment from a customer, e.g. a cheque.', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'POSTAL_MAIL', DESCRIPTION := 'Container: postal mail, including letters, postcards, etc.', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'PUBLIC_EVENT', DESCRIPTION := 'Container: public event, e.g. town fair, expo, etc.', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'KEY_TRANSFER', DESCRIPTION := 'Container: center key transfer between staff members', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		EXECUTE STATEMENT (stmt_addContainerType) (NAME := 'TH_SESSION', DESCRIPTION := 'Container: private tutoring session', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := NULL, DATA_SIZE := NULL)
		INTO :parent_id;
		
		
		/* Create new attribute types */
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'TITLE', DESCRIPTION := 'Attribute: title, or name, of activity. To appear as the label value in the calendar, will likley be calculated for most records.', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := 'STRING', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'CALL_STATUS', DESCRIPTION := 'Attribute: call status', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := 'STRING', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'CONTENT', DESCRIPTION := 'Attribute: content, e.g. of an email', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := 'BLOB', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'RANGE_START_AT', DESCRIPTION := 'Attribute: beginning of datetime range', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := 'DATETIME', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'RANGE_END_AT', DESCRIPTION := 'Attribute: end of datetime range', FORMAT_MASK := NULL, VALIDATION := NULL, DATA_TYPE := 'DATETIME', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'RECIPIENT_LIST_ID', DESCRIPTION := 'Attribute: foreign key to the ADDRESS table for a recipient list', FORMAT_MASK := NULL, VALIDATION := 'ADDRESS', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'PAYMENT_ID', DESCRIPTION := 'Attribute: foriegn key to the PAYMENT table', FORMAT_MASK := NULL, VALIDATION := 'PAYMENT', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'CENTER_LOCATION_ID', DESCRIPTION := 'Attribute: foriegn key to the PARTY_ROLE_LEARNING_CENTER table', FORMAT_MASK := NULL, VALIDATION := 'PARTY_ROLE_LEARNING_CENTER', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'SOURCE_CENTER_LOCATION_ID', DESCRIPTION := 'Attribute: foriegn key to the PARTY_ROLE_LEARNING_CENTER table for the source center', FORMAT_MASK := NULL, VALIDATION := 'PARTY_ROLE_LEARNING_CENTER', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'TARGET_CENTER_LOCATION_ID', DESCRIPTION := 'Attribute: foriegn key to the PARTY_ROLE_LEARNING_CENTER table for the target center', FORMAT_MASK := NULL, VALIDATION := 'PARTY_ROLE_LEARNING_CENTER', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'SOURCE_STAFF_MEMBER_ID', DESCRIPTION := 'Attribute: foriegn key to the PARTY_ROLE_STAFF table for the source staff member', FORMAT_MASK := NULL, VALIDATION := 'PARTY_ROLE_STAFF', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'TARGET_STAFF_MEMBER_ID', DESCRIPTION := 'Attribute: foriegn key to the PARTY_ROLE_STAFF table for the target staff member', FORMAT_MASK := NULL, VALIDATION := 'PARTY_ROLE_STAFF', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		EXECUTE STATEMENT (stmt_addAttributeType) (NAME := 'SALES_ORDER_DETAIL_ID', DESCRIPTION := 'Attribute: foriegn key to the SALES_ORDER_DETAIL table', FORMAT_MASK := NULL, VALIDATION := 'SALES_ORDER_DETAIL', DATA_TYPE := 'GUID', DATA_SIZE := NULL)
		INTO :type_id;
		
        /* Make sure all activity types have the column sourced attributes START_AT, END_AT, IS_COMPLETE, TITLE, NOTE */
        FOR
        SELECT t.NAME FROM ACTIVITY_TYPE t WHERE t.DATA_TYPE IS NULL AND t.NAME <> 'UNASSIGNED'
        INTO :type_name
        DO BEGIN
            EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := type_name, ATTRIBUTE_TYPE_NAME := 'START_AT')
            INTO :result;
            EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := type_name, ATTRIBUTE_TYPE_NAME := 'END_AT')
            INTO :result;
            EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := type_name, ATTRIBUTE_TYPE_NAME := 'IS_COMPLETE')
            INTO :result;
            EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := type_name, ATTRIBUTE_TYPE_NAME := 'TITLE')
            INTO :result;
            EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := type_name, ATTRIBUTE_TYPE_NAME := 'NOTE')
            INTO :result;
        END
        
		/* Add attributes to top level types */
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'MASS_MESSAGE', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'MASS_MESSAGE', ATTRIBUTE_TYPE_NAME := 'RECIPIENT_LIST_ID')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'RECEIVE_PAYMENT', ATTRIBUTE_TYPE_NAME := 'PAYMENT_ID')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'POSTAL_MAIL', ATTRIBUTE_TYPE_NAME := 'DIRECTION')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'POSTAL_MAIL', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'KEY_TRANSFER', ATTRIBUTE_TYPE_NAME := 'CENTER_LOCATION_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'KEY_TRANSFER', ATTRIBUTE_TYPE_NAME := 'SOURCE_STAFF_MEMBER_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'KEY_TRANSFER', ATTRIBUTE_TYPE_NAME := 'TARGET_STAFF_MEMBER_ID')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'DIRECTION')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'POS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'PROGRAM_PRESENTED')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'INTEREST_LEVEL')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'OUTCOME')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'VISIT', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'DIRECTION')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'POS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'RANGE_START_AT')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'RANGE_END_AT')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'CALL_STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'PROGRAM_PRESENTED')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'INTEREST_LEVEL')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'OUTCOME')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'CHANGE_IN_CONTACT_STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CALL', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'ASSESSMENT', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'ASSESSMENT', ATTRIBUTE_TYPE_NAME := 'OUTCOME')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIAL_SESSION', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIAL_SESSION', ATTRIBUTE_TYPE_NAME := 'PROGRAM_PRESENTED')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIAL_SESSION', ATTRIBUTE_TYPE_NAME := 'INTEREST_LEVEL')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIAL_SESSION', ATTRIBUTE_TYPE_NAME := 'OUTCOME')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TH_SESSION', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TH_SESSION', ATTRIBUTE_TYPE_NAME := 'TOPIC')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TH_SESSION', ATTRIBUTE_TYPE_NAME := 'SALES_ORDER_DETAIL_ID')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'SESSION', ATTRIBUTE_TYPE_NAME := 'SALES_ORDER_DETAIL_ID')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'EMPLOYMENT_TEST', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIAL_INSTRUCTOR', ATTRIBUTE_TYPE_NAME := 'STATUS')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIMATHLON', ATTRIBUTE_TYPE_NAME := 'ALIAS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIMATHLON', ATTRIBUTE_TYPE_NAME := 'POS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIMATHLON', ATTRIBUTE_TYPE_NAME := 'GRADE_RANGE_START')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIMATHLON', ATTRIBUTE_TYPE_NAME := 'GRADE_RANGE_END')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TRIMATHLON', ATTRIBUTE_TYPE_NAME := 'MAXIMUM_SCORE')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'GAME_NIGHT', ATTRIBUTE_TYPE_NAME := 'ALIAS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'GAME_NIGHT', ATTRIBUTE_TYPE_NAME := 'MINIMUM_AGE')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'BINDER_TRANSFER', ATTRIBUTE_TYPE_NAME := 'SOURCE_CENTER_LOCATION_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'BINDER_TRANSFER', ATTRIBUTE_TYPE_NAME := 'TARGET_CENTER_LOCATION_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'BINDER_TRANSFER', ATTRIBUTE_TYPE_NAME := 'IS_RECURRING')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'SCHEDULE_CHANGE', ATTRIBUTE_TYPE_NAME := 'ALIAS')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'SCHEDULE_CHANGE', ATTRIBUTE_TYPE_NAME := 'IS_CLOSURE')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TEXT_MESSAGE', ATTRIBUTE_TYPE_NAME := 'DIRECTION')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'TEXT_MESSAGE', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'EMAIL', ATTRIBUTE_TYPE_NAME := 'SOURCE_ADDRESS_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'EMAIL', ATTRIBUTE_TYPE_NAME := 'TARGET_ADDRESS_ID')
		INTO :result;
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'EMAIL', ATTRIBUTE_TYPE_NAME := 'CONTENT')
		INTO :result;
		
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := 'CAMPAIGN', ATTRIBUTE_TYPE_NAME := 'ALIAS')
		INTO :result;
		
        
        /*
		EXECUTE STATEMENT (stmt_addAttributeToActivityType) (ACTIVITY_TYPE_NAME := '', ATTRIBUTE_TYPE_NAME := '')
		INTO :result;
		*/
	END^
	SET TERM ; ^
    
    /* Create ACTIVITY enumerations */
    SET TERM ^ ;
    EXECUTE BLOCK
    AS
    DECLARE VARIABLE stmt_createEnum BLOB;
    DECLARE VARIABLE stmt_addToEnum BLOB;
    DECLARE VARIABLE activity_id GUID;
    BEGIN
        SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'createEnum'
		INTO :stmt_createEnum;
        SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'addToEnum'
		INTO :stmt_addToEnum;
		
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'CALL_STATUS')
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'STATUS')
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'INTEREST_LEVEL')
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'DIRECTION')
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME')
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_createEnum) (ACTIVITY_TYPE_NAME := 'STUDENT_NEEDS')
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'CALL_STATUS', CONTENT_STRING := 'Scheduled', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'CALL_STATUS', CONTENT_STRING := 'Connected', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'CALL_STATUS', CONTENT_STRING := 'Voicemail', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'CALL_STATUS', CONTENT_STRING := 'No Voicemail', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STATUS', CONTENT_STRING := 'Scheduled', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STATUS', CONTENT_STRING := 'Confirmed', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STATUS', CONTENT_STRING := 'Voicemail', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STATUS', CONTENT_STRING := 'Canceled', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'INTEREST_LEVEL', CONTENT_STRING := 'Hot', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'INTEREST_LEVEL', CONTENT_STRING := 'Warm', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'INTEREST_LEVEL', CONTENT_STRING := 'Cold', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'DIRECTION', CONTENT_STRING := 'IN', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'DIRECTION', CONTENT_STRING := 'OUT', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME', CONTENT_STRING := 'Scheduled Visit', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME', CONTENT_STRING := 'Scheduled Assessment', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME', CONTENT_STRING := 'Scheduled Trial Session', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME', CONTENT_STRING := 'Scheduled Call', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'OUTCOME', CONTENT_STRING := 'No Follow Up Necessary', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STUDENT_NEEDS', CONTENT_STRING := 'Supplemental', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STUDENT_NEEDS', CONTENT_STRING := 'Struggling', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STUDENT_NEEDS', CONTENT_STRING := 'Homework Help', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := 'STUDENT_NEEDS', CONTENT_STRING := 'Test Prep', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        INTO :activity_id;
        
        --EXECUTE STATEMENT (stmt_addToEnum) (ACTIVITY_TYPE_NAME := '', CONTENT_STRING := NULL, CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
        --INTO :activity_id;
        
        
        
    END^
    SET TERM ; ^
	
    /* Create seasonal campaign records */
    SET TERM ^ ;
    EXECUTE BLOCK
    AS
    DECLARE VARIABLE stmt_createActivity BLOB;
    DECLARE VARIABLE stmt_updateAttribute BLOB;
    DECLARE VARIABLE campaign_year INTEGER;
    DECLARE VARIABLE activity_id GUID;
    DECLARE VARIABLE child_id GUID;
    BEGIN
        SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'createActivity'
        INTO :stmt_createActivity;
        SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'updateAttribute'
        INTO :stmt_updateAttribute;
        
        FOR
        SELECT 2000 + c.NUM
        FROM CALC_NUMBER c
        WHERE c.NUM >= 11 AND c.NUM <= 20
        INTO :campaign_year
        DO BEGIN
            /* Winter */
            EXECUTE STATEMENT (stmt_createActivity) (ID := NULL, ACTIVITY_TYPE_NAME := 'CAMPAIGN')
            INTO :activity_id;
            
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'TITLE', CONTENT_STRING := campaign_year || ' Winter Campaign', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'START_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('01.01.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'END_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('31.01.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            
            /* Summer */
            EXECUTE STATEMENT (stmt_createActivity) (ID := NULL, ACTIVITY_TYPE_NAME := 'CAMPAIGN')
            INTO :activity_id;
            
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'TITLE', CONTENT_STRING := campaign_year || ' Summer Campaign', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'START_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('01.04.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'END_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('31.08.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            
            /* Fall */
            EXECUTE STATEMENT (stmt_createActivity) (ID := NULL, ACTIVITY_TYPE_NAME := 'CAMPAIGN')
            INTO :activity_id;
            
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'TITLE', CONTENT_STRING := campaign_year || ' Fall Campaign', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'START_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('01.09.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'END_AT', CONTENT_STRING := NULL, CONTENT_DATETIME := CAST('30.11.' || campaign_year AS DATE), CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            
            /* TriMathlon */
            EXECUTE STATEMENT (stmt_createActivity) (ID := NULL, ACTIVITY_TYPE_NAME := 'CAMPAIGN')
            INTO :activity_id;
            
            EXECUTE STATEMENT (stmt_updateAttribute) (ACTIVITY_ID := activity_id, ATTRIBUTE_TYPE_NAME := 'TITLE', CONTENT_STRING := campaign_year || ' TriMathlon Campaign', CONTENT_DATETIME := NULL, CONTENT_NUMERIC := NULL, CONTENT_BOOLEAN := NULL, CONTENT_BLOB := NULL, CONTENT_GUID := NULL)
            INTO :child_id;
            
        END
    END^
    SET TERM ; ^
    
    



    
    
    



 
 
 
 
 
	/* Create COMPONENT_TYPE records 
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE component_id GUID;
	DECLARE VARIABLE id GUID;
	DECLARE VARIABLE unassigned_id GUID;
	DECLARE VARIABLE query_id GUID;
	DECLARE VARIABLE template_id GUID;
	BEGIN
		FOR SELECT c.ID FROM COMPONENT c
		INTO :id DO BEGIN
			DELETE FROM COMPONENT_QUERY q WHERE q.ID = :id;
			DELETE FROM COMPONENT c WHERE c.ID = :id;
			DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
		END
		
		FOR SELECT t.ID FROM COMPONENT_TYPE t WHERE t.DESCRIPTION IS NOT NULL
		INTO :id DO BEGIN
			DELETE FROM COMPONENT_TYPE t WHERE t.ID = :id;
			DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
		END
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'UNASSIGNED', NULL);
		unassigned_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:unassigned_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:unassigned_id, :type_id);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'TEMPLATE', NULL);
		template_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:template_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:template_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :unassigned_id, :template_id, '1', 1);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'QUERY', NULL);
		query_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:query_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:query_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :unassigned_id, :query_id, '1', 2);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'NAME', 'STRING');
		component_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:component_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:component_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :template_id, :component_id, '1', 1);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :query_id, :component_id, '1', 1);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'ALIAS', 'STRING');
		component_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:component_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:component_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :template_id, :component_id, '1', 2);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :query_id, :component_id, '1', 2);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'LANGUAGE', 'STRING');
		component_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:component_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:component_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :template_id, :component_id, '1', 3);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :query_id, :component_id, '1', 3);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'CONTENT', 'BLOB');
		component_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:component_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:component_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :template_id, :component_id, '1', 4);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :query_id, :component_id, '1', 4);
		
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'COMPONENT_TYPE');
		INSERT INTO COMPONENT_TYPE (ID, NAME, DATA_TYPE) VALUES (:type_id, 'ACTION_TYPE', 'STRING');
		component_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:component_id, 'COMPONENT');
		INSERT INTO COMPONENT (ID, COMPONENT_TYPE_ID) VALUES (:component_id, :type_id);
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'COMPONENT_HIERARCHY');
		INSERT INTO COMPONENT_HIERARCHY (ID, PARENT_COMPONENT_ID, CHILD_COMPONENT_ID, IS_TEMPLATE, POS) VALUES (:id, :query_id, :component_id, '1', 5);
	END^
	SET TERM ; ^
*/

	/* Update existing ACTIVITY records to contain the requisite (even if NULL) attributes they are missing 
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE activity_id GUID;
	DECLARE VARIABLE activity_type_id GUID;
	BEGIN
		FOR
		SELECT a.ID, t.ID
		FROM ACTIVITY a
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		WHERE t.ATTRIBUTE = 0 AND t.KIND <> 0
		INTO :activity_id, :activity_type_id
		DO BEGIN
			FOR
			SELECT a2.ID, a2.ACTIVITY_TYPE_ID, ah.POS
			FROM ACTIVITY_HIERARCHY ah
			JOIN ACTIVITY a ON a.ID = ah.PARENT_ACTIVITY_ID
			JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
			WHERE ah.IS_TEMPLATE = '1'
			AND a.ACTIVITY_TYPE_ID = :activity_type_id
			INTO :child_id, :attribute_type_id, :pos
			DO BEGIN
				id = NULL;
				
				SELECT ah2.ID FROM ACTIVITY_HIERARCHY ah2
				JOIN ACTIVITY aa2 ON aa2.ID = ah2.CHILD_ACTIVITY_ID
				WHERE ah2.PARENT_ACTIVITY_ID = :activity_id
				AND aa2.ACTIVITY_TYPE_ID = :attribute_type_id
				INTO :id;
				-- If the type already exists, update the position index 
				IF (id IS NOT NULL) THE BEGIN
					UPDATE ACTIVITY_HIERARCHY ah SET ah.POS = :pos WHERE ah.ID = :id;
				-- If not, add a null value 
				END ELSE BEGIN
					id = GEN_UUID();
					INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ACTIVITY_HIERARCHY');
					INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, POS)
					VALUES (:id, :activity_id, :child_id, :pos)
				END
			END
		END
	END^
	SET TERM ; ^
    
*/
    
    

/* Drop columns */

DROP VIEW VIEW_ACTIVITIES;


ALTER TABLE ACTIVITY_TYPE DROP TYPE_PRECISION;

ALTER TABLE COMPONENT DROP DESCRIPTION;

ALTER TABLE ACTIVITY_TYPE DROP KIND;

ALTER TABLE ACTIVITY_TYPE DROP ATTRIBUTE;

ALTER TABLE COMPONENT DROP ALIAS;

ALTER TABLE COMPONENT DROP NAME;

ALTER TABLE COMPONENT DROP NOTE;

/* Drop tables */
DROP TABLE COMPONENT_QUERY;








CREATE VIEW VIEW_ACTIVITIES (ACTIVITY_ID, ACTIVITY_TYPE_NAME, ATTRIBUTE_ID, ATTRIBUTE_TYPE_NAME, CONTENT_STRING, CONTENT_DATETIME, CONTENT_NUMERIC, CONTENT_BOOLEAN, CONTENT_BLOB, CONTENT_GUID)
AS 

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, a2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, a2.CONTENT_STRING, a2.CONTENT_DATETIME, a2.CONTENT_NUMERIC, a2.CONTENT_BOOLEAN, a2.CONTENT_BLOB, a2.CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
JOIN ACTIVITY_TYPE t2 ON t2.ID = a2.ACTIVITY_TYPE_ID
WHERE ah.IS_TEMPLATE = '0'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, a.START_AT AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB, NULL AS CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'START_AT'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, a.END_AT AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB, NULL AS CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'END_AT'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, a.TITLE AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB, NULL AS CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'TITLE'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, a.IS_COMPLETE AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB, NULL AS CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'IS_COMPLETE'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, a.NOTE AS CONTENT_BLOB, NULL AS CONTENT_GUID
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'NOTE'

ORDER BY 1

;

GRANT SELECT
 ON VIEW_ACTIVITIES TO ROLE MNR_ADMIN;
GRANT SELECT
 ON VIEW_ACTIVITIES TO ROLE MNR_EXEC;
GRANT SELECT
 ON VIEW_ACTIVITIES TO ROLE MNR_READONLY;
GRANT SELECT
 ON VIEW_ACTIVITIES TO ROLE MNR_USER;
GRANT DELETE, INSERT, REFERENCES, SELECT, UPDATE
 ON VIEW_ACTIVITIES TO  SYSDBA WITH GRANT OPTION;















COMMENT ON COLUMN ACTIVITY.CONTENT_STRING IS 'String value of the activity, one of 6 options to store the activity content.';

COMMENT ON COLUMN ACTIVITY.CONTENT_DATETIME IS 'Datetime value of the activity, one of 6 options to store the activity content.';

COMMENT ON COLUMN COMPONENT.CONTENT_GUID IS 'GUID value of the component, one of 6 options to store the component content. This field is a foreign key to the ENTITY_BASE table, allowing it to reference any other record in the database.';

COMMENT ON COLUMN ACTIVITY.CONTENT_NUMERIC IS 'Numeric/Decimal value of the activity, one of 6 options to store the activity content.';

COMMENT ON COLUMN ACTIVITY.CONTENT_BLOB IS 'Blob value of the activity, one of 6 options to store the activity content.';

COMMENT ON COLUMN COMPONENT_HIERARCHY.POS IS 'Integer value indicating the order that the child COMPONENT record should appear among the other child COMPONENT records with the same parent. This is also used for defining an enumeration lookup value.';

COMMENT ON COLUMN COMPONENT_HIERARCHY.IS_TEMPLATE IS 'Boolean flag to indicate whether the relationship between the two COMPONENT records is a template or not. If false, assume the relationship is representing actual data records.';

COMMENT ON TABLE COMPONENT_TYPE IS 'Component types describe the subtypes of COMPONENT records';

COMMENT ON TABLE COMPONENT_HIERARCHY IS 'Intermediate table between COMPONENT and itself, allowing a many to many relationship; each component can be made up of or related to other components.';

COMMENT ON COLUMN COMPONENT.CONTENT_STRING IS 'String value of the component, one of 6 options to store the component content.';

COMMENT ON COLUMN ACTIVITY_HIERARCHY.IS_TEMPLATE IS 'Boolean flag to indicate whether the relationship between the two ACTIVITY records is a template or not. If false, assume the relationship is representing actual data records.';

COMMENT ON COLUMN COMPONENT.ID IS 'GUID Primary Key and Foreign Key to the master supertype ENTITY_BASE.';

COMMENT ON COLUMN ACTIVITY_HIERARCHY.POS IS 'Integer value indicating the order that the child ACTIVITY record should appear among the other child ACTIVITY records with the same parent. This is also used for defining an enumeration lookup value.';

COMMENT ON COLUMN ACTIVITY.CONTENT_BOOLEAN IS 'Boolean value of the activity, one of 6 options to store the activity content.';

COMMENT ON COLUMN COMPONENT.CONTENT_BOOLEAN IS 'Boolean value of the component, one of 6 options to store the component content.';

COMMENT ON COLUMN COMPONENT.CONTENT_BLOB IS 'Blob value of the component, one of 6 options to store the component content.';

COMMENT ON COLUMN ACTIVITY.CONTENT_GUID IS 'GUID value of the activity, one of 6 options to store the activity content. This field is a foreign key to the ENTITY_BASE table, allowing it to reference any other record in the database.';

COMMENT ON COLUMN COMPONENT.CONTENT_DATETIME IS 'Timestamp value of the component, one of 6 options to store the component content.';

COMMENT ON COLUMN COMPONENT.CONTENT_NUMERIC IS 'Numeric value of the component, one of 6 options to store the component content.';

COMMENT ON COLUMN ACTIVITY_HIERARCHY.IS_TEMPLATE IS 'Boolean flag to indicate whether the relationship record represents a templated collection or not';












SET TERM ^ ;
CREATE PROCEDURE PROC_FAMILY_INVITE_LIST_GUID (
    START_DATE_RANGE Date,
    END_DATE_RANGE Date,
    PROGRAM_TYPE_ID GUID )
RETURNS (
    NEW_ID GUID,
    CENTER_CITY Varchar(50),
    CENTER_EMAIL Varchar(255),
    RECIPIENT_EMAIL Varchar(255),
    STUDENT_FIRST_NAMES Varchar(255),
    FAMILY_SURNAME Varchar(50),
    EVENT_NAME Varchar(50),
    SUBJECT Varchar(255) )
AS
BEGIN
    FOR
    Select
        pg.ID As NEW_ID,
        pg1.NAME As CENTER_CITY,
        (SELECT FIRST 1 av.CONTENT FROM VIEW_ADDRESSES_VIRTUAL av WHERE av.PARTY_ID = pg1.ID AND UPPER(av.DESCRIPTION) LIKE '%EMAIL%') AS CENTER_EMAIL,
        LIST(Distinct AV.CONTENT,',') As RECIPIENT_EMAIL,
        LIST(Distinct pp.FIRST_NAME,', ') AS STUDENT_FIRST_NAMES,
        pg.ALIAS As FAMILY_SURNAME
    From ENROLLMENT e
    Inner Join ITEM i On i.ID = e.ITEM_ID
    Inner Join ITEM_HIERARCHY ih ON ih.CHILD_ITEM_ID = i.ID
    Inner Join ITEM ii ON ii.ID = ih.PARENT_ITEM_ID
    Inner Join PARTY_ROLE_STUDENT prs On prs.ID = e.PARTY_ROLE_STUDENT_ID
    Inner Join PARTY_ROLE pr On pr.ID = prs.ID
    Inner Join PARTY_PERSON pp ON pp.ID = pr.PARTY_ID
    Inner Join PARTY_HIERARCHY ph On pp.ID = ph.CHILD_PARTY_ID
    Inner Join PARTY_GROUP pg On pg.ID = ph.PARENT_PARTY_ID
    Inner Join PARTY_HIERARCHY ph1 ON ph1.CHILD_PARTY_ID = pg.ID
    Inner Join PARTY_GROUP pg1 On pg1.ID = ph1.PARENT_PARTY_ID
    Inner Join PARTY_HIERARCHY ph2 On ph2.PARENT_PARTY_ID = pg.ID
    JOIN VIEW_ADDRESSES_VIRTUAL av ON av.PARTY_ID = ph2.CHILD_PARTY_ID
    Where
      pg.GROUP_TYPE = 'FAMILY'
      And e.TERMINATE_AT Is Null
      And pp.DO_NOT_CONTACT <> '1'
      AND ii.ID = :program_type_id
      AND av.DESCRIPTION = 'Email'
      And AV.CONTENT Like '%@%'
      AND av.ADDRESS_IS_ACTIVE = '1'
      AND av.LOCATOR_IS_ACTIVE = '1'
    Group By NEW_ID, CENTER_CITY, FAMILY_SURNAME, pg1.ID
    Order By FAMILY_SURNAME
    INTO :NEW_ID, :CENTER_CITY, :CENTER_EMAIL, :RECIPIENT_EMAIL, :STUDENT_FIRST_NAMES, :FAMILY_SURNAME
    DO BEGIN
        FOR
        SELECT a.TITLE, a.ALIAS
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE CAST(a.START_AT AS DATE) <= :end_date_range
        AND CAST(a.END_AT AS DATE) >= :start_date_range
        AND t.NAME = 'GAME_NIGHT'
        INTO :EVENT_NAME, :SUBJECT
        DO BEGIN
            SUSPEND;
        END
    END
END^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'WARNING: The input "PROGRAM_TYPE_ID" will only work single GUID inputs...

COMBINATIONS ARE NOT SUPPORTED IN THIS PROCEDURE YET!'
  where RDB$PROCEDURE_NAME = 'PROC_FAMILY_INVITE_LIST_GUID';
GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_GUID TO ROLE MNR_ADMIN;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_GUID TO ROLE MNR_EXEC;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_GUID TO ROLE MNR_READONLY;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_GUID TO ROLE MNR_USER;

 
 
 
 
 
 
SET TERM ^ ;
CREATE PROCEDURE PROC_FAMILY_INVITE_LIST_STRING (
    START_DATE_RANGE Date,
    END_DATE_RANGE Date,
    PROGRAM_TYPE_LIST Varchar(50) )
RETURNS (
    NEW_ID GUID,
    CENTER_CITY Varchar(50),
    CENTER_EMAIL Varchar(255),
    RECIPIENT_EMAIL Varchar(255),
    STUDENT_FIRST_NAMES Varchar(255),
    FAMILY_SURNAME Varchar(50),
    EVENT_NAME Varchar(50),
    SUBJECT Varchar(255) )
AS
DECLARE VARIABLE program_type_id GUID;
BEGIN
    SELECT FIRST 1 i.ID
    FROM ITEM i
    WHERE i.NAME = :program_type_list
    INTO :program_type_id;
    
    FOR
    Select
        pg.ID As NEW_ID,
        pg1.NAME As CENTER_CITY,
        (SELECT FIRST 1 av.CONTENT FROM VIEW_ADDRESSES_VIRTUAL av WHERE av.PARTY_ID = pg1.ID AND UPPER(av.DESCRIPTION) LIKE '%EMAIL%') AS CENTER_EMAIL,
        LIST(Distinct AV.CONTENT,',') As RECIPIENT_EMAIL,
        LIST(Distinct pp.FIRST_NAME,', ') AS STUDENT_FIRST_NAMES,
        pg.ALIAS As FAMILY_SURNAME
    From ENROLLMENT e
    Inner Join ITEM i On i.ID = e.ITEM_ID
    Inner Join ITEM_HIERARCHY ih ON ih.CHILD_ITEM_ID = i.ID
    Inner Join ITEM ii ON ii.ID = ih.PARENT_ITEM_ID
    Inner Join PARTY_ROLE_STUDENT prs On prs.ID = e.PARTY_ROLE_STUDENT_ID
    Inner Join PARTY_ROLE pr On pr.ID = prs.ID
    Inner Join PARTY_PERSON pp ON pp.ID = pr.PARTY_ID
    Inner Join PARTY_HIERARCHY ph On pp.ID = ph.CHILD_PARTY_ID
    Inner Join PARTY_GROUP pg On pg.ID = ph.PARENT_PARTY_ID
    Inner Join PARTY_HIERARCHY ph1 ON ph1.CHILD_PARTY_ID = pg.ID
    Inner Join PARTY_GROUP pg1 On pg1.ID = ph1.PARENT_PARTY_ID
    Inner Join PARTY_HIERARCHY ph2 On ph2.PARENT_PARTY_ID = pg.ID
    JOIN VIEW_ADDRESSES_VIRTUAL av ON av.PARTY_ID = ph2.CHILD_PARTY_ID
    Where
      pg.GROUP_TYPE = 'FAMILY'
      And e.TERMINATE_AT Is Null
      And pp.DO_NOT_CONTACT <> '1'
      AND ii.ID = :program_type_id
      AND av.DESCRIPTION = 'Email'
      And AV.CONTENT Like '%@%'
      AND av.ADDRESS_IS_ACTIVE = '1'
      AND av.LOCATOR_IS_ACTIVE = '1'
    Group By NEW_ID, CENTER_CITY, FAMILY_SURNAME, pg1.ID
    Order By FAMILY_SURNAME
    INTO :NEW_ID, :CENTER_CITY, :CENTER_EMAIL, :RECIPIENT_EMAIL, :STUDENT_FIRST_NAMES, :FAMILY_SURNAME
    DO BEGIN
        FOR
        SELECT a.TITLE, a.ALIAS
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        WHERE CAST(a.START_AT AS DATE) <= :end_date_range
        AND CAST(a.END_AT AS DATE) >= :start_date_range
        AND t.NAME = 'GAME_NIGHT'
        INTO :EVENT_NAME, :SUBJECT
        DO BEGIN
            SUSPEND;
        END
    END
END^
SET TERM ; ^

UPDATE RDB$PROCEDURES set
  RDB$DESCRIPTION = 'WARNING: The input "PROGRAM_TYPE_LIST" will only work with one of the following inputs: PROGRAMS_CP, PROGRAMS_TP, PROGRAMS_TH, PROGRAMS_SO

COMBINATIONS ARE NOT SUPPORTED IN THIS PROCEDURE YET!'
  where RDB$PROCEDURE_NAME = 'PROC_FAMILY_INVITE_LIST_STRING';
GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_STRING TO ROLE MNR_ADMIN;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_STRING TO ROLE MNR_EXEC;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_STRING TO ROLE MNR_READONLY;

GRANT EXECUTE
 ON PROCEDURE PROC_FAMILY_INVITE_LIST_STRING TO ROLE MNR_USER;

 
 
SET TERM ^ ;
CREATE PROCEDURE PROC_CREATE_ACTIVITY (
    ACTIVITY_TYPE Varchar(35),
    TITLE Varchar(255),
    ALIAS Varchar(255),
    DESCRIPTION Varchar(300),
    START_DATE Timestamp,
    END_DATE Timestamp,
    PARENT_ACTIVITY_ID GUID )
RETURNS (
    ACTIVITY_ID GUID )
AS
DECLARE VARIABLE activity_type_id GUID;
DECLARE VARIABLE attribute_id GUID;
DECLARE VARIABLE result BOOLEAN;
DECLARE VARIABLE stmt BLOB;
DECLARE VARIABLE id GUID;
BEGIN
    activity_id = NULL;
    
    /* ***** START_SQL_BLOCK A ***** */
    
    SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :activity_type AND t.KIND <> 0
    INTO :activity_type_id;
    
    /* Make sure the activity type is valid */
    IF (activity_type_id IS NULL) THEN BEGIN
        SUSPEND;
        EXIT;
    END
    
    /* Make sure that if a parent activity ID is provided, that it is actually linked to an ACTIVITY record */
    IF (parent_activity_id IS NOT NULL) THEN BEGIN
        SELECT a.ID FROM ACTIVITY a WHERE a.ID = :parent_activity_id
        INTO :parent_activity_id;
        
        IF (parent_activity_id IS NULL) THEN BEGIN
            SUSPEND;
            EXIT;
        END
    END
    
    /* ***** END_SQL_BLOCK A ***** */
    
    /* ***** START_SQL_BLOCK B ***** */
    /* Create the new activity record */
    SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'createActivity' AND m.ROW_ID IS NULL
    INTO :stmt;
    
    activity_id = GEN_UUID();
    EXECUTE STATEMENT (stmt) (ID := activity_id, ACTIVITY_TYPE_NAME := activity_type)
    INTO :activity_id;
    
    IF (activity_id IS NULL) THEN BEGIN
        SUSPEND;
        EXIT;
    END
    /* ***** END_SQL_BLOCK B ***** */
    
    /* ***** START_SQL_BLOCK C ***** */
    SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = 'ACTIVITY' AND m.NAME = 'updateAttribute' AND m.ROW_ID IS NULL
    INTO :stmt;
    
    CONTENT_STRING:Varchar(255),CONTENT_DATETIME:Timestamp,CONTENT_NUMERIC:Decimal(10,8),CONTENT_BOOLEAN:Boolean,CONTENT_BLOB:BLOB,CONTENT_GUID:GUID
    
    /* add attributes */
    EXECUTE STATEMENT(stmt) (
        ACTIVITY_ID := activity_id, 
        ATTRIBUTE_TYPE_NAME := 'TITLE', 
        CONTENT_STRING := title, 
        CONTENT_DATETIME := NULL,
        CONTENT_NUMERIC := NULL,
        CONTENT_BOOLEAN := NULL,
        CONTENT_BLOB := NULL, 
        CONTENT_GUID := NULL
    )
    INTO :attribute_id;
    
    EXECUTE STATEMENT(stmt) (
        ACTIVITY_ID := activity_id, 
        ATTRIBUTE_TYPE_NAME := 'ALIAS', 
        CONTENT_STRING := alias, 
        CONTENT_DATETIME := NULL,
        CONTENT_NUMERIC := NULL,
        CONTENT_BOOLEAN := NULL,
        CONTENT_BLOB := NULL, 
        CONTENT_GUID := NULL
    )
    INTO :attribute_id;
    
    EXECUTE STATEMENT(stmt) (
        ACTIVITY_ID := activity_id, 
        ATTRIBUTE_TYPE_NAME := 'NOTE', 
        CONTENT_STRING := description, 
        CONTENT_DATETIME := NULL,
        CONTENT_NUMERIC := NULL,
        CONTENT_BOOLEAN := NULL,
        CONTENT_BLOB := NULL, 
        CONTENT_GUID := NULL
    )
    INTO :attribute_id;
    
    EXECUTE STATEMENT(stmt) (
        ACTIVITY_ID := activity_id, 
        ATTRIBUTE_TYPE_NAME := 'START_AT', 
        CONTENT_STRING := NULL, 
        CONTENT_DATETIME := start_date,
        CONTENT_NUMERIC := NULL,
        CONTENT_BOOLEAN := NULL,
        CONTENT_BLOB := NULL, 
        CONTENT_GUID := NULL
    )
    INTO :attribute_id;
    
    EXECUTE STATEMENT(stmt) (
        ACTIVITY_ID := activity_id, 
        ATTRIBUTE_TYPE_NAME := 'END_AT', 
        CONTENT_STRING := NULL, 
        CONTENT_DATETIME := end_date,
        CONTENT_NUMERIC := NULL,
        CONTENT_BOOLEAN := NULL,
        CONTENT_BLOB := NULL, 
        CONTENT_GUID := NULL
    )
    INTO :attribute_id;
    
    /* ***** END_SQL_BLOCK C ***** */
    
    /* ***** START_SQL_BLOCK D ***** */
    /* Associate with the parent activity if applicable */
    IF (parent_activity_id IS NOT NULL) THEN BEGIN
        id = GEN_UUID();
        INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ACTIVITY_HIERARCHY');
        INSERT INTO ACTIVITY_HIERARCHY(ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID) VALUES (:id, :parent_activity_id, :activity_id);
    END
    /* ***** END_SQL_BLOCK D ***** */
    
    SUSPEND;
END^
SET TERM ; ^

GRANT EXECUTE
 ON PROCEDURE PROC_CREATE_ACTIVITY TO ROLE MNR_ADMIN;

GRANT EXECUTE
 ON PROCEDURE PROC_CREATE_ACTIVITY TO ROLE MNR_EXEC;

GRANT EXECUTE
 ON PROCEDURE PROC_CREATE_ACTIVITY TO ROLE MNR_USER;

GRANT EXECUTE
 ON PROCEDURE PROC_CREATE_ACTIVITY TO  SYSDBA;

