unit TEST6.BusinessObject.Entity_RAW;

interface

uses
  SysUtils, 
  Generics.Collections, 
  Aurelius.Mapping.Attributes, 
  Aurelius.Types.Blob, 
  Aurelius.Types.DynamicProperties, 
  Aurelius.Types.Nullable, 
  Aurelius.Types.Proxy, 
  Aurelius.Criteria.Dictionary;

type
  TEntityBase = class;
  TItemHierarchy = class;
  TParticipantStatus = class;
  TActivity = class;
  TActivityHierarchy = class;
  TActivityType = class;
  TAddress = class;
  TAddressHierarchy = class;
  TAddressType = class;
  TAttendOrder = class;
  TCertificate = class;
  TChangeReason = class;
  TChangeRequest = class;
  TChangeRequestType = class;
  TComponent = class;
  TComponentHierarchy = class;
  TComponentType = class;
  TEnrollment = class;
  TGender = class;
  TGrade = class;
  TInvoice = class;
  TInvoiceDetail = class;
  TInvoicingEnrollment = class;
  TInvoicingItem = class;
  TInvoicingPaymentAccount = class;
  TInvoicingSched = class;
  TInvoicingSchedDetail = class;
  TInvoicingSchedType = class;
  TItem = class;
  TItemRate = class;
  TItemType = class;
  TLocator = class;
  TParticipant = class;
  TParty = class;
  TPartyHierarchy = class;
  TPartyRole = class;
  TPartyRoleType = class;
  TPartyType = class;
  TPayInvoice = class;
  TPayment = class;
  TPaymentAccount = class;
  TPaymentAccountDetail = class;
  TPaymentAccountMemoType = class;
  TPaymentAccountType = class;
  TPaymentDetail = class;
  TPricePeriod = class;
  TQbAccount = class;
  TReportingSched = class;
  TReportingSchedType = class;
  TRevenueReport = class;
  TSalesOrder = class;
  TSalesOrderDetail = class;
  TSalesOrderDetailMod = class;
  TServiceCategory = class;
  TServiceLevel = class;
  TUnit = class;
  TChangeCancel = class;
  TChangeCredit = class;
  TChangeHold = class;
  TChangeInfo = class;
  TChangeSwitch = class;
  TItemMod = class;
  TItemProduct = class;
  TItemService = class;
  TPartyGroup = class;
  TPartyPerson = class;
  TPartyRoleCustomer = class;
  TPartyRoleGuardian = class;
  TPartyRoleLead = class;
  TPartyRoleLearningCenter = class;
  TPartyRoleStaff = class;
  TPartyRoleStudent = class;
  TPaymentAccountCash = class;
  TPaymentAccountCc = class;
  TPaymentAccountCheque = class;
  TPaymentAccountMemo = class;
  TActivityTableDictionary = class;
  TActivityHierarchyTableDictionary = class;
  TActivityTypeTableDictionary = class;
  TAddressTableDictionary = class;
  TAddressHierarchyTableDictionary = class;
  TAddressTypeTableDictionary = class;
  TAttendOrderTableDictionary = class;
  TCertificateTableDictionary = class;
  TChangeCancelTableDictionary = class;
  TChangeCreditTableDictionary = class;
  TChangeHoldTableDictionary = class;
  TChangeInfoTableDictionary = class;
  TChangeReasonTableDictionary = class;
  TChangeRequestTableDictionary = class;
  TChangeRequestTypeTableDictionary = class;
  TChangeSwitchTableDictionary = class;
  TComponentTableDictionary = class;
  TComponentTypeTableDictionary = class;
  TEnrollmentTableDictionary = class;
  TEntityBaseTableDictionary = class;
  TGenderTableDictionary = class;
  TGradeTableDictionary = class;
  TInvoiceTableDictionary = class;
  TInvoiceDetailTableDictionary = class;
  TInvoicingEnrollmentTableDictionary = class;
  TInvoicingItemTableDictionary = class;
  TInvoicingPaymentAccountTableDictionary = class;
  TInvoicingSchedTableDictionary = class;
  TInvoicingSchedDetailTableDictionary = class;
  TInvoicingSchedTypeTableDictionary = class;
  TItemTableDictionary = class;
  TItemHierarchyTableDictionary = class;
  TItemModTableDictionary = class;
  TItemProductTableDictionary = class;
  TItemRateTableDictionary = class;
  TItemServiceTableDictionary = class;
  TItemTypeTableDictionary = class;
  TLocatorTableDictionary = class;
  TParticipantTableDictionary = class;
  TParticipantStatusTableDictionary = class;
  TPartyTableDictionary = class;
  TPartyGroupTableDictionary = class;
  TPartyHierarchyTableDictionary = class;
  TPartyPersonTableDictionary = class;
  TPartyRoleTableDictionary = class;
  TPartyRoleCustomerTableDictionary = class;
  TPartyRoleGuardianTableDictionary = class;
  TPartyRoleLeadTableDictionary = class;
  TPartyRoleLearningCenterTableDictionary = class;
  TPartyRoleStaffTableDictionary = class;
  TPartyRoleStudentTableDictionary = class;
  TPartyRoleTypeTableDictionary = class;
  TPartyTypeTableDictionary = class;
  TPaymentTableDictionary = class;
  TPaymentAccountTableDictionary = class;
  TPaymentAccountCashTableDictionary = class;
  TPaymentAccountCcTableDictionary = class;
  TPaymentAccountChequeTableDictionary = class;
  TPaymentAccountDetailTableDictionary = class;
  TPaymentAccountMemoTableDictionary = class;
  TPaymentAccountMemoTypeTableDictionary = class;
  TPaymentAccountTypeTableDictionary = class;
  TPaymentDetailTableDictionary = class;
  TPayInvoiceTableDictionary = class;
  TPricePeriodTableDictionary = class;
  TQbAccountTableDictionary = class;
  TReportingSchedTableDictionary = class;
  TReportingSchedTypeTableDictionary = class;
  TRevenueReportTableDictionary = class;
  TSalesOrderTableDictionary = class;
  TSalesOrderDetailTableDictionary = class;
  TSalesOrderDetailModTableDictionary = class;
  TServiceCategoryTableDictionary = class;
  TServiceLevelTableDictionary = class;
  TUnitTableDictionary = class;
  TComponentHierarchyTableDictionary = class;
  
  [Entity]
  [Table('ENTITY_BASE')]
  [Description('Common table which all other tables reference, no record can exist in any other table without first having an entry here. It stores standard information including the generated GUID/UUID primary key, created and updated timestamps, etc.')]
  [Inheritance(TInheritanceStrategy.JoinedTables)]
  [Id('FId', TIdGenerator.None)]
  TEntityBase = class
  private
    [Column('ID', [TColumnProp.Required], 16)]
    [Description('GUID Primary Key')]
    FId: string;
    
    [Column('IS_ACTIVE', [TColumnProp.Required], 1)]
    [Description('Boolean indicator of whether or not the record is active')]
    FIsActive: string;
    
    [Column('CREATED_BY', [TColumnProp.Required], 128)]
    [Description('User who created the record')]
    FCreatedBy: string;
    
    [Column('CREATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of creation')]
    FCreatedAt: TDateTime;
    
    [Column('UPDATED_BY', [TColumnProp.Required], 128)]
    [Description('User who last updated the record')]
    FUpdatedBy: string;
    
    [Column('UPDATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of last update')]
    FUpdatedAt: TDateTime;
    
    [Column('POS', [])]
    [Description('Integer index to track the order that records were created in a given day (for when records are creating at the same instanct and the timestamp is not enough)')]
    FPos: Nullable<Integer>;
    
    [Column('EXTERNAL_SOURCE', [], 35)]
    [Description('String field to store the name of the external source the data came from, e.g. CDS, M2, CYBERSOURCE, etc.')]
    FExternalSource: Nullable<string>;
    
    [Column('EXTERNAL_ID', [], 50)]
    [Description('String field to store an identifier from the external source the data came from.')]
    FExternalId: Nullable<string>;
    
    [Column('TABLE_NAME', [], 35)]
    [Description('String field to store the name of the TABLE that will indicate the subtype that this record is parent to. NOTE: This field does not fill in automatically, though a query can be written to search the entire database and update this field.')]
    FTableName: Nullable<string>;
  public
    property Id: string read FId write FId;
    property IsActive: string read FIsActive write FIsActive;
    property CreatedBy: string read FCreatedBy write FCreatedBy;
    property CreatedAt: TDateTime read FCreatedAt write FCreatedAt;
    property UpdatedBy: string read FUpdatedBy write FUpdatedBy;
    property UpdatedAt: TDateTime read FUpdatedAt write FUpdatedAt;
    property Pos: Nullable<Integer> read FPos write FPos;
    property ExternalSource: Nullable<string> read FExternalSource write FExternalSource;
    property ExternalId: Nullable<string> read FExternalId write FExternalId;
    property TableName: Nullable<string> read FTableName write FTableName;
  end;
  
  [Entity]
  [Table('ITEM_HIERARCHY')]
  [Model('ITEM')]
  [Description('Intermediate table between ITEMs and itself, allowing a many to many relationship. That is, each item can be made up of other items.')]
  [Id('FEntityBase', TIdGenerator.None)]
  [Id('FChildItem', TIdGenerator.None)]
  [Id('FParentItem', TIdGenerator.None)]
  TItemHierarchy = class
  private
    [Column('UNIT_COUNT', [], 10, 5)]
    [Description('Number of children item make up the parent item')]
    FUnitCount: Nullable<Double>;
    
    [Column('UNIT_COUNT_PER_LINE', [], 10, 5)]
    [Description('')]
    FUnitCountPerLine: Nullable<Double>;
    
    [Column('DIVISOR', [], 10, 5)]
    [Description('Divisor used if necessary to represent a non-whole number of children items making up a parent item. For example, a Level 1 binder spine makes is made up of 1/6 of a sheet of non-perforated card stock.')]
    FDivisor: Nullable<Double>;
    
    [Column('IS_RECURRING', [], 1)]
    [Description('Boolean flag to indicate whether the item should automatically recur once all units have been rendered. (e.g. month-to-month programs automatically renew, however blocks of private tutoring and summer programs do not)')]
    FIsRecurring: Nullable<string>;
    
    [Column('POS', [])]
    [Description('Position of the child item in reference to the other child items that make up the associated parent item, to preserve a desired order.')]
    FPos: Nullable<Integer>;
    
    [Column('MIN_COUNT', [], 10, 5)]
    [Description('Minimum value for the child item to appear in the parent item, with the exception of attendance frequency, where this does not indicate the number of times the program appears but the number of times the student attends.')]
    FMinCount: Nullable<Double>;
    
    [Column('MAX_COUNT', [], 10, 5)]
    [Description('Maximum value for the child item to appear in the parent item, with the exception of attendance frequency, where this does not indicate the number of times the program appears but the number of times the student attends.')]
    FMaxCount: Nullable<Double>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildItem: TItem;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentItem: TItem;
  public
    property UnitCount: Nullable<Double> read FUnitCount write FUnitCount;
    property UnitCountPerLine: Nullable<Double> read FUnitCountPerLine write FUnitCountPerLine;
    property Divisor: Nullable<Double> read FDivisor write FDivisor;
    property IsRecurring: Nullable<string> read FIsRecurring write FIsRecurring;
    property Pos: Nullable<Integer> read FPos write FPos;
    property MinCount: Nullable<Double> read FMinCount write FMinCount;
    property MaxCount: Nullable<Double> read FMaxCount write FMaxCount;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
    property ChildItem: TItem read FChildItem write FChildItem;
    property ParentItem: TItem read FParentItem write FParentItem;
  end;
  
  [Entity]
  [Table('PARTICIPANT_STATUS')]
  [Model('ACTIVITY')]
  [Description('')]
  [Id('FId', TIdGenerator.None)]
  TParticipantStatus = class
  private
    [Column('ID', [TColumnProp.Required], 16)]
    [Description('')]
    FId: string;
    
    [Column('STATUS', [TColumnProp.Required], 35)]
    [Description('')]
    FStatus: string;
    
    [Column('DESCRIPTION', [], 1000)]
    [Description('')]
    FDescription: Nullable<string>;
  public
    property Id: string read FId write FId;
    property Status: string read FStatus write FStatus;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('ACTIVITY')]
  [Model('ACTIVITY')]
  [Description('An event that typically is relevant within a time window. This table is hierarchical, so a record here can either be a single attribute of an activity (e.g. START_AT) or a container defining the master event record (e.g. TRIMATHLON).')]
  [PrimaryJoinColumn('ID')]
  TActivity = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('String value of the activity, one of 6 options to store the activity content.')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('Datetime value of the activity, one of 6 options to store the activity content.')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('Numeric/Decimal value of the activity, one of 6 options to store the activity content.')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('Boolean value of the activity, one of 6 options to store the activity content.')]
    FContentBoolean: Nullable<string>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80, 0)]
    [Description('Blob value of the activity, one of 6 options to store the activity content.')]
    FContentBlob: TBlob;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ACTIVITY_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FActivityType: TActivityType;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<string> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property ActivityType: TActivityType read FActivityType write FActivityType;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
  end;
  
  [Entity]
  [Table('ACTIVITY_HIERARCHY')]
  [Model('ACTIVITY')]
  [Description('Intermediate table between ACTIVITY and itself, allowing a many to many relationship; each activity can be made up of or related to other activites. e.g. the TriMathlon consists of 1 main event and is made up of sub-events for each grade level.')]
  [UniqueKey('PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID')]
  [PrimaryJoinColumn('ID')]
  TActivityHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('Integer value indicating the order that the child ACTIVITY record should appear among the other child ACTIVITY records with the same parent. This is also used for defining an enumeration lookup value.')]
    FPos: Nullable<Integer>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the relationship between the two ACTIVITY records is a template or not. If false, assume the relationship is representing actual data records.')]
    FIsTemplate: string;
    
    [Column('IS_SINGLETON', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether only one child activity of the specified type is allowed (value is TRUE), or if multiple child records of that type are allowed (value is FALSE). Valid only when IS_TEMPLATE is TRUE.')]
    FIsSingleton: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildActivity: TActivity;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentActivity: TActivity;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsTemplate: string read FIsTemplate write FIsTemplate;
    property IsSingleton: string read FIsSingleton write FIsSingleton;
    property ChildActivity: TActivity read FChildActivity write FChildActivity;
    property ParentActivity: TActivity read FParentActivity write FParentActivity;
  end;
  
  [Entity]
  [Table('ACTIVITY_TYPE')]
  [Model('ACTIVITY')]
  [Description('This table describes the subtypes that an ACTIVITY can be, which could be a top-level or container type (with its own KIND integer enumeration), or a lower-level detail type (with its own ATTRIBUTE integer enumeration).')]
  [PrimaryJoinColumn('ID')]
  TActivityType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the event type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the event type.')]
    FDescription: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('A character string to describe what a valid entry is for the ACTIVITY record. This most likely will be a RegEx, or a Regular Expression.')]
    FValidation: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('A character string that can be used to define how data in the ACTIVITY record is to be displayed or printed')]
    FFormatMask: Nullable<string>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property Validation: Nullable<string> read FValidation write FValidation;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property DataType: Nullable<string> read FDataType write FDataType;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
  end;
  
  [Entity]
  [Table('ADDRESS')]
  [Model('ADDRESS')]
  [Description('An address is a way to contact a person or place; this table does hold address information and simply acts as a single point for outside tables to reference its subtypes.')]
  [PrimaryJoinColumn('ID')]
  TAddress = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('String value of the address, one of 6 options to store the address content.')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('Timestamp value of the address, one of 6 options to store the address content.')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('Numeric value of the address, one of 6 options to store the address content.')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('Boolean value of the address, one of 6 options to store the address content.')]
    FContentBoolean: Nullable<string>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80)]
    [Description('Blob value of the address, one of 6 options to store the address content.')]
    [DBTypeMemo]
    FContentBlob: TBlob;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ADDRESS_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddressType: TAddressType;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<string> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property AddressType: TAddressType read FAddressType write FAddressType;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
  end;
  
  [Entity]
  [Table('ADDRESS_HIERARCHY')]
  [Model('ADDRESS')]
  [Description('This table is an intermediate or junction table between ADDRESS and itself, allowing a many to many relationship. That is, each address can contain or be related to other address. For example the city "Chatham" can belong to the state "NJ".')]
  [UniqueKey('PARENT_ADDRESS_ID, CHILD_ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TAddressHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('Position of the child ADDRESS in reference to the other child ADDRESS that make up the associated parent ADDRESS, to preserve a desired order.')]
    FPos: Nullable<Integer>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('')]
    FIsTemplate: string;
    
    [Column('IS_SINGLETON', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether only one child address of the specified type is allowed (value is TRUE), or if multiple child records of that type are allowed (value is FALSE). Valid only when IS_TEMPLATE is TRUE.')]
    FIsSingleton: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildAddress: TAddress;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentAddress: TAddress;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsTemplate: string read FIsTemplate write FIsTemplate;
    property IsSingleton: string read FIsSingleton write FIsSingleton;
    property ChildAddress: TAddress read FChildAddress write FChildAddress;
    property ParentAddress: TAddress read FParentAddress write FParentAddress;
  end;
  
  [Entity]
  [Table('ADDRESS_TYPE')]
  [Model('ADDRESS')]
  [Description('This table describes the types of ADDRESS. Each record in this table should correspond to a subtype table beginning with "ADDRESS_".')]
  [PrimaryJoinColumn('ID')]
  TAddressType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the address type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the address type.')]
    FDescription: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('A character string to describe what a valid entry is for the CONTENT of related ADDRESS records. This most likely will be a RegEx, or a Regular Expression.')]
    FValidation: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('A character string that can be used to define how data in the CONTENT field of related ADDRESS records is to be displayed or printed')]
    FFormatMask: Nullable<string>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property Validation: Nullable<string> read FValidation write FValidation;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property DataType: Nullable<string> read FDataType write FDataType;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
  end;
  
  [Entity]
  [Table('ATTEND_ORDER')]
  [Model('ACTIVITY')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TAttendOrder = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FActivity: TActivity;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SALES_ORDER_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FSalesOrderDetail: TSalesOrderDetail;
  public
    property Activity: TActivity read FActivity write FActivity;
    property SalesOrderDetail: TSalesOrderDetail read FSalesOrderDetail write FSalesOrderDetail;
  end;
  
  [Entity]
  [Table('CERTIFICATE')]
  [Model('PAYMENT')]
  [Description('Stores record of all certificates that have been issued, usually through donation to non-profit organization, which can be redeemed for the services specified on the certificate.')]
  [PrimaryJoinColumn('ID')]
  TCertificate = class(TEntityBase)
  private
    [Column('CODE', [TColumnProp.Required], 35)]
    [Description('Unique identifier that is physically printed on the certificate, usually in the format @YYYYMMDD-00# where @ is the first letter of the donating center, YYYYMMDD is the date of the event, and 00# is the number of certificates that have been donated to an event on that day from that center (001 for the first, 002 for the second, etc.)')]
    FCode: string;
    
    [Column('DONATION_VALUE', [], 35)]
    [Description('What the certificate is worth, e.g. "1 month 2nd-12th membership"')]
    FDonationValue: Nullable<string>;
    
    [Column('MAX_AMOUNT', [], 10, 2)]
    [Description('Maximum monetary value of services the certificate can be, e.g. for a certificate good for up to $500 of services, enter 500')]
    FMaxAmount: Nullable<Double>;
    
    [Column('DONATE_TO', [], 50)]
    [Description('Name of the organization that the certificate is being donated to')]
    FDonateTo: Nullable<string>;
    
    [Column('NOTE', [], 1000)]
    [Description('Any notes regarding the certificate and/or the donation in general')]
    FNote: Nullable<string>;
    
    [Column('EVENT_START_AT', [])]
    [Description('Datetime of the event that the certificate will be awarded at')]
    FEventStartAt: Nullable<TDateTime>;
    
    [Column('DELIVER_AT', [])]
    [Description('Datetime that the certificate was delivered or picked up by the organization receiving the donation')]
    FDeliverAt: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_GROUP_ID', [], 'ID')]
    [Description('')]
    FPartyGroup: TPartyGroup;
  public
    property Code: string read FCode write FCode;
    property DonationValue: Nullable<string> read FDonationValue write FDonationValue;
    property MaxAmount: Nullable<Double> read FMaxAmount write FMaxAmount;
    property DonateTo: Nullable<string> read FDonateTo write FDonateTo;
    property Note: Nullable<string> read FNote write FNote;
    property EventStartAt: Nullable<TDateTime> read FEventStartAt write FEventStartAt;
    property DeliverAt: Nullable<TDateTime> read FDeliverAt write FDeliverAt;
    property PartyGroup: TPartyGroup read FPartyGroup write FPartyGroup;
  end;
  
  [Entity]
  [Table('CHANGE_REASON')]
  [Model('CHANGE_REQUEST')]
  [Description('A Change Reason is a commonly referenced reason for creating the Change Request. For example, a common reason that a student might go on hold is due to "Other Priorities" like sports or a vacation.')]
  [PrimaryJoinColumn('ID')]
  TChangeReason = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('CHANGE_REQUEST')]
  [Model('CHANGE_REQUEST')]
  [Description('The record of a request to change an enrollment or any other record input from customer management. For different kinds of requests, there are subtype tables, while this supertype holds common information like who took the request when and why.')]
  [PrimaryJoinColumn('ID')]
  TChangeRequest = class(TEntityBase)
  private
    [Column('IS_APPLIED', [TColumnProp.Required], 1)]
    [Description('')]
    FIsApplied: string;
    
    [Column('REQUEST_BY', [], 128)]
    [Description('Staff member who requested the ECR (or received the request from the customer). TO DO: TURN INTO A FOREIGN KEY TO THE ROLE_EMPLOYEES TABLE.')]
    FRequestBy: Nullable<string>;
    
    [Column('APPROVAL_STATUS', [], 50)]
    [Description('Approval Status of the ECR, can be NEEDS_APPROVAL, APPROVED, or REJECTED. TO DO: TURN THESE OPTIONS INTO REFERENCES TO AN ENUMERATION TABLE.')]
    FApprovalStatus: Nullable<string>;
    
    [Column('APPROVE_BY', [], 128)]
    [Description('Staff member who approved the ECR. TO DO: TURN INTO A FOREIGN KEY TO THE ROLE_EMPLOYEES TABLE.')]
    FApproveBy: Nullable<string>;
    
    [Column('CHANGE_REQUEST_STATUS', [], 50)]
    [Description('Status of the ECR, currently our system has 5 phases of an ECR, which are 1. Added in M2, 2. Reviewed by Center Director, 3. Updated in CDS, 4. Updated in Cybersource, and 5. Updated in Quickbooks.')]
    FChangeRequestStatus: Nullable<string>;
    
    [Column('NOTE', [], 1000)]
    [Description('Noted detailing the situation surrounding the ECR, including information like why it was requested, any special notes on how it was applied or why it was rejected, etc.')]
    FNote: Nullable<string>;
    
    [Column('CDS_ENTER_BY', [], 128)]
    [Description('Legacy field indicating who entered the ECR in the CDS.')]
    FCdsEnterBy: Nullable<string>;
    
    [Column('CYBERSOURCE_ENTER_BY', [], 128)]
    [Description('Legacy field indicating who updated or confirmed the ECR in Cybersource')]
    FCybersourceEnterBy: Nullable<string>;
    
    [Column('IS_APPROVED', [TColumnProp.Required], 1)]
    [Description('')]
    FIsApproved: string;
    
    [Column('UNIT_COUNT', [], 10, 8)]
    [Description('')]
    FUnitCount: Nullable<Double>;
    
    [Column('INCLUSIVE_RANGE', [TColumnProp.Required], 1)]
    [Description('Boolean flag indicates if date range is inclusive, default is TRUE. E.G. Inclusive cancel request: date of the cancel is the last day of the enrollment, however if marked as exclusive the cancel date is the day after the end of the enrollment.')]
    FInclusiveRange: string;
    
    [Column('CHANGE_REQUEST_ID', [], 16)]
    [Description('Foreign key to the same change request table, which allows for a change request record to be related to another change request record. This is particularly useful when applying a change request that generates another change request.')]
    FChangeRequestId: Nullable<string>;
    
    [Column('APPLY_AT', [])]
    [Description('Datetime that the change request was applied.')]
    FApplyAt: Nullable<TDateTime>;
    
    [Column('REQUEST_AT', [])]
    [Description('Datetime that the change request was requested.')]
    FRequestAt: Nullable<TDateTime>;
    
    [Column('APPROVE_AT', [])]
    [Description('Datetime that the change request was approved.')]
    FApproveAt: Nullable<TDateTime>;
    
    [Column('CDS_ENTER_AT', [])]
    [Description('Datetime that the change request was entered into the CDS.')]
    FCdsEnterAt: Nullable<TDateTime>;
    
    [Column('CYBERSOURCE_ENTER_AT', [])]
    [Description('Datetime that the change request was reconciled in Cybersource.')]
    FCybersourceEnterAt: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHANGE_REASON_ID', [], 'ID')]
    [Description('')]
    FChangeReason: TChangeReason;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ENROLLMENT_ID', [], 'ID')]
    [Description('')]
    FEnrollment: TEnrollment;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_ITEM_ID', [], 'ID')]
    [Description('')]
    FItem: TItem;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [], 'ID')]
    [Description('')]
    FParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHANGE_REQUEST_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChangeRequestType: TChangeRequestType;
  public
    property IsApplied: string read FIsApplied write FIsApplied;
    property RequestBy: Nullable<string> read FRequestBy write FRequestBy;
    property ApprovalStatus: Nullable<string> read FApprovalStatus write FApprovalStatus;
    property ApproveBy: Nullable<string> read FApproveBy write FApproveBy;
    property ChangeRequestStatus: Nullable<string> read FChangeRequestStatus write FChangeRequestStatus;
    property Note: Nullable<string> read FNote write FNote;
    property CdsEnterBy: Nullable<string> read FCdsEnterBy write FCdsEnterBy;
    property CybersourceEnterBy: Nullable<string> read FCybersourceEnterBy write FCybersourceEnterBy;
    property IsApproved: string read FIsApproved write FIsApproved;
    property UnitCount: Nullable<Double> read FUnitCount write FUnitCount;
    property InclusiveRange: string read FInclusiveRange write FInclusiveRange;
    property ChangeRequestId: Nullable<string> read FChangeRequestId write FChangeRequestId;
    property ApplyAt: Nullable<TDateTime> read FApplyAt write FApplyAt;
    property RequestAt: Nullable<TDateTime> read FRequestAt write FRequestAt;
    property ApproveAt: Nullable<TDateTime> read FApproveAt write FApproveAt;
    property CdsEnterAt: Nullable<TDateTime> read FCdsEnterAt write FCdsEnterAt;
    property CybersourceEnterAt: Nullable<TDateTime> read FCybersourceEnterAt write FCybersourceEnterAt;
    property ChangeReason: TChangeReason read FChangeReason write FChangeReason;
    property Enrollment: TEnrollment read FEnrollment write FEnrollment;
    property Item: TItem read FItem write FItem;
    property Party: TParty read FParty write FParty;
    property ChangeRequestType: TChangeRequestType read FChangeRequestType write FChangeRequestType;
  end;
  
  [Entity]
  [Table('CHANGE_REQUEST_TYPE')]
  [Model('CHANGE_REQUEST')]
  [Description('This table describes the subtypes that a Change Request can be. Each record in this table should correspond to a subtype table beginning with "CHANGE_".')]
  [PrimaryJoinColumn('ID')]
  TChangeRequestType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the ECR type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the item type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('COMPONENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TComponent = class(TEntityBase)
  private
    [Column('CONTENT_STRING', [], 255)]
    [Description('String value of the component, one of 6 options to store the component content.')]
    FContentString: Nullable<string>;
    
    [Column('CONTENT_DATETIME', [])]
    [Description('Timestamp value of the component, one of 6 options to store the component content.')]
    FContentDatetime: Nullable<TDateTime>;
    
    [Column('CONTENT_NUMERIC', [], 10, 8)]
    [Description('Numeric value of the component, one of 6 options to store the component content.')]
    FContentNumeric: Nullable<Double>;
    
    [Column('CONTENT_BOOLEAN', [], 1)]
    [Description('Boolean value of the component, one of 6 options to store the component content.')]
    FContentBoolean: Nullable<string>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80)]
    [Description('Blob value of the component, one of 6 options to store the component content.')]
    [DBTypeMemo]
    FContentBlob: TBlob;
    
    [Column('VERSION_MAJOR', [TColumnProp.Required])]
    [Description('Manual Incrementing Integer to describe the Major version number, value must be provided upon insert. Major version numbers should be increased when Systemic changes are being made.')]
    FVersionMajor: Integer;
    
    [Column('VERSION_MINOR', [TColumnProp.Required], 2)]
    [Description('Manual Incrementing Integer to describe the Minor version number, value must be provided upon insert. Minor version numbers should be increased when Local changes are being made.')]
    FVersionMinor: string;
    
    [Column('VERSION_BUILD', [TColumnProp.Required], 4)]
    [Description('Manual Incrementing Integer to describe the Build number, value must be provided upon insert. Build numbers should be increased when Superficial changes are being made.')]
    FVersionBuild: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('COMPONENT_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FComponentType: TComponentType;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CONTENT_GUID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
  public
    property ContentString: Nullable<string> read FContentString write FContentString;
    property ContentDatetime: Nullable<TDateTime> read FContentDatetime write FContentDatetime;
    property ContentNumeric: Nullable<Double> read FContentNumeric write FContentNumeric;
    property ContentBoolean: Nullable<string> read FContentBoolean write FContentBoolean;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property VersionMajor: Integer read FVersionMajor write FVersionMajor;
    property VersionMinor: string read FVersionMinor write FVersionMinor;
    property VersionBuild: string read FVersionBuild write FVersionBuild;
    property ComponentType: TComponentType read FComponentType write FComponentType;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
  end;
  
  [Entity]
  [Table('COMPONENT_HIERARCHY')]
  [Description('Intermediate table between COMPONENT and itself, allowing a many to many relationship; each component can be made up of or related to other components.')]
  [UniqueKey('PARENT_COMPONENT_ID, CHILD_COMPONENT_ID')]
  [PrimaryJoinColumn('ID')]
  TComponentHierarchy = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('Integer value indicating the order that the child COMPONENT record should appear among the other child COMPONENT records with the same parent. This is also used for defining an enumeration lookup value.')]
    FPos: Nullable<Integer>;
    
    [Column('IS_TEMPLATE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the relationship between the two COMPONENT records is a template or not. If false, assume the relationship is representing actual data records.')]
    FIsTemplate: string;
    
    [Column('IS_SINGLETON', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether only one child component of the specified type is allowed (value is TRUE), or if multiple child records of that type are allowed (value is FALSE). Valid only when IS_TEMPLATE is TRUE.')]
    FIsSingleton: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_COMPONENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildComponent: TComponent;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_COMPONENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentComponent: TComponent;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsTemplate: string read FIsTemplate write FIsTemplate;
    property IsSingleton: string read FIsSingleton write FIsSingleton;
    property ChildComponent: TComponent read FChildComponent write FChildComponent;
    property ParentComponent: TComponent read FParentComponent write FParentComponent;
  end;
  
  [Entity]
  [Table('COMPONENT_TYPE')]
  [Description('Component types describe the subtypes of COMPONENT records')]
  [PrimaryJoinColumn('ID')]
  TComponentType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the type')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the type')]
    FDescription: Nullable<string>;
    
    [Column('VALIDATION', [], 255)]
    [Description('')]
    FValidation: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('')]
    FFormatMask: Nullable<string>;
    
    [Column('DATA_TYPE', [], 35)]
    [Description('')]
    FDataType: Nullable<string>;
    
    [Column('DATA_SIZE', [])]
    [Description('')]
    FDataSize: Nullable<Integer>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property Validation: Nullable<string> read FValidation write FValidation;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property DataType: Nullable<string> read FDataType write FDataType;
    property DataSize: Nullable<Integer> read FDataSize write FDataSize;
  end;
  
  [Entity]
  [Table('ENROLLMENT')]
  [Model('ENROLLMENT')]
  [Description('An Enrollment Record is an instance of a Program Offering that is tied to a Student, that is, it is the representation that a Student has enrolled in a Program Offering which has start and end dates.')]
  [PrimaryJoinColumn('ID')]
  TEnrollment = class(TEntityBase)
  private
    [Column('BLOCK', [], 10)]
    [Description('Block number to be used to keep track of Private Tutoring blocks.')]
    FBlock: Nullable<string>;
    
    [Column('NOTE', [], 1000)]
    [Description('Blob text field to record notes pertaining to the enrollment record.')]
    FNote: Nullable<string>;
    
    [Column('LEGACY_ID', [], 17)]
    [Description('')]
    FLegacyId: Nullable<string>;
    
    [Column('LEGACY_CODE', [], 35)]
    [Description('Legacy program code from the CDS.')]
    FLegacyCode: Nullable<string>;
    
    [Column('ORIGINAL_PRICE_PERIOD_ID', [TColumnProp.Required], 16)]
    [Description('')]
    FOriginalPricePeriodId: string;
    
    [Column('AGREE_AT', [])]
    [Description('Datetime that the enrollment agreement was signed, i.e. when the agreement was made.')]
    FAgreeAt: Nullable<TDateTime>;
    
    [Column('AGREE_START_AT', [])]
    [Description('Datetime originally agreed upon for the enrollment to start')]
    FAgreeStartAt: Nullable<TDateTime>;
    
    [Column('AGREE_END_AT', [])]
    [Description('Datetime originally agreed upon for the enrollment to end')]
    FAgreeEndAt: Nullable<TDateTime>;
    
    [Column('TERMINATE_AT', [])]
    [Description('Datetime the enrollment actually ended')]
    FTerminateAt: Nullable<TDateTime>;
    
    [Column('PROJECTED_END_AT', [])]
    [Description('Datetime used for calculation to project when an enrollment is going to end')]
    FProjectedEndAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItem: TItem;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_STUDENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRoleStudent: TPartyRoleStudent;
  public
    property Block: Nullable<string> read FBlock write FBlock;
    property Note: Nullable<string> read FNote write FNote;
    property LegacyId: Nullable<string> read FLegacyId write FLegacyId;
    property LegacyCode: Nullable<string> read FLegacyCode write FLegacyCode;
    property OriginalPricePeriodId: string read FOriginalPricePeriodId write FOriginalPricePeriodId;
    property AgreeAt: Nullable<TDateTime> read FAgreeAt write FAgreeAt;
    property AgreeStartAt: Nullable<TDateTime> read FAgreeStartAt write FAgreeStartAt;
    property AgreeEndAt: Nullable<TDateTime> read FAgreeEndAt write FAgreeEndAt;
    property TerminateAt: Nullable<TDateTime> read FTerminateAt write FTerminateAt;
    property ProjectedEndAt: Nullable<TDateTime> read FProjectedEndAt write FProjectedEndAt;
    property Item: TItem read FItem write FItem;
    property PartyRoleStudent: TPartyRoleStudent read FPartyRoleStudent write FPartyRoleStudent;
  end;
  
  [Entity]
  [Table('GENDER')]
  [Model('PARTY')]
  [Description('Stores the gender a Contact can be listed as. Useful mostly for the other information stored in this table which will help with email/mail merge fields (various gender-specific pronouns like she/he, her/him/ hers/his, herself/himself, Ms./Mr. etc.)')]
  [PrimaryJoinColumn('ID')]
  TGender = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Gender name: female, male, or neutral.')]
    FName: string;
    
    [Column('SUBJECT_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Subject pronoun: she, he, one.')]
    FSubjectPronoun: string;
    
    [Column('OBJECT_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Object pronoun: her, him, one.')]
    FObjectPronoun: string;
    
    [Column('POSSESSIVE_DET_PRONOUN', [], 10)]
    [Description('Possessive determiner pronoun: hers, his, ones.')]
    FPossessiveDetPronoun: Nullable<string>;
    
    [Column('POSSESSIVE_PRONOUN', [], 10)]
    [Description('Possessive pronoun: hers, his.')]
    FPossessivePronoun: Nullable<string>;
    
    [Column('REFLEXIVE_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Reflexive pronoun: herself, himself, oneself.')]
    FReflexivePronoun: string;
    
    [Column('TITLE', [], 5)]
    [Description('Title or prefix: Ms. and Mr.')]
    FTitle: Nullable<string>;
  public
    property Name: string read FName write FName;
    property SubjectPronoun: string read FSubjectPronoun write FSubjectPronoun;
    property ObjectPronoun: string read FObjectPronoun write FObjectPronoun;
    property PossessiveDetPronoun: Nullable<string> read FPossessiveDetPronoun write FPossessiveDetPronoun;
    property PossessivePronoun: Nullable<string> read FPossessivePronoun write FPossessivePronoun;
    property ReflexivePronoun: string read FReflexivePronoun write FReflexivePronoun;
    property Title: Nullable<string> read FTitle write FTitle;
  end;
  
  [Entity]
  [Table('GRADE')]
  [Model('PARTY_ROLE')]
  [Description('A table to store the grade a student can be listed as. For all grades above highschool, e.g. college or otherwise, the grade will simply be listed as "Adult" and its numeric value will be 13.')]
  [PrimaryJoinColumn('ID')]
  TGrade = class(TEntityBase)
  private
    [Column('SERVICE_LEVEL_ID', [], 16)]
    [Description('Foreign Key to the service level that the grade belongs to.')]
    FServiceLevelId: Nullable<string>;
    
    [Column('GRADE', [])]
    [Description('')]
    FGrade: Nullable<Integer>;
    
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the grade, e.g. 0 is Kindergarten, 13 is Adult.')]
    FName: string;
  public
    property ServiceLevelId: Nullable<string> read FServiceLevelId write FServiceLevelId;
    property Grade: Nullable<Integer> read FGrade write FGrade;
    property Name: string read FName write FName;
  end;
  
  [Entity]
  [Table('INVOICE')]
  [Model('INVOICE')]
  [Description('An Invoice is a summary of the invoice details it contains, which are generated from the order line items table as a way to flexibly partition the amount owed into manageable pieces or to be combined into one total amount due.')]
  [PrimaryJoinColumn('ID')]
  TInvoice = class(TEntityBase)
  private
    [Column('IS_VOIDED', [TColumnProp.Required], 1)]
    [Description('Boolean to show that the invoice has been voided. TO DO: Add a constraint that does not allow already booked invoices to be voided.')]
    FIsVoided: string;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the invoice.')]
    FNote: Nullable<string>;
    
    [Column('AMOUNT', [], 10, 2)]
    [Description('Filled by a calculation (sum the INVOICE_DETAIL_AMOUNT values for all related INVOICE_DETAIL records) whenever an INVOICE is created or modified. While its value could be calculated on demand, it is stored here for quick access.')]
    FAmount: Nullable<Double>;
    
    [Column('POS', [])]
    [Description('Integer index to indicate the order the INVOICE should be sorted by (useful when generating invoices for INSTALLMENT subscriptions)')]
    FPos: Nullable<Integer>;
    
    [Column('DUE_AT', [])]
    [Description('Datetime that the invoice is due')]
    FDueAt: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHANGE_REQUEST_ID', [], 'ID')]
    [Description('')]
    FChangeRequest: TChangeRequest;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSched: TInvoicingSched;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_CUSTOMER_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRoleCustomer: TPartyRoleCustomer;
  public
    property IsVoided: string read FIsVoided write FIsVoided;
    property Note: Nullable<string> read FNote write FNote;
    property Amount: Nullable<Double> read FAmount write FAmount;
    property Pos: Nullable<Integer> read FPos write FPos;
    property DueAt: Nullable<TDateTime> read FDueAt write FDueAt;
    property ChangeRequest: TChangeRequest read FChangeRequest write FChangeRequest;
    property InvoicingSched: TInvoicingSched read FInvoicingSched write FInvoicingSched;
    property PartyRoleCustomer: TPartyRoleCustomer read FPartyRoleCustomer write FPartyRoleCustomer;
  end;
  
  [Entity]
  [Table('INVOICE_DETAIL')]
  [Model('INVOICE')]
  [Description('Acts as an intermediate table between SALES_ORDER and INVOICE. Each record specifies how much of the related line item is being invoiced via the UNIT_COUNT, and specified how much is costs via UNIT_RATE and DIVISOR.')]
  [PrimaryJoinColumn('ID')]
  TInvoiceDetail = class(TEntityBase)
  private
    [Column('UNIT_RATE', [TColumnProp.Required], 10, 5)]
    [Description('Unit rate (price per unit if the item is a product or service, or value/percentage if the item is a modification item) This should be copied directly from the ITEM_RATE table, but editable here for manually overriding the price/rate of an item.')]
    FUnitRate: Double;
    
    [Column('DIVISOR', [TColumnProp.Required])]
    [Description('Determines whether the UNIT_RATE is a value (DIVISOR value of 1) or a percentage (DIVISOR value of 100), This should be copied directly from the ITEM_RATE table, but editable here for manually overriding the price/rate of an item.')]
    FDivisor: Integer;
    
    [Column('NOTE', [], 4000)]
    [Description('Notes about the invoice line. Useful in particular for explaining manual overrides to the unit count, rate, etc.')]
    FNote: Nullable<string>;
    
    [Column('POS', [])]
    [Description('The index of the detail item within the invoice--that is--the position that it appears amongst the other detail items.')]
    FPos: Nullable<Integer>;
    
    [Column('UNIT_COUNT', [], 10, 8)]
    [Description('Quantity of the item.')]
    FUnitCount: Nullable<Double>;
    
    [Column('AMOUNT', [], 10, 8)]
    [Description('This field is not a dynamically calculated field, but is filled by a calculation whenever an INVOICE is created or modified. While its value could be calculated on demand, it is stored here for quick access.')]
    FAmount: Nullable<Double>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoice: TInvoice;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_MOD_ID', [], 'ID')]
    [Description('')]
    FItemMod: TItemMod;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SALES_ORDER_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FSalesOrderDetail: TSalesOrderDetail;
  public
    property UnitRate: Double read FUnitRate write FUnitRate;
    property Divisor: Integer read FDivisor write FDivisor;
    property Note: Nullable<string> read FNote write FNote;
    property Pos: Nullable<Integer> read FPos write FPos;
    property UnitCount: Nullable<Double> read FUnitCount write FUnitCount;
    property Amount: Nullable<Double> read FAmount write FAmount;
    property Invoice: TInvoice read FInvoice write FInvoice;
    property ItemMod: TItemMod read FItemMod write FItemMod;
    property SalesOrderDetail: TSalesOrderDetail read FSalesOrderDetail write FSalesOrderDetail;
  end;
  
  [Entity]
  [Table('INVOICING_ENROLLMENT')]
  [Model('INVOICE')]
  [Description('Intermediary table between INVOICING_SCHED and ENROLLMENT for an N-to-N relationship between them, allowing for multiple enrollments to use the same invoicing schedule, or for one enrollment to be split between multiple invoicing schedules.')]
  [UniqueKey('INVOICING_SCHED_ID, ENROLLMENT_ID')]
  [PrimaryJoinColumn('ID')]
  TInvoicingEnrollment = class(TEntityBase)
  private
    [Column('START_AT', [])]
    [Description('')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('')]
    FEndAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ENROLLMENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FEnrollment: TEnrollment;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSched: TInvoicingSched;
  public
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
    property Enrollment: TEnrollment read FEnrollment write FEnrollment;
    property InvoicingSched: TInvoicingSched read FInvoicingSched write FInvoicingSched;
  end;
  
  [Entity]
  [Table('INVOICING_ITEM')]
  [Model('INVOICE')]
  [Description('Intermediary table between INVOICING_SCHED and ITEM to allow a many-to-many relationship between the two, allowing the default invoicing preferences for that Item are saved.')]
  [UniqueKey('INVOICING_SCHED_ID, ITEM_ID')]
  [PrimaryJoinColumn('ID')]
  TInvoicingItem = class(TEntityBase)
  private
    [Column('NOTE', [], 255)]
    [Description('Notes about the relationship between the INVOICING_SCHED record and the ITEM record')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSched: TInvoicingSched;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItem: TItem;
  public
    property Note: Nullable<string> read FNote write FNote;
    property InvoicingSched: TInvoicingSched read FInvoicingSched write FInvoicingSched;
    property Item: TItem read FItem write FItem;
  end;
  
  [Entity]
  [Table('INVOICING_PAYMENT_ACCOUNT')]
  [Model('INVOICE')]
  [Description('Connects INVOICING_SCHED and PAYMENT_ACCOUNT for an N-to-N relationship, allowing Invoicing sched to have multiple payment accounts (e.g. represent cc history), or a payment account with multiple invoicing sched (one cc paying for multiple enrollments).')]
  [PrimaryJoinColumn('ID')]
  TInvoicingPaymentAccount = class(TEntityBase)
  private
    [Column('START_AT', [])]
    [Description('Datetime marking the start of the range when the referenced Credit Card is being used by the referenced Invoicing Schedule.')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('Datetime marking the end of the range when the referenced Credit Card is being used by the referenced Invoicing Schedule.')]
    FEndAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSched: TInvoicingSched;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ACCOUNT_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPaymentAccountDetail: TPaymentAccountDetail;
  public
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
    property InvoicingSched: TInvoicingSched read FInvoicingSched write FInvoicingSched;
    property PaymentAccountDetail: TPaymentAccountDetail read FPaymentAccountDetail write FPaymentAccountDetail;
  end;
  
  [Entity]
  [Table('INVOICING_SCHED')]
  [Model('INVOICE')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TInvoicingSched = class(TEntityBase)
  private
    [Column('PERCENTAGE', [TColumnProp.Required], 10, 5)]
    [Description('Percentage of the amount due to invoice at a time, useful for splitting payments. 50% should be recorded as "50", not "0.5".')]
    FPercentage: Double;
    
    [Column('INITIAL_AMOUNT', [], 10, 2)]
    [Description('Initial amount charged when setting up a recurring invoicing schedule, this is calculated after the invoices are generated.')]
    FInitialAmount: Nullable<Double>;
    
    [Column('RECURRING_AMOUNT', [], 10, 2)]
    [Description('')]
    FRecurringAmount: Nullable<Double>;
    
    [Column('INSTALLMENT_COUNT', [])]
    [Description('Indiciates the number of installments, or total number of invoices, that the order details should be split between. Used only for "INSTALLMENT" invoicing')]
    FInstallmentCount: Nullable<Integer>;
    
    [Column('UNIT_COUNT_PER_CYCLE', [])]
    [Description('The number of units that make up a recurring cycle. For example, if CALENDAR_MONTH is indicated as the unit and the value of this field is 3, than the recurring cycle is 3 Calendar Months.')]
    FUnitCountPerCycle: Nullable<Integer>;
    
    [Column('DAY_OF_UNIT', [])]
    [Description('Integer to represent the day of the unit, for example if the unit is "CALENDAR_MONTH" than this number will represent the day of the month, if the unit is "CALENDAR_WEEK" than this unit will represent the day of the week, e.g. 4 = Wednesday.')]
    FDayOfUnit: Nullable<Integer>;
    
    [Column('CURRENCY', [], 20)]
    [Description('')]
    FCurrency: Nullable<string>;
    
    [Column('PAYMENT_METHOD', [], 20)]
    [Description('')]
    FPaymentMethod: Nullable<string>;
    
    [Column('COMBINE_ORDERS', [], 1)]
    [Description('Boolean flag to indicate whether to combine order details from multiple different orders or not, default to false.')]
    FCombineOrders: Nullable<string>;
    
    [Column('NOTE', [], 255)]
    [Description('')]
    FNote: Nullable<string>;
    
    [Column('SEPARATE_LIFETIME_UNITS', [], 1)]
    [Description('Boolean flag to indicate whether any LIFETIME units found in the sales order details should be separated from the regular invoicing or not.')]
    FSeparateLifetimeUnits: Nullable<string>;
    
    [Column('SUBSCRIPTION_ID', [], 30)]
    [Description('')]
    FSubscriptionId: Nullable<string>;
    
    [Column('START_AT', [])]
    [Description('')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('')]
    FEndAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSchedType: TInvoicingSchedType;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('UNIT_ID', [], 'ID')]
    [Description('')]
    FUnit_: TUnit;
  public
    property Percentage: Double read FPercentage write FPercentage;
    property InitialAmount: Nullable<Double> read FInitialAmount write FInitialAmount;
    property RecurringAmount: Nullable<Double> read FRecurringAmount write FRecurringAmount;
    property InstallmentCount: Nullable<Integer> read FInstallmentCount write FInstallmentCount;
    property UnitCountPerCycle: Nullable<Integer> read FUnitCountPerCycle write FUnitCountPerCycle;
    property DayOfUnit: Nullable<Integer> read FDayOfUnit write FDayOfUnit;
    property Currency: Nullable<string> read FCurrency write FCurrency;
    property PaymentMethod: Nullable<string> read FPaymentMethod write FPaymentMethod;
    property CombineOrders: Nullable<string> read FCombineOrders write FCombineOrders;
    property Note: Nullable<string> read FNote write FNote;
    property SeparateLifetimeUnits: Nullable<string> read FSeparateLifetimeUnits write FSeparateLifetimeUnits;
    property SubscriptionId: Nullable<string> read FSubscriptionId write FSubscriptionId;
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
    property InvoicingSchedType: TInvoicingSchedType read FInvoicingSchedType write FInvoicingSchedType;
    property Unit_: TUnit read FUnit_ write FUnit_;
  end;
  
  [Entity]
  [Table('INVOICING_SCHED_DETAIL')]
  [Model('INVOICE')]
  [Description('Tracks status of payments auto-scheduled by an invoicing schedule. Can be used to indicate if the scheduled payment is paid, voided, or what the billing status (including billing approval status) is.')]
  [PrimaryJoinColumn('ID')]
  TInvoicingSchedDetail = class(TEntityBase)
  private
    [Column('SCHEDULE_INDEX', [])]
    [Description('Synonymous with the payment number, e.g. the first payment auto-generated by an invoicing schedule would be 1, the second would be 2, and so on.')]
    FScheduleIndex: Nullable<Integer>;
    
    [Column('AUTO_SUGGEST', [], 100)]
    [Description('Automatically generated suggestion for what to do with the auto-generated payment, as well as any important notes, e.g. Approve, Confirm Return From Hold, Skip, Confirm Change and Approve, etc.')]
    FAutoSuggest: Nullable<string>;
    
    [Column('STATUS', [], 50)]
    [Description('Status options include "Voided", "Paid in Full", as well as proposed actions like "To Apply Credit", "To Skip", "To Change and Approve", "To Investigate" etc.')]
    FStatus: Nullable<string>;
    
    [Column('CONTENT_BLOB', [TColumnProp.Lazy], 80, 0)]
    [Description('Notes about the status, can be auto-generated (e.g. Paid in full by automapping CC payment) or manually input by user')]
    FContentBlob: TBlob;
    
    [Column('ACTION_REQUIRED', [], 50)]
    [Description('Indicates if any further action is required to finish processing the auto-payment, e.g. follow through with existing paperwork for late change requests, get credit card update, confirm receipt of cheque/cash payment, etc.')]
    FActionRequired: Nullable<string>;
    
    [Column('COMPLETE_BY', [], 35)]
    [Description('Person who completed the action required')]
    FCompleteBy: Nullable<string>;
    
    [Column('MIN_COUNT', [])]
    [Description('Allows override of MIN_COUNT parameter that is used in determing low-attendance students: acts as the least sessions attended to NOT receive a call.')]
    FMinCount: Nullable<Integer>;
    
    [Column('MAX_COUNT', [])]
    [Description('Allows override of MAX_COUNT parameter that is used in determing over-attendance students: acts as the most sessions attended to NOT receive a call.')]
    FMaxCount: Nullable<Integer>;
    
    [Column('SCHEDULE_AT', [])]
    [Description('')]
    FScheduleAt: Nullable<TDateTime>;
    
    [Column('COMPLETE_AT', [])]
    [Description('')]
    FCompleteAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICING_SCHED_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoicingSched: TInvoicingSched;
  public
    property ScheduleIndex: Nullable<Integer> read FScheduleIndex write FScheduleIndex;
    property AutoSuggest: Nullable<string> read FAutoSuggest write FAutoSuggest;
    property Status: Nullable<string> read FStatus write FStatus;
    property ContentBlob: TBlob read FContentBlob write FContentBlob;
    property ActionRequired: Nullable<string> read FActionRequired write FActionRequired;
    property CompleteBy: Nullable<string> read FCompleteBy write FCompleteBy;
    property MinCount: Nullable<Integer> read FMinCount write FMinCount;
    property MaxCount: Nullable<Integer> read FMaxCount write FMaxCount;
    property ScheduleAt: Nullable<TDateTime> read FScheduleAt write FScheduleAt;
    property CompleteAt: Nullable<TDateTime> read FCompleteAt write FCompleteAt;
    property InvoicingSched: TInvoicingSched read FInvoicingSched write FInvoicingSched;
  end;
  
  [Entity]
  [Table('INVOICING_SCHED_TYPE')]
  [Model('INVOICE')]
  [Description('Invoicing Schedule Type categorizes the two main types of invoicing schedules, RECURRING and INSTALLMENT.')]
  [PrimaryJoinColumn('ID')]
  TInvoicingSchedType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the type')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the type')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('ITEM')]
  [Model('ITEM')]
  [Description('Items are either services, products, or modifiers of those (e.g. discounts) and are subtyped as such. This supertype holds common information like the unit of measure, name, and description.')]
  [PrimaryJoinColumn('ID')]
  TItem = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the item.')]
    FName: string;
    
    [Column('ALIAS', [], 35)]
    [Description('')]
    FAlias: Nullable<string>;
    
    [Column('DESCRIPTION', [], 1000)]
    [Description('Human readable description of the item.')]
    FDescription: Nullable<string>;
    
    [Column('TERM_AND_CONDITION', [TColumnProp.Lazy], 80, 0)]
    [Description('Blob field containing the terms and conditions associated with the item.')]
    FTermAndCondition: TBlob;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('QB_ACCOUNT_ID', [], 'ID')]
    [Description('')]
    FQbAccount: TQbAccount;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('REPORTING_SCHED_ID', [], 'ID')]
    [Description('')]
    FReportingSched: TReportingSched;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('UNIT_ID', [], 'ID')]
    [Description('')]
    FUnit_: TUnit;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItemType: TItemType;
  public
    property Name: string read FName write FName;
    property Alias: Nullable<string> read FAlias write FAlias;
    property Description: Nullable<string> read FDescription write FDescription;
    property TermAndCondition: TBlob read FTermAndCondition write FTermAndCondition;
    property QbAccount: TQbAccount read FQbAccount write FQbAccount;
    property ReportingSched: TReportingSched read FReportingSched write FReportingSched;
    property Unit_: TUnit read FUnit_ write FUnit_;
    property ItemType: TItemType read FItemType write FItemType;
  end;
  
  [Entity]
  [Table('ITEM_RATE')]
  [Model('ITEM')]
  [Description('Intermediary table between ITEM and PRICE_PERIOD allowing a many to many relationship so that an item can be defined once and still have a different price or rate for each price period.')]
  [UniqueKey('ITEM_ID, PRICE_PERIOD_ID')]
  [PrimaryJoinColumn('ID')]
  TItemRate = class(TEntityBase)
  private
    [Column('LOCATION_ID', [], 16)]
    [Description('')]
    FLocationId: Nullable<string>;
    
    [Column('DIVISOR', [TColumnProp.Required])]
    [Description('')]
    FDivisor: Integer;
    
    [Column('UNIT_RATE', [TColumnProp.Required], 10, 5)]
    [Description('Unit rate for the item. This is called RATE instead of PRICE because discounts and other ITEMS_MODIFICATIONS can be percent based--in which case this field would hold that percentation (for a 10% discount the UNIT_RATE would be -10)')]
    FUnitRate: Double;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItem: TItem;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PRICE_PERIOD_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPricePeriod: TPricePeriod;
  public
    property LocationId: Nullable<string> read FLocationId write FLocationId;
    property Divisor: Integer read FDivisor write FDivisor;
    property UnitRate: Double read FUnitRate write FUnitRate;
    property Item: TItem read FItem write FItem;
    property PricePeriod: TPricePeriod read FPricePeriod write FPricePeriod;
  end;
  
  [Entity]
  [Table('ITEM_TYPE')]
  [Model('ITEM')]
  [Description('This table describes the subtypes that an ITEM can be. Each record in this table should correspond to a subtype table beginning with "ITEMS_".')]
  [PrimaryJoinColumn('ID')]
  TItemType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the item type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the item type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('LOCATOR')]
  [Model('ADDRESS')]
  [Description('This table is the intermediate or junction table between Parties and Addresses allowing a many to many relationship.')]
  [UniqueKey('PARTY_ID, ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TLocator = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Address: TAddress read FAddress write FAddress;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PARTICIPANT')]
  [Model('ACTIVITY')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TParticipant = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('')]
    FRelationship: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ACTIVITY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FActivity: TActivity;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTICIPANT_STATUS_ID', [], 'ID')]
    [Description('')]
    FParticipantStatus: TParticipantStatus;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRole: TPartyRole;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property Activity: TActivity read FActivity write FActivity;
    property ParticipantStatus: TParticipantStatus read FParticipantStatus write FParticipantStatus;
    property PartyRole: TPartyRole read FPartyRole write FPartyRole;
  end;
  
  [Entity]
  [Table('PARTY')]
  [Model('PARTY')]
  [Description('A party is either a person or account that is associated with the business in some way, This table is the supertype for the entity types PERSON and GROUP.')]
  [PrimaryJoinColumn('ID')]
  TParty = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyType: TPartyType;
  public
    property PartyType: TPartyType read FPartyType write FPartyType;
  end;
  
  [Entity]
  [Table('PARTY_HIERARCHY')]
  [Model('PARTY')]
  [Description('This table is the intermediate or junction table between Parties and itself, allowing a many to many relationship. e.g. one party could represent a family and the parties related to it are the members of that family.')]
  [UniqueKey('PARENT_PARTY_ID, CHILD_PARTY_ID')]
  [PrimaryJoinColumn('ID')]
  TPartyHierarchy = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('The type of relationship (e.g. Member, Assistant, Representative, etc), can be null to show loosely related parties. The type of relationship should be thought of as {Child} is a {Relationship} of {Parent}.')]
    FRelationship: Nullable<string>;
    
    [Column('IS_PRIMARY', [TColumnProp.Required], 1)]
    [Description('Indicates that given a series of other parties related to the parent party, this relationship should be considered the primary one. For exampe, each family account must have at least one primary contact.')]
    FIsPrimary: string;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the relationship between parties.')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHILD_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARENT_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentParty: TParty;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property IsPrimary: string read FIsPrimary write FIsPrimary;
    property Note: Nullable<string> read FNote write FNote;
    property ChildParty: TParty read FChildParty write FChildParty;
    property ParentParty: TParty read FParentParty write FParentParty;
  end;
  
  [Entity]
  [Table('PARTY_ROLE')]
  [Model('PARTY_ROLE')]
  [Description('This table is a supertype for all party roles. This table acts as the intermediary or joint table between parties and the roles that they can play.')]
  [PrimaryJoinColumn('ID')]
  TPartyRole = class(TEntityBase)
  private
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the the role this party is playing.')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyRoleType: TPartyRoleType;
  public
    property Note: Nullable<string> read FNote write FNote;
    property Party: TParty read FParty write FParty;
    property PartyRoleType: TPartyRoleType read FPartyRoleType write FPartyRoleType;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_TYPE')]
  [Model('PARTY_ROLE')]
  [Description('This table describes the various roles that a PARTY (either a person or account) can play. Each record in this table should correspond to a subtype table beginning with "PARTY_ROLE_".')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Primay Key and Name of the party role.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the party role.')]
    FDescription: Nullable<string>;
    
    [Column('IS_PERSON_ROLE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the entity role can be played by a person type party.')]
    FIsPersonRole: string;
    
    [Column('IS_GROUP_ROLE', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the entity role can be played by an account type party.')]
    FIsGroupRole: string;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property IsPersonRole: string read FIsPersonRole write FIsPersonRole;
    property IsGroupRole: string read FIsGroupRole write FIsGroupRole;
  end;
  
  [Entity]
  [Table('PARTY_TYPE')]
  [Model('PARTY')]
  [Description('This table describes the subtypes that a PARTY can be. Each record in this table should correspond to a subtype table beginning with "PARTIES_".')]
  [PrimaryJoinColumn('ID')]
  TPartyType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the party type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the party type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('PAY_INVOICE')]
  [Model('PAYMENT')]
  [Description('Intermediate table between PAYMENT and INVOICE allowing a many to many relationship. Additional to the relating a Payment to an Invoice, each record specifies how much of each payment is being applied to each invoice.')]
  [UniqueKey('PAYMENT_ID, INVOICE_ID')]
  [PrimaryJoinColumn('ID')]
  TPayInvoice = class(TEntityBase)
  private
    [Column('AMOUNT', [], 10, 2)]
    [Description('Amount of the payment being applied to the invoice.')]
    FAmount: Nullable<Double>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('INVOICE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FInvoice: TInvoice;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPayment: TPayment;
  public
    property Amount: Nullable<Double> read FAmount write FAmount;
    property Invoice: TInvoice read FInvoice write FInvoice;
    property Payment: TPayment read FPayment write FPayment;
  end;
  
  [Entity]
  [Table('PAYMENT')]
  [Model('PAYMENT')]
  [Description('A Payment fulfills an Invoice and is associated with a Party. A payment can consist of one to many payment details, which can be successful or unsuccessful and of varying payment methods.')]
  [PrimaryJoinColumn('ID')]
  TPayment = class(TEntityBase)
  private
    [Column('AMOUNT', [], 10, 2)]
    [Description('The amount of the payment (currency).')]
    FAmount: Nullable<Double>;
    
    [Column('NOTE', [], 300)]
    [Description('Human-readable notes regarding the payment.')]
    FNote: Nullable<string>;
    
    [Column('CDS_ID2_LIST', [], 300)]
    [Description('String list of the ID2 field from the legacy CDS (Centralized Data Spreadsheet), a unique identifier for enrollment records. This field may be a list as a single payment can apply to multiple enrollment records, used to help map historical records.')]
    FCdsId2List: Nullable<string>;
    
    [Column('STATUS', [], 20)]
    [Description('')]
    FStatus: Nullable<string>;
    
    [Column('APPLIED_AMOUNT', [TColumnProp.Required], 10, 2)]
    [Description('Amount of the payment being applied to the invoice.')]
    FAppliedAmount: Double;
    
    [Column('PROCESS_AT', [])]
    [Description('Datetime that the payment was processed')]
    FProcessAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Amount: Nullable<Double> read FAmount write FAmount;
    property Note: Nullable<string> read FNote write FNote;
    property CdsId2List: Nullable<string> read FCdsId2List write FCdsId2List;
    property Status: Nullable<string> read FStatus write FStatus;
    property AppliedAmount: Double read FAppliedAmount write FAppliedAmount;
    property ProcessAt: Nullable<TDateTime> read FProcessAt write FProcessAt;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT')]
  [Model('PAYMENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccount = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ACCOUNT_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPaymentAccountType: TPaymentAccountType;
  public
    property Party: TParty read FParty write FParty;
    property PaymentAccountType: TPaymentAccountType read FPaymentAccountType write FPaymentAccountType;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_DETAIL')]
  [Model('PAYMENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountDetail = class(TEntityBase)
  private
    [Column('FIRST_NAME_ON_CARD', [], 35)]
    [Description('')]
    FFirstNameOnCard: Nullable<string>;
    
    [Column('LAST_NAME_ON_CARD', [], 35)]
    [Description('')]
    FLastNameOnCard: Nullable<string>;
    
    [Column('EXPIRATION_DATE', [])]
    [Description('')]
    FExpirationDate: Nullable<TDateTime>;
    
    [Column('CVN', [], 10)]
    [Description('')]
    FCvn: Nullable<string>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ADDRESS_ID', [], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ACCOUNT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPaymentAccount: TPaymentAccount;
  public
    property FirstNameOnCard: Nullable<string> read FFirstNameOnCard write FFirstNameOnCard;
    property LastNameOnCard: Nullable<string> read FLastNameOnCard write FLastNameOnCard;
    property ExpirationDate: Nullable<TDateTime> read FExpirationDate write FExpirationDate;
    property Cvn: Nullable<string> read FCvn write FCvn;
    property Address: TAddress read FAddress write FAddress;
    property PaymentAccount: TPaymentAccount read FPaymentAccount write FPaymentAccount;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_MEMO_TYPE')]
  [Model('PAYMENT')]
  [Description('Describes the subtypes that a memo type transaction can be. Each record in this table should correspond to a subtype table beginning with "TXN_MEMO_"')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountMemoType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the TXN_MEMO type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the memo type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_TYPE')]
  [Model('PAYMENT')]
  [Description('This table describes the subtypes that a Payment account can be.')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the ECR type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the item type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('PAYMENT_DETAIL')]
  [Model('PAYMENT')]
  [Description('A single record of a monetary transfer through some payment method, can be successful or unsuccessful. This detail stores the transaction timestamp, amount, and reference to the related payment account to indicate the payment method type.')]
  [PrimaryJoinColumn('ID')]
  TPaymentDetail = class(TEntityBase)
  private
    [Column('PROCESS_AT', [])]
    [Description('Datetime that the transaction was processed')]
    FProcessAt: Nullable<TDateTime>;
    
    [Column('AMOUNT', [TColumnProp.Required], 10, 2)]
    [Description('Cash value of the transaction.')]
    FAmount: Double;
    
    [Column('ACTION_TYPE', [], 50)]
    [Description('')]
    FActionType: Nullable<string>;
    
    [Column('STATUS', [], 50)]
    [Description('')]
    FStatus: Nullable<string>;
    
    [Column('TRANSACTION_DETAIL', [], 255)]
    [Description('')]
    FTransactionDetail: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPayment: TPayment;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ACCOUNT_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPaymentAccountDetail: TPaymentAccountDetail;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_DETAIL_ID', [], 'ID')]
    [Description('')]
    FPaymentDetail: TPaymentDetail;
  public
    property ProcessAt: Nullable<TDateTime> read FProcessAt write FProcessAt;
    property Amount: Double read FAmount write FAmount;
    property ActionType: Nullable<string> read FActionType write FActionType;
    property Status: Nullable<string> read FStatus write FStatus;
    property TransactionDetail: Nullable<string> read FTransactionDetail write FTransactionDetail;
    property Payment: TPayment read FPayment write FPayment;
    property PaymentAccountDetail: TPaymentAccountDetail read FPaymentAccountDetail write FPaymentAccountDetail;
    property PaymentDetail: TPaymentDetail read FPaymentDetail write FPaymentDetail;
  end;
  
  [Entity]
  [Table('PRICE_PERIOD')]
  [Model('ITEM')]
  [Description('A time-period during which the pricing for a service or program is defined. This allows the active storage of old pricing, needed, for example, when the fees for a service must be increased while many customers remain in a contract with the old pricing.')]
  [PrimaryJoinColumn('ID')]
  TPricePeriod = class(TEntityBase)
  private
    [Column('NAME', [], 35)]
    [Description('Name of the price period, format is "PricePeriod_YYYYMM-YYYYMM", where the first YYYYMM is the year and month the price period begins, and the second is the year and month when the price period will end.')]
    FName: Nullable<string>;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('')]
    FDescription: Nullable<string>;
    
    [Column('START_AT', [])]
    [Description('Datetime to mark the start of the range that the price period is active for')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('Datetime to mark the end of the range that the price period is active for')]
    FEndAt: Nullable<TDateTime>;
  public
    property Name: Nullable<string> read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
  end;
  
  [Entity]
  [Table('QB_ACCOUNT')]
  [Model('ITEM')]
  [Description('Stores the various tiered levels of financial accounts needed for importing data into quickbooks.')]
  [PrimaryJoinColumn('ID')]
  TQbAccount = class(TEntityBase)
  private
    [Column('ACCOUNT_NUMBER', [])]
    [Description('')]
    FAccountNumber: Nullable<Integer>;
    
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('')]
    FName: string;
    
    [Column('QB_ACCOUNT_TYPE', [TColumnProp.Required], 35)]
    [Description('')]
    FQbAccountType: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('')]
    FDescription: Nullable<string>;
  public
    property AccountNumber: Nullable<Integer> read FAccountNumber write FAccountNumber;
    property Name: string read FName write FName;
    property QbAccountType: string read FQbAccountType write FQbAccountType;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('REPORTING_SCHED')]
  [Model('REVENUE_REPORT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TReportingSched = class(TEntityBase)
  private
    [Column('INSTALLMENT_COUNT', [])]
    [Description('Parameter to be used by INSTALLMENT reporting schedule types to indicate how many pieces the revenue should be broken up into')]
    FInstallmentCount: Nullable<Integer>;
    
    [Column('INSTALLMENT_SHIFT', [], 10)]
    [Description('Parameter to be used by INSTALLMENT reporting schedule types to indicate whether revenue should be shifted towards the LEFT, CENTER, or RIGHT of the date range')]
    FInstallmentShift: Nullable<string>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('UNIT_ID', [], 'ID')]
    [Description('')]
    FUnit_: TUnit;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('REPORTING_SCHED_TYPE_ID', [], 'ID')]
    [Description('')]
    FReportingSchedType: TReportingSchedType;
  public
    property InstallmentCount: Nullable<Integer> read FInstallmentCount write FInstallmentCount;
    property InstallmentShift: Nullable<string> read FInstallmentShift write FInstallmentShift;
    property Unit_: TUnit read FUnit_ write FUnit_;
    property ReportingSchedType: TReportingSchedType read FReportingSchedType write FReportingSchedType;
  end;
  
  [Entity]
  [Table('REPORTING_SCHED_TYPE')]
  [Model('REVENUE_REPORT')]
  [Description('Describes the different ways of reporting revenue for an ordered item')]
  [PrimaryJoinColumn('ID')]
  TReportingSchedType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the reporting type')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the reporting type')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('REVENUE_REPORT')]
  [Model('REVENUE_REPORT')]
  [Description('Records how revenue for each item ordered via the SALES_ORDER_DETAIL table is recognized. For services, this table can be filled via a report to the ATTENDANCE table. This table will be used to generate revenue reports for accrual accounting.')]
  [PrimaryJoinColumn('ID')]
  TRevenueReport = class(TEntityBase)
  private
    [Column('IS_CORRECTION', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the reporting record is actually just a correction for a previously incorrectly reported record, or if it is just a regular reporting record.')]
    FIsCorrection: string;
    
    [Column('IS_EXPIRED', [], 1)]
    [Description('Boolean flag to indicate whether the quantity is being reported for the line item because the items are expired (e.g. prepaid private tutoring expires after one year)')]
    FIsExpired: Nullable<string>;
    
    [Column('BOOK_UNIT_COUNT', [TColumnProp.Required], 10, 8)]
    [Description('The quantity being reported for the order line item')]
    FBookUnitCount: Double;
    
    [Column('BOOK_AMOUNT', [TColumnProp.Required], 10, 8)]
    [Description('The amount of revenue reported for the order line item')]
    FBookAmount: Double;
    
    [Column('REPORTING_NOTE', [], 1000)]
    [Description('Any notes regarding the order line item being reported. For example, if a correction is being added, the notes here can explain why.')]
    FReportingNote: Nullable<string>;
    
    [Column('BOOK_AT', [])]
    [Description('Datetime that the revenue and quanity for the order line is shown to be collected')]
    FBookAt: Nullable<TDateTime>;
    
    [Column('REPORT_AT', [])]
    [Description('Datetime that the revenue for the line was reported, i.e. the day the report including the revenue was submitted. Useful if a mistake is found during an audit to see if and when the revenue was wrongly reported so that a correction can be submitted.')]
    FReportAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_GROUP_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyGroup: TPartyGroup;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('QB_ACCOUNT_ID', [], 'ID')]
    [Description('')]
    FQbAccount: TQbAccount;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SALES_ORDER_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FSalesOrderDetail: TSalesOrderDetail;
  public
    property IsCorrection: string read FIsCorrection write FIsCorrection;
    property IsExpired: Nullable<string> read FIsExpired write FIsExpired;
    property BookUnitCount: Double read FBookUnitCount write FBookUnitCount;
    property BookAmount: Double read FBookAmount write FBookAmount;
    property ReportingNote: Nullable<string> read FReportingNote write FReportingNote;
    property BookAt: Nullable<TDateTime> read FBookAt write FBookAt;
    property ReportAt: Nullable<TDateTime> read FReportAt write FReportAt;
    property PartyGroup: TPartyGroup read FPartyGroup write FPartyGroup;
    property QbAccount: TQbAccount read FQbAccount write FQbAccount;
    property SalesOrderDetail: TSalesOrderDetail read FSalesOrderDetail write FSalesOrderDetail;
  end;
  
  [Entity]
  [Table('SALES_ORDER')]
  [Model('SALES_ORDER')]
  [Description('The record of an obligation, both for the customer to pay for the items listed in the order and for the provider to deliver these items to the customer. Each record relates to an ENROLLMENT, PARTY_ROLE_CUSTOMER, or a CHANGE_REQUEST member.')]
  [PrimaryJoinColumn('ID')]
  TSalesOrder = class(TEntityBase)
  private
    [Column('NOTE', [], 1000)]
    [Description('Notes regarding the order.')]
    FNote: Nullable<string>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CHANGE_REQUEST_ID', [], 'ID')]
    [Description('')]
    FChangeRequest: TChangeRequest;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ENROLLMENT_ID', [], 'ID')]
    [Description('')]
    FEnrollment: TEnrollment;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ROLE_CUSTOMER_ID', [], 'ID')]
    [Description('')]
    FPartyRoleCustomer: TPartyRoleCustomer;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PRICE_PERIOD_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPricePeriod: TPricePeriod;
  public
    property Note: Nullable<string> read FNote write FNote;
    property ChangeRequest: TChangeRequest read FChangeRequest write FChangeRequest;
    property Enrollment: TEnrollment read FEnrollment write FEnrollment;
    property PartyRoleCustomer: TPartyRoleCustomer read FPartyRoleCustomer write FPartyRoleCustomer;
    property PricePeriod: TPricePeriod read FPricePeriod write FPricePeriod;
  end;
  
  [Entity]
  [Table('SALES_ORDER_DETAIL')]
  [Model('SALES_ORDER')]
  [Description('A detail item of a service/product obligation, it links to a specific item that is being purchased. Detail items are combined under the parent SALES_ORDER.')]
  [PrimaryJoinColumn('ID')]
  TSalesOrderDetail = class(TEntityBase)
  private
    [Column('IS_CANCELLED', [TColumnProp.Required], 1)]
    [Description('')]
    FIsCancelled: string;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes about the order line. Useful in particular for explaining manual overrides to the order detail properties.')]
    FNote: Nullable<string>;
    
    [Column('INVOICE_NUMBER', [])]
    [Description('When this order detail is invoiced, which invoice will it be included in? One-Time fees should always be INVOICE_NUMBER = 0, with all following invoice numbers starting and 1,2,3,... Prepaid in full order details should all have an invoice number of 1.')]
    FInvoiceNumber: Nullable<Integer>;
    
    [Column('UNIT_COUNT', [], 10, 8)]
    [Description('Number of units being ordered.')]
    FUnitCount: Nullable<Double>;
    
    [Column('START_AT', [])]
    [Description('Datetime the order line was started.')]
    FStartAt: Nullable<TDateTime>;
    
    [Column('END_AT', [])]
    [Description('Datetime the order line was completed.')]
    FEndAt: Nullable<TDateTime>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItem: TItem;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('REPORTING_SCHED_ID', [], 'ID')]
    [Description('')]
    FReportingSched: TReportingSched;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SALES_ORDER_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FSalesOrder: TSalesOrder;
  public
    property IsCancelled: string read FIsCancelled write FIsCancelled;
    property Note: Nullable<string> read FNote write FNote;
    property InvoiceNumber: Nullable<Integer> read FInvoiceNumber write FInvoiceNumber;
    property UnitCount: Nullable<Double> read FUnitCount write FUnitCount;
    property StartAt: Nullable<TDateTime> read FStartAt write FStartAt;
    property EndAt: Nullable<TDateTime> read FEndAt write FEndAt;
    property Item: TItem read FItem write FItem;
    property ReportingSched: TReportingSched read FReportingSched write FReportingSched;
    property SalesOrder: TSalesOrder read FSalesOrder write FSalesOrder;
  end;
  
  [Entity]
  [Table('SALES_ORDER_DETAIL_MOD')]
  [Model('SALES_ORDER')]
  [Description('Each line item in SALES_ORDER_DETAIL can be related to zero to many ITEM_MODs (like discounts and taxes).')]
  [PrimaryJoinColumn('ID')]
  TSalesOrderDetailMod = class(TEntityBase)
  private
    [Column('POS', [])]
    [Description('The order in which modification items should be applied (only applies to modification items, e.g. which discount to apply first when multiple discounts are present).')]
    FPos: Nullable<Integer>;
    
    [Column('IS_OVERRIDE', [TColumnProp.Required], 1)]
    [Description('Boolean flag that indicates if the decision to apply the modification item to the associated sales order detail has been manually overriden, that is, any auto-updating software should not change the value in the "IS_APPLIED" field.')]
    FIsOverride: string;
    
    [Column('IS_APPLIED', [TColumnProp.Required], 1)]
    [Description('Boolean flag to indicate whether the modification should be applied or not. The criteria for the modification item should be evaluated upon first adding the record to this table and upon every enrollment change request that follows.')]
    FIsApplied: string;
    
    [Column('NOTE', [], 300)]
    [Description('Notes regarding the modification to the basket detail.')]
    FNote: Nullable<string>;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_MOD_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FItemMod: TItemMod;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SALES_ORDER_DETAIL_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FSalesOrderDetail: TSalesOrderDetail;
  public
    property Pos: Nullable<Integer> read FPos write FPos;
    property IsOverride: string read FIsOverride write FIsOverride;
    property IsApplied: string read FIsApplied write FIsApplied;
    property Note: Nullable<string> read FNote write FNote;
    property ItemMod: TItemMod read FItemMod write FItemMod;
    property SalesOrderDetail: TSalesOrderDetail read FSalesOrderDetail write FSalesOrderDetail;
  end;
  
  [Entity]
  [Table('SERVICE_CATEGORY')]
  [Model('ITEM')]
  [Description('Describes the different general service categories that are available--independent of how these services are priced or distributed. Currently includes records like CurriculumPlus Instruction, TutoringPlus Instruction, Private Tutoring Instruction, etc.')]
  [PrimaryJoinColumn('ID')]
  TServiceCategory = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the service category.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the service category.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('SERVICE_LEVEL')]
  [Model('ITEM')]
  [Description('Stores the service levels which roughly correspond to the age of a student. Students below 1st grade are Level 0, grades 2-4 are Level 1, 5-8 are Level 2, 9-12 are Level 3, and students that have graduated highschool are Level 4.')]
  [PrimaryJoinColumn('ID')]
  TServiceLevel = class(TEntityBase)
  private
    [Column('SERVICE_LEVEL', [])]
    [Description('')]
    FServiceLevel: Nullable<Integer>;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the service level.')]
    FDescription: Nullable<string>;
  public
    property ServiceLevel: Nullable<Integer> read FServiceLevel write FServiceLevel;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('UNIT')]
  [Model('ITEM')]
  [Description('Used by various other tables to describe what unit a record is partitioned into. Common units may be common standards (like time units: HOUR, DAY, WEEK, MONTH) or they can be more arbitraty and specific to our business needs (like SESSION or SINGLETON).')]
  [PrimaryJoinColumn('ID')]
  TUnit = class(TEntityBase)
  private
    [Column('NAME', [], 35)]
    [Description('Name of the unit.')]
    FName: Nullable<string>;
    
    [Column('UNIT_TYPE', [], 35)]
    [Description('Type of unit, e.g. TIME, LENGTH, CAPACITY, BUSINESS_CONSTRUCT, etc.')]
    FUnitType: Nullable<string>;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Human readable description of the unit.')]
    FDescription: Nullable<string>;
    
    [Column('LOGIC_LINE_COUNT', [], 1000)]
    [Description('SQL code to calculate the number of ORDER_DETAILS records are needed, given the total :unit_count and :start_date as parameters.')]
    FLogicLineCount: Nullable<string>;
    
    [Column('LOGIC_START_DATE', [], 1000)]
    [Description('SQL code to calculate the DATE_BEGAN value for each ORDER_DETAILS record that is generated, given :start_date and :id_calc_number (index/position) as parameters.')]
    FLogicStartDate: Nullable<string>;
    
    [Column('LOGIC_END_DATE', [], 1000)]
    [Description('SQL code to calculate the DATE_COMPLETED value for each ORDER_DETAILS record that is generated, given :start_date and :id_calc_number (index/position) as parameters.')]
    FLogicEndDate: Nullable<string>;
    
    [Column('LOGIC_UNIT_COUNT', [], 1000)]
    [Description('SQL code to calculate the UNIT_COUNT value for each ORDER_DETAILS record that is generated, given :start_date and :id_calc_number (index/position) as parameters.')]
    FLogicUnitCount: Nullable<string>;
  public
    property Name: Nullable<string> read FName write FName;
    property UnitType: Nullable<string> read FUnitType write FUnitType;
    property Description: Nullable<string> read FDescription write FDescription;
    property LogicLineCount: Nullable<string> read FLogicLineCount write FLogicLineCount;
    property LogicStartDate: Nullable<string> read FLogicStartDate write FLogicStartDate;
    property LogicEndDate: Nullable<string> read FLogicEndDate write FLogicEndDate;
    property LogicUnitCount: Nullable<string> read FLogicUnitCount write FLogicUnitCount;
  end;
  
  [Entity]
  [Table('CHANGE_CANCEL')]
  [Model('CHANGE_REQUEST')]
  [Description('When the enrollment record of a student is to be ended. If the end date is before a contract is complete, a cancellation fee may apply.')]
  [PrimaryJoinColumn('ID')]
  TChangeCancel = class(TChangeRequest)
  private
    [Column('IS_CONTRACT_COMPLETE', [TColumnProp.Required], 1)]
    [Description('Boolean flag indicating if the contract was completed when the cancellation was occured.')]
    FIsContractComplete: string;
    
    [Column('CANCELLATION_FEE', [], 10, 5)]
    [Description('Total amount of the Cancellation fee: this value is calculated in the INVOICE_DETAIL table and can later be placed for easy access here.')]
    FCancellationFee: Nullable<Double>;
    
    [Column('CANCEL_AT', [])]
    [Description('Datetime that the cancellation is effective (the date that should be reflected in the billing of the customer, used as the actual end date of the enrollment record).')]
    FCancelAt: Nullable<TDateTime>;
  public
    property IsContractComplete: string read FIsContractComplete write FIsContractComplete;
    property CancellationFee: Nullable<Double> read FCancellationFee write FCancellationFee;
    property CancelAt: Nullable<TDateTime> read FCancelAt write FCancelAt;
  end;
  
  [Entity]
  [Table('CHANGE_CREDIT')]
  [Model('CHANGE_REQUEST')]
  [Description('When a customer is owed something, either in the form of a cash refund, a credit towards an existing or upcoming balance, or a service in kind.')]
  [PrimaryJoinColumn('ID')]
  TChangeCredit = class(TChangeRequest)
  private
    [Column('ISSUE_TYPE', [], 100)]
    [Description('General category of why the credit was issued, common options are: Billing Error, Contract Dispute, Overpayment.')]
    FIssueType: Nullable<string>;
    
    [Column('CREDIT_TYPE', [], 50)]
    [Description('Indicates how the credit was given, whether as a cash amount that was applied to the customers account as "store credit", as a cash refund given back to the customer, or as a service in kind (e.g. extra sessions of private tutoring).')]
    FCreditType: Nullable<string>;
    
    [Column('AMOUNT', [], 10, 2)]
    [Description('Cash value of the credit.')]
    FAmount: Nullable<Double>;
    
    [Column('CREDIT_AT', [])]
    [Description('Datetime used to help identify the credit, usually the date that the credit was created.')]
    FCreditAt: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_DETAIL_ID', [], 'ID')]
    [Description('')]
    FPaymentDetail: TPaymentDetail;
  public
    property IssueType: Nullable<string> read FIssueType write FIssueType;
    property CreditType: Nullable<string> read FCreditType write FCreditType;
    property Amount: Nullable<Double> read FAmount write FAmount;
    property CreditAt: Nullable<TDateTime> read FCreditAt write FCreditAt;
    property PaymentDetail: TPaymentDetail read FPaymentDetail write FPaymentDetail;
  end;
  
  [Entity]
  [Table('CHANGE_HOLD')]
  [Model('CHANGE_REQUEST')]
  [Description('When a customer requests a membership to be "on hold" for some period of time. The date range during which the student will not attend as well as the date range during which they will not be billed are both stored, in case they are different.')]
  [PrimaryJoinColumn('ID')]
  TChangeHold = class(TChangeRequest)
  private
    [Column('ATTENDANCE_HOLD_START_AT', [])]
    [Description('Beginning of the datetime range during which the student will not be attending.')]
    FAttendanceHoldStartAt: Nullable<TDateTime>;
    
    [Column('ATTENDANCE_HOLD_END_AT', [])]
    [Description('End of the datetime range during which the student will not be attending.')]
    FAttendanceHoldEndAt: Nullable<TDateTime>;
    
    [Column('BILLING_HOLD_START_AT', [])]
    [Description('Beginning of the datetime range during which the student will not be billed.')]
    FBillingHoldStartAt: Nullable<TDateTime>;
    
    [Column('BILLING_HOLD_END_AT', [])]
    [Description('End of the datetime range during which the student will not be billed.')]
    FBillingHoldEndAt: Nullable<TDateTime>;
  public
    property AttendanceHoldStartAt: Nullable<TDateTime> read FAttendanceHoldStartAt write FAttendanceHoldStartAt;
    property AttendanceHoldEndAt: Nullable<TDateTime> read FAttendanceHoldEndAt write FAttendanceHoldEndAt;
    property BillingHoldStartAt: Nullable<TDateTime> read FBillingHoldStartAt write FBillingHoldStartAt;
    property BillingHoldEndAt: Nullable<TDateTime> read FBillingHoldEndAt write FBillingHoldEndAt;
  end;
  
  [Entity]
  [Table('CHANGE_INFO')]
  [Model('CHANGE_REQUEST')]
  [Description('An Info change request is when information for the given party is to be changed, e.g. new email address, correction of name spelling, request for DO NOT CONTACT, etc.')]
  [PrimaryJoinColumn('ID')]
  TChangeInfo = class(TChangeRequest)
  private
    [Column('FIELD_NAME', [], 35)]
    [Description('Name of the field to update for the referenced PARTY, e.g. FIRST_NAME, DO_NOT_CONTACT, etc.')]
    FFieldName: Nullable<string>;
    
    [Column('OLD_VALUE', [], 50)]
    [Description('50 character string to hold the old value of the field being changed.')]
    FOldValue: Nullable<string>;
    
    [Column('NEW_VALUE', [], 50)]
    [Description('50 character string to hold the old value of the field being changed.')]
    FNewValue: Nullable<string>;
    
    [Column('REPLACE_METHOD', [], 35)]
    [Description('Method of replacing the old value, options are "Replace All", "Replace Selected", "Add Additional"')]
    FReplaceMethod: Nullable<string>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('NEW_ADDRESS_ID', [], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('OLD_ADDRESS_ID', [], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ENTITY_BASE_ID', [], 'ID')]
    [Description('')]
    FEntityBase: TEntityBase;
  public
    property FieldName: Nullable<string> read FFieldName write FFieldName;
    property OldValue: Nullable<string> read FOldValue write FOldValue;
    property NewValue: Nullable<string> read FNewValue write FNewValue;
    property ReplaceMethod: Nullable<string> read FReplaceMethod write FReplaceMethod;
    property Address: TAddress read FAddress write FAddress;
    property Address: TAddress read FAddress write FAddress;
    property EntityBase: TEntityBase read FEntityBase write FEntityBase;
  end;
  
  [Entity]
  [Table('CHANGE_SWITCH')]
  [Model('CHANGE_REQUEST')]
  [Description('When a customer changes from one membership program to another, effectively cancelling the current enrollment and starting a new one.')]
  [PrimaryJoinColumn('ID')]
  TChangeSwitch = class(TChangeRequest)
  private
    [Column('OLD_ENROLLMENT_END_AT', [])]
    [Description('Datetime that the students current enrollment record is to end (same purpose as the CHANGE_CANCEL cancel_at). To be used as the Actual End Date of the enrollment record.')]
    FOldEnrollmentEndAt: Nullable<TDateTime>;
    
    [Column('NEW_ENROLLMENT_START_AT', [])]
    [Description('Datetime that the student is to start their new enrollment. To be used as the Start Date of the new enrollment record.')]
    FNewEnrollmentStartAt: Nullable<TDateTime>;
    
    [Column('NEW_ENROLLMENT_END_AT', [])]
    [Description('Datetime that projects the end of the new enrollment the student is switching to.')]
    FNewEnrollmentEndAt: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ENROLLMENT_ID', [], 'ID')]
    [Description('')]
    FEnrollment: TEnrollment;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('ITEM_ID', [], 'ID')]
    [Description('')]
    FItem: TItem;
  public
    property OldEnrollmentEndAt: Nullable<TDateTime> read FOldEnrollmentEndAt write FOldEnrollmentEndAt;
    property NewEnrollmentStartAt: Nullable<TDateTime> read FNewEnrollmentStartAt write FNewEnrollmentStartAt;
    property NewEnrollmentEndAt: Nullable<TDateTime> read FNewEnrollmentEndAt write FNewEnrollmentEndAt;
    property Enrollment: TEnrollment read FEnrollment write FEnrollment;
    property Item: TItem read FItem write FItem;
  end;
  
  [Entity]
  [Table('ITEM_MOD')]
  [Model('ITEM')]
  [Description('A modification item is not a thing that can be sold to customers by itself, rather it modifies a service or item being sold to a customer. For example, discounts and taxes modify item prices.')]
  [PrimaryJoinColumn('ID')]
  TItemMod = class(TItem)
  private
    [Column('MOD_USE_COUNT', [])]
    [Description('DRAFT FIELD: Maximum number of times that a modification can be used.')]
    FModUseCount: Nullable<Integer>;
    
    [Column('MOD_BLOB', [TColumnProp.Lazy], 80, 0)]
    [Description('DRAFT FIELD: BLOB field to hold the code/logic to apply a complex modification.')]
    FModBlob: TBlob;
  public
    property ModUseCount: Nullable<Integer> read FModUseCount write FModUseCount;
    property ModBlob: TBlob read FModBlob write FModBlob;
  end;
  
  [Entity]
  [Table('ITEM_PRODUCT')]
  [Model('ITEM')]
  [Description('A product item is a physical inventory item that can be sold to customers or used in the centers.')]
  [PrimaryJoinColumn('ID')]
  TItemProduct = class(TItem)
  private
    [Column('INVENTORY_ID', [], 16)]
    [Description('Number of items that are currently in stock.')]
    FInventoryId: Nullable<string>;
  public
    property InventoryId: Nullable<string> read FInventoryId write FInventoryId;
  end;
  
  [Entity]
  [Table('ITEM_SERVICE')]
  [Model('ITEM')]
  [Description('A service item is the description of a service to be offerred to a customer. It holds information specific to the service, like the frequency, whether it is prorateable, units per report, unit count. etc. It is a subtype of the supertype table: ITEMS.')]
  [PrimaryJoinColumn('ID')]
  TItemService = class(TItem)
  private
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SERVICE_CATEGORY_ID', [], 'ID')]
    [Description('')]
    FServiceCategory: TServiceCategory;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('SERVICE_LEVEL_ID', [], 'ID')]
    [Description('')]
    FServiceLevel: TServiceLevel;
  public
    property ServiceCategory: TServiceCategory read FServiceCategory write FServiceCategory;
    property ServiceLevel: TServiceLevel read FServiceLevel write FServiceLevel;
  end;
  
  [Entity]
  [Table('PARTY_GROUP')]
  [Model('PARTY')]
  [Description('A group is a party that, like a person, can be associated with multiple addresses as well as other parties. A group can be a Family, a Mathnasium learning center, an Organization, etc.')]
  [PrimaryJoinColumn('ID')]
  TPartyGroup = class(TParty)
  private
    [Column('NAME', [], 75)]
    [Description('Name of the account.')]
    FName: Nullable<string>;
    
    [Column('ALIAS', [], 35)]
    [Description('Alias or alternative name for the account.')]
    FAlias: Nullable<string>;
    
    [Column('GROUP_TYPE', [], 50)]
    [Description('Account type, e.g. Organization, Family, Learning Center, Office, etc.')]
    FGroupType: Nullable<string>;
    
    [Column('IS_FOR_PROFIT', [], 1)]
    [Description('Boolean flag to indicate whether the organization is a for profit (true) or a not-for-profirt (false).')]
    FIsForProfit: Nullable<string>;
    
    [Column('NOTE', [], 1000)]
    [Description('Notes concerning the account.')]
    FNote: Nullable<string>;
  public
    property Name: Nullable<string> read FName write FName;
    property Alias: Nullable<string> read FAlias write FAlias;
    property GroupType: Nullable<string> read FGroupType write FGroupType;
    property IsForProfit: Nullable<string> read FIsForProfit write FIsForProfit;
    property Note: Nullable<string> read FNote write FNote;
  end;
  
  [Entity]
  [Table('PARTY_PERSON')]
  [Model('PARTY')]
  [Description('This table simply stores the names of a person (first, last, middle, nickname, prefix, suffix), gender, birthday, and medical info.')]
  [PrimaryJoinColumn('ID')]
  TPartyPerson = class(TParty)
  private
    [Column('PREFIX', [], 10)]
    [Description('Prefix of the person, currently these are only Mr. and Ms., for which only Adults have values--however this should likely be changed to options like Dr. or Sir., since the values of Mr. and Ms. can be implied from the gender table.')]
    FPrefix: Nullable<string>;
    
    [Column('FIRST_NAME', [], 35)]
    [Description('First name of the person (Nicknames do not belong here, as there is a separate field for that).')]
    FFirstName: Nullable<string>;
    
    [Column('MIDDLE_NAMES', [], 35)]
    [Description('Middle names of the person, this field can represent a single middle initial, or simply all names between the first name and last name of the person, which, when combined, should result in the persons full name.')]
    FMiddleNames: Nullable<string>;
    
    [Column('LAST_NAME', [], 35)]
    [Description('Last name, surname, or family name of the person. Should not include any suffix like Jr. or Sr., as there is a suffix field for that.')]
    FLastName: Nullable<string>;
    
    [Column('SUFFIX', [], 10)]
    [Description('Suffix of the person, like Sr., Jr., III, etc. TO DO: Standardize these options, if a father is the Sr and the son is Jr, should this field say Jr an Sr, or should it say I and II? Or First and Second?')]
    FSuffix: Nullable<string>;
    
    [Column('NICK_NAME', [], 35)]
    [Description('Nick name of the person, should default to First Name but allow changes made to it. This should be the name that the person prefers to be called (e.g. Nina instead of Cristina, Apple instead of Appolonia, Billy instead of William, etc.)')]
    FNickName: Nullable<string>;
    
    [Column('MEDICAL_INFO', [], 255)]
    [Description('Any imporant medical information regarding the person, like severe allergies, heart conditions, anything requiring special care or medication, etc.')]
    FMedicalInfo: Nullable<string>;
    
    [Column('DO_NOT_CONTACT', [TColumnProp.Required], 1)]
    [Description('A boolean flag indicating that the person does not wish to be contacted. If a single entity in an account is marked as DO_NOT_CONTACT, then all members of the account should be considered DO_NOT_CONTACT.')]
    FDoNotContact: string;
    
    [Column('BIRTH_DAY', [])]
    [Description('')]
    FBirthDay: Nullable<Integer>;
    
    [Column('BIRTH_MONTH', [])]
    [Description('')]
    FBirthMonth: Nullable<Integer>;
    
    [Column('BIRTH_YEAR', [])]
    [Description('')]
    FBirthYear: Nullable<Integer>;
    
    [Column('BIRTH_DATE', [])]
    [Description('')]
    FBirthDate: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('GENDER_ID', [], 'ID')]
    [Description('')]
    FGender: TGender;
  public
    property Prefix: Nullable<string> read FPrefix write FPrefix;
    property FirstName: Nullable<string> read FFirstName write FFirstName;
    property MiddleNames: Nullable<string> read FMiddleNames write FMiddleNames;
    property LastName: Nullable<string> read FLastName write FLastName;
    property Suffix: Nullable<string> read FSuffix write FSuffix;
    property NickName: Nullable<string> read FNickName write FNickName;
    property MedicalInfo: Nullable<string> read FMedicalInfo write FMedicalInfo;
    property DoNotContact: string read FDoNotContact write FDoNotContact;
    property BirthDay: Nullable<Integer> read FBirthDay write FBirthDay;
    property BirthMonth: Nullable<Integer> read FBirthMonth write FBirthMonth;
    property BirthYear: Nullable<Integer> read FBirthYear write FBirthYear;
    property BirthDate: Nullable<TDateTime> read FBirthDate write FBirthDate;
    property Gender: TGender read FGender write FGender;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_CUSTOMER')]
  [Model('PARTY_ROLE')]
  [Description('The role of a person who is receiving a service. Customers make payments on services being received, but a person can still play the role of a customer even if they are not the bill payer.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleCustomer = class(TPartyRole)
  end;
  
  [Entity]
  [Table('PARTY_ROLE_GUARDIAN')]
  [Model('PARTY_ROLE')]
  [Description('The the role of a person who is partially or fully responsible for another person. Parents, Legal Guardians, Family Members, Nannies, Emergency Contacts, and so on are all examples of guardians.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleGuardian = class(TPartyRole)
  private
    [Column('RELATIONSHIP', [TColumnProp.Required], 35)]
    [Description('How this Guardian is related to the Party who has them as their guardian. For example, if the referenced party is the granddaughter of this guardian, then this relationship field should read "Grandmother".')]
    FRelationship: string;
    
    [Column('IS_EMERGENCY_CONTACT', [TColumnProp.Required], 1)]
    [Description('')]
    FIsEmergencyContact: string;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Relationship: string read FRelationship write FRelationship;
    property IsEmergencyContact: string read FIsEmergencyContact write FIsEmergencyContact;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_LEAD')]
  [Model('PARTY_ROLE')]
  [Description('A Lead is the role of a person who is a prospective customer. All members of a family (guardians & students) have the role of being a lead.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleLead = class(TPartyRole)
  private
    [Column('CONVERT_AT', [])]
    [Description('Datetime that the lead/prospective was converted')]
    FConvertAt: Nullable<TDateTime>;
    
    [Column('INITIAL_CONTACT_AT', [])]
    [Description('Date that the lead initially contacted us')]
    FInitialContactAt: Nullable<TDateTime>;
  public
    property ConvertAt: Nullable<TDateTime> read FConvertAt write FConvertAt;
    property InitialContactAt: Nullable<TDateTime> read FInitialContactAt write FInitialContactAt;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_LEARNING_CENTER')]
  [Model('PARTY_ROLE')]
  [Description('A Learning Center is a role of a venue that acts as a space for instructors to use Mathnasium methods to instruct students.')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleLearningCenter = class(TPartyRole)
  private
    [Column('STUDENT_NUMBER_PREFIX', [TColumnProp.Required], 1)]
    [Description('')]
    FStudentNumberPrefix: string;
    
    [Column('NEXT_STUDENT_NUMBER', [TColumnProp.Required])]
    [Description('')]
    FNextStudentNumber: Integer;
  public
    property StudentNumberPrefix: string read FStudentNumberPrefix write FStudentNumberPrefix;
    property NextStudentNumber: Integer read FNextStudentNumber write FNextStudentNumber;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_STAFF')]
  [Model('PARTY_ROLE')]
  [Description('A staff member is the role of a person who receives wages/salary. For example, all instructors and administrators have the role of staff')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleStaff = class(TPartyRole)
  private
    [Column('HIRE_AT', [])]
    [Description('Datetime that the staff member was hired')]
    FHireAt: Nullable<TDateTime>;
  public
    property HireAt: Nullable<TDateTime> read FHireAt write FHireAt;
  end;
  
  [Entity]
  [Table('PARTY_ROLE_STUDENT')]
  [Model('PARTY_ROLE')]
  [Description('A Student is the role of a person who is receiving educational services, that is, one who "enrolls" in a "program offering".')]
  [UniqueKey('STUDENT_NUMBER')]
  [PrimaryJoinColumn('ID')]
  TPartyRoleStudent = class(TPartyRole)
  private
    [Column('STUDENT_NUMBER', [], 12)]
    [Description('Legacy field representing the student number, which begins with a letter indicating the center location the student attends followed by a 5 digit number which increments as students enroll.')]
    FStudentNumber: Nullable<string>;
    
    [Column('LIVES_WITH', [], 35)]
    [Description('Indicates any notable living situations, like if the student lives with only one parent or switches between different parent homes.')]
    FLivesWith: Nullable<string>;
    
    [Column('CONSENT_CONTACT_SCHOOL', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to contact the students school or teacher.')]
    FConsentContactSchool: Nullable<string>;
    
    [Column('CONSENT_LEAVE', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to allow the student to leave the center unattended.')]
    FConsentLeave: Nullable<string>;
    
    [Column('CONSENT_PHOTOGRAPH', [], 1)]
    [Description('Boolean flag indicating whether permission has been granted to photograph or film the student while they are in the center.')]
    FConsentPhotograph: Nullable<string>;
    
    [Column('SCHOOL_ENTRY_DATE', [])]
    [Description('This date should always be September 1 of the year that the student entered Kindergarten (grade 0). To determine the current grade, determine the year difference between this date and the current date, just as you would determine age from birthdate.')]
    FSchoolEntryDate: Nullable<TDateTime>;
    
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('GRADE_ID', [], 'ID')]
    [Description('')]
    FGrade: TGrade;
  public
    property StudentNumber: Nullable<string> read FStudentNumber write FStudentNumber;
    property LivesWith: Nullable<string> read FLivesWith write FLivesWith;
    property ConsentContactSchool: Nullable<string> read FConsentContactSchool write FConsentContactSchool;
    property ConsentLeave: Nullable<string> read FConsentLeave write FConsentLeave;
    property ConsentPhotograph: Nullable<string> read FConsentPhotograph write FConsentPhotograph;
    property SchoolEntryDate: Nullable<TDateTime> read FSchoolEntryDate write FSchoolEntryDate;
    property Grade: TGrade read FGrade write FGrade;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_CASH')]
  [Model('PAYMENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountCash = class(TPaymentAccount)
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_CC')]
  [Model('PAYMENT')]
  [Description('Each record represents an encrypted credit card number related to a PARTY record. Details like the billing address, name on card, expiration date, etc. are stored in PAYMENT_ACCOUNT_DETAIL.')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountCc = class(TPaymentAccount)
  private
    [Column('AUTO_BILL_DAY', [])]
    [Description('Preferred day of the month for auto-billing this credit card, must be an integer between 1 and 28.')]
    FAutoBillDay: Nullable<Integer>;
    
    [Column('CC_NUMBER', [], 50)]
    [Description('')]
    FCcNumber: Nullable<string>;
    
    [Column('LAST_FOUR', [], 4)]
    [Description('')]
    FLastFour: Nullable<string>;
    
    [Column('CARD_TYPE', [], 35)]
    [Description('')]
    FCardType: Nullable<string>;
  public
    property AutoBillDay: Nullable<Integer> read FAutoBillDay write FAutoBillDay;
    property CcNumber: Nullable<string> read FCcNumber write FCcNumber;
    property LastFour: Nullable<string> read FLastFour write FLastFour;
    property CardType: Nullable<string> read FCardType write FCardType;
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_CHEQUE')]
  [Model('PAYMENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountCheque = class(TPaymentAccount)
  end;
  
  [Entity]
  [Table('PAYMENT_ACCOUNT_MEMO')]
  [Model('PAYMENT')]
  [Description('')]
  [PrimaryJoinColumn('ID')]
  TPaymentAccountMemo = class(TPaymentAccount)
  private
    [Association([], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('CERTIFICATE_ID', [], 'ID')]
    [Description('')]
    FCertificate: TCertificate;
    
    [Association([TAssociationProp.Required], CascadeTypeAll - [TCascadeType.Remove])]
    [JoinColumn('PAYMENT_ACCOUNT_MEMO_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPaymentAccountMemoType: TPaymentAccountMemoType;
  public
    property Certificate: TCertificate read FCertificate write FCertificate;
    property PaymentAccountMemoType: TPaymentAccountMemoType read FPaymentAccountMemoType write FPaymentAccountMemoType;
  end;
  
  TDicDictionary = class
  private
    FActivity: TActivityTableDictionary;
    FActivityHierarchy: TActivityHierarchyTableDictionary;
    FActivityType: TActivityTypeTableDictionary;
    FAddress: TAddressTableDictionary;
    FAddressHierarchy: TAddressHierarchyTableDictionary;
    FAddressType: TAddressTypeTableDictionary;
    FAttendOrder: TAttendOrderTableDictionary;
    FCertificate: TCertificateTableDictionary;
    FChangeCancel: TChangeCancelTableDictionary;
    FChangeCredit: TChangeCreditTableDictionary;
    FChangeHold: TChangeHoldTableDictionary;
    FChangeInfo: TChangeInfoTableDictionary;
    FChangeReason: TChangeReasonTableDictionary;
    FChangeRequest: TChangeRequestTableDictionary;
    FChangeRequestType: TChangeRequestTypeTableDictionary;
    FChangeSwitch: TChangeSwitchTableDictionary;
    FComponent: TComponentTableDictionary;
    FComponentType: TComponentTypeTableDictionary;
    FEnrollment: TEnrollmentTableDictionary;
    FEntityBase: TEntityBaseTableDictionary;
    FGender: TGenderTableDictionary;
    FGrade: TGradeTableDictionary;
    FInvoice: TInvoiceTableDictionary;
    FInvoiceDetail: TInvoiceDetailTableDictionary;
    FInvoicingEnrollment: TInvoicingEnrollmentTableDictionary;
    FInvoicingItem: TInvoicingItemTableDictionary;
    FInvoicingPaymentAccount: TInvoicingPaymentAccountTableDictionary;
    FInvoicingSched: TInvoicingSchedTableDictionary;
    FInvoicingSchedDetail: TInvoicingSchedDetailTableDictionary;
    FInvoicingSchedType: TInvoicingSchedTypeTableDictionary;
    FItem: TItemTableDictionary;
    FItemHierarchy: TItemHierarchyTableDictionary;
    FItemMod: TItemModTableDictionary;
    FItemProduct: TItemProductTableDictionary;
    FItemRate: TItemRateTableDictionary;
    FItemService: TItemServiceTableDictionary;
    FItemType: TItemTypeTableDictionary;
    FLocator: TLocatorTableDictionary;
    FParticipant: TParticipantTableDictionary;
    FParticipantStatus: TParticipantStatusTableDictionary;
    FParty: TPartyTableDictionary;
    FPartyGroup: TPartyGroupTableDictionary;
    FPartyHierarchy: TPartyHierarchyTableDictionary;
    FPartyPerson: TPartyPersonTableDictionary;
    FPartyRole: TPartyRoleTableDictionary;
    FPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
    FPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
    FPartyRoleLead: TPartyRoleLeadTableDictionary;
    FPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
    FPartyRoleStaff: TPartyRoleStaffTableDictionary;
    FPartyRoleStudent: TPartyRoleStudentTableDictionary;
    FPartyRoleType: TPartyRoleTypeTableDictionary;
    FPartyType: TPartyTypeTableDictionary;
    FPayment: TPaymentTableDictionary;
    FPaymentAccount: TPaymentAccountTableDictionary;
    FPaymentAccountCash: TPaymentAccountCashTableDictionary;
    FPaymentAccountCc: TPaymentAccountCcTableDictionary;
    FPaymentAccountCheque: TPaymentAccountChequeTableDictionary;
    FPaymentAccountDetail: TPaymentAccountDetailTableDictionary;
    FPaymentAccountMemo: TPaymentAccountMemoTableDictionary;
    FPaymentAccountMemoType: TPaymentAccountMemoTypeTableDictionary;
    FPaymentAccountType: TPaymentAccountTypeTableDictionary;
    FPaymentDetail: TPaymentDetailTableDictionary;
    FPayInvoice: TPayInvoiceTableDictionary;
    FPricePeriod: TPricePeriodTableDictionary;
    FQbAccount: TQbAccountTableDictionary;
    FReportingSched: TReportingSchedTableDictionary;
    FReportingSchedType: TReportingSchedTypeTableDictionary;
    FRevenueReport: TRevenueReportTableDictionary;
    FSalesOrder: TSalesOrderTableDictionary;
    FSalesOrderDetail: TSalesOrderDetailTableDictionary;
    FSalesOrderDetailMod: TSalesOrderDetailModTableDictionary;
    FServiceCategory: TServiceCategoryTableDictionary;
    FServiceLevel: TServiceLevelTableDictionary;
    FUnit: TUnitTableDictionary;
    FComponentHierarchy: TComponentHierarchyTableDictionary;
    function GetActivity: TActivityTableDictionary;
    function GetActivityHierarchy: TActivityHierarchyTableDictionary;
    function GetActivityType: TActivityTypeTableDictionary;
    function GetAddress: TAddressTableDictionary;
    function GetAddressHierarchy: TAddressHierarchyTableDictionary;
    function GetAddressType: TAddressTypeTableDictionary;
    function GetAttendOrder: TAttendOrderTableDictionary;
    function GetCertificate: TCertificateTableDictionary;
    function GetChangeCancel: TChangeCancelTableDictionary;
    function GetChangeCredit: TChangeCreditTableDictionary;
    function GetChangeHold: TChangeHoldTableDictionary;
    function GetChangeInfo: TChangeInfoTableDictionary;
    function GetChangeReason: TChangeReasonTableDictionary;
    function GetChangeRequest: TChangeRequestTableDictionary;
    function GetChangeRequestType: TChangeRequestTypeTableDictionary;
    function GetChangeSwitch: TChangeSwitchTableDictionary;
    function GetComponent: TComponentTableDictionary;
    function GetComponentType: TComponentTypeTableDictionary;
    function GetEnrollment: TEnrollmentTableDictionary;
    function GetEntityBase: TEntityBaseTableDictionary;
    function GetGender: TGenderTableDictionary;
    function GetGrade: TGradeTableDictionary;
    function GetInvoice: TInvoiceTableDictionary;
    function GetInvoiceDetail: TInvoiceDetailTableDictionary;
    function GetInvoicingEnrollment: TInvoicingEnrollmentTableDictionary;
    function GetInvoicingItem: TInvoicingItemTableDictionary;
    function GetInvoicingPaymentAccount: TInvoicingPaymentAccountTableDictionary;
    function GetInvoicingSched: TInvoicingSchedTableDictionary;
    function GetInvoicingSchedDetail: TInvoicingSchedDetailTableDictionary;
    function GetInvoicingSchedType: TInvoicingSchedTypeTableDictionary;
    function GetItem: TItemTableDictionary;
    function GetItemHierarchy: TItemHierarchyTableDictionary;
    function GetItemMod: TItemModTableDictionary;
    function GetItemProduct: TItemProductTableDictionary;
    function GetItemRate: TItemRateTableDictionary;
    function GetItemService: TItemServiceTableDictionary;
    function GetItemType: TItemTypeTableDictionary;
    function GetLocator: TLocatorTableDictionary;
    function GetParticipant: TParticipantTableDictionary;
    function GetParticipantStatus: TParticipantStatusTableDictionary;
    function GetParty: TPartyTableDictionary;
    function GetPartyGroup: TPartyGroupTableDictionary;
    function GetPartyHierarchy: TPartyHierarchyTableDictionary;
    function GetPartyPerson: TPartyPersonTableDictionary;
    function GetPartyRole: TPartyRoleTableDictionary;
    function GetPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
    function GetPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
    function GetPartyRoleLead: TPartyRoleLeadTableDictionary;
    function GetPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
    function GetPartyRoleStaff: TPartyRoleStaffTableDictionary;
    function GetPartyRoleStudent: TPartyRoleStudentTableDictionary;
    function GetPartyRoleType: TPartyRoleTypeTableDictionary;
    function GetPartyType: TPartyTypeTableDictionary;
    function GetPayment: TPaymentTableDictionary;
    function GetPaymentAccount: TPaymentAccountTableDictionary;
    function GetPaymentAccountCash: TPaymentAccountCashTableDictionary;
    function GetPaymentAccountCc: TPaymentAccountCcTableDictionary;
    function GetPaymentAccountCheque: TPaymentAccountChequeTableDictionary;
    function GetPaymentAccountDetail: TPaymentAccountDetailTableDictionary;
    function GetPaymentAccountMemo: TPaymentAccountMemoTableDictionary;
    function GetPaymentAccountMemoType: TPaymentAccountMemoTypeTableDictionary;
    function GetPaymentAccountType: TPaymentAccountTypeTableDictionary;
    function GetPaymentDetail: TPaymentDetailTableDictionary;
    function GetPayInvoice: TPayInvoiceTableDictionary;
    function GetPricePeriod: TPricePeriodTableDictionary;
    function GetQbAccount: TQbAccountTableDictionary;
    function GetReportingSched: TReportingSchedTableDictionary;
    function GetReportingSchedType: TReportingSchedTypeTableDictionary;
    function GetRevenueReport: TRevenueReportTableDictionary;
    function GetSalesOrder: TSalesOrderTableDictionary;
    function GetSalesOrderDetail: TSalesOrderDetailTableDictionary;
    function GetSalesOrderDetailMod: TSalesOrderDetailModTableDictionary;
    function GetServiceCategory: TServiceCategoryTableDictionary;
    function GetServiceLevel: TServiceLevelTableDictionary;
    function GetUnit: TUnitTableDictionary;
    function GetComponentHierarchy: TComponentHierarchyTableDictionary;
  public
    destructor Destroy; override;
    property Activity: TActivityTableDictionary read GetActivity;
    property ActivityHierarchy: TActivityHierarchyTableDictionary read GetActivityHierarchy;
    property ActivityType: TActivityTypeTableDictionary read GetActivityType;
    property Address: TAddressTableDictionary read GetAddress;
    property AddressHierarchy: TAddressHierarchyTableDictionary read GetAddressHierarchy;
    property AddressType: TAddressTypeTableDictionary read GetAddressType;
    property AttendOrder: TAttendOrderTableDictionary read GetAttendOrder;
    property Certificate: TCertificateTableDictionary read GetCertificate;
    property ChangeCancel: TChangeCancelTableDictionary read GetChangeCancel;
    property ChangeCredit: TChangeCreditTableDictionary read GetChangeCredit;
    property ChangeHold: TChangeHoldTableDictionary read GetChangeHold;
    property ChangeInfo: TChangeInfoTableDictionary read GetChangeInfo;
    property ChangeReason: TChangeReasonTableDictionary read GetChangeReason;
    property ChangeRequest: TChangeRequestTableDictionary read GetChangeRequest;
    property ChangeRequestType: TChangeRequestTypeTableDictionary read GetChangeRequestType;
    property ChangeSwitch: TChangeSwitchTableDictionary read GetChangeSwitch;
    property Component: TComponentTableDictionary read GetComponent;
    property ComponentType: TComponentTypeTableDictionary read GetComponentType;
    property Enrollment: TEnrollmentTableDictionary read GetEnrollment;
    property EntityBase: TEntityBaseTableDictionary read GetEntityBase;
    property Gender: TGenderTableDictionary read GetGender;
    property Grade: TGradeTableDictionary read GetGrade;
    property Invoice: TInvoiceTableDictionary read GetInvoice;
    property InvoiceDetail: TInvoiceDetailTableDictionary read GetInvoiceDetail;
    property InvoicingEnrollment: TInvoicingEnrollmentTableDictionary read GetInvoicingEnrollment;
    property InvoicingItem: TInvoicingItemTableDictionary read GetInvoicingItem;
    property InvoicingPaymentAccount: TInvoicingPaymentAccountTableDictionary read GetInvoicingPaymentAccount;
    property InvoicingSched: TInvoicingSchedTableDictionary read GetInvoicingSched;
    property InvoicingSchedDetail: TInvoicingSchedDetailTableDictionary read GetInvoicingSchedDetail;
    property InvoicingSchedType: TInvoicingSchedTypeTableDictionary read GetInvoicingSchedType;
    property Item: TItemTableDictionary read GetItem;
    property ItemHierarchy: TItemHierarchyTableDictionary read GetItemHierarchy;
    property ItemMod: TItemModTableDictionary read GetItemMod;
    property ItemProduct: TItemProductTableDictionary read GetItemProduct;
    property ItemRate: TItemRateTableDictionary read GetItemRate;
    property ItemService: TItemServiceTableDictionary read GetItemService;
    property ItemType: TItemTypeTableDictionary read GetItemType;
    property Locator: TLocatorTableDictionary read GetLocator;
    property Participant: TParticipantTableDictionary read GetParticipant;
    property ParticipantStatus: TParticipantStatusTableDictionary read GetParticipantStatus;
    property Party: TPartyTableDictionary read GetParty;
    property PartyGroup: TPartyGroupTableDictionary read GetPartyGroup;
    property PartyHierarchy: TPartyHierarchyTableDictionary read GetPartyHierarchy;
    property PartyPerson: TPartyPersonTableDictionary read GetPartyPerson;
    property PartyRole: TPartyRoleTableDictionary read GetPartyRole;
    property PartyRoleCustomer: TPartyRoleCustomerTableDictionary read GetPartyRoleCustomer;
    property PartyRoleGuardian: TPartyRoleGuardianTableDictionary read GetPartyRoleGuardian;
    property PartyRoleLead: TPartyRoleLeadTableDictionary read GetPartyRoleLead;
    property PartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary read GetPartyRoleLearningCenter;
    property PartyRoleStaff: TPartyRoleStaffTableDictionary read GetPartyRoleStaff;
    property PartyRoleStudent: TPartyRoleStudentTableDictionary read GetPartyRoleStudent;
    property PartyRoleType: TPartyRoleTypeTableDictionary read GetPartyRoleType;
    property PartyType: TPartyTypeTableDictionary read GetPartyType;
    property Payment: TPaymentTableDictionary read GetPayment;
    property PaymentAccount: TPaymentAccountTableDictionary read GetPaymentAccount;
    property PaymentAccountCash: TPaymentAccountCashTableDictionary read GetPaymentAccountCash;
    property PaymentAccountCc: TPaymentAccountCcTableDictionary read GetPaymentAccountCc;
    property PaymentAccountCheque: TPaymentAccountChequeTableDictionary read GetPaymentAccountCheque;
    property PaymentAccountDetail: TPaymentAccountDetailTableDictionary read GetPaymentAccountDetail;
    property PaymentAccountMemo: TPaymentAccountMemoTableDictionary read GetPaymentAccountMemo;
    property PaymentAccountMemoType: TPaymentAccountMemoTypeTableDictionary read GetPaymentAccountMemoType;
    property PaymentAccountType: TPaymentAccountTypeTableDictionary read GetPaymentAccountType;
    property PaymentDetail: TPaymentDetailTableDictionary read GetPaymentDetail;
    property PayInvoice: TPayInvoiceTableDictionary read GetPayInvoice;
    property PricePeriod: TPricePeriodTableDictionary read GetPricePeriod;
    property QbAccount: TQbAccountTableDictionary read GetQbAccount;
    property ReportingSched: TReportingSchedTableDictionary read GetReportingSched;
    property ReportingSchedType: TReportingSchedTypeTableDictionary read GetReportingSchedType;
    property RevenueReport: TRevenueReportTableDictionary read GetRevenueReport;
    property SalesOrder: TSalesOrderTableDictionary read GetSalesOrder;
    property SalesOrderDetail: TSalesOrderDetailTableDictionary read GetSalesOrderDetail;
    property SalesOrderDetailMod: TSalesOrderDetailModTableDictionary read GetSalesOrderDetailMod;
    property ServiceCategory: TServiceCategoryTableDictionary read GetServiceCategory;
    property ServiceLevel: TServiceLevelTableDictionary read GetServiceLevel;
    property Unit: TUnitTableDictionary read GetUnit;
    property ComponentHierarchy: TComponentHierarchyTableDictionary read GetComponentHierarchy;
  end;
  
  TActivityTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FActivityType: TDictionaryAssociation;
    FEntityBase: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property ActivityType: TDictionaryAssociation read FActivityType;
    property EntityBase: TDictionaryAssociation read FEntityBase;
  end;
  
  TActivityHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FIsSingleton: TDictionaryAttribute;
    FChildActivity: TDictionaryAssociation;
    FParentActivity: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property IsSingleton: TDictionaryAttribute read FIsSingleton;
    property ChildActivity: TDictionaryAssociation read FChildActivity;
    property ParentActivity: TDictionaryAssociation read FParentActivity;
  end;
  
  TActivityTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property Validation: TDictionaryAttribute read FValidation;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property DataType: TDictionaryAttribute read FDataType;
    property DataSize: TDictionaryAttribute read FDataSize;
  end;
  
  TAddressTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FAddressType: TDictionaryAssociation;
    FEntityBase: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property AddressType: TDictionaryAssociation read FAddressType;
    property EntityBase: TDictionaryAssociation read FEntityBase;
  end;
  
  TAddressHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FIsSingleton: TDictionaryAttribute;
    FChildAddress: TDictionaryAssociation;
    FParentAddress: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property IsSingleton: TDictionaryAttribute read FIsSingleton;
    property ChildAddress: TDictionaryAssociation read FChildAddress;
    property ParentAddress: TDictionaryAssociation read FParentAddress;
  end;
  
  TAddressTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property Validation: TDictionaryAttribute read FValidation;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property DataType: TDictionaryAttribute read FDataType;
    property DataSize: TDictionaryAttribute read FDataSize;
  end;
  
  TAttendOrderTableDictionary = class
  private
    FActivity: TDictionaryAssociation;
    FSalesOrderDetail: TDictionaryAssociation;
  public
    constructor Create;
    property Activity: TDictionaryAssociation read FActivity;
    property SalesOrderDetail: TDictionaryAssociation read FSalesOrderDetail;
  end;
  
  TCertificateTableDictionary = class
  private
    FCode: TDictionaryAttribute;
    FDonationValue: TDictionaryAttribute;
    FMaxAmount: TDictionaryAttribute;
    FDonateTo: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FEventStartAt: TDictionaryAttribute;
    FDeliverAt: TDictionaryAttribute;
    FPartyGroup: TDictionaryAssociation;
  public
    constructor Create;
    property Code: TDictionaryAttribute read FCode;
    property DonationValue: TDictionaryAttribute read FDonationValue;
    property MaxAmount: TDictionaryAttribute read FMaxAmount;
    property DonateTo: TDictionaryAttribute read FDonateTo;
    property Note: TDictionaryAttribute read FNote;
    property EventStartAt: TDictionaryAttribute read FEventStartAt;
    property DeliverAt: TDictionaryAttribute read FDeliverAt;
    property PartyGroup: TDictionaryAssociation read FPartyGroup;
  end;
  
  TChangeCancelTableDictionary = class
  private
    FIsContractComplete: TDictionaryAttribute;
    FCancellationFee: TDictionaryAttribute;
    FCancelAt: TDictionaryAttribute;
  public
    constructor Create;
    property IsContractComplete: TDictionaryAttribute read FIsContractComplete;
    property CancellationFee: TDictionaryAttribute read FCancellationFee;
    property CancelAt: TDictionaryAttribute read FCancelAt;
  end;
  
  TChangeCreditTableDictionary = class
  private
    FIssueType: TDictionaryAttribute;
    FCreditType: TDictionaryAttribute;
    FAmount: TDictionaryAttribute;
    FCreditAt: TDictionaryAttribute;
    FPaymentDetail: TDictionaryAssociation;
  public
    constructor Create;
    property IssueType: TDictionaryAttribute read FIssueType;
    property CreditType: TDictionaryAttribute read FCreditType;
    property Amount: TDictionaryAttribute read FAmount;
    property CreditAt: TDictionaryAttribute read FCreditAt;
    property PaymentDetail: TDictionaryAssociation read FPaymentDetail;
  end;
  
  TChangeHoldTableDictionary = class
  private
    FAttendanceHoldStartAt: TDictionaryAttribute;
    FAttendanceHoldEndAt: TDictionaryAttribute;
    FBillingHoldStartAt: TDictionaryAttribute;
    FBillingHoldEndAt: TDictionaryAttribute;
  public
    constructor Create;
    property AttendanceHoldStartAt: TDictionaryAttribute read FAttendanceHoldStartAt;
    property AttendanceHoldEndAt: TDictionaryAttribute read FAttendanceHoldEndAt;
    property BillingHoldStartAt: TDictionaryAttribute read FBillingHoldStartAt;
    property BillingHoldEndAt: TDictionaryAttribute read FBillingHoldEndAt;
  end;
  
  TChangeInfoTableDictionary = class
  private
    FFieldName: TDictionaryAttribute;
    FOldValue: TDictionaryAttribute;
    FNewValue: TDictionaryAttribute;
    FReplaceMethod: TDictionaryAttribute;
    FAddress: TDictionaryAssociation;
    FAddress: TDictionaryAssociation;
    FEntityBase: TDictionaryAssociation;
  public
    constructor Create;
    property FieldName: TDictionaryAttribute read FFieldName;
    property OldValue: TDictionaryAttribute read FOldValue;
    property NewValue: TDictionaryAttribute read FNewValue;
    property ReplaceMethod: TDictionaryAttribute read FReplaceMethod;
    property Address: TDictionaryAssociation read FAddress;
    property Address: TDictionaryAssociation read FAddress;
    property EntityBase: TDictionaryAssociation read FEntityBase;
  end;
  
  TChangeReasonTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TChangeRequestTableDictionary = class
  private
    FIsApplied: TDictionaryAttribute;
    FRequestBy: TDictionaryAttribute;
    FApprovalStatus: TDictionaryAttribute;
    FApproveBy: TDictionaryAttribute;
    FChangeRequestStatus: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FCdsEnterBy: TDictionaryAttribute;
    FCybersourceEnterBy: TDictionaryAttribute;
    FIsApproved: TDictionaryAttribute;
    FUnitCount: TDictionaryAttribute;
    FInclusiveRange: TDictionaryAttribute;
    FChangeRequestId: TDictionaryAttribute;
    FApplyAt: TDictionaryAttribute;
    FRequestAt: TDictionaryAttribute;
    FApproveAt: TDictionaryAttribute;
    FCdsEnterAt: TDictionaryAttribute;
    FCybersourceEnterAt: TDictionaryAttribute;
    FChangeReason: TDictionaryAssociation;
    FEnrollment: TDictionaryAssociation;
    FItem: TDictionaryAssociation;
    FParty: TDictionaryAssociation;
    FChangeRequestType: TDictionaryAssociation;
  public
    constructor Create;
    property IsApplied: TDictionaryAttribute read FIsApplied;
    property RequestBy: TDictionaryAttribute read FRequestBy;
    property ApprovalStatus: TDictionaryAttribute read FApprovalStatus;
    property ApproveBy: TDictionaryAttribute read FApproveBy;
    property ChangeRequestStatus: TDictionaryAttribute read FChangeRequestStatus;
    property Note: TDictionaryAttribute read FNote;
    property CdsEnterBy: TDictionaryAttribute read FCdsEnterBy;
    property CybersourceEnterBy: TDictionaryAttribute read FCybersourceEnterBy;
    property IsApproved: TDictionaryAttribute read FIsApproved;
    property UnitCount: TDictionaryAttribute read FUnitCount;
    property InclusiveRange: TDictionaryAttribute read FInclusiveRange;
    property ChangeRequestId: TDictionaryAttribute read FChangeRequestId;
    property ApplyAt: TDictionaryAttribute read FApplyAt;
    property RequestAt: TDictionaryAttribute read FRequestAt;
    property ApproveAt: TDictionaryAttribute read FApproveAt;
    property CdsEnterAt: TDictionaryAttribute read FCdsEnterAt;
    property CybersourceEnterAt: TDictionaryAttribute read FCybersourceEnterAt;
    property ChangeReason: TDictionaryAssociation read FChangeReason;
    property Enrollment: TDictionaryAssociation read FEnrollment;
    property Item: TDictionaryAssociation read FItem;
    property Party: TDictionaryAssociation read FParty;
    property ChangeRequestType: TDictionaryAssociation read FChangeRequestType;
  end;
  
  TChangeRequestTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TChangeSwitchTableDictionary = class
  private
    FOldEnrollmentEndAt: TDictionaryAttribute;
    FNewEnrollmentStartAt: TDictionaryAttribute;
    FNewEnrollmentEndAt: TDictionaryAttribute;
    FEnrollment: TDictionaryAssociation;
    FItem: TDictionaryAssociation;
  public
    constructor Create;
    property OldEnrollmentEndAt: TDictionaryAttribute read FOldEnrollmentEndAt;
    property NewEnrollmentStartAt: TDictionaryAttribute read FNewEnrollmentStartAt;
    property NewEnrollmentEndAt: TDictionaryAttribute read FNewEnrollmentEndAt;
    property Enrollment: TDictionaryAssociation read FEnrollment;
    property Item: TDictionaryAssociation read FItem;
  end;
  
  TComponentTableDictionary = class
  private
    FContentString: TDictionaryAttribute;
    FContentDatetime: TDictionaryAttribute;
    FContentNumeric: TDictionaryAttribute;
    FContentBoolean: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FVersionMajor: TDictionaryAttribute;
    FVersionMinor: TDictionaryAttribute;
    FVersionBuild: TDictionaryAttribute;
    FComponentType: TDictionaryAssociation;
    FEntityBase: TDictionaryAssociation;
  public
    constructor Create;
    property ContentString: TDictionaryAttribute read FContentString;
    property ContentDatetime: TDictionaryAttribute read FContentDatetime;
    property ContentNumeric: TDictionaryAttribute read FContentNumeric;
    property ContentBoolean: TDictionaryAttribute read FContentBoolean;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property VersionMajor: TDictionaryAttribute read FVersionMajor;
    property VersionMinor: TDictionaryAttribute read FVersionMinor;
    property VersionBuild: TDictionaryAttribute read FVersionBuild;
    property ComponentType: TDictionaryAssociation read FComponentType;
    property EntityBase: TDictionaryAssociation read FEntityBase;
  end;
  
  TComponentTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FValidation: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FDataType: TDictionaryAttribute;
    FDataSize: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property Validation: TDictionaryAttribute read FValidation;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property DataType: TDictionaryAttribute read FDataType;
    property DataSize: TDictionaryAttribute read FDataSize;
  end;
  
  TEnrollmentTableDictionary = class
  private
    FBlock: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FLegacyId: TDictionaryAttribute;
    FLegacyCode: TDictionaryAttribute;
    FOriginalPricePeriodId: TDictionaryAttribute;
    FAgreeAt: TDictionaryAttribute;
    FAgreeStartAt: TDictionaryAttribute;
    FAgreeEndAt: TDictionaryAttribute;
    FTerminateAt: TDictionaryAttribute;
    FProjectedEndAt: TDictionaryAttribute;
    FItem: TDictionaryAssociation;
    FPartyRoleStudent: TDictionaryAssociation;
  public
    constructor Create;
    property Block: TDictionaryAttribute read FBlock;
    property Note: TDictionaryAttribute read FNote;
    property LegacyId: TDictionaryAttribute read FLegacyId;
    property LegacyCode: TDictionaryAttribute read FLegacyCode;
    property OriginalPricePeriodId: TDictionaryAttribute read FOriginalPricePeriodId;
    property AgreeAt: TDictionaryAttribute read FAgreeAt;
    property AgreeStartAt: TDictionaryAttribute read FAgreeStartAt;
    property AgreeEndAt: TDictionaryAttribute read FAgreeEndAt;
    property TerminateAt: TDictionaryAttribute read FTerminateAt;
    property ProjectedEndAt: TDictionaryAttribute read FProjectedEndAt;
    property Item: TDictionaryAssociation read FItem;
    property PartyRoleStudent: TDictionaryAssociation read FPartyRoleStudent;
  end;
  
  TEntityBaseTableDictionary = class
  private
    FId: TDictionaryAttribute;
    FIsActive: TDictionaryAttribute;
    FCreatedBy: TDictionaryAttribute;
    FCreatedAt: TDictionaryAttribute;
    FUpdatedBy: TDictionaryAttribute;
    FUpdatedAt: TDictionaryAttribute;
    FPos: TDictionaryAttribute;
    FExternalSource: TDictionaryAttribute;
    FExternalId: TDictionaryAttribute;
    FTableName: TDictionaryAttribute;
  public
    constructor Create;
    property Id: TDictionaryAttribute read FId;
    property IsActive: TDictionaryAttribute read FIsActive;
    property CreatedBy: TDictionaryAttribute read FCreatedBy;
    property CreatedAt: TDictionaryAttribute read FCreatedAt;
    property UpdatedBy: TDictionaryAttribute read FUpdatedBy;
    property UpdatedAt: TDictionaryAttribute read FUpdatedAt;
    property Pos: TDictionaryAttribute read FPos;
    property ExternalSource: TDictionaryAttribute read FExternalSource;
    property ExternalId: TDictionaryAttribute read FExternalId;
    property TableName: TDictionaryAttribute read FTableName;
  end;
  
  TGenderTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FSubjectPronoun: TDictionaryAttribute;
    FObjectPronoun: TDictionaryAttribute;
    FPossessiveDetPronoun: TDictionaryAttribute;
    FPossessivePronoun: TDictionaryAttribute;
    FReflexivePronoun: TDictionaryAttribute;
    FTitle: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property SubjectPronoun: TDictionaryAttribute read FSubjectPronoun;
    property ObjectPronoun: TDictionaryAttribute read FObjectPronoun;
    property PossessiveDetPronoun: TDictionaryAttribute read FPossessiveDetPronoun;
    property PossessivePronoun: TDictionaryAttribute read FPossessivePronoun;
    property ReflexivePronoun: TDictionaryAttribute read FReflexivePronoun;
    property Title: TDictionaryAttribute read FTitle;
  end;
  
  TGradeTableDictionary = class
  private
    FServiceLevelId: TDictionaryAttribute;
    FGrade: TDictionaryAttribute;
    FName: TDictionaryAttribute;
  public
    constructor Create;
    property ServiceLevelId: TDictionaryAttribute read FServiceLevelId;
    property Grade: TDictionaryAttribute read FGrade;
    property Name: TDictionaryAttribute read FName;
  end;
  
  TInvoiceTableDictionary = class
  private
    FIsVoided: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FAmount: TDictionaryAttribute;
    FPos: TDictionaryAttribute;
    FDueAt: TDictionaryAttribute;
    FChangeRequest: TDictionaryAssociation;
    FInvoicingSched: TDictionaryAssociation;
    FPartyRoleCustomer: TDictionaryAssociation;
  public
    constructor Create;
    property IsVoided: TDictionaryAttribute read FIsVoided;
    property Note: TDictionaryAttribute read FNote;
    property Amount: TDictionaryAttribute read FAmount;
    property Pos: TDictionaryAttribute read FPos;
    property DueAt: TDictionaryAttribute read FDueAt;
    property ChangeRequest: TDictionaryAssociation read FChangeRequest;
    property InvoicingSched: TDictionaryAssociation read FInvoicingSched;
    property PartyRoleCustomer: TDictionaryAssociation read FPartyRoleCustomer;
  end;
  
  TInvoiceDetailTableDictionary = class
  private
    FUnitRate: TDictionaryAttribute;
    FDivisor: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FPos: TDictionaryAttribute;
    FUnitCount: TDictionaryAttribute;
    FAmount: TDictionaryAttribute;
    FInvoice: TDictionaryAssociation;
    FItemMod: TDictionaryAssociation;
    FSalesOrderDetail: TDictionaryAssociation;
  public
    constructor Create;
    property UnitRate: TDictionaryAttribute read FUnitRate;
    property Divisor: TDictionaryAttribute read FDivisor;
    property Note: TDictionaryAttribute read FNote;
    property Pos: TDictionaryAttribute read FPos;
    property UnitCount: TDictionaryAttribute read FUnitCount;
    property Amount: TDictionaryAttribute read FAmount;
    property Invoice: TDictionaryAssociation read FInvoice;
    property ItemMod: TDictionaryAssociation read FItemMod;
    property SalesOrderDetail: TDictionaryAssociation read FSalesOrderDetail;
  end;
  
  TInvoicingEnrollmentTableDictionary = class
  private
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
    FEnrollment: TDictionaryAssociation;
    FInvoicingSched: TDictionaryAssociation;
  public
    constructor Create;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
    property Enrollment: TDictionaryAssociation read FEnrollment;
    property InvoicingSched: TDictionaryAssociation read FInvoicingSched;
  end;
  
  TInvoicingItemTableDictionary = class
  private
    FNote: TDictionaryAttribute;
    FInvoicingSched: TDictionaryAssociation;
    FItem: TDictionaryAssociation;
  public
    constructor Create;
    property Note: TDictionaryAttribute read FNote;
    property InvoicingSched: TDictionaryAssociation read FInvoicingSched;
    property Item: TDictionaryAssociation read FItem;
  end;
  
  TInvoicingPaymentAccountTableDictionary = class
  private
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
    FInvoicingSched: TDictionaryAssociation;
    FPaymentAccountDetail: TDictionaryAssociation;
  public
    constructor Create;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
    property InvoicingSched: TDictionaryAssociation read FInvoicingSched;
    property PaymentAccountDetail: TDictionaryAssociation read FPaymentAccountDetail;
  end;
  
  TInvoicingSchedTableDictionary = class
  private
    FPercentage: TDictionaryAttribute;
    FInitialAmount: TDictionaryAttribute;
    FRecurringAmount: TDictionaryAttribute;
    FInstallmentCount: TDictionaryAttribute;
    FUnitCountPerCycle: TDictionaryAttribute;
    FDayOfUnit: TDictionaryAttribute;
    FCurrency: TDictionaryAttribute;
    FPaymentMethod: TDictionaryAttribute;
    FCombineOrders: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FSeparateLifetimeUnits: TDictionaryAttribute;
    FSubscriptionId: TDictionaryAttribute;
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
    FInvoicingSchedType: TDictionaryAssociation;
    FUnit_: TDictionaryAssociation;
  public
    constructor Create;
    property Percentage: TDictionaryAttribute read FPercentage;
    property InitialAmount: TDictionaryAttribute read FInitialAmount;
    property RecurringAmount: TDictionaryAttribute read FRecurringAmount;
    property InstallmentCount: TDictionaryAttribute read FInstallmentCount;
    property UnitCountPerCycle: TDictionaryAttribute read FUnitCountPerCycle;
    property DayOfUnit: TDictionaryAttribute read FDayOfUnit;
    property Currency: TDictionaryAttribute read FCurrency;
    property PaymentMethod: TDictionaryAttribute read FPaymentMethod;
    property CombineOrders: TDictionaryAttribute read FCombineOrders;
    property Note: TDictionaryAttribute read FNote;
    property SeparateLifetimeUnits: TDictionaryAttribute read FSeparateLifetimeUnits;
    property SubscriptionId: TDictionaryAttribute read FSubscriptionId;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
    property InvoicingSchedType: TDictionaryAssociation read FInvoicingSchedType;
    property Unit_: TDictionaryAssociation read FUnit_;
  end;
  
  TInvoicingSchedDetailTableDictionary = class
  private
    FScheduleIndex: TDictionaryAttribute;
    FAutoSuggest: TDictionaryAttribute;
    FStatus: TDictionaryAttribute;
    FContentBlob: TDictionaryAttribute;
    FActionRequired: TDictionaryAttribute;
    FCompleteBy: TDictionaryAttribute;
    FMinCount: TDictionaryAttribute;
    FMaxCount: TDictionaryAttribute;
    FScheduleAt: TDictionaryAttribute;
    FCompleteAt: TDictionaryAttribute;
    FInvoicingSched: TDictionaryAssociation;
  public
    constructor Create;
    property ScheduleIndex: TDictionaryAttribute read FScheduleIndex;
    property AutoSuggest: TDictionaryAttribute read FAutoSuggest;
    property Status: TDictionaryAttribute read FStatus;
    property ContentBlob: TDictionaryAttribute read FContentBlob;
    property ActionRequired: TDictionaryAttribute read FActionRequired;
    property CompleteBy: TDictionaryAttribute read FCompleteBy;
    property MinCount: TDictionaryAttribute read FMinCount;
    property MaxCount: TDictionaryAttribute read FMaxCount;
    property ScheduleAt: TDictionaryAttribute read FScheduleAt;
    property CompleteAt: TDictionaryAttribute read FCompleteAt;
    property InvoicingSched: TDictionaryAssociation read FInvoicingSched;
  end;
  
  TInvoicingSchedTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TItemTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FAlias: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FTermAndCondition: TDictionaryAttribute;
    FQbAccount: TDictionaryAssociation;
    FReportingSched: TDictionaryAssociation;
    FUnit_: TDictionaryAssociation;
    FItemType: TDictionaryAssociation;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Alias: TDictionaryAttribute read FAlias;
    property Description: TDictionaryAttribute read FDescription;
    property TermAndCondition: TDictionaryAttribute read FTermAndCondition;
    property QbAccount: TDictionaryAssociation read FQbAccount;
    property ReportingSched: TDictionaryAssociation read FReportingSched;
    property Unit_: TDictionaryAssociation read FUnit_;
    property ItemType: TDictionaryAssociation read FItemType;
  end;
  
  TItemHierarchyTableDictionary = class
  private
    FUnitCount: TDictionaryAttribute;
    FUnitCountPerLine: TDictionaryAttribute;
    FDivisor: TDictionaryAttribute;
    FIsRecurring: TDictionaryAttribute;
    FPos: TDictionaryAttribute;
    FMinCount: TDictionaryAttribute;
    FMaxCount: TDictionaryAttribute;
    FEntityBase: TDictionaryAssociation;
    FChildItem: TDictionaryAssociation;
    FParentItem: TDictionaryAssociation;
  public
    constructor Create;
    property UnitCount: TDictionaryAttribute read FUnitCount;
    property UnitCountPerLine: TDictionaryAttribute read FUnitCountPerLine;
    property Divisor: TDictionaryAttribute read FDivisor;
    property IsRecurring: TDictionaryAttribute read FIsRecurring;
    property Pos: TDictionaryAttribute read FPos;
    property MinCount: TDictionaryAttribute read FMinCount;
    property MaxCount: TDictionaryAttribute read FMaxCount;
    property EntityBase: TDictionaryAssociation read FEntityBase;
    property ChildItem: TDictionaryAssociation read FChildItem;
    property ParentItem: TDictionaryAssociation read FParentItem;
  end;
  
  TItemModTableDictionary = class
  private
    FModUseCount: TDictionaryAttribute;
    FModBlob: TDictionaryAttribute;
  public
    constructor Create;
    property ModUseCount: TDictionaryAttribute read FModUseCount;
    property ModBlob: TDictionaryAttribute read FModBlob;
  end;
  
  TItemProductTableDictionary = class
  private
    FInventoryId: TDictionaryAttribute;
  public
    constructor Create;
    property InventoryId: TDictionaryAttribute read FInventoryId;
  end;
  
  TItemRateTableDictionary = class
  private
    FLocationId: TDictionaryAttribute;
    FDivisor: TDictionaryAttribute;
    FUnitRate: TDictionaryAttribute;
    FItem: TDictionaryAssociation;
    FPricePeriod: TDictionaryAssociation;
  public
    constructor Create;
    property LocationId: TDictionaryAttribute read FLocationId;
    property Divisor: TDictionaryAttribute read FDivisor;
    property UnitRate: TDictionaryAttribute read FUnitRate;
    property Item: TDictionaryAssociation read FItem;
    property PricePeriod: TDictionaryAssociation read FPricePeriod;
  end;
  
  TItemServiceTableDictionary = class
  private
    FServiceCategory: TDictionaryAssociation;
    FServiceLevel: TDictionaryAssociation;
  public
    constructor Create;
    property ServiceCategory: TDictionaryAssociation read FServiceCategory;
    property ServiceLevel: TDictionaryAssociation read FServiceLevel;
  end;
  
  TItemTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TLocatorTableDictionary = class
  private
    FAddress: TDictionaryAssociation;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Address: TDictionaryAssociation read FAddress;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TParticipantTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FActivity: TDictionaryAssociation;
    FParticipantStatus: TDictionaryAssociation;
    FPartyRole: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property Activity: TDictionaryAssociation read FActivity;
    property ParticipantStatus: TDictionaryAssociation read FParticipantStatus;
    property PartyRole: TDictionaryAssociation read FPartyRole;
  end;
  
  TParticipantStatusTableDictionary = class
  private
    FId: TDictionaryAttribute;
    FStatus: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Id: TDictionaryAttribute read FId;
    property Status: TDictionaryAttribute read FStatus;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TPartyTableDictionary = class
  private
    FPartyType: TDictionaryAssociation;
  public
    constructor Create;
    property PartyType: TDictionaryAssociation read FPartyType;
  end;
  
  TPartyGroupTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FAlias: TDictionaryAttribute;
    FGroupType: TDictionaryAttribute;
    FIsForProfit: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Alias: TDictionaryAttribute read FAlias;
    property GroupType: TDictionaryAttribute read FGroupType;
    property IsForProfit: TDictionaryAttribute read FIsForProfit;
    property Note: TDictionaryAttribute read FNote;
  end;
  
  TPartyHierarchyTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FIsPrimary: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FChildParty: TDictionaryAssociation;
    FParentParty: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsPrimary: TDictionaryAttribute read FIsPrimary;
    property Note: TDictionaryAttribute read FNote;
    property ChildParty: TDictionaryAssociation read FChildParty;
    property ParentParty: TDictionaryAssociation read FParentParty;
  end;
  
  TPartyPersonTableDictionary = class
  private
    FPrefix: TDictionaryAttribute;
    FFirstName: TDictionaryAttribute;
    FMiddleNames: TDictionaryAttribute;
    FLastName: TDictionaryAttribute;
    FSuffix: TDictionaryAttribute;
    FNickName: TDictionaryAttribute;
    FMedicalInfo: TDictionaryAttribute;
    FDoNotContact: TDictionaryAttribute;
    FBirthDay: TDictionaryAttribute;
    FBirthMonth: TDictionaryAttribute;
    FBirthYear: TDictionaryAttribute;
    FBirthDate: TDictionaryAttribute;
    FGender: TDictionaryAssociation;
  public
    constructor Create;
    property Prefix: TDictionaryAttribute read FPrefix;
    property FirstName: TDictionaryAttribute read FFirstName;
    property MiddleNames: TDictionaryAttribute read FMiddleNames;
    property LastName: TDictionaryAttribute read FLastName;
    property Suffix: TDictionaryAttribute read FSuffix;
    property NickName: TDictionaryAttribute read FNickName;
    property MedicalInfo: TDictionaryAttribute read FMedicalInfo;
    property DoNotContact: TDictionaryAttribute read FDoNotContact;
    property BirthDay: TDictionaryAttribute read FBirthDay;
    property BirthMonth: TDictionaryAttribute read FBirthMonth;
    property BirthYear: TDictionaryAttribute read FBirthYear;
    property BirthDate: TDictionaryAttribute read FBirthDate;
    property Gender: TDictionaryAssociation read FGender;
  end;
  
  TPartyRoleTableDictionary = class
  private
    FNote: TDictionaryAttribute;
    FParty: TDictionaryAssociation;
    FPartyRoleType: TDictionaryAssociation;
  public
    constructor Create;
    property Note: TDictionaryAttribute read FNote;
    property Party: TDictionaryAssociation read FParty;
    property PartyRoleType: TDictionaryAssociation read FPartyRoleType;
  end;
  
  TPartyRoleCustomerTableDictionary = class
  end;
  
  TPartyRoleGuardianTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FIsEmergencyContact: TDictionaryAttribute;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsEmergencyContact: TDictionaryAttribute read FIsEmergencyContact;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TPartyRoleLeadTableDictionary = class
  private
    FConvertAt: TDictionaryAttribute;
    FInitialContactAt: TDictionaryAttribute;
  public
    constructor Create;
    property ConvertAt: TDictionaryAttribute read FConvertAt;
    property InitialContactAt: TDictionaryAttribute read FInitialContactAt;
  end;
  
  TPartyRoleLearningCenterTableDictionary = class
  private
    FStudentNumberPrefix: TDictionaryAttribute;
    FNextStudentNumber: TDictionaryAttribute;
  public
    constructor Create;
    property StudentNumberPrefix: TDictionaryAttribute read FStudentNumberPrefix;
    property NextStudentNumber: TDictionaryAttribute read FNextStudentNumber;
  end;
  
  TPartyRoleStaffTableDictionary = class
  private
    FHireAt: TDictionaryAttribute;
  public
    constructor Create;
    property HireAt: TDictionaryAttribute read FHireAt;
  end;
  
  TPartyRoleStudentTableDictionary = class
  private
    FStudentNumber: TDictionaryAttribute;
    FLivesWith: TDictionaryAttribute;
    FConsentContactSchool: TDictionaryAttribute;
    FConsentLeave: TDictionaryAttribute;
    FConsentPhotograph: TDictionaryAttribute;
    FSchoolEntryDate: TDictionaryAttribute;
    FGrade: TDictionaryAssociation;
  public
    constructor Create;
    property StudentNumber: TDictionaryAttribute read FStudentNumber;
    property LivesWith: TDictionaryAttribute read FLivesWith;
    property ConsentContactSchool: TDictionaryAttribute read FConsentContactSchool;
    property ConsentLeave: TDictionaryAttribute read FConsentLeave;
    property ConsentPhotograph: TDictionaryAttribute read FConsentPhotograph;
    property SchoolEntryDate: TDictionaryAttribute read FSchoolEntryDate;
    property Grade: TDictionaryAssociation read FGrade;
  end;
  
  TPartyRoleTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FIsPersonRole: TDictionaryAttribute;
    FIsGroupRole: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property IsPersonRole: TDictionaryAttribute read FIsPersonRole;
    property IsGroupRole: TDictionaryAttribute read FIsGroupRole;
  end;
  
  TPartyTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TPaymentTableDictionary = class
  private
    FAmount: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FCdsId2List: TDictionaryAttribute;
    FStatus: TDictionaryAttribute;
    FAppliedAmount: TDictionaryAttribute;
    FProcessAt: TDictionaryAttribute;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Amount: TDictionaryAttribute read FAmount;
    property Note: TDictionaryAttribute read FNote;
    property CdsId2List: TDictionaryAttribute read FCdsId2List;
    property Status: TDictionaryAttribute read FStatus;
    property AppliedAmount: TDictionaryAttribute read FAppliedAmount;
    property ProcessAt: TDictionaryAttribute read FProcessAt;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TPaymentAccountTableDictionary = class
  private
    FParty: TDictionaryAssociation;
    FPaymentAccountType: TDictionaryAssociation;
  public
    constructor Create;
    property Party: TDictionaryAssociation read FParty;
    property PaymentAccountType: TDictionaryAssociation read FPaymentAccountType;
  end;
  
  TPaymentAccountCashTableDictionary = class
  end;
  
  TPaymentAccountCcTableDictionary = class
  private
    FAutoBillDay: TDictionaryAttribute;
    FCcNumber: TDictionaryAttribute;
    FLastFour: TDictionaryAttribute;
    FCardType: TDictionaryAttribute;
  public
    constructor Create;
    property AutoBillDay: TDictionaryAttribute read FAutoBillDay;
    property CcNumber: TDictionaryAttribute read FCcNumber;
    property LastFour: TDictionaryAttribute read FLastFour;
    property CardType: TDictionaryAttribute read FCardType;
  end;
  
  TPaymentAccountChequeTableDictionary = class
  end;
  
  TPaymentAccountDetailTableDictionary = class
  private
    FFirstNameOnCard: TDictionaryAttribute;
    FLastNameOnCard: TDictionaryAttribute;
    FExpirationDate: TDictionaryAttribute;
    FCvn: TDictionaryAttribute;
    FAddress: TDictionaryAssociation;
    FPaymentAccount: TDictionaryAssociation;
  public
    constructor Create;
    property FirstNameOnCard: TDictionaryAttribute read FFirstNameOnCard;
    property LastNameOnCard: TDictionaryAttribute read FLastNameOnCard;
    property ExpirationDate: TDictionaryAttribute read FExpirationDate;
    property Cvn: TDictionaryAttribute read FCvn;
    property Address: TDictionaryAssociation read FAddress;
    property PaymentAccount: TDictionaryAssociation read FPaymentAccount;
  end;
  
  TPaymentAccountMemoTableDictionary = class
  private
    FCertificate: TDictionaryAssociation;
    FPaymentAccountMemoType: TDictionaryAssociation;
  public
    constructor Create;
    property Certificate: TDictionaryAssociation read FCertificate;
    property PaymentAccountMemoType: TDictionaryAssociation read FPaymentAccountMemoType;
  end;
  
  TPaymentAccountMemoTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TPaymentAccountTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TPaymentDetailTableDictionary = class
  private
    FProcessAt: TDictionaryAttribute;
    FAmount: TDictionaryAttribute;
    FActionType: TDictionaryAttribute;
    FStatus: TDictionaryAttribute;
    FTransactionDetail: TDictionaryAttribute;
    FPayment: TDictionaryAssociation;
    FPaymentAccountDetail: TDictionaryAssociation;
    FPaymentDetail: TDictionaryAssociation;
  public
    constructor Create;
    property ProcessAt: TDictionaryAttribute read FProcessAt;
    property Amount: TDictionaryAttribute read FAmount;
    property ActionType: TDictionaryAttribute read FActionType;
    property Status: TDictionaryAttribute read FStatus;
    property TransactionDetail: TDictionaryAttribute read FTransactionDetail;
    property Payment: TDictionaryAssociation read FPayment;
    property PaymentAccountDetail: TDictionaryAssociation read FPaymentAccountDetail;
    property PaymentDetail: TDictionaryAssociation read FPaymentDetail;
  end;
  
  TPayInvoiceTableDictionary = class
  private
    FAmount: TDictionaryAttribute;
    FInvoice: TDictionaryAssociation;
    FPayment: TDictionaryAssociation;
  public
    constructor Create;
    property Amount: TDictionaryAttribute read FAmount;
    property Invoice: TDictionaryAssociation read FInvoice;
    property Payment: TDictionaryAssociation read FPayment;
  end;
  
  TPricePeriodTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
  end;
  
  TQbAccountTableDictionary = class
  private
    FAccountNumber: TDictionaryAttribute;
    FName: TDictionaryAttribute;
    FQbAccountType: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property AccountNumber: TDictionaryAttribute read FAccountNumber;
    property Name: TDictionaryAttribute read FName;
    property QbAccountType: TDictionaryAttribute read FQbAccountType;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TReportingSchedTableDictionary = class
  private
    FInstallmentCount: TDictionaryAttribute;
    FInstallmentShift: TDictionaryAttribute;
    FUnit_: TDictionaryAssociation;
    FReportingSchedType: TDictionaryAssociation;
  public
    constructor Create;
    property InstallmentCount: TDictionaryAttribute read FInstallmentCount;
    property InstallmentShift: TDictionaryAttribute read FInstallmentShift;
    property Unit_: TDictionaryAssociation read FUnit_;
    property ReportingSchedType: TDictionaryAssociation read FReportingSchedType;
  end;
  
  TReportingSchedTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TRevenueReportTableDictionary = class
  private
    FIsCorrection: TDictionaryAttribute;
    FIsExpired: TDictionaryAttribute;
    FBookUnitCount: TDictionaryAttribute;
    FBookAmount: TDictionaryAttribute;
    FReportingNote: TDictionaryAttribute;
    FBookAt: TDictionaryAttribute;
    FReportAt: TDictionaryAttribute;
    FPartyGroup: TDictionaryAssociation;
    FQbAccount: TDictionaryAssociation;
    FSalesOrderDetail: TDictionaryAssociation;
  public
    constructor Create;
    property IsCorrection: TDictionaryAttribute read FIsCorrection;
    property IsExpired: TDictionaryAttribute read FIsExpired;
    property BookUnitCount: TDictionaryAttribute read FBookUnitCount;
    property BookAmount: TDictionaryAttribute read FBookAmount;
    property ReportingNote: TDictionaryAttribute read FReportingNote;
    property BookAt: TDictionaryAttribute read FBookAt;
    property ReportAt: TDictionaryAttribute read FReportAt;
    property PartyGroup: TDictionaryAssociation read FPartyGroup;
    property QbAccount: TDictionaryAssociation read FQbAccount;
    property SalesOrderDetail: TDictionaryAssociation read FSalesOrderDetail;
  end;
  
  TSalesOrderTableDictionary = class
  private
    FNote: TDictionaryAttribute;
    FChangeRequest: TDictionaryAssociation;
    FEnrollment: TDictionaryAssociation;
    FPartyRoleCustomer: TDictionaryAssociation;
    FPricePeriod: TDictionaryAssociation;
  public
    constructor Create;
    property Note: TDictionaryAttribute read FNote;
    property ChangeRequest: TDictionaryAssociation read FChangeRequest;
    property Enrollment: TDictionaryAssociation read FEnrollment;
    property PartyRoleCustomer: TDictionaryAssociation read FPartyRoleCustomer;
    property PricePeriod: TDictionaryAssociation read FPricePeriod;
  end;
  
  TSalesOrderDetailTableDictionary = class
  private
    FIsCancelled: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FInvoiceNumber: TDictionaryAttribute;
    FUnitCount: TDictionaryAttribute;
    FStartAt: TDictionaryAttribute;
    FEndAt: TDictionaryAttribute;
    FItem: TDictionaryAssociation;
    FReportingSched: TDictionaryAssociation;
    FSalesOrder: TDictionaryAssociation;
  public
    constructor Create;
    property IsCancelled: TDictionaryAttribute read FIsCancelled;
    property Note: TDictionaryAttribute read FNote;
    property InvoiceNumber: TDictionaryAttribute read FInvoiceNumber;
    property UnitCount: TDictionaryAttribute read FUnitCount;
    property StartAt: TDictionaryAttribute read FStartAt;
    property EndAt: TDictionaryAttribute read FEndAt;
    property Item: TDictionaryAssociation read FItem;
    property ReportingSched: TDictionaryAssociation read FReportingSched;
    property SalesOrder: TDictionaryAssociation read FSalesOrder;
  end;
  
  TSalesOrderDetailModTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsOverride: TDictionaryAttribute;
    FIsApplied: TDictionaryAttribute;
    FNote: TDictionaryAttribute;
    FItemMod: TDictionaryAssociation;
    FSalesOrderDetail: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsOverride: TDictionaryAttribute read FIsOverride;
    property IsApplied: TDictionaryAttribute read FIsApplied;
    property Note: TDictionaryAttribute read FNote;
    property ItemMod: TDictionaryAssociation read FItemMod;
    property SalesOrderDetail: TDictionaryAssociation read FSalesOrderDetail;
  end;
  
  TServiceCategoryTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TServiceLevelTableDictionary = class
  private
    FServiceLevel: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property ServiceLevel: TDictionaryAttribute read FServiceLevel;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TUnitTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FUnitType: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FLogicLineCount: TDictionaryAttribute;
    FLogicStartDate: TDictionaryAttribute;
    FLogicEndDate: TDictionaryAttribute;
    FLogicUnitCount: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property UnitType: TDictionaryAttribute read FUnitType;
    property Description: TDictionaryAttribute read FDescription;
    property LogicLineCount: TDictionaryAttribute read FLogicLineCount;
    property LogicStartDate: TDictionaryAttribute read FLogicStartDate;
    property LogicEndDate: TDictionaryAttribute read FLogicEndDate;
    property LogicUnitCount: TDictionaryAttribute read FLogicUnitCount;
  end;
  
  TComponentHierarchyTableDictionary = class
  private
    FPos: TDictionaryAttribute;
    FIsTemplate: TDictionaryAttribute;
    FIsSingleton: TDictionaryAttribute;
    FChildComponent: TDictionaryAssociation;
    FParentComponent: TDictionaryAssociation;
  public
    constructor Create;
    property Pos: TDictionaryAttribute read FPos;
    property IsTemplate: TDictionaryAttribute read FIsTemplate;
    property IsSingleton: TDictionaryAttribute read FIsSingleton;
    property ChildComponent: TDictionaryAssociation read FChildComponent;
    property ParentComponent: TDictionaryAssociation read FParentComponent;
  end;
  
function Dic: TDicDictionary;

implementation

var
  __Dic: TDicDictionary;

function Dic: TDicDictionary;
begin
  if __Dic = nil then __Dic := TDicDictionary.Create;
  result := __Dic
end;

{ TDicDictionary }

destructor TDicDictionary.Destroy;
begin
  if FComponentHierarchy <> nil then FComponentHierarchy.Free;
  if FUnit <> nil then FUnit.Free;
  if FServiceLevel <> nil then FServiceLevel.Free;
  if FServiceCategory <> nil then FServiceCategory.Free;
  if FSalesOrderDetailMod <> nil then FSalesOrderDetailMod.Free;
  if FSalesOrderDetail <> nil then FSalesOrderDetail.Free;
  if FSalesOrder <> nil then FSalesOrder.Free;
  if FRevenueReport <> nil then FRevenueReport.Free;
  if FReportingSchedType <> nil then FReportingSchedType.Free;
  if FReportingSched <> nil then FReportingSched.Free;
  if FQbAccount <> nil then FQbAccount.Free;
  if FPricePeriod <> nil then FPricePeriod.Free;
  if FPayInvoice <> nil then FPayInvoice.Free;
  if FPaymentDetail <> nil then FPaymentDetail.Free;
  if FPaymentAccountType <> nil then FPaymentAccountType.Free;
  if FPaymentAccountMemoType <> nil then FPaymentAccountMemoType.Free;
  if FPaymentAccountMemo <> nil then FPaymentAccountMemo.Free;
  if FPaymentAccountDetail <> nil then FPaymentAccountDetail.Free;
  if FPaymentAccountCheque <> nil then FPaymentAccountCheque.Free;
  if FPaymentAccountCc <> nil then FPaymentAccountCc.Free;
  if FPaymentAccountCash <> nil then FPaymentAccountCash.Free;
  if FPaymentAccount <> nil then FPaymentAccount.Free;
  if FPayment <> nil then FPayment.Free;
  if FPartyType <> nil then FPartyType.Free;
  if FPartyRoleType <> nil then FPartyRoleType.Free;
  if FPartyRoleStudent <> nil then FPartyRoleStudent.Free;
  if FPartyRoleStaff <> nil then FPartyRoleStaff.Free;
  if FPartyRoleLearningCenter <> nil then FPartyRoleLearningCenter.Free;
  if FPartyRoleLead <> nil then FPartyRoleLead.Free;
  if FPartyRoleGuardian <> nil then FPartyRoleGuardian.Free;
  if FPartyRoleCustomer <> nil then FPartyRoleCustomer.Free;
  if FPartyRole <> nil then FPartyRole.Free;
  if FPartyPerson <> nil then FPartyPerson.Free;
  if FPartyHierarchy <> nil then FPartyHierarchy.Free;
  if FPartyGroup <> nil then FPartyGroup.Free;
  if FParty <> nil then FParty.Free;
  if FParticipantStatus <> nil then FParticipantStatus.Free;
  if FParticipant <> nil then FParticipant.Free;
  if FLocator <> nil then FLocator.Free;
  if FItemType <> nil then FItemType.Free;
  if FItemService <> nil then FItemService.Free;
  if FItemRate <> nil then FItemRate.Free;
  if FItemProduct <> nil then FItemProduct.Free;
  if FItemMod <> nil then FItemMod.Free;
  if FItemHierarchy <> nil then FItemHierarchy.Free;
  if FItem <> nil then FItem.Free;
  if FInvoicingSchedType <> nil then FInvoicingSchedType.Free;
  if FInvoicingSchedDetail <> nil then FInvoicingSchedDetail.Free;
  if FInvoicingSched <> nil then FInvoicingSched.Free;
  if FInvoicingPaymentAccount <> nil then FInvoicingPaymentAccount.Free;
  if FInvoicingItem <> nil then FInvoicingItem.Free;
  if FInvoicingEnrollment <> nil then FInvoicingEnrollment.Free;
  if FInvoiceDetail <> nil then FInvoiceDetail.Free;
  if FInvoice <> nil then FInvoice.Free;
  if FGrade <> nil then FGrade.Free;
  if FGender <> nil then FGender.Free;
  if FEntityBase <> nil then FEntityBase.Free;
  if FEnrollment <> nil then FEnrollment.Free;
  if FComponentType <> nil then FComponentType.Free;
  if FComponent <> nil then FComponent.Free;
  if FChangeSwitch <> nil then FChangeSwitch.Free;
  if FChangeRequestType <> nil then FChangeRequestType.Free;
  if FChangeRequest <> nil then FChangeRequest.Free;
  if FChangeReason <> nil then FChangeReason.Free;
  if FChangeInfo <> nil then FChangeInfo.Free;
  if FChangeHold <> nil then FChangeHold.Free;
  if FChangeCredit <> nil then FChangeCredit.Free;
  if FChangeCancel <> nil then FChangeCancel.Free;
  if FCertificate <> nil then FCertificate.Free;
  if FAttendOrder <> nil then FAttendOrder.Free;
  if FAddressType <> nil then FAddressType.Free;
  if FAddressHierarchy <> nil then FAddressHierarchy.Free;
  if FAddress <> nil then FAddress.Free;
  if FActivityType <> nil then FActivityType.Free;
  if FActivityHierarchy <> nil then FActivityHierarchy.Free;
  if FActivity <> nil then FActivity.Free;
  inherited;
end;

function TDicDictionary.GetActivity: TActivityTableDictionary;
begin
  if FActivity = nil then FActivity := TActivityTableDictionary.Create;
  result := FActivity;
end;

function TDicDictionary.GetActivityHierarchy: TActivityHierarchyTableDictionary;
begin
  if FActivityHierarchy = nil then FActivityHierarchy := TActivityHierarchyTableDictionary.Create;
  result := FActivityHierarchy;
end;

function TDicDictionary.GetActivityType: TActivityTypeTableDictionary;
begin
  if FActivityType = nil then FActivityType := TActivityTypeTableDictionary.Create;
  result := FActivityType;
end;

function TDicDictionary.GetAddress: TAddressTableDictionary;
begin
  if FAddress = nil then FAddress := TAddressTableDictionary.Create;
  result := FAddress;
end;

function TDicDictionary.GetAddressHierarchy: TAddressHierarchyTableDictionary;
begin
  if FAddressHierarchy = nil then FAddressHierarchy := TAddressHierarchyTableDictionary.Create;
  result := FAddressHierarchy;
end;

function TDicDictionary.GetAddressType: TAddressTypeTableDictionary;
begin
  if FAddressType = nil then FAddressType := TAddressTypeTableDictionary.Create;
  result := FAddressType;
end;

function TDicDictionary.GetAttendOrder: TAttendOrderTableDictionary;
begin
  if FAttendOrder = nil then FAttendOrder := TAttendOrderTableDictionary.Create;
  result := FAttendOrder;
end;

function TDicDictionary.GetCertificate: TCertificateTableDictionary;
begin
  if FCertificate = nil then FCertificate := TCertificateTableDictionary.Create;
  result := FCertificate;
end;

function TDicDictionary.GetChangeCancel: TChangeCancelTableDictionary;
begin
  if FChangeCancel = nil then FChangeCancel := TChangeCancelTableDictionary.Create;
  result := FChangeCancel;
end;

function TDicDictionary.GetChangeCredit: TChangeCreditTableDictionary;
begin
  if FChangeCredit = nil then FChangeCredit := TChangeCreditTableDictionary.Create;
  result := FChangeCredit;
end;

function TDicDictionary.GetChangeHold: TChangeHoldTableDictionary;
begin
  if FChangeHold = nil then FChangeHold := TChangeHoldTableDictionary.Create;
  result := FChangeHold;
end;

function TDicDictionary.GetChangeInfo: TChangeInfoTableDictionary;
begin
  if FChangeInfo = nil then FChangeInfo := TChangeInfoTableDictionary.Create;
  result := FChangeInfo;
end;

function TDicDictionary.GetChangeReason: TChangeReasonTableDictionary;
begin
  if FChangeReason = nil then FChangeReason := TChangeReasonTableDictionary.Create;
  result := FChangeReason;
end;

function TDicDictionary.GetChangeRequest: TChangeRequestTableDictionary;
begin
  if FChangeRequest = nil then FChangeRequest := TChangeRequestTableDictionary.Create;
  result := FChangeRequest;
end;

function TDicDictionary.GetChangeRequestType: TChangeRequestTypeTableDictionary;
begin
  if FChangeRequestType = nil then FChangeRequestType := TChangeRequestTypeTableDictionary.Create;
  result := FChangeRequestType;
end;

function TDicDictionary.GetChangeSwitch: TChangeSwitchTableDictionary;
begin
  if FChangeSwitch = nil then FChangeSwitch := TChangeSwitchTableDictionary.Create;
  result := FChangeSwitch;
end;

function TDicDictionary.GetComponent: TComponentTableDictionary;
begin
  if FComponent = nil then FComponent := TComponentTableDictionary.Create;
  result := FComponent;
end;

function TDicDictionary.GetComponentType: TComponentTypeTableDictionary;
begin
  if FComponentType = nil then FComponentType := TComponentTypeTableDictionary.Create;
  result := FComponentType;
end;

function TDicDictionary.GetEnrollment: TEnrollmentTableDictionary;
begin
  if FEnrollment = nil then FEnrollment := TEnrollmentTableDictionary.Create;
  result := FEnrollment;
end;

function TDicDictionary.GetEntityBase: TEntityBaseTableDictionary;
begin
  if FEntityBase = nil then FEntityBase := TEntityBaseTableDictionary.Create;
  result := FEntityBase;
end;

function TDicDictionary.GetGender: TGenderTableDictionary;
begin
  if FGender = nil then FGender := TGenderTableDictionary.Create;
  result := FGender;
end;

function TDicDictionary.GetGrade: TGradeTableDictionary;
begin
  if FGrade = nil then FGrade := TGradeTableDictionary.Create;
  result := FGrade;
end;

function TDicDictionary.GetInvoice: TInvoiceTableDictionary;
begin
  if FInvoice = nil then FInvoice := TInvoiceTableDictionary.Create;
  result := FInvoice;
end;

function TDicDictionary.GetInvoiceDetail: TInvoiceDetailTableDictionary;
begin
  if FInvoiceDetail = nil then FInvoiceDetail := TInvoiceDetailTableDictionary.Create;
  result := FInvoiceDetail;
end;

function TDicDictionary.GetInvoicingEnrollment: TInvoicingEnrollmentTableDictionary;
begin
  if FInvoicingEnrollment = nil then FInvoicingEnrollment := TInvoicingEnrollmentTableDictionary.Create;
  result := FInvoicingEnrollment;
end;

function TDicDictionary.GetInvoicingItem: TInvoicingItemTableDictionary;
begin
  if FInvoicingItem = nil then FInvoicingItem := TInvoicingItemTableDictionary.Create;
  result := FInvoicingItem;
end;

function TDicDictionary.GetInvoicingPaymentAccount: TInvoicingPaymentAccountTableDictionary;
begin
  if FInvoicingPaymentAccount = nil then FInvoicingPaymentAccount := TInvoicingPaymentAccountTableDictionary.Create;
  result := FInvoicingPaymentAccount;
end;

function TDicDictionary.GetInvoicingSched: TInvoicingSchedTableDictionary;
begin
  if FInvoicingSched = nil then FInvoicingSched := TInvoicingSchedTableDictionary.Create;
  result := FInvoicingSched;
end;

function TDicDictionary.GetInvoicingSchedDetail: TInvoicingSchedDetailTableDictionary;
begin
  if FInvoicingSchedDetail = nil then FInvoicingSchedDetail := TInvoicingSchedDetailTableDictionary.Create;
  result := FInvoicingSchedDetail;
end;

function TDicDictionary.GetInvoicingSchedType: TInvoicingSchedTypeTableDictionary;
begin
  if FInvoicingSchedType = nil then FInvoicingSchedType := TInvoicingSchedTypeTableDictionary.Create;
  result := FInvoicingSchedType;
end;

function TDicDictionary.GetItem: TItemTableDictionary;
begin
  if FItem = nil then FItem := TItemTableDictionary.Create;
  result := FItem;
end;

function TDicDictionary.GetItemHierarchy: TItemHierarchyTableDictionary;
begin
  if FItemHierarchy = nil then FItemHierarchy := TItemHierarchyTableDictionary.Create;
  result := FItemHierarchy;
end;

function TDicDictionary.GetItemMod: TItemModTableDictionary;
begin
  if FItemMod = nil then FItemMod := TItemModTableDictionary.Create;
  result := FItemMod;
end;

function TDicDictionary.GetItemProduct: TItemProductTableDictionary;
begin
  if FItemProduct = nil then FItemProduct := TItemProductTableDictionary.Create;
  result := FItemProduct;
end;

function TDicDictionary.GetItemRate: TItemRateTableDictionary;
begin
  if FItemRate = nil then FItemRate := TItemRateTableDictionary.Create;
  result := FItemRate;
end;

function TDicDictionary.GetItemService: TItemServiceTableDictionary;
begin
  if FItemService = nil then FItemService := TItemServiceTableDictionary.Create;
  result := FItemService;
end;

function TDicDictionary.GetItemType: TItemTypeTableDictionary;
begin
  if FItemType = nil then FItemType := TItemTypeTableDictionary.Create;
  result := FItemType;
end;

function TDicDictionary.GetLocator: TLocatorTableDictionary;
begin
  if FLocator = nil then FLocator := TLocatorTableDictionary.Create;
  result := FLocator;
end;

function TDicDictionary.GetParticipant: TParticipantTableDictionary;
begin
  if FParticipant = nil then FParticipant := TParticipantTableDictionary.Create;
  result := FParticipant;
end;

function TDicDictionary.GetParticipantStatus: TParticipantStatusTableDictionary;
begin
  if FParticipantStatus = nil then FParticipantStatus := TParticipantStatusTableDictionary.Create;
  result := FParticipantStatus;
end;

function TDicDictionary.GetParty: TPartyTableDictionary;
begin
  if FParty = nil then FParty := TPartyTableDictionary.Create;
  result := FParty;
end;

function TDicDictionary.GetPartyGroup: TPartyGroupTableDictionary;
begin
  if FPartyGroup = nil then FPartyGroup := TPartyGroupTableDictionary.Create;
  result := FPartyGroup;
end;

function TDicDictionary.GetPartyHierarchy: TPartyHierarchyTableDictionary;
begin
  if FPartyHierarchy = nil then FPartyHierarchy := TPartyHierarchyTableDictionary.Create;
  result := FPartyHierarchy;
end;

function TDicDictionary.GetPartyPerson: TPartyPersonTableDictionary;
begin
  if FPartyPerson = nil then FPartyPerson := TPartyPersonTableDictionary.Create;
  result := FPartyPerson;
end;

function TDicDictionary.GetPartyRole: TPartyRoleTableDictionary;
begin
  if FPartyRole = nil then FPartyRole := TPartyRoleTableDictionary.Create;
  result := FPartyRole;
end;

function TDicDictionary.GetPartyRoleCustomer: TPartyRoleCustomerTableDictionary;
begin
  if FPartyRoleCustomer = nil then FPartyRoleCustomer := TPartyRoleCustomerTableDictionary.Create;
  result := FPartyRoleCustomer;
end;

function TDicDictionary.GetPartyRoleGuardian: TPartyRoleGuardianTableDictionary;
begin
  if FPartyRoleGuardian = nil then FPartyRoleGuardian := TPartyRoleGuardianTableDictionary.Create;
  result := FPartyRoleGuardian;
end;

function TDicDictionary.GetPartyRoleLead: TPartyRoleLeadTableDictionary;
begin
  if FPartyRoleLead = nil then FPartyRoleLead := TPartyRoleLeadTableDictionary.Create;
  result := FPartyRoleLead;
end;

function TDicDictionary.GetPartyRoleLearningCenter: TPartyRoleLearningCenterTableDictionary;
begin
  if FPartyRoleLearningCenter = nil then FPartyRoleLearningCenter := TPartyRoleLearningCenterTableDictionary.Create;
  result := FPartyRoleLearningCenter;
end;

function TDicDictionary.GetPartyRoleStaff: TPartyRoleStaffTableDictionary;
begin
  if FPartyRoleStaff = nil then FPartyRoleStaff := TPartyRoleStaffTableDictionary.Create;
  result := FPartyRoleStaff;
end;

function TDicDictionary.GetPartyRoleStudent: TPartyRoleStudentTableDictionary;
begin
  if FPartyRoleStudent = nil then FPartyRoleStudent := TPartyRoleStudentTableDictionary.Create;
  result := FPartyRoleStudent;
end;

function TDicDictionary.GetPartyRoleType: TPartyRoleTypeTableDictionary;
begin
  if FPartyRoleType = nil then FPartyRoleType := TPartyRoleTypeTableDictionary.Create;
  result := FPartyRoleType;
end;

function TDicDictionary.GetPartyType: TPartyTypeTableDictionary;
begin
  if FPartyType = nil then FPartyType := TPartyTypeTableDictionary.Create;
  result := FPartyType;
end;

function TDicDictionary.GetPayment: TPaymentTableDictionary;
begin
  if FPayment = nil then FPayment := TPaymentTableDictionary.Create;
  result := FPayment;
end;

function TDicDictionary.GetPaymentAccount: TPaymentAccountTableDictionary;
begin
  if FPaymentAccount = nil then FPaymentAccount := TPaymentAccountTableDictionary.Create;
  result := FPaymentAccount;
end;

function TDicDictionary.GetPaymentAccountCash: TPaymentAccountCashTableDictionary;
begin
  if FPaymentAccountCash = nil then FPaymentAccountCash := TPaymentAccountCashTableDictionary.Create;
  result := FPaymentAccountCash;
end;

function TDicDictionary.GetPaymentAccountCc: TPaymentAccountCcTableDictionary;
begin
  if FPaymentAccountCc = nil then FPaymentAccountCc := TPaymentAccountCcTableDictionary.Create;
  result := FPaymentAccountCc;
end;

function TDicDictionary.GetPaymentAccountCheque: TPaymentAccountChequeTableDictionary;
begin
  if FPaymentAccountCheque = nil then FPaymentAccountCheque := TPaymentAccountChequeTableDictionary.Create;
  result := FPaymentAccountCheque;
end;

function TDicDictionary.GetPaymentAccountDetail: TPaymentAccountDetailTableDictionary;
begin
  if FPaymentAccountDetail = nil then FPaymentAccountDetail := TPaymentAccountDetailTableDictionary.Create;
  result := FPaymentAccountDetail;
end;

function TDicDictionary.GetPaymentAccountMemo: TPaymentAccountMemoTableDictionary;
begin
  if FPaymentAccountMemo = nil then FPaymentAccountMemo := TPaymentAccountMemoTableDictionary.Create;
  result := FPaymentAccountMemo;
end;

function TDicDictionary.GetPaymentAccountMemoType: TPaymentAccountMemoTypeTableDictionary;
begin
  if FPaymentAccountMemoType = nil then FPaymentAccountMemoType := TPaymentAccountMemoTypeTableDictionary.Create;
  result := FPaymentAccountMemoType;
end;

function TDicDictionary.GetPaymentAccountType: TPaymentAccountTypeTableDictionary;
begin
  if FPaymentAccountType = nil then FPaymentAccountType := TPaymentAccountTypeTableDictionary.Create;
  result := FPaymentAccountType;
end;

function TDicDictionary.GetPaymentDetail: TPaymentDetailTableDictionary;
begin
  if FPaymentDetail = nil then FPaymentDetail := TPaymentDetailTableDictionary.Create;
  result := FPaymentDetail;
end;

function TDicDictionary.GetPayInvoice: TPayInvoiceTableDictionary;
begin
  if FPayInvoice = nil then FPayInvoice := TPayInvoiceTableDictionary.Create;
  result := FPayInvoice;
end;

function TDicDictionary.GetPricePeriod: TPricePeriodTableDictionary;
begin
  if FPricePeriod = nil then FPricePeriod := TPricePeriodTableDictionary.Create;
  result := FPricePeriod;
end;

function TDicDictionary.GetQbAccount: TQbAccountTableDictionary;
begin
  if FQbAccount = nil then FQbAccount := TQbAccountTableDictionary.Create;
  result := FQbAccount;
end;

function TDicDictionary.GetReportingSched: TReportingSchedTableDictionary;
begin
  if FReportingSched = nil then FReportingSched := TReportingSchedTableDictionary.Create;
  result := FReportingSched;
end;

function TDicDictionary.GetReportingSchedType: TReportingSchedTypeTableDictionary;
begin
  if FReportingSchedType = nil then FReportingSchedType := TReportingSchedTypeTableDictionary.Create;
  result := FReportingSchedType;
end;

function TDicDictionary.GetRevenueReport: TRevenueReportTableDictionary;
begin
  if FRevenueReport = nil then FRevenueReport := TRevenueReportTableDictionary.Create;
  result := FRevenueReport;
end;

function TDicDictionary.GetSalesOrder: TSalesOrderTableDictionary;
begin
  if FSalesOrder = nil then FSalesOrder := TSalesOrderTableDictionary.Create;
  result := FSalesOrder;
end;

function TDicDictionary.GetSalesOrderDetail: TSalesOrderDetailTableDictionary;
begin
  if FSalesOrderDetail = nil then FSalesOrderDetail := TSalesOrderDetailTableDictionary.Create;
  result := FSalesOrderDetail;
end;

function TDicDictionary.GetSalesOrderDetailMod: TSalesOrderDetailModTableDictionary;
begin
  if FSalesOrderDetailMod = nil then FSalesOrderDetailMod := TSalesOrderDetailModTableDictionary.Create;
  result := FSalesOrderDetailMod;
end;

function TDicDictionary.GetServiceCategory: TServiceCategoryTableDictionary;
begin
  if FServiceCategory = nil then FServiceCategory := TServiceCategoryTableDictionary.Create;
  result := FServiceCategory;
end;

function TDicDictionary.GetServiceLevel: TServiceLevelTableDictionary;
begin
  if FServiceLevel = nil then FServiceLevel := TServiceLevelTableDictionary.Create;
  result := FServiceLevel;
end;

function TDicDictionary.GetUnit: TUnitTableDictionary;
begin
  if FUnit = nil then FUnit := TUnitTableDictionary.Create;
  result := FUnit;
end;

function TDicDictionary.GetComponentHierarchy: TComponentHierarchyTableDictionary;
begin
  if FComponentHierarchy = nil then FComponentHierarchy := TComponentHierarchyTableDictionary.Create;
  result := FComponentHierarchy;
end;

{ TActivityTableDictionary }

constructor TActivityTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FActivityType := TDictionaryAssociation.Create('ActivityType');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
end;

{ TActivityHierarchyTableDictionary }

constructor TActivityHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FIsSingleton := TDictionaryAttribute.Create('IsSingleton');
  FChildActivity := TDictionaryAssociation.Create('ChildActivity');
  FParentActivity := TDictionaryAssociation.Create('ParentActivity');
end;

{ TActivityTypeTableDictionary }

constructor TActivityTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FValidation := TDictionaryAttribute.Create('Validation');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FDataType := TDictionaryAttribute.Create('DataType');
  FDataSize := TDictionaryAttribute.Create('DataSize');
end;

{ TAddressTableDictionary }

constructor TAddressTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FAddressType := TDictionaryAssociation.Create('AddressType');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
end;

{ TAddressHierarchyTableDictionary }

constructor TAddressHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FIsSingleton := TDictionaryAttribute.Create('IsSingleton');
  FChildAddress := TDictionaryAssociation.Create('ChildAddress');
  FParentAddress := TDictionaryAssociation.Create('ParentAddress');
end;

{ TAddressTypeTableDictionary }

constructor TAddressTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FValidation := TDictionaryAttribute.Create('Validation');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FDataType := TDictionaryAttribute.Create('DataType');
  FDataSize := TDictionaryAttribute.Create('DataSize');
end;

{ TAttendOrderTableDictionary }

constructor TAttendOrderTableDictionary.Create;
begin
  inherited;
  FActivity := TDictionaryAssociation.Create('Activity');
  FSalesOrderDetail := TDictionaryAssociation.Create('SalesOrderDetail');
end;

{ TCertificateTableDictionary }

constructor TCertificateTableDictionary.Create;
begin
  inherited;
  FCode := TDictionaryAttribute.Create('Code');
  FDonationValue := TDictionaryAttribute.Create('DonationValue');
  FMaxAmount := TDictionaryAttribute.Create('MaxAmount');
  FDonateTo := TDictionaryAttribute.Create('DonateTo');
  FNote := TDictionaryAttribute.Create('Note');
  FEventStartAt := TDictionaryAttribute.Create('EventStartAt');
  FDeliverAt := TDictionaryAttribute.Create('DeliverAt');
  FPartyGroup := TDictionaryAssociation.Create('PartyGroup');
end;

{ TChangeCancelTableDictionary }

constructor TChangeCancelTableDictionary.Create;
begin
  inherited;
  FIsContractComplete := TDictionaryAttribute.Create('IsContractComplete');
  FCancellationFee := TDictionaryAttribute.Create('CancellationFee');
  FCancelAt := TDictionaryAttribute.Create('CancelAt');
end;

{ TChangeCreditTableDictionary }

constructor TChangeCreditTableDictionary.Create;
begin
  inherited;
  FIssueType := TDictionaryAttribute.Create('IssueType');
  FCreditType := TDictionaryAttribute.Create('CreditType');
  FAmount := TDictionaryAttribute.Create('Amount');
  FCreditAt := TDictionaryAttribute.Create('CreditAt');
  FPaymentDetail := TDictionaryAssociation.Create('PaymentDetail');
end;

{ TChangeHoldTableDictionary }

constructor TChangeHoldTableDictionary.Create;
begin
  inherited;
  FAttendanceHoldStartAt := TDictionaryAttribute.Create('AttendanceHoldStartAt');
  FAttendanceHoldEndAt := TDictionaryAttribute.Create('AttendanceHoldEndAt');
  FBillingHoldStartAt := TDictionaryAttribute.Create('BillingHoldStartAt');
  FBillingHoldEndAt := TDictionaryAttribute.Create('BillingHoldEndAt');
end;

{ TChangeInfoTableDictionary }

constructor TChangeInfoTableDictionary.Create;
begin
  inherited;
  FFieldName := TDictionaryAttribute.Create('FieldName');
  FOldValue := TDictionaryAttribute.Create('OldValue');
  FNewValue := TDictionaryAttribute.Create('NewValue');
  FReplaceMethod := TDictionaryAttribute.Create('ReplaceMethod');
  FAddress := TDictionaryAssociation.Create('Address');
  FAddress := TDictionaryAssociation.Create('Address');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
end;

{ TChangeReasonTableDictionary }

constructor TChangeReasonTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TChangeRequestTableDictionary }

constructor TChangeRequestTableDictionary.Create;
begin
  inherited;
  FIsApplied := TDictionaryAttribute.Create('IsApplied');
  FRequestBy := TDictionaryAttribute.Create('RequestBy');
  FApprovalStatus := TDictionaryAttribute.Create('ApprovalStatus');
  FApproveBy := TDictionaryAttribute.Create('ApproveBy');
  FChangeRequestStatus := TDictionaryAttribute.Create('ChangeRequestStatus');
  FNote := TDictionaryAttribute.Create('Note');
  FCdsEnterBy := TDictionaryAttribute.Create('CdsEnterBy');
  FCybersourceEnterBy := TDictionaryAttribute.Create('CybersourceEnterBy');
  FIsApproved := TDictionaryAttribute.Create('IsApproved');
  FUnitCount := TDictionaryAttribute.Create('UnitCount');
  FInclusiveRange := TDictionaryAttribute.Create('InclusiveRange');
  FChangeRequestId := TDictionaryAttribute.Create('ChangeRequestId');
  FApplyAt := TDictionaryAttribute.Create('ApplyAt');
  FRequestAt := TDictionaryAttribute.Create('RequestAt');
  FApproveAt := TDictionaryAttribute.Create('ApproveAt');
  FCdsEnterAt := TDictionaryAttribute.Create('CdsEnterAt');
  FCybersourceEnterAt := TDictionaryAttribute.Create('CybersourceEnterAt');
  FChangeReason := TDictionaryAssociation.Create('ChangeReason');
  FEnrollment := TDictionaryAssociation.Create('Enrollment');
  FItem := TDictionaryAssociation.Create('Item');
  FParty := TDictionaryAssociation.Create('Party');
  FChangeRequestType := TDictionaryAssociation.Create('ChangeRequestType');
end;

{ TChangeRequestTypeTableDictionary }

constructor TChangeRequestTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TChangeSwitchTableDictionary }

constructor TChangeSwitchTableDictionary.Create;
begin
  inherited;
  FOldEnrollmentEndAt := TDictionaryAttribute.Create('OldEnrollmentEndAt');
  FNewEnrollmentStartAt := TDictionaryAttribute.Create('NewEnrollmentStartAt');
  FNewEnrollmentEndAt := TDictionaryAttribute.Create('NewEnrollmentEndAt');
  FEnrollment := TDictionaryAssociation.Create('Enrollment');
  FItem := TDictionaryAssociation.Create('Item');
end;

{ TComponentTableDictionary }

constructor TComponentTableDictionary.Create;
begin
  inherited;
  FContentString := TDictionaryAttribute.Create('ContentString');
  FContentDatetime := TDictionaryAttribute.Create('ContentDatetime');
  FContentNumeric := TDictionaryAttribute.Create('ContentNumeric');
  FContentBoolean := TDictionaryAttribute.Create('ContentBoolean');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FVersionMajor := TDictionaryAttribute.Create('VersionMajor');
  FVersionMinor := TDictionaryAttribute.Create('VersionMinor');
  FVersionBuild := TDictionaryAttribute.Create('VersionBuild');
  FComponentType := TDictionaryAssociation.Create('ComponentType');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
end;

{ TComponentTypeTableDictionary }

constructor TComponentTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FValidation := TDictionaryAttribute.Create('Validation');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FDataType := TDictionaryAttribute.Create('DataType');
  FDataSize := TDictionaryAttribute.Create('DataSize');
end;

{ TEnrollmentTableDictionary }

constructor TEnrollmentTableDictionary.Create;
begin
  inherited;
  FBlock := TDictionaryAttribute.Create('Block');
  FNote := TDictionaryAttribute.Create('Note');
  FLegacyId := TDictionaryAttribute.Create('LegacyId');
  FLegacyCode := TDictionaryAttribute.Create('LegacyCode');
  FOriginalPricePeriodId := TDictionaryAttribute.Create('OriginalPricePeriodId');
  FAgreeAt := TDictionaryAttribute.Create('AgreeAt');
  FAgreeStartAt := TDictionaryAttribute.Create('AgreeStartAt');
  FAgreeEndAt := TDictionaryAttribute.Create('AgreeEndAt');
  FTerminateAt := TDictionaryAttribute.Create('TerminateAt');
  FProjectedEndAt := TDictionaryAttribute.Create('ProjectedEndAt');
  FItem := TDictionaryAssociation.Create('Item');
  FPartyRoleStudent := TDictionaryAssociation.Create('PartyRoleStudent');
end;

{ TEntityBaseTableDictionary }

constructor TEntityBaseTableDictionary.Create;
begin
  inherited;
  FId := TDictionaryAttribute.Create('Id');
  FIsActive := TDictionaryAttribute.Create('IsActive');
  FCreatedBy := TDictionaryAttribute.Create('CreatedBy');
  FCreatedAt := TDictionaryAttribute.Create('CreatedAt');
  FUpdatedBy := TDictionaryAttribute.Create('UpdatedBy');
  FUpdatedAt := TDictionaryAttribute.Create('UpdatedAt');
  FPos := TDictionaryAttribute.Create('Pos');
  FExternalSource := TDictionaryAttribute.Create('ExternalSource');
  FExternalId := TDictionaryAttribute.Create('ExternalId');
  FTableName := TDictionaryAttribute.Create('TableName');
end;

{ TGenderTableDictionary }

constructor TGenderTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FSubjectPronoun := TDictionaryAttribute.Create('SubjectPronoun');
  FObjectPronoun := TDictionaryAttribute.Create('ObjectPronoun');
  FPossessiveDetPronoun := TDictionaryAttribute.Create('PossessiveDetPronoun');
  FPossessivePronoun := TDictionaryAttribute.Create('PossessivePronoun');
  FReflexivePronoun := TDictionaryAttribute.Create('ReflexivePronoun');
  FTitle := TDictionaryAttribute.Create('Title');
end;

{ TGradeTableDictionary }

constructor TGradeTableDictionary.Create;
begin
  inherited;
  FServiceLevelId := TDictionaryAttribute.Create('ServiceLevelId');
  FGrade := TDictionaryAttribute.Create('Grade');
  FName := TDictionaryAttribute.Create('Name');
end;

{ TInvoiceTableDictionary }

constructor TInvoiceTableDictionary.Create;
begin
  inherited;
  FIsVoided := TDictionaryAttribute.Create('IsVoided');
  FNote := TDictionaryAttribute.Create('Note');
  FAmount := TDictionaryAttribute.Create('Amount');
  FPos := TDictionaryAttribute.Create('Pos');
  FDueAt := TDictionaryAttribute.Create('DueAt');
  FChangeRequest := TDictionaryAssociation.Create('ChangeRequest');
  FInvoicingSched := TDictionaryAssociation.Create('InvoicingSched');
  FPartyRoleCustomer := TDictionaryAssociation.Create('PartyRoleCustomer');
end;

{ TInvoiceDetailTableDictionary }

constructor TInvoiceDetailTableDictionary.Create;
begin
  inherited;
  FUnitRate := TDictionaryAttribute.Create('UnitRate');
  FDivisor := TDictionaryAttribute.Create('Divisor');
  FNote := TDictionaryAttribute.Create('Note');
  FPos := TDictionaryAttribute.Create('Pos');
  FUnitCount := TDictionaryAttribute.Create('UnitCount');
  FAmount := TDictionaryAttribute.Create('Amount');
  FInvoice := TDictionaryAssociation.Create('Invoice');
  FItemMod := TDictionaryAssociation.Create('ItemMod');
  FSalesOrderDetail := TDictionaryAssociation.Create('SalesOrderDetail');
end;

{ TInvoicingEnrollmentTableDictionary }

constructor TInvoicingEnrollmentTableDictionary.Create;
begin
  inherited;
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
  FEnrollment := TDictionaryAssociation.Create('Enrollment');
  FInvoicingSched := TDictionaryAssociation.Create('InvoicingSched');
end;

{ TInvoicingItemTableDictionary }

constructor TInvoicingItemTableDictionary.Create;
begin
  inherited;
  FNote := TDictionaryAttribute.Create('Note');
  FInvoicingSched := TDictionaryAssociation.Create('InvoicingSched');
  FItem := TDictionaryAssociation.Create('Item');
end;

{ TInvoicingPaymentAccountTableDictionary }

constructor TInvoicingPaymentAccountTableDictionary.Create;
begin
  inherited;
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
  FInvoicingSched := TDictionaryAssociation.Create('InvoicingSched');
  FPaymentAccountDetail := TDictionaryAssociation.Create('PaymentAccountDetail');
end;

{ TInvoicingSchedTableDictionary }

constructor TInvoicingSchedTableDictionary.Create;
begin
  inherited;
  FPercentage := TDictionaryAttribute.Create('Percentage');
  FInitialAmount := TDictionaryAttribute.Create('InitialAmount');
  FRecurringAmount := TDictionaryAttribute.Create('RecurringAmount');
  FInstallmentCount := TDictionaryAttribute.Create('InstallmentCount');
  FUnitCountPerCycle := TDictionaryAttribute.Create('UnitCountPerCycle');
  FDayOfUnit := TDictionaryAttribute.Create('DayOfUnit');
  FCurrency := TDictionaryAttribute.Create('Currency');
  FPaymentMethod := TDictionaryAttribute.Create('PaymentMethod');
  FCombineOrders := TDictionaryAttribute.Create('CombineOrders');
  FNote := TDictionaryAttribute.Create('Note');
  FSeparateLifetimeUnits := TDictionaryAttribute.Create('SeparateLifetimeUnits');
  FSubscriptionId := TDictionaryAttribute.Create('SubscriptionId');
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
  FInvoicingSchedType := TDictionaryAssociation.Create('InvoicingSchedType');
  FUnit_ := TDictionaryAssociation.Create('Unit_');
end;

{ TInvoicingSchedDetailTableDictionary }

constructor TInvoicingSchedDetailTableDictionary.Create;
begin
  inherited;
  FScheduleIndex := TDictionaryAttribute.Create('ScheduleIndex');
  FAutoSuggest := TDictionaryAttribute.Create('AutoSuggest');
  FStatus := TDictionaryAttribute.Create('Status');
  FContentBlob := TDictionaryAttribute.Create('ContentBlob');
  FActionRequired := TDictionaryAttribute.Create('ActionRequired');
  FCompleteBy := TDictionaryAttribute.Create('CompleteBy');
  FMinCount := TDictionaryAttribute.Create('MinCount');
  FMaxCount := TDictionaryAttribute.Create('MaxCount');
  FScheduleAt := TDictionaryAttribute.Create('ScheduleAt');
  FCompleteAt := TDictionaryAttribute.Create('CompleteAt');
  FInvoicingSched := TDictionaryAssociation.Create('InvoicingSched');
end;

{ TInvoicingSchedTypeTableDictionary }

constructor TInvoicingSchedTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TItemTableDictionary }

constructor TItemTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FAlias := TDictionaryAttribute.Create('Alias');
  FDescription := TDictionaryAttribute.Create('Description');
  FTermAndCondition := TDictionaryAttribute.Create('TermAndCondition');
  FQbAccount := TDictionaryAssociation.Create('QbAccount');
  FReportingSched := TDictionaryAssociation.Create('ReportingSched');
  FUnit_ := TDictionaryAssociation.Create('Unit_');
  FItemType := TDictionaryAssociation.Create('ItemType');
end;

{ TItemHierarchyTableDictionary }

constructor TItemHierarchyTableDictionary.Create;
begin
  inherited;
  FUnitCount := TDictionaryAttribute.Create('UnitCount');
  FUnitCountPerLine := TDictionaryAttribute.Create('UnitCountPerLine');
  FDivisor := TDictionaryAttribute.Create('Divisor');
  FIsRecurring := TDictionaryAttribute.Create('IsRecurring');
  FPos := TDictionaryAttribute.Create('Pos');
  FMinCount := TDictionaryAttribute.Create('MinCount');
  FMaxCount := TDictionaryAttribute.Create('MaxCount');
  FEntityBase := TDictionaryAssociation.Create('EntityBase');
  FChildItem := TDictionaryAssociation.Create('ChildItem');
  FParentItem := TDictionaryAssociation.Create('ParentItem');
end;

{ TItemModTableDictionary }

constructor TItemModTableDictionary.Create;
begin
  inherited;
  FModUseCount := TDictionaryAttribute.Create('ModUseCount');
  FModBlob := TDictionaryAttribute.Create('ModBlob');
end;

{ TItemProductTableDictionary }

constructor TItemProductTableDictionary.Create;
begin
  inherited;
  FInventoryId := TDictionaryAttribute.Create('InventoryId');
end;

{ TItemRateTableDictionary }

constructor TItemRateTableDictionary.Create;
begin
  inherited;
  FLocationId := TDictionaryAttribute.Create('LocationId');
  FDivisor := TDictionaryAttribute.Create('Divisor');
  FUnitRate := TDictionaryAttribute.Create('UnitRate');
  FItem := TDictionaryAssociation.Create('Item');
  FPricePeriod := TDictionaryAssociation.Create('PricePeriod');
end;

{ TItemServiceTableDictionary }

constructor TItemServiceTableDictionary.Create;
begin
  inherited;
  FServiceCategory := TDictionaryAssociation.Create('ServiceCategory');
  FServiceLevel := TDictionaryAssociation.Create('ServiceLevel');
end;

{ TItemTypeTableDictionary }

constructor TItemTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TLocatorTableDictionary }

constructor TLocatorTableDictionary.Create;
begin
  inherited;
  FAddress := TDictionaryAssociation.Create('Address');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TParticipantTableDictionary }

constructor TParticipantTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FActivity := TDictionaryAssociation.Create('Activity');
  FParticipantStatus := TDictionaryAssociation.Create('ParticipantStatus');
  FPartyRole := TDictionaryAssociation.Create('PartyRole');
end;

{ TParticipantStatusTableDictionary }

constructor TParticipantStatusTableDictionary.Create;
begin
  inherited;
  FId := TDictionaryAttribute.Create('Id');
  FStatus := TDictionaryAttribute.Create('Status');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TPartyTableDictionary }

constructor TPartyTableDictionary.Create;
begin
  inherited;
  FPartyType := TDictionaryAssociation.Create('PartyType');
end;

{ TPartyGroupTableDictionary }

constructor TPartyGroupTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FAlias := TDictionaryAttribute.Create('Alias');
  FGroupType := TDictionaryAttribute.Create('GroupType');
  FIsForProfit := TDictionaryAttribute.Create('IsForProfit');
  FNote := TDictionaryAttribute.Create('Note');
end;

{ TPartyHierarchyTableDictionary }

constructor TPartyHierarchyTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsPrimary := TDictionaryAttribute.Create('IsPrimary');
  FNote := TDictionaryAttribute.Create('Note');
  FChildParty := TDictionaryAssociation.Create('ChildParty');
  FParentParty := TDictionaryAssociation.Create('ParentParty');
end;

{ TPartyPersonTableDictionary }

constructor TPartyPersonTableDictionary.Create;
begin
  inherited;
  FPrefix := TDictionaryAttribute.Create('Prefix');
  FFirstName := TDictionaryAttribute.Create('FirstName');
  FMiddleNames := TDictionaryAttribute.Create('MiddleNames');
  FLastName := TDictionaryAttribute.Create('LastName');
  FSuffix := TDictionaryAttribute.Create('Suffix');
  FNickName := TDictionaryAttribute.Create('NickName');
  FMedicalInfo := TDictionaryAttribute.Create('MedicalInfo');
  FDoNotContact := TDictionaryAttribute.Create('DoNotContact');
  FBirthDay := TDictionaryAttribute.Create('BirthDay');
  FBirthMonth := TDictionaryAttribute.Create('BirthMonth');
  FBirthYear := TDictionaryAttribute.Create('BirthYear');
  FBirthDate := TDictionaryAttribute.Create('BirthDate');
  FGender := TDictionaryAssociation.Create('Gender');
end;

{ TPartyRoleTableDictionary }

constructor TPartyRoleTableDictionary.Create;
begin
  inherited;
  FNote := TDictionaryAttribute.Create('Note');
  FParty := TDictionaryAssociation.Create('Party');
  FPartyRoleType := TDictionaryAssociation.Create('PartyRoleType');
end;

{ TPartyRoleGuardianTableDictionary }

constructor TPartyRoleGuardianTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsEmergencyContact := TDictionaryAttribute.Create('IsEmergencyContact');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TPartyRoleLeadTableDictionary }

constructor TPartyRoleLeadTableDictionary.Create;
begin
  inherited;
  FConvertAt := TDictionaryAttribute.Create('ConvertAt');
  FInitialContactAt := TDictionaryAttribute.Create('InitialContactAt');
end;

{ TPartyRoleLearningCenterTableDictionary }

constructor TPartyRoleLearningCenterTableDictionary.Create;
begin
  inherited;
  FStudentNumberPrefix := TDictionaryAttribute.Create('StudentNumberPrefix');
  FNextStudentNumber := TDictionaryAttribute.Create('NextStudentNumber');
end;

{ TPartyRoleStaffTableDictionary }

constructor TPartyRoleStaffTableDictionary.Create;
begin
  inherited;
  FHireAt := TDictionaryAttribute.Create('HireAt');
end;

{ TPartyRoleStudentTableDictionary }

constructor TPartyRoleStudentTableDictionary.Create;
begin
  inherited;
  FStudentNumber := TDictionaryAttribute.Create('StudentNumber');
  FLivesWith := TDictionaryAttribute.Create('LivesWith');
  FConsentContactSchool := TDictionaryAttribute.Create('ConsentContactSchool');
  FConsentLeave := TDictionaryAttribute.Create('ConsentLeave');
  FConsentPhotograph := TDictionaryAttribute.Create('ConsentPhotograph');
  FSchoolEntryDate := TDictionaryAttribute.Create('SchoolEntryDate');
  FGrade := TDictionaryAssociation.Create('Grade');
end;

{ TPartyRoleTypeTableDictionary }

constructor TPartyRoleTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FIsPersonRole := TDictionaryAttribute.Create('IsPersonRole');
  FIsGroupRole := TDictionaryAttribute.Create('IsGroupRole');
end;

{ TPartyTypeTableDictionary }

constructor TPartyTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TPaymentTableDictionary }

constructor TPaymentTableDictionary.Create;
begin
  inherited;
  FAmount := TDictionaryAttribute.Create('Amount');
  FNote := TDictionaryAttribute.Create('Note');
  FCdsId2List := TDictionaryAttribute.Create('CdsId2List');
  FStatus := TDictionaryAttribute.Create('Status');
  FAppliedAmount := TDictionaryAttribute.Create('AppliedAmount');
  FProcessAt := TDictionaryAttribute.Create('ProcessAt');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TPaymentAccountTableDictionary }

constructor TPaymentAccountTableDictionary.Create;
begin
  inherited;
  FParty := TDictionaryAssociation.Create('Party');
  FPaymentAccountType := TDictionaryAssociation.Create('PaymentAccountType');
end;

{ TPaymentAccountCcTableDictionary }

constructor TPaymentAccountCcTableDictionary.Create;
begin
  inherited;
  FAutoBillDay := TDictionaryAttribute.Create('AutoBillDay');
  FCcNumber := TDictionaryAttribute.Create('CcNumber');
  FLastFour := TDictionaryAttribute.Create('LastFour');
  FCardType := TDictionaryAttribute.Create('CardType');
end;

{ TPaymentAccountDetailTableDictionary }

constructor TPaymentAccountDetailTableDictionary.Create;
begin
  inherited;
  FFirstNameOnCard := TDictionaryAttribute.Create('FirstNameOnCard');
  FLastNameOnCard := TDictionaryAttribute.Create('LastNameOnCard');
  FExpirationDate := TDictionaryAttribute.Create('ExpirationDate');
  FCvn := TDictionaryAttribute.Create('Cvn');
  FAddress := TDictionaryAssociation.Create('Address');
  FPaymentAccount := TDictionaryAssociation.Create('PaymentAccount');
end;

{ TPaymentAccountMemoTableDictionary }

constructor TPaymentAccountMemoTableDictionary.Create;
begin
  inherited;
  FCertificate := TDictionaryAssociation.Create('Certificate');
  FPaymentAccountMemoType := TDictionaryAssociation.Create('PaymentAccountMemoType');
end;

{ TPaymentAccountMemoTypeTableDictionary }

constructor TPaymentAccountMemoTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TPaymentAccountTypeTableDictionary }

constructor TPaymentAccountTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TPaymentDetailTableDictionary }

constructor TPaymentDetailTableDictionary.Create;
begin
  inherited;
  FProcessAt := TDictionaryAttribute.Create('ProcessAt');
  FAmount := TDictionaryAttribute.Create('Amount');
  FActionType := TDictionaryAttribute.Create('ActionType');
  FStatus := TDictionaryAttribute.Create('Status');
  FTransactionDetail := TDictionaryAttribute.Create('TransactionDetail');
  FPayment := TDictionaryAssociation.Create('Payment');
  FPaymentAccountDetail := TDictionaryAssociation.Create('PaymentAccountDetail');
  FPaymentDetail := TDictionaryAssociation.Create('PaymentDetail');
end;

{ TPayInvoiceTableDictionary }

constructor TPayInvoiceTableDictionary.Create;
begin
  inherited;
  FAmount := TDictionaryAttribute.Create('Amount');
  FInvoice := TDictionaryAssociation.Create('Invoice');
  FPayment := TDictionaryAssociation.Create('Payment');
end;

{ TPricePeriodTableDictionary }

constructor TPricePeriodTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
end;

{ TQbAccountTableDictionary }

constructor TQbAccountTableDictionary.Create;
begin
  inherited;
  FAccountNumber := TDictionaryAttribute.Create('AccountNumber');
  FName := TDictionaryAttribute.Create('Name');
  FQbAccountType := TDictionaryAttribute.Create('QbAccountType');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TReportingSchedTableDictionary }

constructor TReportingSchedTableDictionary.Create;
begin
  inherited;
  FInstallmentCount := TDictionaryAttribute.Create('InstallmentCount');
  FInstallmentShift := TDictionaryAttribute.Create('InstallmentShift');
  FUnit_ := TDictionaryAssociation.Create('Unit_');
  FReportingSchedType := TDictionaryAssociation.Create('ReportingSchedType');
end;

{ TReportingSchedTypeTableDictionary }

constructor TReportingSchedTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TRevenueReportTableDictionary }

constructor TRevenueReportTableDictionary.Create;
begin
  inherited;
  FIsCorrection := TDictionaryAttribute.Create('IsCorrection');
  FIsExpired := TDictionaryAttribute.Create('IsExpired');
  FBookUnitCount := TDictionaryAttribute.Create('BookUnitCount');
  FBookAmount := TDictionaryAttribute.Create('BookAmount');
  FReportingNote := TDictionaryAttribute.Create('ReportingNote');
  FBookAt := TDictionaryAttribute.Create('BookAt');
  FReportAt := TDictionaryAttribute.Create('ReportAt');
  FPartyGroup := TDictionaryAssociation.Create('PartyGroup');
  FQbAccount := TDictionaryAssociation.Create('QbAccount');
  FSalesOrderDetail := TDictionaryAssociation.Create('SalesOrderDetail');
end;

{ TSalesOrderTableDictionary }

constructor TSalesOrderTableDictionary.Create;
begin
  inherited;
  FNote := TDictionaryAttribute.Create('Note');
  FChangeRequest := TDictionaryAssociation.Create('ChangeRequest');
  FEnrollment := TDictionaryAssociation.Create('Enrollment');
  FPartyRoleCustomer := TDictionaryAssociation.Create('PartyRoleCustomer');
  FPricePeriod := TDictionaryAssociation.Create('PricePeriod');
end;

{ TSalesOrderDetailTableDictionary }

constructor TSalesOrderDetailTableDictionary.Create;
begin
  inherited;
  FIsCancelled := TDictionaryAttribute.Create('IsCancelled');
  FNote := TDictionaryAttribute.Create('Note');
  FInvoiceNumber := TDictionaryAttribute.Create('InvoiceNumber');
  FUnitCount := TDictionaryAttribute.Create('UnitCount');
  FStartAt := TDictionaryAttribute.Create('StartAt');
  FEndAt := TDictionaryAttribute.Create('EndAt');
  FItem := TDictionaryAssociation.Create('Item');
  FReportingSched := TDictionaryAssociation.Create('ReportingSched');
  FSalesOrder := TDictionaryAssociation.Create('SalesOrder');
end;

{ TSalesOrderDetailModTableDictionary }

constructor TSalesOrderDetailModTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsOverride := TDictionaryAttribute.Create('IsOverride');
  FIsApplied := TDictionaryAttribute.Create('IsApplied');
  FNote := TDictionaryAttribute.Create('Note');
  FItemMod := TDictionaryAssociation.Create('ItemMod');
  FSalesOrderDetail := TDictionaryAssociation.Create('SalesOrderDetail');
end;

{ TServiceCategoryTableDictionary }

constructor TServiceCategoryTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TServiceLevelTableDictionary }

constructor TServiceLevelTableDictionary.Create;
begin
  inherited;
  FServiceLevel := TDictionaryAttribute.Create('ServiceLevel');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TUnitTableDictionary }

constructor TUnitTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FUnitType := TDictionaryAttribute.Create('UnitType');
  FDescription := TDictionaryAttribute.Create('Description');
  FLogicLineCount := TDictionaryAttribute.Create('LogicLineCount');
  FLogicStartDate := TDictionaryAttribute.Create('LogicStartDate');
  FLogicEndDate := TDictionaryAttribute.Create('LogicEndDate');
  FLogicUnitCount := TDictionaryAttribute.Create('LogicUnitCount');
end;

{ TComponentHierarchyTableDictionary }

constructor TComponentHierarchyTableDictionary.Create;
begin
  inherited;
  FPos := TDictionaryAttribute.Create('Pos');
  FIsTemplate := TDictionaryAttribute.Create('IsTemplate');
  FIsSingleton := TDictionaryAttribute.Create('IsSingleton');
  FChildComponent := TDictionaryAssociation.Create('ChildComponent');
  FParentComponent := TDictionaryAssociation.Create('ParentComponent');
end;

initialization
  RegisterEntity(TActivity);
  RegisterEntity(TActivityHierarchy);
  RegisterEntity(TActivityType);
  RegisterEntity(TAddress);
  RegisterEntity(TAddressHierarchy);
  RegisterEntity(TAddressType);
  RegisterEntity(TAttendOrder);
  RegisterEntity(TCertificate);
  RegisterEntity(TChangeCancel);
  RegisterEntity(TChangeCredit);
  RegisterEntity(TChangeHold);
  RegisterEntity(TChangeInfo);
  RegisterEntity(TChangeReason);
  RegisterEntity(TChangeRequest);
  RegisterEntity(TChangeRequestType);
  RegisterEntity(TChangeSwitch);
  RegisterEntity(TComponent);
  RegisterEntity(TComponentType);
  RegisterEntity(TEnrollment);
  RegisterEntity(TEntityBase);
  RegisterEntity(TGender);
  RegisterEntity(TGrade);
  RegisterEntity(TInvoice);
  RegisterEntity(TInvoiceDetail);
  RegisterEntity(TInvoicingEnrollment);
  RegisterEntity(TInvoicingItem);
  RegisterEntity(TInvoicingPaymentAccount);
  RegisterEntity(TInvoicingSched);
  RegisterEntity(TInvoicingSchedDetail);
  RegisterEntity(TInvoicingSchedType);
  RegisterEntity(TItem);
  RegisterEntity(TItemHierarchy);
  RegisterEntity(TItemMod);
  RegisterEntity(TItemProduct);
  RegisterEntity(TItemRate);
  RegisterEntity(TItemService);
  RegisterEntity(TItemType);
  RegisterEntity(TLocator);
  RegisterEntity(TParticipant);
  RegisterEntity(TParticipantStatus);
  RegisterEntity(TParty);
  RegisterEntity(TPartyGroup);
  RegisterEntity(TPartyHierarchy);
  RegisterEntity(TPartyPerson);
  RegisterEntity(TPartyRole);
  RegisterEntity(TPartyRoleCustomer);
  RegisterEntity(TPartyRoleGuardian);
  RegisterEntity(TPartyRoleLead);
  RegisterEntity(TPartyRoleLearningCenter);
  RegisterEntity(TPartyRoleStaff);
  RegisterEntity(TPartyRoleStudent);
  RegisterEntity(TPartyRoleType);
  RegisterEntity(TPartyType);
  RegisterEntity(TPayment);
  RegisterEntity(TPaymentAccount);
  RegisterEntity(TPaymentAccountCash);
  RegisterEntity(TPaymentAccountCc);
  RegisterEntity(TPaymentAccountCheque);
  RegisterEntity(TPaymentAccountDetail);
  RegisterEntity(TPaymentAccountMemo);
  RegisterEntity(TPaymentAccountMemoType);
  RegisterEntity(TPaymentAccountType);
  RegisterEntity(TPaymentDetail);
  RegisterEntity(TPayInvoice);
  RegisterEntity(TPricePeriod);
  RegisterEntity(TQbAccount);
  RegisterEntity(TReportingSched);
  RegisterEntity(TReportingSchedType);
  RegisterEntity(TRevenueReport);
  RegisterEntity(TSalesOrder);
  RegisterEntity(TSalesOrderDetail);
  RegisterEntity(TSalesOrderDetailMod);
  RegisterEntity(TServiceCategory);
  RegisterEntity(TServiceLevel);
  RegisterEntity(TUnit);
  RegisterEntity(TComponentHierarchy);

finalization
  if __Dic <> nil then __Dic.Free

end.
