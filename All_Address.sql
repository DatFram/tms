/* --------------------------------------------------------------
Requires TEST6 database.

UpdateSQL statements can be found after the query SQL
NOTE: 20 TFDUpdateSQL instances are required to fully support this query

PURPOSE:
	All Addresses
	Returns all postal addresses
	
	The Fields returned by this query are as follows:
	ID - GUID uniquely representing the master address record CHAR(16) Octets
	STREET1 - Address street line 1
	STREET2 - Address street line 1
	CITY - City
	STATE - State
	POSTAL_CODE - Postal Code (not formatted yet, though this could be done in the query as well to add a dash between the 5 digit zip and the 4 digit post stop)
	COUNTRY - Country
	STREET1_ID
	STREET2_ID
	CITY_ID
	STATE_ID
	POSTAL_CODE_ID
	COUNTRY_ID
	
NOTES:
    Outer query fetches all ADDRESS records marked as
	US_POSTAL_ADDRESS, and each inner query fetches the detail
	address that makes up one part of the postal address, e.g.
	Street1, Street2, City, etc.
	
PERFORMANCE:
	- 0.1 seconds in FlameRobin
	
CREATED: 2017-05-16
UPDATED: 2017-05-20 with changed schema

Alternative Query: 
	This query does not show the address on one line, rather it
	shows each constituent part of the query as its own line.

	SELECT
		a.ID,
		t2.NAME,
		a2.CONTENTS,
		ah.POSITION_INDEX,
		a2.ID AS DETAIL_ID
	FROM ADDRESS a
	JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
	JOIN ADDRESS_HIERARCHY ah ON ah.PARENT_ADDRESS_ID = a.ID 
	JOIN ADDRESS a2 ON a2.ID = ah.CHILD_ADDRESS_ID
	JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
	WHERE t.NAME = 'US_POSTAL_ADDRESS'
	ORDER BY a.ID, ah.POSITION_INDEX
*/

SELECT
    a.ID,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ADDRESS1') AS ADDRESS1,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ADDRESS1') AS ADDRESS2,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'CITY') AS CITY,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STATE') AS STATE,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ZIPCODE') AS ZIPCODE,
    (SELECT a2.CONTENT 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'COUNTRY') AS COUNTRY,
    (SELECT a2.ID 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ADDRESS1' AND ah2.POSITION_INDEX = 1) AS ADDRESS1_ID,
    (SELECT a2.ID 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ADDRESS2' AND ah2.POSITION_INDEX = 2) AS ADDRESS2_ID,
    (SELECT a2.ID 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'CITY') AS CITY_ID,
    (SELECT a2.ID 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STATE') AS STATE_ID,
    (SELECT a2.ID
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'ZIPCODE') AS ZIPCODE_ID,
    (SELECT a2.ID
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'COUNTRY') AS COUNTRY_ID
FROM ADDRESS a
JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
WHERE t.NAME = 'POSTAL_ADDRESS_US'
ORDER BY a.ID

/* *****************************************************
	1. Master ENTITY_BASE statements
*/

/* DeleteSQL: */

DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID;

/* FetchRowSQL: */

SELECT
    a.ID,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STREET' AND ah2.POSITION_INDEX = 1) AS STREET1,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STREET' AND ah2.POSITION_INDEX = 2) AS STREET2,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'CITY') AS CITY,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STATE') AS STATE,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'POSTAL_CODE') AS POSTAL_CODE,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'COUNTRY') AS COUNTRY
FROM ADDRESS a
JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
WHERE t.NAME = 'US_POSTAL_ADDRESS'
AND a.ID = :OLD_ID;

/* InsertSQL: */

INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:NEW_ID, 'ADDRESS');

/* ModifySQL: */

UPDATE ENTITY_BASE eb SET eb.ID = :NEW_ID WHERE eb.ID = :OLD_ID;

/* *****************************************************
	2. Master ADDRESS statements
*/

/* DeleteSQL: */

DELETE FROM ADDRESS a WHERE a.ID = :OLD_ID;

/* FetchRowSQL: */

SELECT
    a.ID,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STREET' AND ah2.POSITION_INDEX = 1) AS STREET1,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STREET' AND ah2.POSITION_INDEX = 2) AS STREET2,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'CITY') AS CITY,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'STATE') AS STATE,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'POSTAL_CODE') AS POSTAL_CODE,
    (SELECT a2.CONTENTS 
    FROM ADDRESS a2 
    JOIN ADDRESS_HIERARCHY ah2 ON ah2.CHILD_ADDRESS_ID = a2.ID 
    JOIN ADDRESS_TYPE t2 ON t2.ID = a2.ADDRESS_TYPE_ID
    WHERE ah2.PARENT_ADDRESS_ID = a.ID AND t2.NAME = 'COUNTRY') AS COUNTRY
FROM ADDRESS a
JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
WHERE t.NAME = 'US_POSTAL_ADDRESS'
AND a.ID = :OLD_ID;

/* InsertSQL: */

INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
SELECT :NEW_ID, t.ID
FROM ADDRESS_TYPE t WHERE t.NAME = 'US_POSTAL_ADDRESS';

/* ModifySQL: */

UPDATE ADDRESS a SET a.ID = :NEW_ID
WHERE a.ID = :OLD_ID;


/* *****************************************************
	3. STREET1 ENTITY_BASE statements
*/

/* DeleteSQL: */

DELETE FROM ADDRESS a WHERE a.ID = :OLD_ID;

/* FetchRowSQL: */

/* *****************************************************
	4. STREET1 ADDRESS statements
*/


/* *****************************************************
	5. STREET1 ADDRESS_HIERARCHY statements
*/


/* *****************************************************
	6. STREET2 ENTITY_BASE statements
*/


/* *****************************************************
	7. STREET2 ADDRESS statements
*/


/* *****************************************************
	8. STREET2 ADDRESS_HIERARCHY statements
*/


/* *****************************************************
	9. CITY ENTITY_BASE statements
*/


/* *****************************************************
	10. CITY ADDRESS statements
*/


/* *****************************************************
	11. CITY ADDRESS_HIERARCHY statements
*/


/* *****************************************************
	12. STATE ENTITY_BASE statements
*/


/* *****************************************************
	13. STATE ADDRESS statements
*/


/* *****************************************************
	14. STATE ADDRESS_HIERARCHY statements
*/


/* *****************************************************
	15. POSTAL_CODE ENTITY_BASE statements
*/


/* *****************************************************
	16. POSTAL_CODE ADDRESS statements
*/


/* *****************************************************
	17. POSTAL_CODE ADDRESS_HIERARCHY statements
*/


/* *****************************************************
	18. COUNTRY ENTITY_BASE statements
*/


/* *****************************************************
	19. COUNTRY ADDRESS statements
*/


/* *****************************************************
	20. COUNTRY ADDRESS_HIERARCHY statements
*/



