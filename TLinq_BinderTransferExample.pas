
var
  Activities: TList<TActivity>;
  Activity: TActivity;
  Participants: TList<TParticipant>;
  Participant: TParticipant;
  B: Byte;
begin
  // fetch all binder transfer activities
  Activities := Manager.Find<TActivity>
    .CreateAlias('ActivityType', 't')
    .Where(Linq['t.Name'] = 'BINDER_TRANSFER')
    .List;
  
  // So far I only have one:
  Activity = Activities[0];
  
  // Fetch all of the participants related to the selected activity
  // NOT SURE IF THIS IS THE RIGHT WAY TO MATCH A RECORD BY ID?
  Participants := Manager.Find<TParticipant>
    .CreateAlias('Activity', 'a')
    .Where(Linq['a.Id'] = Activity.Id )
    .List;
  
  // Iterate through the participants
  for B := 0 to (Participants.Count - 1) do
  begin
    Participant = Participants[i];
    // Use Participant.Party to access party details?
    // Participant.Relationship indicates 'SOURCE_LOCATION', 'TARGET_LOCATION', or 'SUBJECT'
    // If Partipant.Party.PartyType.Name = 'GROUP' Then Participant.Party.PartyGroup.Name
    // If Partipant.Party.PartyType.Name = 'PERSON' Then Participant.Party.PartyPerson.FirstName
  end;
  
end;