object TEST6_Activity_DataAccess_Connection: TTEST6_Activity_DataAccess_Connection
  OldCreateOrder = True
  Height = 198
  Width = 282
  object Connection: TFDConnection
    Params.Strings = (
      'ConnectionDef=TEST6')
    BeforeCommit = ConnectionBeforeCommit
    AfterCommit = ConnectionAfterCommit
    Left = 40
    Top = 32
  end
  object FDQuery: TFDQuery
    BeforeEdit = FDQueryBeforeEdit
    AfterPost = FDQueryAfterPost
    OnUpdateRecord = FDQueryUpdateRecord
    Connection = Connection
    UpdateObject = FDUpdateSQL_Person
    SQL.Strings = (
      'SELECT * FROM ACTIVITY')
    Left = 128
    Top = 32
  end
  object FDUpdateSQL_Activity1: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ENTITY_BASE (ID, TABLE_NAME)'
      'VALUES (:ACTIVITY_ID, 'ACTIVITY')')
    DeleteSQL.Strings = (
      'DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID')
    Left = 128
    Top = 80
  end
  object FDUpdateSQL_Activity2: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID)'
      'SELECT :ACTIVITY_ID, t.ID FROM ACTIVITY_TYPE t'
      'WHERE t.NAME = :NEW_ACTIVITY_TYPE')
    DeleteSQL.Strings = (
      'DELETE FROM ACTIVITY a WHERE a.ID = :OLD_ID')
    Left = 128
    Top = 130
  end
  object FDUpdateSQL_Party3Person: TFDUpdateSQL
    Connection = Connection
    InsertSQL.Strings = (
      'INSERT')
    ModifySQL.Strings = (
      'UPDATE')
    DeleteSQL.Strings = (
      'DELETE')
    FetchRowSQL.Strings = (
      'SELECT')
    Left = 128
    Top = 180
  end
  object FDMoniRemoteClientLink1: TFDMoniRemoteClientLink
    Tracing = True
    Left = 40
    Top = 80
  end
end