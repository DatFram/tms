unit TEST6_Excerpt;

interface

uses
//
  System.SysUtils
, System.Generics.Collections
//
, Aurelius.Mapping.Attributes
, Aurelius.Types.Blob
, Aurelius.Types.DynamicProperties
, Aurelius.Types.Nullable
, Aurelius.Types.Proxy
, Aurelius.Criteria.Dictionary
//

;

type
  TEntityBase = class;
  TAddress = class;
  TAddressHierarchy = class;
  TAddressType = class;
  TGender = class;
  TLocator = class;
  TParty = class;
  TPartyHierarchy = class;
  TPartyType = class;
  TPartyGroup = class;
  TPartyPerson = class;
  TAddressTableDictionary = class;
  TAddressHierarchyTableDictionary = class;
  TAddressTypeTableDictionary = class;
  TEntityBaseTableDictionary = class;
  TGenderTableDictionary = class;
  TLocatorTableDictionary = class;
  TPartyTableDictionary = class;
  TPartyGroupTableDictionary = class;
  TPartyHierarchyTableDictionary = class;
  TPartyPersonTableDictionary = class;
  TPartyTypeTableDictionary = class;
  
  [Entity]
  [Table('ENTITY_BASE')]
  [Description('Common table which all other tables reference, no record can exist in any other table without first having an entry here. It stores standard information including the generated GUID/UUID primary key, created and updated timestamps, etc.')]
  [Inheritance(TInheritanceStrategy.JoinedTables)]
  [Id('FId', TIdGenerator.None)]
  TEntityBase = class
  private
    [Column('ID', [TColumnProp.Required], 16)]
    [Description('GUID Primary Key')]
    FId: TGuid;
    
    [Column('IS_ACTIVE', [TColumnProp.Required], 1)]
    [Description('Boolean indicator of whether or not the record is active')]
    FIsActive: Boolean;
    
    [Column('CREATED_BY', [TColumnProp.Required], 128)]
    [Description('User who created the record')]
    FCreatedBy: string;
    
    [Column('CREATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of creation')]
    FCreatedAt: TDateTime;
    
    [Column('UPDATED_BY', [TColumnProp.Required], 128)]
    [Description('User who last updated the record')]
    FUpdatedBy: string;
    
    [Column('UPDATED_AT', [TColumnProp.Required])]
    [Description('Timestamp of last update')]
    FUpdatedAt: TDateTime;
    
    [Column('POSITION_INDEX', [])]
    [Description('Integer index to track the order that records were created in a given day (for when records are creating at the same instanct and the timestamp is not enough)')]
    FPositionIndex: Nullable<Integer>;
    
    [Column('EXTERNAL_SOURCE', [], 35)]
    [Description('String field to store the name of the external source the data came from, e.g. CDS, M2, CYBERSOURCE, etc.')]
    FExternalSource: Nullable<string>;
    
    [Column('EXTERNAL_ID', [], 50)]
    [Description('String field to store an identifier from the external source the data came from.')]
    FExternalId: Nullable<string>;
    
    [Column('TABLE_NAME', [], 35)]
    [Description('String field to store the name of the TABLE that will indicate the subtype that this record is parent to. NOTE: This field does not fill in automatically, though a query can be written to search the entire database and update this field.')]
    FTableName: Nullable<string>;
  public
    property Id: TGuid read FId write FId;
    property IsActive: Boolean read FIsActive write FIsActive;
    property CreatedBy: string read FCreatedBy write FCreatedBy;
    property CreatedAt: TDateTime read FCreatedAt write FCreatedAt;
    property UpdatedBy: string read FUpdatedBy write FUpdatedBy;
    property UpdatedAt: TDateTime read FUpdatedAt write FUpdatedAt;
    property PositionIndex: Nullable<Integer> read FPositionIndex write FPositionIndex;
    property ExternalSource: Nullable<string> read FExternalSource write FExternalSource;
    property ExternalId: Nullable<string> read FExternalId write FExternalId;
    property TableName: Nullable<string> read FTableName write FTableName;
  end;
  
  [Entity]
  [Table('ADDRESS')]
  [Description('An address is a way to contact a person or place; this table does hold address information and simply acts as a single point for outside tables to reference its subtypes.')]
  [PrimaryJoinColumn('ID')]
  TAddress = class(TEntityBase)
  private
    [Column('CONTENTS', [], 255)]
    [Description('Text value of the address. E.G. an email address, digits of a phone number, street address, country abbreviation, etc')]
    FContents: Nullable<string>;
    
    [Column('FORMAT_MASK', [], 255)]
    [Description('A character string that can be used to define how data in CONTENTS is to be displayed or printed')]
    FFormatMask: Nullable<string>;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the address, for abbreviated contents like countries or states, this can hold the full name. E.g. the ADDRESS records with contents of "NJ" can have a description of "New Jersey".')]
    FDescription: Nullable<string>;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('ADDRESS_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddressType: TAddressType;
  public
    property Contents: Nullable<string> read FContents write FContents;
    property FormatMask: Nullable<string> read FFormatMask write FFormatMask;
    property Description: Nullable<string> read FDescription write FDescription;
    property AddressType: TAddressType read FAddressType write FAddressType;
  end;
  
  [Entity]
  [Table('ADDRESS_HIERARCHY')]
  [Description('This table is an intermediate or junction table between ADDRESS and itself, allowing a many to many relationship. That is, each address can contain or be related to other address. For example the city "Chatham" can belong to the state "NJ".')]
  [UniqueKey('PARENT_ADDRESS_ID, CHILD_ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TAddressHierarchy = class(TEntityBase)
  private
    [Column('POSITION_INDEX', [])]
    [Description('Position of the child ADDRESS in reference to the other child ADDRESS that make up the associated parent ADDRESS, to preserve a desired order.')]
    FPositionIndex: Nullable<Integer>;
    
    [Column('RELATIONSHIP', [], 35)]
    [Description('Indicates the type of relationship between the parent and child ADDRESS.')]
    FRelationship: Nullable<string>;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('CHILD_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildAddress: TAddress;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('PARENT_ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentAddress: TAddress;
  public
    property PositionIndex: Nullable<Integer> read FPositionIndex write FPositionIndex;
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property ChildAddress: TAddress read FChildAddress write FChildAddress;
    property ParentAddress: TAddress read FParentAddress write FParentAddress;
  end;
  
  [Entity]
  [Table('ADDRESS_TYPE')]
  [Description('This table describes the types of ADDRESS. Each record in this table should correspond to a subtype table beginning with "ADDRESS_".')]
  [PrimaryJoinColumn('ID')]
  TAddressType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the address type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the address type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('GENDER')]
  [Description('Stores the gender a Contact can be listed as. Useful mostly for the other information stored in this table which will help with email/mail merge fields (various gender-specific pronouns like she/he, her/him/ hers/his, herself/himself, Ms./Mr. etc.)')]
  [PrimaryJoinColumn('ID')]
  TGender = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Gender name: female, male, or neutral.')]
    FName: string;
    
    [Column('SUBJECT_PRONOUN', [TColumnProp.Required], 3)]
    [Description('Subject pronoun: she, he, one.')]
    FSubjectPronoun: string;
    
    [Column('OBJECT_PRONOUN', [TColumnProp.Required], 3)]
    [Description('Object pronoun: her, him, one.')]
    FObjectPronoun: string;
    
    [Column('POSSESSIVE_DET_PRONOUN', [], 5)]
    [Description('Possessive determiner pronoun: hers, his, ones.')]
    FPossessiveDetPronoun: Nullable<string>;
    
    [Column('POSSESSIVE_PRONOUN', [], 5)]
    [Description('Possessive pronoun: hers, his.')]
    FPossessivePronoun: Nullable<string>;
    
    [Column('REFLEXIVE_PRONOUN', [TColumnProp.Required], 10)]
    [Description('Reflexive pronoun: herself, himself, oneself.')]
    FReflexivePronoun: string;
    
    [Column('TITLE', [], 5)]
    [Description('Title or prefix: Ms. and Mr.')]
    FTitle: Nullable<string>;
  public
    property Name: string read FName write FName;
    property SubjectPronoun: string read FSubjectPronoun write FSubjectPronoun;
    property ObjectPronoun: string read FObjectPronoun write FObjectPronoun;
    property PossessiveDetPronoun: Nullable<string> read FPossessiveDetPronoun write FPossessiveDetPronoun;
    property PossessivePronoun: Nullable<string> read FPossessivePronoun write FPossessivePronoun;
    property ReflexivePronoun: string read FReflexivePronoun write FReflexivePronoun;
    property Title: Nullable<string> read FTitle write FTitle;
  end;
  
  [Entity]
  [Table('LOCATOR')]
  [Description('This table is the intermediate or junction table between Parties and Addresses allowing a many to many relationship.')]
  [UniqueKey('PARTY_ID, ADDRESS_ID')]
  [PrimaryJoinColumn('ID')]
  TLocator = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('ADDRESS_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FAddress: TAddress;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParty: TParty;
  public
    property Address: TAddress read FAddress write FAddress;
    property Party: TParty read FParty write FParty;
  end;
  
  [Entity]
  [Table('PARTY')]
  [Description('A party is either a person or account that is associated with the business in some way, This table is the supertype for the entity types PERSON and GROUP.')]
  [PrimaryJoinColumn('ID')]
  TParty = class(TEntityBase)
  private
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('PARTY_TYPE_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FPartyType: TPartyType;
  public
    property PartyType: TPartyType read FPartyType write FPartyType;
  end;
  
  [Entity]
  [Table('PARTY_HIERARCHY')]
  [Description('This table is the intermediate or junction table between Parties and itself, allowing a many to many relationship. e.g. one party could represent a family and the parties related to it are the members of that family.')]
  [UniqueKey('PARENT_PARTY_ID, CHILD_PARTY_ID')]
  [PrimaryJoinColumn('ID')]
  TPartyHierarchy = class(TEntityBase)
  private
    [Column('RELATIONSHIP', [], 35)]
    [Description('The type of relationship (e.g. Member, Assistant, Representative, etc), can be null to show loosely related parties. The type of relationship should be thought of as {Child} is a {Relationship} of {Parent}.')]
    FRelationship: Nullable<string>;
    
    [Column('IS_PRIMARY', [TColumnProp.Required], 1)]
    [Description('Indicates that given a series of other parties related to the parent party, this relationship should be considered the primary one. For exampe, each family account must have at least one primary contact.')]
    FIsPrimary: Boolean;
    
    [Column('NOTES', [], 1000)]
    [Description('Notes regarding the relationship between parties.')]
    FNotes: Nullable<string>;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('CHILD_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FChildParty: TParty;
    
    [Association([TAssociationProp.Required], [])]
    [JoinColumn('PARENT_PARTY_ID', [TColumnProp.Required], 'ID')]
    [Description('')]
    FParentParty: TParty;
  public
    property Relationship: Nullable<string> read FRelationship write FRelationship;
    property IsPrimary: Boolean read FIsPrimary write FIsPrimary;
    property Notes: Nullable<string> read FNotes write FNotes;
    property ChildParty: TParty read FChildParty write FChildParty;
    property ParentParty: TParty read FParentParty write FParentParty;
  end;
  
  [Entity]
  [Table('PARTY_TYPE')]
  [Description('This table describes the subtypes that a PARTY can be. Each record in this table should correspond to a subtype table beginning with "PARTIES_".')]
  [PrimaryJoinColumn('ID')]
  TPartyType = class(TEntityBase)
  private
    [Column('NAME', [TColumnProp.Required], 35)]
    [Description('Name of the party type.')]
    FName: string;
    
    [Column('DESCRIPTION', [], 255)]
    [Description('Description of the party type.')]
    FDescription: Nullable<string>;
  public
    property Name: string read FName write FName;
    property Description: Nullable<string> read FDescription write FDescription;
  end;
  
  [Entity]
  [Table('PARTY_GROUP')]
  [Description('A group is a party that, like a person, can be associated with multiple addresses as well as other parties. A group can be a Family, a Mathnasium learning center, an Organization, etc.')]
  [PrimaryJoinColumn('ID')]
  TPartyGroup = class(TParty)
  private
    [Column('NAME', [], 75)]
    [Description('Name of the account.')]
    FName: Nullable<string>;
    
    [Column('ALIAS', [], 35)]
    [Description('Alias or alternative name for the account.')]
    FAlias: Nullable<string>;
    
    [Column('GROUP_TYPE', [], 50)]
    [Description('Account type, e.g. Organization, Family, Learning Center, Office, etc.')]
    FGroupType: Nullable<string>;
    
    [Column('IS_FOR_PROFIT', [], 1)]
    [Description('Boolean flag to indicate whether the organization is a for profit (true) or a not-for-profirt (false).')]
    FIsForProfit: Nullable<Boolean>;
    
    [Column('NOTES', [], 1000)]
    [Description('Notes concerning the account.')]
    FNotes: Nullable<string>;
  public
    property Name: Nullable<string> read FName write FName;
    property Alias: Nullable<string> read FAlias write FAlias;
    property GroupType: Nullable<string> read FGroupType write FGroupType;
    property IsForProfit: Nullable<Boolean> read FIsForProfit write FIsForProfit;
    property Notes: Nullable<string> read FNotes write FNotes;
  end;
  
  [Entity]
  [Table('PARTY_PERSON')]
  [Description('This table simply stores the names of a person (first, last, middle, nickname, prefix, suffix), gender, birthday, and medical info.')]
  [PrimaryJoinColumn('ID')]
  TPartyPerson = class(TParty)
  private
    [Column('PREFIX', [], 10)]
    [Description('Prefix of the person, currently these are only Mr. and Ms., for which only Adults have values--however this should likely be changed to options like Dr. or Sir., since the values of Mr. and Ms. can be implied from the gender table.')]
    FPrefix: Nullable<string>;
    
    [Column('FIRST_NAME', [], 35)]
    [Description('First name of the person (Nicknames do not belong here, as there is a separate field for that).')]
    FFirstName: Nullable<string>;
    
    [Column('MIDDLE_NAMES', [], 35)]
    [Description('Middle names of the person, this field can represent a single middle initial, or simply all names between the first name and last name of the person, which, when combined, should result in the persons full name.')]
    FMiddleNames: Nullable<string>;
    
    [Column('LAST_NAME', [], 35)]
    [Description('Last name, surname, or family name of the person. Should not include any suffix like Jr. or Sr., as there is a suffix field for that.')]
    FLastName: Nullable<string>;
    
    [Column('SUFFIX', [], 10)]
    [Description('Suffix of the person, like Sr., Jr., III, etc. TO DO: Standardize these options, if a father is the Sr and the son is Jr, should this field say Jr an Sr, or should it say I and II? Or First and Second?')]
    FSuffix: Nullable<string>;
    
    [Column('NICK_NAME', [], 35)]
    [Description('Nick name of the person, should default to First Name but allow changes made to it. This should be the name that the person prefers to be called (e.g. Nina instead of Cristina, Apple instead of Appolonia, Billy instead of William, etc.)')]
    FNickName: Nullable<string>;
    
    [Column('MEDICAL_INFO', [], 255)]
    [Description('Any imporant medical information regarding the person, like severe allergies, heart conditions, anything requiring special care or medication, etc.')]
    FMedicalInfo: Nullable<string>;
    
    [Column('DO_NOT_CONTACT', [TColumnProp.Required], 1)]
    [Description('A boolean flag indicating that the person does not wish to be contacted. If a single entity in an account is marked as DO_NOT_CONTACT, then all members of the account should be considered DO_NOT_CONTACT.')]
    FDoNotContact: Boolean;
    
    [Column('BIRTHDATE', [])]
    [Description('')]
    FBirthdate: Nullable<TDateTime>;
    
    [Association([], [])]
    [JoinColumn('GENDER_ID', [], 'ID')]
    [Description('')]
    FGender: TGender;
  public
    property Prefix: Nullable<string> read FPrefix write FPrefix;
    property FirstName: Nullable<string> read FFirstName write FFirstName;
    property MiddleNames: Nullable<string> read FMiddleNames write FMiddleNames;
    property LastName: Nullable<string> read FLastName write FLastName;
    property Suffix: Nullable<string> read FSuffix write FSuffix;
    property NickName: Nullable<string> read FNickName write FNickName;
    property MedicalInfo: Nullable<string> read FMedicalInfo write FMedicalInfo;
    property DoNotContact: Boolean read FDoNotContact write FDoNotContact;
    property Birthdate: Nullable<TDateTime> read FBirthdate write FBirthdate;
    property Gender: TGender read FGender write FGender;
  end;
  
  TDicDictionary = class
  private
    FAddress: TAddressTableDictionary;
    FAddressHierarchy: TAddressHierarchyTableDictionary;
    FAddressType: TAddressTypeTableDictionary;
    FEntityBase: TEntityBaseTableDictionary;
    FGender: TGenderTableDictionary;
    FLocator: TLocatorTableDictionary;
    FParty: TPartyTableDictionary;
    FPartyGroup: TPartyGroupTableDictionary;
    FPartyHierarchy: TPartyHierarchyTableDictionary;
    FPartyPerson: TPartyPersonTableDictionary;
    FPartyType: TPartyTypeTableDictionary;
    function GetAddress: TAddressTableDictionary;
    function GetAddressHierarchy: TAddressHierarchyTableDictionary;
    function GetAddressType: TAddressTypeTableDictionary;
    function GetEntityBase: TEntityBaseTableDictionary;
    function GetGender: TGenderTableDictionary;
    function GetLocator: TLocatorTableDictionary;
    function GetParty: TPartyTableDictionary;
    function GetPartyGroup: TPartyGroupTableDictionary;
    function GetPartyHierarchy: TPartyHierarchyTableDictionary;
    function GetPartyPerson: TPartyPersonTableDictionary;
    function GetPartyType: TPartyTypeTableDictionary;
  public
    destructor Destroy; override;
    property Address: TAddressTableDictionary read GetAddress;
    property AddressHierarchy: TAddressHierarchyTableDictionary read GetAddressHierarchy;
    property AddressType: TAddressTypeTableDictionary read GetAddressType;
    property EntityBase: TEntityBaseTableDictionary read GetEntityBase;
    property Gender: TGenderTableDictionary read GetGender;
    property Locator: TLocatorTableDictionary read GetLocator;
    property Party: TPartyTableDictionary read GetParty;
    property PartyGroup: TPartyGroupTableDictionary read GetPartyGroup;
    property PartyHierarchy: TPartyHierarchyTableDictionary read GetPartyHierarchy;
    property PartyPerson: TPartyPersonTableDictionary read GetPartyPerson;
    property PartyType: TPartyTypeTableDictionary read GetPartyType;
  end;
  
  TAddressTableDictionary = class
  private
    FContents: TDictionaryAttribute;
    FFormatMask: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
    FAddressType: TDictionaryAssociation;
  public
    constructor Create;
    property Contents: TDictionaryAttribute read FContents;
    property FormatMask: TDictionaryAttribute read FFormatMask;
    property Description: TDictionaryAttribute read FDescription;
    property AddressType: TDictionaryAssociation read FAddressType;
  end;
  
  TAddressHierarchyTableDictionary = class
  private
    FPositionIndex: TDictionaryAttribute;
    FRelationship: TDictionaryAttribute;
    FChildAddress: TDictionaryAssociation;
    FParentAddress: TDictionaryAssociation;
  public
    constructor Create;
    property PositionIndex: TDictionaryAttribute read FPositionIndex;
    property Relationship: TDictionaryAttribute read FRelationship;
    property ChildAddress: TDictionaryAssociation read FChildAddress;
    property ParentAddress: TDictionaryAssociation read FParentAddress;
  end;
  
  TAddressTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
  TEntityBaseTableDictionary = class
  private
    FId: TDictionaryAttribute;
    FIsActive: TDictionaryAttribute;
    FCreatedBy: TDictionaryAttribute;
    FCreatedAt: TDictionaryAttribute;
    FUpdatedBy: TDictionaryAttribute;
    FUpdatedAt: TDictionaryAttribute;
    FPositionIndex: TDictionaryAttribute;
    FExternalSource: TDictionaryAttribute;
    FExternalId: TDictionaryAttribute;
    FTableName: TDictionaryAttribute;
  public
    constructor Create;
    property Id: TDictionaryAttribute read FId;
    property IsActive: TDictionaryAttribute read FIsActive;
    property CreatedBy: TDictionaryAttribute read FCreatedBy;
    property CreatedAt: TDictionaryAttribute read FCreatedAt;
    property UpdatedBy: TDictionaryAttribute read FUpdatedBy;
    property UpdatedAt: TDictionaryAttribute read FUpdatedAt;
    property PositionIndex: TDictionaryAttribute read FPositionIndex;
    property ExternalSource: TDictionaryAttribute read FExternalSource;
    property ExternalId: TDictionaryAttribute read FExternalId;
    property TableName: TDictionaryAttribute read FTableName;
  end;
  
  TGenderTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FSubjectPronoun: TDictionaryAttribute;
    FObjectPronoun: TDictionaryAttribute;
    FPossessiveDetPronoun: TDictionaryAttribute;
    FPossessivePronoun: TDictionaryAttribute;
    FReflexivePronoun: TDictionaryAttribute;
    FTitle: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property SubjectPronoun: TDictionaryAttribute read FSubjectPronoun;
    property ObjectPronoun: TDictionaryAttribute read FObjectPronoun;
    property PossessiveDetPronoun: TDictionaryAttribute read FPossessiveDetPronoun;
    property PossessivePronoun: TDictionaryAttribute read FPossessivePronoun;
    property ReflexivePronoun: TDictionaryAttribute read FReflexivePronoun;
    property Title: TDictionaryAttribute read FTitle;
  end;
  
  TLocatorTableDictionary = class
  private
    FAddress: TDictionaryAssociation;
    FParty: TDictionaryAssociation;
  public
    constructor Create;
    property Address: TDictionaryAssociation read FAddress;
    property Party: TDictionaryAssociation read FParty;
  end;
  
  TPartyTableDictionary = class
  private
    FPartyType: TDictionaryAssociation;
  public
    constructor Create;
    property PartyType: TDictionaryAssociation read FPartyType;
  end;
  
  TPartyGroupTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FAlias: TDictionaryAttribute;
    FGroupType: TDictionaryAttribute;
    FIsForProfit: TDictionaryAttribute;
    FNotes: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Alias: TDictionaryAttribute read FAlias;
    property GroupType: TDictionaryAttribute read FGroupType;
    property IsForProfit: TDictionaryAttribute read FIsForProfit;
    property Notes: TDictionaryAttribute read FNotes;
  end;
  
  TPartyHierarchyTableDictionary = class
  private
    FRelationship: TDictionaryAttribute;
    FIsPrimary: TDictionaryAttribute;
    FNotes: TDictionaryAttribute;
    FChildParty: TDictionaryAssociation;
    FParentParty: TDictionaryAssociation;
  public
    constructor Create;
    property Relationship: TDictionaryAttribute read FRelationship;
    property IsPrimary: TDictionaryAttribute read FIsPrimary;
    property Notes: TDictionaryAttribute read FNotes;
    property ChildParty: TDictionaryAssociation read FChildParty;
    property ParentParty: TDictionaryAssociation read FParentParty;
  end;
  
  TPartyPersonTableDictionary = class
  private
    FPrefix: TDictionaryAttribute;
    FFirstName: TDictionaryAttribute;
    FMiddleNames: TDictionaryAttribute;
    FLastName: TDictionaryAttribute;
    FSuffix: TDictionaryAttribute;
    FNickName: TDictionaryAttribute;
    FMedicalInfo: TDictionaryAttribute;
    FDoNotContact: TDictionaryAttribute;
    FBirthdate: TDictionaryAttribute;
    FGender: TDictionaryAssociation;
  public
    constructor Create;
    property Prefix: TDictionaryAttribute read FPrefix;
    property FirstName: TDictionaryAttribute read FFirstName;
    property MiddleNames: TDictionaryAttribute read FMiddleNames;
    property LastName: TDictionaryAttribute read FLastName;
    property Suffix: TDictionaryAttribute read FSuffix;
    property NickName: TDictionaryAttribute read FNickName;
    property MedicalInfo: TDictionaryAttribute read FMedicalInfo;
    property DoNotContact: TDictionaryAttribute read FDoNotContact;
    property Birthdate: TDictionaryAttribute read FBirthdate;
    property Gender: TDictionaryAssociation read FGender;
  end;
  
  TPartyTypeTableDictionary = class
  private
    FName: TDictionaryAttribute;
    FDescription: TDictionaryAttribute;
  public
    constructor Create;
    property Name: TDictionaryAttribute read FName;
    property Description: TDictionaryAttribute read FDescription;
  end;
  
function Dic: TDicDictionary;

implementation

var
  __Dic: TDicDictionary;

function Dic: TDicDictionary;
begin
  if __Dic = nil then __Dic := TDicDictionary.Create;
  result := __Dic
end;

{ TDicDictionary}

destructor TDicDictionary.Destroy;
begin
  if FPartyType <> nil then FPartyType.Free;
  if FPartyPerson <> nil then FPartyPerson.Free;
  if FPartyHierarchy <> nil then FPartyHierarchy.Free;
  if FPartyGroup <> nil then FPartyGroup.Free;
  if FParty <> nil then FParty.Free;
  if FLocator <> nil then FLocator.Free;
  if FGender <> nil then FGender.Free;
  if FEntityBase <> nil then FEntityBase.Free;
  if FAddressType <> nil then FAddressType.Free;
  if FAddressHierarchy <> nil then FAddressHierarchy.Free;
  if FAddress <> nil then FAddress.Free;
  inherited;
end;

function TDicDictionary.GetAddress: TAddressTableDictionary;
begin
  if FAddress = nil then FAddress := TAddressTableDictionary.Create;
  result := FAddress;
end;

function TDicDictionary.GetAddressHierarchy: TAddressHierarchyTableDictionary;
begin
  if FAddressHierarchy = nil then FAddressHierarchy := TAddressHierarchyTableDictionary.Create;
  result := FAddressHierarchy;
end;

function TDicDictionary.GetAddressType: TAddressTypeTableDictionary;
begin
  if FAddressType = nil then FAddressType := TAddressTypeTableDictionary.Create;
  result := FAddressType;
end;

function TDicDictionary.GetEntityBase: TEntityBaseTableDictionary;
begin
  if FEntityBase = nil then FEntityBase := TEntityBaseTableDictionary.Create;
  result := FEntityBase;
end;

function TDicDictionary.GetGender: TGenderTableDictionary;
begin
  if FGender = nil then FGender := TGenderTableDictionary.Create;
  result := FGender;
end;

function TDicDictionary.GetLocator: TLocatorTableDictionary;
begin
  if FLocator = nil then FLocator := TLocatorTableDictionary.Create;
  result := FLocator;
end;

function TDicDictionary.GetParty: TPartyTableDictionary;
begin
  if FParty = nil then FParty := TPartyTableDictionary.Create;
  result := FParty;
end;

function TDicDictionary.GetPartyGroup: TPartyGroupTableDictionary;
begin
  if FPartyGroup = nil then FPartyGroup := TPartyGroupTableDictionary.Create;
  result := FPartyGroup;
end;

function TDicDictionary.GetPartyHierarchy: TPartyHierarchyTableDictionary;
begin
  if FPartyHierarchy = nil then FPartyHierarchy := TPartyHierarchyTableDictionary.Create;
  result := FPartyHierarchy;
end;

function TDicDictionary.GetPartyPerson: TPartyPersonTableDictionary;
begin
  if FPartyPerson = nil then FPartyPerson := TPartyPersonTableDictionary.Create;
  result := FPartyPerson;
end;

function TDicDictionary.GetPartyType: TPartyTypeTableDictionary;
begin
  if FPartyType = nil then FPartyType := TPartyTypeTableDictionary.Create;
  result := FPartyType;
end;

{ TAddressTableDictionary}

constructor TAddressTableDictionary.Create;
begin
  inherited;
  FContents := TDictionaryAttribute.Create('Contents');
  FFormatMask := TDictionaryAttribute.Create('FormatMask');
  FDescription := TDictionaryAttribute.Create('Description');
  FAddressType := TDictionaryAssociation.Create('AddressType');
end;

{ TAddressHierarchyTableDictionary}

constructor TAddressHierarchyTableDictionary.Create;
begin
  inherited;
  FPositionIndex := TDictionaryAttribute.Create('PositionIndex');
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FChildAddress := TDictionaryAssociation.Create('ChildAddress');
  FParentAddress := TDictionaryAssociation.Create('ParentAddress');
end;

{ TAddressTypeTableDictionary}

constructor TAddressTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

{ TEntityBaseTableDictionary}

constructor TEntityBaseTableDictionary.Create;
begin
  inherited;
  FId := TDictionaryAttribute.Create('Id');
  FIsActive := TDictionaryAttribute.Create('IsActive');
  FCreatedBy := TDictionaryAttribute.Create('CreatedBy');
  FCreatedAt := TDictionaryAttribute.Create('CreatedAt');
  FUpdatedBy := TDictionaryAttribute.Create('UpdatedBy');
  FUpdatedAt := TDictionaryAttribute.Create('UpdatedAt');
  FPositionIndex := TDictionaryAttribute.Create('PositionIndex');
  FExternalSource := TDictionaryAttribute.Create('ExternalSource');
  FExternalId := TDictionaryAttribute.Create('ExternalId');
  FTableName := TDictionaryAttribute.Create('TableName');
end;

{ TGenderTableDictionary}

constructor TGenderTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FSubjectPronoun := TDictionaryAttribute.Create('SubjectPronoun');
  FObjectPronoun := TDictionaryAttribute.Create('ObjectPronoun');
  FPossessiveDetPronoun := TDictionaryAttribute.Create('PossessiveDetPronoun');
  FPossessivePronoun := TDictionaryAttribute.Create('PossessivePronoun');
  FReflexivePronoun := TDictionaryAttribute.Create('ReflexivePronoun');
  FTitle := TDictionaryAttribute.Create('Title');
end;

{ TLocatorTableDictionary}

constructor TLocatorTableDictionary.Create;
begin
  inherited;
  FAddress := TDictionaryAssociation.Create('Address');
  FParty := TDictionaryAssociation.Create('Party');
end;

{ TPartyTableDictionary}

constructor TPartyTableDictionary.Create;
begin
  inherited;
  FPartyType := TDictionaryAssociation.Create('PartyType');
end;

{ TPartyGroupTableDictionary}

constructor TPartyGroupTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FAlias := TDictionaryAttribute.Create('Alias');
  FGroupType := TDictionaryAttribute.Create('GroupType');
  FIsForProfit := TDictionaryAttribute.Create('IsForProfit');
  FNotes := TDictionaryAttribute.Create('Notes');
end;

{ TPartyHierarchyTableDictionary}

constructor TPartyHierarchyTableDictionary.Create;
begin
  inherited;
  FRelationship := TDictionaryAttribute.Create('Relationship');
  FIsPrimary := TDictionaryAttribute.Create('IsPrimary');
  FNotes := TDictionaryAttribute.Create('Notes');
  FChildParty := TDictionaryAssociation.Create('ChildParty');
  FParentParty := TDictionaryAssociation.Create('ParentParty');
end;

{ TPartyPersonTableDictionary}

constructor TPartyPersonTableDictionary.Create;
begin
  inherited;
  FPrefix := TDictionaryAttribute.Create('Prefix');
  FFirstName := TDictionaryAttribute.Create('FirstName');
  FMiddleNames := TDictionaryAttribute.Create('MiddleNames');
  FLastName := TDictionaryAttribute.Create('LastName');
  FSuffix := TDictionaryAttribute.Create('Suffix');
  FNickName := TDictionaryAttribute.Create('NickName');
  FMedicalInfo := TDictionaryAttribute.Create('MedicalInfo');
  FDoNotContact := TDictionaryAttribute.Create('DoNotContact');
  FBirthdate := TDictionaryAttribute.Create('Birthdate');
  FGender := TDictionaryAssociation.Create('Gender');
end;

{ TPartyTypeTableDictionary}

constructor TPartyTypeTableDictionary.Create;
begin
  inherited;
  FName := TDictionaryAttribute.Create('Name');
  FDescription := TDictionaryAttribute.Create('Description');
end;

initialization
  RegisterEntity(TAddress);
  RegisterEntity(TAddressHierarchy);
  RegisterEntity(TAddressType);
  RegisterEntity(TEntityBase);
  RegisterEntity(TGender);
  RegisterEntity(TLocator);
  RegisterEntity(TParty);
  RegisterEntity(TPartyGroup);
  RegisterEntity(TPartyHierarchy);
  RegisterEntity(TPartyPerson);
  RegisterEntity(TPartyType);

finalization
  if __Dic <> nil then __Dic.Free

end.
