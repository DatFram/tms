/* --------------------------------------------------------------
PURPOSE:
	
	All Families
	(Lists all families and the centers they are registered to or
	associated with)
	
	References:
	Stack Overflow having the same issue as we are:
		http://stackoverflow.com/questions/28175328/tfdupdatesql-not-posting-updates-on-datasnap-server
	Embarcadero documentation of what we want to do:
		http://docwiki.embarcadero.com/CodeExamples/Tokyo/en/FireDAC.TFDQuery.OnUpdateRecord_Sample
	
	So, to summarize today's understanding:
	- Each table that you want to update/insert/delete needs a TFDUpdateSQL component
	- Tables that you do not want to update/insert/delete do NOT need a TFDUpdateSQL component
	- You need to write your own frigging plumbing to sequence your TFDUpdateSQL statements in order that you want with code written in the .pas file of the project
	- When writing sequencing for a form (e.g. order of statement calls) make sure it is topped with a heading that describes what is going on (see embarcadero example)
	
CREATED: 2017-05-12
*/

SELECT
    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, pg2.NAME AS CENTER
FROM PARTY_PERSON pp
JOIN GENDER g ON g.ID = pp.GENDER_ID
JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID
JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID
JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID
JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID
ORDER BY pg2.NAME, pg.NAME, ph.RELATIONSHIP, pp.FIRST_NAME;

/*
UPDATE PARTY_HIERARCHY ph SET ph.RELATIONSHIP = :NEW_RELATIONSHIP
WHERE ph.PARENT_PARTY_ID = :NEW_FAMILY_ID AND ph.CHILD_PARTY_ID = :NEW_ID;

UPDATE PARTY_GROUP pg SET pg.ID = :NEW_ID,
pg.NAME = :NEW_FAMILY_NAME;
*/

/* *****************************************************
	PARTY_PERSON statements 
*/

/* DeleteSQL: */

DELETE FROM PARTY_PERSON pp WHERE pp.ID = :OLD_ID;

/* FetchRowSQL: */

SELECT
    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, pg2.NAME AS CENTER
FROM PARTY_PERSON pp
JOIN GENDER g ON g.ID = pp.GENDER_ID
JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID
JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID
JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID
JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID
WHERE pp.ID = :OLD_ID;

/* InsertSQL: */

INSERT INTO PARTY_PERSON (ID, FIRST_NAME, LAST_NAME, GENDER_ID)
SELECT :NEW_ID, :NEW_FIRST_NAME, :NEW_LAST_NAME, g.ID
FROM GENDER g WHERE g.NAME = :NEW_GENDER;

/* ModifySQL: */

UPDATE PARTY_PERSON pp SET pp.ID = :NEW_ID,
pp.FIRST_NAME = :NEW_FIRST_NAME,
pp.LAST_NAME = :NEW_LAST_NAME,
pp.GENDER_ID = (SELECT g.ID FROM GENDER g WHERE g.NAME = :NEW_GENDER)
WHERE pp.ID = :OLD_ID;


/* *****************************************************
	PARTY statements
*/

/* DeleteSQL: */

DELETE FROM PARTY p WHERE p.ID = :OLD_ID;

/* FetchRowSQL: */

SELECT
    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, pg2.NAME AS CENTER
FROM PARTY_PERSON pp
JOIN GENDER g ON g.ID = pp.GENDER_ID
JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID
JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID
JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID
JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID
WHERE pp.ID = :OLD_ID;

/* InsertSQL: */

INSERT INTO PARTY (ID, PARTY_TYPE_ID)
SELECT :NEW_ID, t.ID FROM PARTY_TYPE t WHERE t.NAME = 'PARTY';

/* ModifySQL: */

UPDATE PARTY p SET p.ID = :NEW_ID WHERE p.ID = :OLD_ID;

/* *****************************************************
	ENTITY_BASE statements
*/

/* DeleteSQL: */

DELETE FROM ENTITY_BASE eb WHERE eb.ID = :OLD_ID;

/* FetchRowSQL: */

SELECT
    pp.ID, pp.FIRST_NAME, pp.LAST_NAME, g.NAME AS GENDER, g.POSSESSIVE_DET_PRONOUN, ph.RELATIONSHIP, pg.ID AS FAMILY_ID, pg.NAME AS FAMILY_NAME, pg2.NAME AS CENTER
FROM PARTY_PERSON pp
JOIN GENDER g ON g.ID = pp.GENDER_ID
JOIN PARTY_HIERARCHY ph ON ph.CHILD_PARTY_ID = pp.ID
JOIN PARTY_GROUP pg ON pg.ID = ph.PARENT_PARTY_ID
JOIN PARTY_HIERARCHY ph2 ON ph2.CHILD_PARTY_ID = pg.ID
JOIN PARTY_GROUP pg2 ON pg2.ID = ph2.PARENT_PARTY_ID
WHERE pp.ID = :OLD_ID;

/* InsertSQL: */

INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:NEW_ID, 'PARTY_PERSON');

/* ModifySQL: */

UPDATE ENTITY_BASE eb SET eb.ID = :NEW_ID WHERE eb.ID = :OLD_ID;
