/* ***************************************************

The following SQL script is to perform schema changes
via DDL statements against the ADDRESS related schema
to further normalize the table structure and mimick
the hierarchical table structure of the ACTIVITY schema.

Schema change summary:
- Modify ADDRESS tables and content within for a more
normalized organization (in particular of ACTIVITY_TYPE)

2018-05-18 Runtime: 4:15 min

Written for the TEST6 database.

-------- Change Log ---------
- Created 2017-MM_DD HeatherG
- Updated 2018-05-18 HeatherG
    Move changes to ADDRESS table to another schema update file, as we are not ready for it yet

*************************************************** */


ALTER TABLE ADDRESS_TYPE ADD DATA_TYPE
  VARCHAR(35);

ALTER TABLE ADDRESS_TYPE ADD DATA_SIZE
  INTEGER;

ALTER TABLE ADDRESS ADD CONTENT_GUID
  GUID;

ALTER TABLE ADDRESS ADD CONTENT_BLOB
  BLOB SUB_TYPE TEXT SEGMENT SIZE 80;

ALTER TABLE ADDRESS_HIERARCHY ADD IS_TEMPLATE
  BOOLEAN NOT NULL;

ALTER TABLE ADDRESS ADD CONTENT_STRING
  VARCHAR(255);

ALTER TABLE ADDRESS ADD CONTENT_DATETIME
  TIMESTAMP;

ALTER TABLE ADDRESS ADD CONTENT_BOOLEAN
  BOOLEAN;

ALTER TABLE ADDRESS ADD CONTENT_NUMERIC
  DECIMAL(10,8);

/* Add new constraints */  
ALTER TABLE ADDRESS ADD CONSTRAINT FK_ADDRESS_ENTITY_BASE
  FOREIGN KEY (CONTENT_GUID)
  REFERENCES ENTITY_BASE (ID);


ALTER TABLE ADDRESS ALTER ID POSITION 1;
ALTER TABLE ADDRESS ALTER ADDRESS_TYPE_ID POSITION 2;
ALTER TABLE ADDRESS ALTER CONTENT_STRING POSITION 3;
ALTER TABLE ADDRESS ALTER CONTENT_DATETIME POSITION 4;
ALTER TABLE ADDRESS ALTER CONTENT_NUMERIC POSITION 5;
ALTER TABLE ADDRESS ALTER CONTENT_BOOLEAN POSITION 6;
ALTER TABLE ADDRESS ALTER CONTENT_BLOB POSITION 7;
ALTER TABLE ADDRESS ALTER CONTENT_GUID POSITION 8;
ALTER TABLE ADDRESS ALTER CONTENT POSITION 9;
ALTER TABLE ADDRESS ALTER DESCRIPTION POSITION 10;



/* ETL data before dropping tables/colums */
	/* Update DATA_TYPE values */
	DELETE FROM ENTITY_BASE eb WHERE EXISTS (SELECT 1 FROM ADDRESS_TYPE t WHERE t.KIND = 0 AND t.NAME <> 'UNASSIGNED' AND t.ID = eb.ID);
	UPDATE ADDRESS_TYPE t SET t.DATA_TYPE = 'STRING' WHERE t.NAME NOT IN ('UNASSIGNED','POSTAL_ADDRESS_US','PHONE_NUMBER_INTERNATIONAL','EMAIL');
	






    
    
	/* Hierarchically relate ADDRESS types */
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE address_id GUID;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
	DECLARE VARIABLE id GUID;
	BEGIN
		type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:type_id, 'ADDRESS_TYPE');
		INSERT INTO ADDRESS_TYPE (ID, NAME, DESCRIPTION, DATA_TYPE) VALUES (:type_id, 'LABEL', 'Attribute: label, e.g. label a PHONE_NUMBER_INTERNATIONAL record with "Mobile Phone"', 'STRING');
		
		FOR
		SELECT t2.ID
		FROM ADDRESS_TYPE t2
		INTO :type_id
		DO BEGIN
			address_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
			INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING) VALUES (:address_id, :type_id, 'TEMPLATE');
		END
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'UNASSIGNED' INTO :parent_id;
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'POSTAL_ADDRESS_US' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 1);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'PHONE_NUMBER_INTERNATIONAL' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 2);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'EMAIL' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 3);
		
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'POSTAL_ADDRESS_US' INTO :parent_id;
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'ADDRESS1' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 1);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'ADDRESS2' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1',2);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'CITY' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 3);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'STATE' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 4);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'ZIPCODE' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 5);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'COUNTRY' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 6);
		
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'PHONE_NUMBER_INTERNATIONAL' INTO :parent_id;
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'COUNTRY_CODE' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 1);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'AREA_CODE' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 2);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'PHONE_NUMBER' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 3);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'EXTENSION' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 4);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'LABEL' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 5);
		
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'EMAIL' INTO :parent_id;
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'EMAIL_ADDRESS' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 1);
		
		SELECT a.ID FROM ADDRESS a JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID WHERE a.CONTENT_STRING = 'TEMPLATE'
		AND t.NAME = 'EMAIL_ALIAS' INTO :child_id;
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, '1', 2);
		
		
	END^
	SET TERM ; ^
	
	/* Move ADDRESS CONTENT */
	UPDATE ADDRESS a SET a.CONTENT_STRING = a.CONTENT WHERE a.CONTENT IS NOT NULL;
	
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE address_id GUID;
	DECLARE VARIABLE attribute_id GUID;
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE id GUID;
	DECLARE VARIABLE label Varchar(35);
	BEGIN
		FOR
		SELECT a.ID, a.DESCRIPTION, t2.ID
		FROM ADDRESS a
		JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID
		JOIN ADDRESS_TYPE t2 ON t2.NAME = 'LABEL'
		WHERE t.NAME = 'PHONE_NUMBER'
		AND a.DESCRIPTION IS NOT NULL
		INTO :address_id, :label, :type_id
		DO BEGIN
			attribute_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:attribute_id, 'ADDRESS');
			INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
			VALUES (:attribute_id, :type_id, :label);
			
			id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
			INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID) VALUES (:id, :address_id, :attribute_id);
		END
	END^
	SET TERM ; ^
	
	
	/* Create center phone numbers */
	SET TERM ^ ;
	EXECUTE BLOCK
	AS
	DECLARE VARIABLE address_id GUID;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE country_code_id GUID;
	DECLARE VARIABLE area_code_id GUID;
	DECLARE VARIABLE id GUID;
	BEGIN
		
		country_code_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:country_code_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :country_code_id, t.ID, '1' FROM ADDRESS_TYPE t WHERE t.NAME = 'COUNTRY_CODE';
		
		area_code_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:area_code_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :area_code_id, t.ID, '973' FROM ADDRESS_TYPE t WHERE t.NAME = 'AREA_CODE';

		/* Chatham 1-973-6652818 */
		parent_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:parent_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
		SELECT :parent_id, t.ID FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER_INTERNATIONAL';

		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :country_code_id, '0');

		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :area_code_id, '0');
		
		address_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :address_id, t.ID, '6652818' FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER';
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :address_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'LOCATOR');
		INSERT INTO LOCATOR (ID, ADDRESS_ID, PARTY_ID)
		SELECT :id, :parent_id, pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = 'Chatham';
		
		/* Livingston 1-973-5124040 */
		parent_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:parent_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
		SELECT :parent_id, t.ID FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER_INTERNATIONAL';
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :country_code_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :area_code_id, '0');
		
		address_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :address_id, t.ID, '5124040' FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER';
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :address_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'LOCATOR');
		INSERT INTO LOCATOR (ID, ADDRESS_ID, PARTY_ID)
		SELECT :id, :parent_id, pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = 'Livingston';
		
		
		
		area_code_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:area_code_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :area_code_id, t.ID, '908' FROM ADDRESS_TYPE t WHERE t.NAME = 'AREA_CODE';
		
		/* Summit 1-908-5171111 */
		parent_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:parent_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
		SELECT :parent_id, t.ID FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER_INTERNATIONAL';
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :country_code_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :area_code_id, '0');
		
		address_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :address_id, t.ID, '5171111' FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER';
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :address_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'LOCATOR');
		INSERT INTO LOCATOR (ID, ADDRESS_ID, PARTY_ID)
		SELECT :id, :parent_id, pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = 'Summit';
		
		/* Westfield 1-908-4284251 */
		parent_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:parent_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
		SELECT :parent_id, t.ID FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER_INTERNATIONAL';
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :country_code_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :area_code_id, '0');
		
		address_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :address_id, t.ID, '4284251' FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER';
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :address_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'LOCATOR');
		INSERT INTO LOCATOR (ID, ADDRESS_ID, PARTY_ID)
		SELECT :id, :parent_id, pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = 'Westfield';
		
		/* Basking Ridge 1-908-9050840 */
		parent_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:parent_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID)
		SELECT :parent_id, t.ID FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER_INTERNATIONAL';
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :country_code_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :area_code_id, '0');
		
		address_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:address_id, 'ADDRESS');
		INSERT INTO ADDRESS (ID, ADDRESS_TYPE_ID, CONTENT_STRING)
		SELECT :address_id, t.ID, '9050840' FROM ADDRESS_TYPE t WHERE t.NAME = 'PHONE_NUMBER';
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'ADDRESS_HIERARCHY');
		INSERT INTO ADDRESS_HIERARCHY (ID, PARENT_ADDRESS_ID, CHILD_ADDRESS_ID, IS_TEMPLATE)
		VALUES (:id, :parent_id, :address_id, '0');
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, 'LOCATOR');
		INSERT INTO LOCATOR (ID, ADDRESS_ID, PARTY_ID)
		SELECT :id, :parent_id, pg.ID FROM PARTY_GROUP pg WHERE pg.NAME = 'Basking Ridge';
		
	END^
	SET TERM ; ^

    
    
    
	UPDATE ADDRESS_HIERARCHY ah SET ah.IS_TEMPLATE = '0' WHERE ah.IS_TEMPLATE IS NULL;
    
    
    
DROP VIEW VIEW_ADDRESSES_POSTAL;
DROP VIEW VIEW_ADDRESSES_VIRTUAL;


ALTER TABLE ADDRESS_TYPE DROP KIND;  

ALTER TABLE ADDRESS_HIERARCHY DROP RELATIONSHIP;

ALTER TABLE ADDRESS DROP CONTENT;

ALTER TABLE ADDRESS DROP DESCRIPTION;



COMMENT ON COLUMN ADDRESS.CONTENT_GUID IS 'GUID value of the address, one of 6 options to store the address content. This field is a foreign key to the ENTITY_BASE table, allowing it to reference any other record in the database.';

COMMENT ON COLUMN ADDRESS.ADDRESS_TYPE_ID IS '';

COMMENT ON COLUMN ADDRESS.CONTENT_STRING IS 'String value of the address, one of 6 options to store the address content.';

COMMENT ON COLUMN ADDRESS.CONTENT_DATETIME IS 'Timestamp value of the address, one of 6 options to store the address content.';

COMMENT ON COLUMN ADDRESS.CONTENT_NUMERIC IS 'Numeric value of the address, one of 6 options to store the address content.';

COMMENT ON COLUMN ADDRESS.CONTENT_BOOLEAN IS 'Boolean value of the address, one of 6 options to store the address content.';

COMMENT ON COLUMN ADDRESS.CONTENT_BLOB IS 'Blob value of the address, one of 6 options to store the address content.';

    