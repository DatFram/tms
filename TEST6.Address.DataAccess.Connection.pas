procedure TTEST6_Address_DataAccess_Connection.FDQueryUpdateRecord( aSender: TDataSet;
  aRequest: TFDUpdateRequest; var aAction: TFDErrorAction; aOptions:
    TFDUpdateRowOptions );
var
  lIndex: Integer;
  lValue: String;
  lGUIDMaster: TGUID;
  lGUIDDetail: TGUID;
  lGUIDDetailHierarchy: TGUID;
begin

  // Assume SOME_DETAIL and SOME_MASTER tables are related one-to-many.
  // FDQuery.SQL is a join of these two tables. FDUpdateSQL_SomeDetail posts
  // Insert|Fetch|Modfy|Delete (aka CRUD) changes to SOME_DETAIL table while
  // FDUpdateSQL_SomeMaster posts to SOME_MASTER table. At first, we
  // post changes to SOME_DETAIL (or sub type) table, at second to SOME_MASTER
  // (master) one. Note, however, in the case of Insert, a record in the master
  // table SOME_MASTER must be created BEFORE the detail(s) in the SOME_DETAIL.

  try
  // *************************************************************************
  // Each item in the ADDRESS table is related to one-or-one other ADDRESS items
  // (principally, "content") via the ADDRESS_HIERARCHY table where HEATHER TO DESCRIBE.
  // The "ADDRESS" table has a foreign key to "ADDRESS_TYPE" table to indicate
  // the type of each record within the ADDRESS table. The CRUD process for an
  // item in the ADDRESS table (embodied hereunder in code) flows as follows:
  //
  // CREATE: To create a brand new full master postal address, you need to 
  //   1. Create a null CONTENT record in the ADDRESS table of type US_POSTAL_ADDRESS
  //   2. Create 5-6 ADDRESS records with CONTENT values to reflect the types
  //      STREET, CITY, STATE, POSTAL_CODE, COUNTRY (alternatively, you do not
  //      need to create any records if all of the detail ADDRESS records already
  //      exists, you just need to fetch their IDs)
  //   3. Create ADDRESS_HIERARCHY records for each of the detail ADDRESS records
  //      to link them to the master ADDRESS record by setting the PARENT_ADDRESS_ID
  //      to the master ADDRESS ID and the CHILD_ADDRESS_ID to each detail ADDRESS ID
  //   NOTE: steps 1 and 2 do not have to happen in any particular order, but
  //   step 3 must happen after steps 1 and 2.
  // UPDATE: To update a detail of the full master postal address, you need to 
  //   1. CREATE the new detail (or get reference to it if it already exists)
  //   2. CREATE a new ADDRESS_HIERARCHY record in the same fashion of step 3 in CREATE
  //   3. UPDATE the old ADDRESS_HIERARCHY record that linked the old detail
  //      address to the master address by setting its isActive value to '0'
  //   NOTE: If the new detail address had previously been associated with the
  //   master address (i.e. already has an ADDRESS_HIERARCHY record that was 
  //   marked with isActive = '0') Then you will to replace step 2 above with an
  //   UPDATE statement to modify the isActive value of the existing ADDRESS_HIERARCHY
  // 

  //      
  //
  //   1. Update the Master (parent) address record
  //   2. Update the Detail (child) address record
  //   3. Udate the Hierarchy (connector) record
  //
  // *************************************************************************
    Log( aRequest );

    if not (aRequest in [arLock, arUnlock]) then begin
      // address1Master and address2Master are only used to CREATE/INSERT or 
      // DELETE master address records into the ENTITY_BASE and ADDRESS tables
      // *********************************************************************
      // ISSUE: What if you do not want to create a new master address, and
      // rather just want to add details to an existing master record?
      // Maybe setting up a Master-Detail type form would help (requiring
      // one FDQuery to return the master address and one to return the details)
      // *********************************************************************
      
      // If CREATING/INSERTING a new record, then pass in a new GUID for the
      // master address creation: first in the ENTITY_BASE table. 
      FDUpdateSQL_address1Master.ConnectionName := FDQuery.ConnectionName;
      FDUpdateSQL_address1Master.DataSet := FDQuery;
      if aRequest = arInsert then begin
        CreateGUID( lGUIDMaster );
        FDUpdateSQL_address1Master.Commands[aRequest].ParamByName( 'PARENT_ID' ).Value := lGUIDMaster.ToByteArray;
      end;
      FDUpdateSQL_address1Master.Apply( aRequest, aAction, aOptions);

      if aAction = eaApplied then begin
        // If CREATING/INSERTING a new record, then pass in a new GUID for the
        // master address creation: next in the ADDRESS table.
        FDUpdateSQL_address2Master.ConnectionName := FDQuery.ConnectionName;
        FDUpdateSQL_address2Master.DataSet := FDQuery;
        if aRequest = arInsert then begin
          FDUpdateSQL_address1Master.Commands[aRequest].ParamByName( 'PARENT_ID' ).Value := lGUIDMaster.ToByteArray;
        end;
        FDUpdateSQL_address2Master.Apply( aRequest, aAction, aOptions );
        
        if aAction = eaApplied then begin
          // address3Detail and address4Detail are used to CREATE/INSERT new
          // detail address records (IFF the new CONTENT specified does not
          // already exist) into the ENTITY_BASE and ADDRESS tables, DELETE
          // detail address records (IFF that detail record is not referenced
          // by any other address).
          //
          // *****************************************************************
          // ISSUE:
          // Currently UPDATING/MODIFYING existing detail address records is
          // not implemented so as to protect against modifying a record that
          // is referenced by multiple other master address records. 
          // However, what if you want to correct a mispelling that has plagued
          // all the associated records?
          // *****************************************************************
          
          // If CREATING/INSERTING or UPDATING/MODIFYING, then pass in a new
          // GUID for the detail address creation: first in the ENTITY_BASE table.
          FDUpdateSQL_address3Detail.ConnectionName := FDQuery.ConnectionName;
          FDUpdateSQL_address3Detail.DataSet := FDQuery;
		  if aRequest in [arInsert, arUpdate] then begin
            CreateGUID( lGUIDDetail );
            FDUpdateSQL_address3Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToByteArray;
          end;
          FDUpdateSQL_address3Detail.Apply( aRequest, aAction, aOptions);

          if aAction = eaApplied then begin
            // If CREATING/INSERTING or UPDATING/MODIFYING, then pass in a new
            // GUID for the detail address creation: next in the ADDRESS table.
            FDUpdateSQL_address4Detail.ConnectionName := FDQuery.ConnectionName;
            FDUpdateSQL_address4Detail.DataSet := FDQuery;
			if aRequest in [arInsert, arUpdate] then begin
              FDUpdateSQL_address4Detail.Commands[aRequest].ParamByName( 'CHILD_ID' ).Value := lGUIDDetail.ToByteArray;
            end;
            FDUpdateSQL_address4Detail.Apply( aRequest, aAction, aOptions);

            if aAction = eaApplied then begin
              // address5DetailHierarchy and address6DetailHierarchy are used to
              // CREATE/INSERT new address hierarchy records to connect the
              // specified master and detail address records together (IFF the
              // relationship does not already exist) into the ENTITY_BASE and
              // ADDRESS_HIERARCHY tables, or DELETE address hierarchy records
              
              // If CREATING/INSERTING or UPDATING/MODIFYING, then pass in a new
              // GUID for the hierarchy creation: first in the ENTITY_BASE table.
              FDUpdateSQL_address5DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
              FDUpdateSQL_address5DetailHierarchy.DataSet := FDQuery;
			  if aRequest in [arInsert, arUpdate] then begin
                CreateGUID( lGUIDDetailHierarchy );
			    FDUpdateSQL_address5DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToByteArray;
              end;
              FDUpdateSQL_address5DetailHierarchy.Apply( aRequest, aAction, aOptions );

              if aAction = eaApplied then begin
                // If CREATING/INSERTING or UPDATING/MODIFYING, then pass in a new
                // GUID for the hierarchy creation: next in the ADDRESS_HIERARCHY table.
                FDUpdateSQL_address6DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                FDUpdateSQL_address6DetailHierarchy.DataSet := FDQuery;
				if aRequest in [arInsert, arUpdate] then begin
                  FDUpdateSQL_address6DetailHierarchy.Commands[aRequest].ParamByName( 'HIERARCHY_ID' ).Value := lGUIDDetailHierarchy.ToByteArray;
                end;
                FDUpdateSQL_address6DetailHierarchy.Apply( aRequest, aAction, aOptions );
                
                if aAction = eaApplied then begin
                  // address7DetailHierarchy is used when UPDATING/MODIFYING to mark
                  // the old detail address previously associated with the master address
                  // (but now being replaced) as inactive via the IS_ACTIVE flag in the
                  // ENTITY_BASE table. This does not update the actual detail address, but
                  // the hierarchical relationship between the detail and master addresses
                  FDUpdateSQL_address7DetailHierarchy.ConnectionName := FDQuery.ConnectionName;
                  FDUpdateSQL_address7DetailHierarchy.DataSet := FDQuery;
                  FDUpdateSQL_address7DetailHierarchy.Apply( aRequest, aAction, aOptions );
                  
                  // *********************************************************
                  // ISSUE: What if you want to mark an address hierarchy record
                  // as active that was previously marked as inactive? Perhaps
                  // we need YET ANOTHER FDUPdateSQL component?
                  // FDUpdateSQL_address8DetailHierarchy?
                  // *********************************************************
                  
                  // Commit only if all previous queries ran successfully
				  if aAction = eaApplied then begin
                    Connection.Commit;
				  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
	// Can this line be entirely removed? If it is successful, then aAction will already be set to eaApplied
    aAction := eaApplied;
  except
    on E: Exception do begin
      CodeSite.SendException( E );
      Connection.Rollback;
      raise E;
    end;
  end;

  //lSQL := FDUpdateSQL_Person.ModifySQl.Text;
//  lIndex := TEST6_Address_View_Lister_SQL.lbxCommands.Items.Add( lRequest );
//  TEST6_Address_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

  lValue := aSender.FieldByName( 'CONTENT' ).Value;
  lIndex := TEST6_Address_View_Lister_SQL.lbxCommands.Items.Add( lValue );
  TEST6_Address_View_Lister_SQL.lbxCommands.ItemIndex := lIndex;

end;
