inherited TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions: TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions
  Width = 430
  Height = 350
  ExplicitWidth = 430
  ExplicitHeight = 350
  object Label1: TLabel
    Left = 16
    Top = 48
    Width = 44
    Height = 13
    Caption = 'End Date'
  end
  object Label2: TLabel
    Left = 16
    Top = 16
    Width = 50
    Height = 13
    Caption = 'Start Date'
  end
  object deExStartDate: TDateTimePicker
    Left = 88
    Top = 16
    Width = 186
    Height = 21
    Date = 42573.162961655090000000
    Time = 42573.162961655090000000
    TabOrder = 0
  end
  object deExEndDate: TDateTimePicker
    Left = 88
    Top = 48
    Width = 186
    Height = 21
    Date = 42573.162961655090000000
    Time = 42573.162961655090000000
    TabOrder = 1
  end
  object teExStartTime: TDateTimePicker
    Left = 200
    Top = 16
    Width = 186
    Height = 21
    Date = 42966.609027777780000000
    Time = 42966.609027777780000000
    ShowCheckbox = True
    TabOrder = 2
  end
  object teExEndTime: TDateTimePicker
    Left = 200
    Top = 48
    Width = 186
    Height = 21
    Date = 42966.609027777780000000
    Time = 42966.609027777780000000
    ShowCheckbox = True
    TabOrder = 3
  end
  object lbEx: TListBox
    Left = 16
    Top = 96
    Width = 289
    Height = 240
    ItemHeight = 13
    TabOrder = 5
  end
  object btnAdd: TButton
    Left = 328
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Add'
    TabOrder = 7
  end
  object btnDelete: TButton
    Left = 328
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 4
  end
  object btnClear: TButton
    Left = 328
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 6
  end
  object ActionList: TActionList
    Left = 352
    Top = 216
  end
end
