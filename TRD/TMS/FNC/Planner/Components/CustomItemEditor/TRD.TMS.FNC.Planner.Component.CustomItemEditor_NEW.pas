unit TRD.TMS.FNC.Planner.Component.CustomItemEditor_NEW;
// -----------------------------------------------------------------------------
// REFERENCES
//
//  Concerning creating a custom editor for TMS Planner item, see:
//  - http://tmssoftware.be/site/forum/forum_posts.asp?TID=2891&OB=ASC
//  - https://www.tmssoftware.com/site/forum/forum_posts.asp?TID=458&title=create-own-planner-item-editor
//  - http://www.tmssoftware.biz/Download/Manuals/TMS%20TPLANNER.pdf (Pages 29 to 31)
//
//  Concerning the VERY ANNOYING error "Control" has no parent running under VCL, see:
//  - https://stackoverflow.com/questions/3776028/the-control-xxx-has-no-parent-window
//
//  Concerning leaving dates blank, TPlannerDatePicker allows entry to blank dates
//  as discussed here:
//  - http://tmssoftware.be/site/forum/forum_posts.asp?TID=7546&KW=planner+custom+editor&PID=29447&title=inline-editor-eddateedit#29447
//
// -----------------------------------------------------------------------------
// TODO (Last updated 2016-07-22)
//
//  1. ...
//
// -----------------------------------------------------------------------------
{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}{$modeswitch advancedrecords}
{$endif USING_LCL}

{$ifdef USING_LCL}
  {$define CMNLIB}
  {$define LCLLIB}
{$endif USING_LCL}

{$ifdef USING_VCL}
  {$define CMNLIB}
  {$undef LCLLIB}
  {$undef FMXLIB}
{$endif USING_VCL}

{$ifdef USING_FMX}
  {$define FMXLIB}
  {$undef LCLLIB}
  {$undef CMBLIB}
{$endif USING_FMX}

interface

uses
{$ifndef USING_LCL}
  System.Classes
, System.UITypes
, System.SysUtils
  {$ifdef USING_VCL}
, Vcl.Controls
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.Forms
//
, VCL.TMSFNCTypes
, VCL.TMSFNCCustomControl
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Controls
, Fmx.StdCtrls
, Fmx.ExtCtrls
, Fmx.Forms
//
, FMX.TMSFNCTypes
, FMX.TMSFNCCustomControl
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
  Classes
, SysUtils
//
, Controls
, StdCtrls
, ExtCtrls
, Forms
//
, LCLTMSFNCTypes
, LCLTMSFNCCustomControl
, LCLTMSFNCCustomComponent
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
;

resourcestring
  sTMSFNCPlannerItemEditorOK = 'OK';
  sTMSFNCPlannerItemEditorCancel = 'Cancel';

type
  //TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW = class;
  TTMSFNCPlannerItemEditor = class;

  {$IFDEF FMXLIB}
  TTMSFNCPlannerItemEditorParent = TFmxObject;
  {$ENDIF}
  {$IFDEF CMNLIB}
  TTMSFNCPlannerItemEditorParent = TWinControl;
  {$ENDIF}
  {$IFNDEF LCLLIB}
  [ComponentPlatformsAttribute( TMSPlatformsDesktop )]
  {$ENDIF}

  //TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW = class( TTMSFNCPlannerCustomItemEditor )
  TTMSFNCPlannerItemEditor = class( VCL.TMSFNCPlannerItemEditor.TTMSFNCPlannerItemEditor )
    private
      fPanel: TPanel;
      fCopyPlanner: TTMSFNCPlanner;
      fButtonOK, FButtonCancel: TButton;
      fPlanner: TTMSFNCCustomPlanner;
      fPlannerItem: TTMSFNCPlannerItem;
      fCopyPlannerItem: TTMSFNCPlannerItem;
      procedure SetPlannerItem( const Value: TTMSFNCPlannerItem );
    protected
      function GetInstance: NativeUInt; override;
      procedure BuildEditor( aParent: TTMSFNCPlannerItemEditorParent ); override;
    public
      function Execute: TModalResult; reintroduce;
    // *************************************************************************
//    // called to create singleton instance???
//      function CreateInstance: TTMSFNCPlannerCustomItemEditor; override;
//    // called once to create editor View
//      procedure CreateCustomContentPanel; override;
//    // called repeatedly to syn location of PlannerItem
//      procedure InitializeCustomContentPanel; override;
//    // return custom editor to edit event for a particular PlannerItem
//      procedure GetCustomContentPanel( aItem: TTMSFNCPlannerItem;
//        var aContentPanel: TTMSFNCPlannerEditingDialogContentPanel ); override;
//    // populate controls within custom editor with the PlannerItem property settings
//      procedure ItemToCustomContentPanel( aItem: TTMSFNCPlannerItem; aContentPanel:
//        TTMSFNCPlannerEditingDialogContentPanel ); override;
//    // update PlannerItem properties with values from controls within custom editor
//      procedure CustomContentPanelToItem( aContentPanel:
//        TTMSFNCPlannerEditingDialogContentPanel; aItem: TTMSFNCPlannerItem ); override;
    // *************************************************************************
      property PlannerItem: TTMSFNCPlannerItem read fPlannerItem write SetPlannerItem;
  end;

// Short name alias
  //TTMSPlannerCustomItemEditor_NEW = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW;

implementation

type
  TTMSFNCCustomPlannerOpen = class( TTMSFNCPlanner );


(*
// =============================================================================

{ TTRD_TMS_FNC_Planner_Component_CustomItemEditor }

// -----------------------------------------------------------------------------
// existential methods

function TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.CreateInstance: TTMSFNCPlannerCustomItemEditor;
begin
  raise Exception.Create( 'TBD: Determine where and how this is being called?' );
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.CreateCustomContentPanel;
begin
  inherited;

end;


// -----------------------------------------------------------------------------
// requisite custom item editor overrides

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.CustomContentPanelToItem(
  aContentPanel: TTMSFNCPlannerEditingDialogContentPanel;
  aItem: TTMSFNCPlannerItem);
begin
  inherited;

end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.GetCustomContentPanel(
  aItem: TTMSFNCPlannerItem;
  var aContentPanel: TTMSFNCPlannerEditingDialogContentPanel);
begin
  inherited;

end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.InitializeCustomContentPanel;
begin
  inherited;

end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_NEW.ItemToCustomContentPanel(
  aItem: TTMSFNCPlannerItem;
  aContentPanel: TTMSFNCPlannerEditingDialogContentPanel);
begin
  inherited;

end;
*)


// =============================================================================

{ TTMSFNCPlannerItemEditor - perverted for our needs }

// -----------------------------------------------------------------------------
// TMS methods for ItemEditor as component (see )

procedure TTMSFNCPlannerItemEditor.BuildEditor(aParent: TTMSFNCPlannerItemEditorParent);
var
  c: TTMSFNCPlannerEditingDialog;
begin
  if Assigned(aParent) and Assigned(fCopyPlanner) and Assigned(fCopyPlannerItem) then begin
    c := TTMSFNCCustomPlannerOpen(fCopyPlanner).GetEditingDialog(fCopyPlannerItem.Index);
    if Assigned(c.Panel) and Assigned(c.BottomPanel) then begin
      c.BottomPanel.Visible := False;
      {$IFDEF FMXLIB}
      if aParent is TCommonCustomForm then begin
        (aParent as TCommonCustomForm).Width := Round(c.Panel.Width);
        (aParent as TCommonCustomForm).Height := Round(c.Panel.Height + 37);
      end;
      {$ENDIF}
      {$IFDEF CMNLIB}
      aParent.Width := Round(c.Panel.Width);
      aParent.Height := Round(c.Panel.Height + 37);
      {$ENDIF}

      fCopyPlanner.OpenEditingDialog(fCopyPlannerItem.StartTime, fCopyPlannerItem.EndTime, fCopyPlannerItem.Resource, fCopyPlannerItem.Title,
        fCopyPlannerItem.Text, fCopyPlannerItem.Index, aParent);

      {$IFDEF FMXLIB}
      c.Panel.Align := TAlignLayout.Client;
      {$ENDIF}
      {$IFDEF CMNLIB}
      c.Panel.Align := alClient;
      {$ENDIF}

      fPanel := TPanel.Create(aParent);
      {$IFDEF FMXLIB}
      fPanel.Align := TAlignLayout.Bottom;
      {$ENDIF}
      {$IFDEF CMNLIB}
      fPanel.Align := alBottom;
      {$ENDIF}
      fPanel.Height := 37;
      fPanel.Parent := aParent;

      FButtonCancel := TButton.Create(fPanel);
      FButtonCancel.ModalResult := mrCancel;
      FButtonCancel.Parent := fPanel;
      {$IFDEF FMXLIB}
      FButtonCancel.Align := TAlignLayout.Right;
      FButtonCancel.Text := TranslateTextEx(sTMSFNCPlannerItemEditorCancel);
      {$ENDIF}
      {$IFDEF CMNLIB}
      FButtonCancel.Align := alRight;
      FButtonCancel.Caption := TranslateTextEx(sTMSFNCPlannerItemEditorCancel);
      {$IFDEF VCLLIB}
      FButtonCancel.AlignWithMargins := True;
      {$ENDIF}
      {$ENDIF}
      {$IFDEF LCLLIB}
      FButtonCancel.BorderSpacing.Right := 5;
      FButtonCancel.BorderSpacing.Top := 5;
      FButtonCancel.BorderSpacing.Bottom := 5;
      FButtonCancel.BorderSpacing.Left := 5;
      {$ENDIF}
      {$IFNDEF LCLLIB}
      FButtonCancel.Margins.Right := 5;
      FButtonCancel.Margins.Top := 5;
      FButtonCancel.Margins.Bottom := 5;
      FButtonCancel.Margins.Left := 5;
      {$ENDIF}

      fButtonOK := TButton.Create(fPanel);
      fButtonOK.ModalResult := mrOk;
      fButtonOK.Default := True;
      fButtonOK.Parent := fPanel;
      {$IFDEF FMXLIB}
      fButtonOK.Align := TAlignLayout.Right;
      fButtonOK.Text := TranslateTextEx(sTMSFNCPlannerItemEditorOK);
      {$ENDIF}
      {$IFDEF CMNLIB}
      fButtonOK.Align := alRight;
      fButtonOK.Caption := TranslateTextEx(sTMSFNCPlannerItemEditorOK);
      {$IFDEF VCLLIB}
      fButtonOK.AlignWithMargins := True;
      {$ENDIF}
      {$ENDIF}
      {$IFNDEF LCLLIB}
      fButtonOK.Margins.Right := 0;
      fButtonOK.Margins.Top := 5;
      fButtonOK.Margins.Bottom := 5;
      fButtonOK.Margins.Left := 5;
      {$ENDIF}
      {$IFDEF LCLLIB}
      fButtonOK.BorderSpacing.Right := 0;
      fButtonOK.BorderSpacing.Top := 5;
      fButtonOK.BorderSpacing.Bottom := 5;
      fButtonOK.BorderSpacing.Left := 5;
      {$ENDIF}
    end;
  end;
end;

function TTMSFNCPlannerItemEditor.Execute: TModalResult;
var
  frm: TTMSFNCCustomDesignerForm;
  c: TTMSFNCPlannerEditingDialog;
begin
  if not Assigned(fPlannerItem) then
    raise Exception.Create( 'No Planner Item Assigned.' );

  fPlanner := nil;
  if fPlannerItem.Planner is TTMSFNCCustomPlanner then
    fPlanner := fPlannerItem.Planner as TTMSFNCCustomPlanner;

  if not Assigned(fPlanner) then
    raise Exception.Create( 'No Planner attached to Planner Item.' );

  fCopyPlanner := TTMSFNCPlanner.Create( Self );
  fCopyPlanner.Assign( fPlanner );
  fCopyPlanner.Items.Clear;
  fCopyPlannerItem := fCopyPlanner.Items.Add;
  fCopyPlannerItem.Assign( fPlannerItem );
  if Assigned( TTMSFNCCustomPlannerOpen( fPlanner ).ItemEditor ) then
    TTMSFNCCustomPlannerOpen( fCopyPlanner ).ItemEditor := TTMSFNCCustomPlannerOpen( fPlanner ).ItemEditor.CreateInstance;

  frm := TTMSFNCCustomDesignerForm.CreateNew( Application );
  frm.Caption := 'Planner Item Editor';
  {$IFDEF FMXLIB}
  frm.Position := TFormPosition.ScreenCenter;
  frm.BorderIcons := [TBorderIcon.biSystemMenu];
  frm.BorderStyle := TFmxFormBorderStyle.Single;
  {$ENDIF}
  {$IFDEF CMNLIB}
  frm.Position := poScreenCenter;
  frm.BorderIcons := [ biSystemMenu ];
  frm.BorderStyle := bsSingle;
  {$ENDIF}
  BuildEditor( frm );
  Result := frm.ShowModal;
  fCopyPlanner.CloseEditingDialog( Result = mrCancel );
  c := TTMSFNCCustomPlannerOpen( fCopyPlanner ).GetEditingDialog( fCopyPlannerItem.Index );
  if Assigned( c.Panel ) then
    c.Panel.Parent := nil;

  fPlannerItem.BeginUpdate;
  fPlannerItem.Assign( fCopyPlannerItem );
  fPlannerItem.EndUpdate;

  if Assigned( TTMSFNCCustomPlannerOpen( fCopyPlanner ).ItemEditor ) then
    TTMSFNCCustomPlannerOpen( fCopyPlanner ).ItemEditor.Free;
  fCopyPlanner.Free;
  frm.Free;
end;

function TTMSFNCPlannerItemEditor.GetInstance: NativeUInt;
begin
  Result := HInstance;
end;

procedure TTMSFNCPlannerItemEditor.SetPlannerItem(
  const Value: TTMSFNCPlannerItem);
begin
  fPlannerItem := Value;
end;


end.
