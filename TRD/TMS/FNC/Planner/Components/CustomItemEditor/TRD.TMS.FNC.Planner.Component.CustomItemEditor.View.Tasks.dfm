inherited TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks: TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks
  Width = 425
  Height = 350
  ExplicitWidth = 425
  ExplicitHeight = 350
  object Label1: TLabel
    Left = 24
    Top = 16
    Width = 73
    Height = 13
    Caption = 'Available Tasks'
  end
  object Label2: TLabel
    Left = 256
    Top = 16
    Width = 71
    Height = 13
    Caption = 'Selected Tasks'
  end
  object ListBox1: TListBox
    Left = 24
    Top = 40
    Width = 145
    Height = 289
    ItemHeight = 13
    Sorted = True
    TabOrder = 0
  end
  object ListBox2: TListBox
    Left = 256
    Top = 40
    Width = 145
    Height = 289
    ItemHeight = 13
    Sorted = True
    TabOrder = 1
  end
end
