unit Sample.View.PlannerItem;

interface

{$ifdef fpc}
 {$define USING_LCL}
 {$undef USING_VCL}
 {$undef USING_FMX}
{$else NOT fpc}
 {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.Classes
, System.SysUtils
  {$ifdef USING_VCL}
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.Controls
//, Vcl.Memo
//
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCColorPicker
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.StdCtrls
, Fmx.ExtCtrls
, Fmx.Controls
, Fmx.Memo
//
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCColorPicker
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
, FMX.TMSFNCGraphics
  {$endif USING_FMX}
{$else USING_LCL}
, StdCtrls
, ExtCtrls
, Controls
, Memo
//
, LCLTMSFNCCustomComponent
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCColorPicker
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif}

, Sample.View.PlannerItem._Interface

;

type

  TSampleViewModelPlannerItem = class(TInterfacedObject, ISampleViewModelPlannerItem)
  private
    FTasks: TStringList;
    FEmployee: string;
    FGender: Integer;
    FGCalendarID: string;
    procedure SetTasks(const ATasks: TStringList);
    function GetTasks: TStringList;
    procedure SetEmployee(const AEmployee: string);
    function GetEmployee: string;
    procedure SetGender(const AGender: Integer);
    function GetGender: Integer;
    procedure SetGCalendarID(const AGCalendarID: string);
    function GetGCalendarID: string;
  public
    constructor Create;
    destructor Destroy; override;
    property Tasks: TStringList read GetTasks write SetTasks;
    property Employee: string read GetEmployee write SetEmployee;
    property Gender: Integer read GetGender write SetGender;
    property GCalendarID: string read GetGCalendarID write SetGCalendarID;
  end;

//  TSampleViewModelPlannerItem = class( TTMSFNCPlannerItem )
//  private
//    FEmployee: string;
//    FGender: string;
//    FGCalendarID: string;
//    procedure SetEmployee(const AEmployee: string);
//    function GetEmployee: string;
//    procedure SetGender(const AGender: Integer);
//    function GetGender: Integer;
//    procedure SetGCalendarID(const AGCalendarID: string);
//    function GetGCalendarID: string;
//  public
//    procedure Assign(Source: TPersistent); override;
//  published
//    property Employee: string read FEmployee write SetEmployee;
//    property Gender: string read FGender write SetGender;
//    property GCalendarID: string read FGCalendarID write SetGCalendarID;
//  end;
//
//  TSampleViewModelPlannerItems = class( TTMSFNCPlannerItems )
//  protected
//    function GetItemClass: TCollectionItemClass; override;
//  end;
//
//  TSampleViewModelPlannerData = class( TTMSFNCPlannerData )
//  public
//    //Is this the right function?
//    function CreateItems: TTMSFNCPlannerItems; override;
//  end;

implementation

{TSampleViewModelPlannerItem}

constructor TSampleViewModelPlannerItem.Create;
begin
  FTasks := TStringList.Create;
end;

procedure TSampleViewModelPlannerItem.SetTasks(const ATasks: TStringList);
begin
  FTasks := ATasks;
end;

function TSampleViewModelPlannerItem.GetTasks;
begin
  Result := FTasks;
end;

procedure TSampleViewModelPlannerItem.SetEmployee(const AEmployee: string);
begin
  FEmployee := AEmployee;
end;

function TSampleViewModelPlannerItem.GetEmployee;
begin
  Result := FEmployee;
end;

procedure TSampleViewModelPlannerItem.SetGender(const AGender: Integer);
begin
  FGender := AGender;
end;

function TSampleViewModelPlannerItem.GetGender;
begin
  Result := FGender;
end;

procedure TSampleViewModelPlannerItem.SetGCalendarID(const AGCalendarID: string);
begin
  FGCalendarID := AGCalendarID;
end;

function TSampleViewModelPlannerItem.GetGCalendarID;
begin
  Result := FGCalendarID;
end;

destructor TSampleViewModelPlannerItem.Destroy;
begin
  FTasks.Free;
  inherited;
end;

{TSampleViewModelPlannerItem}

//procedure TSampleViewModelPlannerItem.SetEmployee(const AEmployee: string);
//begin
//  if FEmployee <> AEmployee then begin
//    FEmployee := AEmployee;
//    UpdateItem;
//  end;
//end;
//
//function TSampleViewModelPlannerItem.GetEmployee;
//begin
//  Result := FEmployee;
//end;
//
//procedure TSampleViewModelPlannerItem.SetGender(const AGender: string);
//begin
//  if FGender <> AGender then begin
//    FGender := AGender;
//    UpdateItem;
//  end;
//end;
//
//function TSampleViewModelPlannerItem.GetGender;
//begin
//  Result := FGender;
//end;
//
//procedure TSampleViewModelPlannerItem.SetGCalendarID(const AGCalendarID: string);
//begin
//  if FGCalendarID <> AGCalendarID then begin
//    FGCalendarID := AGCalendarID;
//    UpdateItem;
//  end;
//end;
//
//function TSampleViewModelPlannerItem.GetGCalendarID;
//begin
//  Result := FGCalendarID;
//end;
//
//procedure TSampleViewModelPlannerItem.Assign(Source: TPersistent);
//begin
//  if Source is TSampleViewModelPlannerItem then begin
//    inherited;
//    FEmployee := (Source as TSampleViewModelPlannerItem).Employee;
//    FGender := (Source as TSampleViewModelPlannerItem).Gender;
//    FGCalendarID := (Source as TSampleViewModelPlannerItem).GCalendarID;
//  end;
//end;

//{TSampleViewModelPlannerItems}
//
//function TSampleViewModelPlannerItems.GetItemClass: TCollectionItemClass;
//begin
//  Result := TSampleViewModelPlannerItem;
//end;
//
//{TSampleViewModelPlannerData}
//
//function TSampleViewModelPlannerData.CreateItems: TTMSFNCPlannerItems;
//begin
//  Result := TSampleViewModelPlannerItems.Create(self);
//end;

end.
