unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL;
// -----------------------------------------------------------------------------
// MADE FROM VCL VERSION UNDER TMS FNC PLANNER
// -----------------------------------------------------------------------------
// REFERENCES
//
//  Concerning creating a custom editor for TMS Planner item, see:
//  - http://tmssoftware.be/site/forum/forum_posts.asp?TID=2891&OB=ASC
//  - https://www.tmssoftware.com/site/forum/forum_posts.asp?TID=458&title=create-own-planner-item-editor
//  - http://www.tmssoftware.biz/Download/Manuals/TMS%20TPLANNER.pdf (Pages 29 to 31)
//
//  Concerning the VERY ANNOYING error "Control" has no parent running under VCL, see:
//  - https://stackoverflow.com/questions/3776028/the-control-xxx-has-no-parent-window
//
//  Concerning leaving dates blank, TPlannerDatePicker allows entry to blank dates
//  as discussed here:
//  - http://tmssoftware.be/site/forum/forum_posts.asp?TID=7546&KW=planner+custom+editor&PID=29447&title=inline-editor-eddateedit#29447
//
// -----------------------------------------------------------------------------
// TODO (Last updated 2016-07-22)
//
//  1. ...
//
// -----------------------------------------------------------------------------
{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}{$modeswitch advancedrecords}
{$endif USING_LCL}

{$ifdef USING_LCL}
  {$define CMNLIB}
  {$define LCLLIB}
{$endif USING_LCL}

{$ifdef USING_FMX}
  {$define FMXLIB}
  {$undef LCLLIB}
  {$undef CMBLIB}
{$endif USING_FMX}

{$ifdef USING_VCL}
  {$define CMNLIB}
  {$undef LCLLIB}
  {$undef FMXLIB}
{$endif USING_VCL}

interface

{.$I VCL.TMSFNCDefines.inc}

uses
{$ifndef USING_LCL}
  System.Classes
, System.Types
, System.UITypes
, System.SysUtils
, System.Generics.Collections
, System.DateUtils
  {$ifdef USING_VCL}
, Vcl.Controls
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.Forms
, Vcl.ComCtrls
//
, VCL.TMSFNCTypes
, VCL.TMSFNCUtils
, VCL.TMSFNCGraphics
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCCustomControl
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Controls
, Fmx.StdCtrls
, Fmx.ExtCtrls
, Fmx.Forms
, Fmx.Layouts
, Fmx.TabControl
, Fmx.DateTimeCtrls
, Fmx.ListBox
, Fmx.Edit
, Fmx.Memo
//
, FMX.TMSFNCTypes
, FMX.TMSFNCUtils
, FMX.TMSFNCGraphics
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCCustomControl
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
  Classes
, SysUtils
//
, Controls
, StdCtrls
, ExtCtrls
, Forms
, DateTimePicker
, fgl
//
, LCLTMSFNCTypes
, LCLTMSFNCUtils
, LCLTMSFNCGraphics
, LCLTMSFNCGraphicsTypes
, LCLTMSFNCCustomControl
, LCLTMSFNCCustomComponent
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
;

const
  MAJ_VER = 1; // Major version nr.
  MIN_VER = 0; // Minor version nr.
  REL_VER = 0; // Release nr.
  BLD_VER = 0; // Build nr.

  // version history
  // v1.0.0.0 : first release

type
  TTMSFNCPlannerItemEditorRadioGroupButtons = TList<TRadioButton>;

  TTMSFNCPlannerItemEditorRadioGroup = class(TGroupBox)
  private
    FButtons: TTMSFNCPlannerItemEditorRadioGroupButtons;
    FItems: TStrings;
    FItemIndex: Integer;
    FColumns: Integer;
    FReading: Boolean;
    FUpdating: Boolean;
    function GetButtons(Index: Integer): TRadioButton;
    procedure ArrangeButtons;
    procedure ButtonClick(Sender: TObject);
    procedure ItemsChange(Sender: TObject);
    procedure SetButtonCount(Value: Integer);
    procedure SetColumns(Value: Integer);
    procedure SetItemIndex(Value: Integer);
    procedure SetItems(Value: TStrings);
    procedure UpdateButtons;
  protected
    {$IFDEF LCLLIB}
    procedure DoSetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    {$ENDIF}
    procedure Loaded; override;
    property Columns: Integer read FColumns write SetColumns default 1;
    property ItemIndex: Integer read FItemIndex write SetItemIndex default -1;
    property Items: TStrings read FItems write SetItems;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Resize; override;
    property Buttons[Index: Integer]: TRadioButton read GetButtons;
  end;

  {$IFDEF FMXLIB}
  TTMSFNCPlannerItemEditorTabControl = class(TTabControl);
  TTMSFNCPlannerItemEditorTabItem = class(TTabItem);
  TTMSFNCPlannerItemEditorLayout = class(TLayout);
  {$ENDIF}
  {$IFDEF CMNLIB}
  TTMSFNCPlannerItemEditorTabControl = class(TPageControl);
  TTMSFNCPlannerItemEditorTabItem = class(TTabSheet);
  TTMSFNCPlannerItemEditorLayout = class(TPanel)
  public
    constructor Create(AOwner: TComponent); override;
  end;
  {$ENDIF}

  TTMSFNCPlannerItemEditorRecurrencyContentPanel = record
    ContentPanel: TPanel;
    TabControl, TabControlRecurrency: TTMSFNCPlannerItemEditorTabControl;
    TabGeneral: TTMSFNCPlannerItemEditorTabItem;
    StartTimeLabel: TLabel;
    EndTimeLabel: TLabel;
    StartTimeEdit: TTMSFNCPlannerTimeEdit;
    EndTimeEdit: TTMSFNCPlannerTimeEdit;
    StartDateEdit: TTMSFNCPlannerDateEdit;
    EndDateEdit: TTMSFNCPlannerDateEdit;
    ResourceLabel: TLabel;
    ResourceComboBox: TComboBox;
    TitleLabel, TextLabel: TLabel;
    TitleEdit: TEdit;
    TextMemo: TMemo;
    TabRecurrency: TTMSFNCPlannerItemEditorTabItem;
    TabRecurrencySettings: TTMSFNCPlannerItemEditorTabItem;
    TabRecurrencyExceptions: TTMSFNCPlannerItemEditorTabItem;
    PatternLayout: TTMSFNCPlannerItemEditorLayout;
    Frequency, Range: TTMSFNCPlannerItemEditorRadioGroup;
    PatternDetails: TGroupBox;
    Date: TTMSFNCPlannerDateEdit;
    Occur: TEdit;
    OccurLabel: TLabel;
    Interval: TEdit;
    IntervalLabel: TLabel;
    DayLayout, DayWeekLayout, WeekLayout, YearLayout: TTMSFNCPlannerItemEditorLayout;
    Mon, Tue, Wed, Thu, Fri, Sat, Sun: TCheckBox;
    Day, WeekDay: TRadioButton;
    MonthDay, SpecialDay: TRadioButton;
    YearDay, YearSpecialDay: TRadioButton;
    ComboWeekNum, ComboDay, ComboYearWeekNum, ComboYearDay: TComboBox;
    YearCheck1, YearCheck2, YearCheck3, YearCheck4, YearCheck5, YearCheck6, YearCheck7,
    YearCheck8, YearCheck9, YearCheck10, YearCheck11, YearCheck12: TCheckBox;
    ExceptionStartDate, ExceptionEndDate: TTMSFNCPlannerDateEdit;
    ExceptionStartTime, ExceptionEndTime: TTMSFNCPlannerTimeEdit;
    ExceptionList: TListBox;
    ExceptionAdd, ExceptionDelete, ExceptionClear: TButton;
  end;

  {$IFNDEF LCLLIB}
  [ComponentPlatformsAttribute(TMSPlatforms)]
  {$ENDIF}
  TTMSPlannerCustomItemEditor_VCL = class(TTMSFNCPlannerCustomItemEditor)
  private
    FCustomContentPanelCreated: Boolean;
    FRecurrency: String;
    FInsertResource: Integer;
    FInterval: TEdit;
    FIntervalLabel: TLabel;
    FContentPanelSet: Boolean;
    FExDates: TTMSFNCRecurrencyDateItems;
    FExList: TListBox;
    FContentPanel: TPanel;
    FTextMemo: TMemo;
    FTabControl, FTabControlRecurrency: TTMSFNCPlannerItemEditorTabControl;
    FTabGeneral, FTabRecurrency, FTabRecurrencySettings, FTabRecurrencyExceptions: TTMSFNCPlannerItemEditorTabItem;
    FDialogEndDate, FDialogStartDate: TDate;
    FDialogEndTime, FDialogStartTime: TTime;
    FPatternLayout: TTMSFNCPlannerItemEditorLayout;
    FStartTimeLabel, FEndTimeLabel, FTextLabel,
    FTitleLabel, FResourceLabel: TLabel;
    FStartTimeEdit, FEndTimeEdit: TTMSFNCPlannerTimeEdit;
    FResourcesComboBox: TComboBox;
    FStartDateEdit, FEndDateEdit: TTMSFNCPlannerDateEdit;
    FTitleEdit: TEdit;
    FFreq: TTMSFNCPlannerItemEditorRadioGroup;
    FRange: TTMSFNCPlannerItemEditorRadioGroup;
    FPatternDetails: TGroupBox;
    FOccur: TEdit;
    FOccurLabel: TLabel;
    FDate: TTMSFNCPlannerDateEdit;
    FMon, FTue, FWed, FThu, FFri, FSat, FSun: TCheckBox;
    FDay, FYearDay, FMonthDay, FWeekDay, FSpecialDay, FYearSpecialDay: TRadioButton;
    FCWeekNum, FCDay, FCYearWeekNum, FCYearDay: TComboBox;
    FExsd, FExed: TTMSFNCPlannerDateEdit;
    FExst, FExet: TTMSFNCPlannerTimeEdit;
    FBtnAdd, FBtnDelete, FBtnClear: TButton;
    FYCK1, FYCK2, FYCK3, FYCK4, FYCK5, FYCK6, FYCK7, FYCK8, FYCK9, FYCK10, FYCK11, FYCK12: TCheckBox;
    FDayL, FYearL, FWeekL, FDayWeekL: TTMSFNCPlannerItemEditorLayout;
  protected
    function GetVersion: String; override;
    procedure RegisterRuntimeClasses; override;
    procedure ShowLayouts(AIndex: Integer); virtual;
    procedure StartDateEditChanged(Sender: TObject); virtual;
    procedure EndDateEditChanged(Sender: TObject); virtual;
    procedure StartTimeEditChanged(Sender: TObject); virtual;
    procedure EndTimeEditChanged(Sender: TObject); virtual;
    procedure FreqChanged(Sender: TObject);
    procedure BtnAddClicked(Sender: TObject);
    procedure BtnDeleteClicked(Sender: TObject);
    procedure BtnClearClicked(Sender: TObject);
  public
    procedure CreateCustomContentPanel; override;
    procedure InitializeCustomContentPanel; override;
    function CreateInstance: TTMSFNCPlannerCustomItemEditor; override;
    function Panel: TPanel; virtual;
    function CustomContentPanel: TTMSFNCPlannerItemEditorRecurrencyContentPanel; virtual;
    procedure GetCustomContentPanel({%H-}AItem: TTMSFNCPlannerItem; var AContentPanel: TTMSFNCPlannerEditingDialogContentPanel); override;
    procedure ItemToCustomContentPanel(AItem: TTMSFNCPlannerItem; {%H-}AContentPanel: TTMSFNCPlannerEditingDialogContentPanel); override;
    procedure CustomContentPanelToItem({%H-}AContentPanel: TTMSFNCPlannerEditingDialogContentPanel; AItem: TTMSFNCPlannerItem); override;
    procedure ItemToRecurrency(ARecurrency: String); virtual;
    procedure RecurrencyToItem(AItem: TTMSFNCPlannerItem); virtual;
    function GetRecurrency: String;
    procedure SetRecurrency(ARecurrency: String);
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Version: String read GetVersion;
  end;

implementation

{.$R TMSFNCPlannerItemEditorRecurrency.res}

type
  TTMSFNCCustomPlannerOpen = class(TTMSFNCCustomPlanner);

{ TTMSFNCPlannerItemEditorRecurrency }

procedure TTMSPlannerCustomItemEditor_VCL.CreateCustomContentPanel;
begin
  if FCustomContentPanelCreated then
    Exit;

  FContentPanel := TPanel.Create(Self);
  {$IFDEF FMXMOBILE}
  FContentPanel.Height := 450;
  FContentPanel.Width := 500;
  {$ELSE}
  FContentPanel.Height := 325;
  FContentPanel.Width := 450;
  {$ENDIF}

  FTabControl := TTMSFNCPlannerItemEditorTabControl.Create(FContentPanel);
  {$IFDEF FMXLIB}
  FTabControl.Align := TAlignLayout.Client;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabControl.Align := alClient;
  {$ENDIF}
  FTabControl.Parent := FContentPanel;

  FTabGeneral := TTMSFNCPlannerItemEditorTabItem.Create(FTabControl);
  FTabGeneral.Text := TranslateTextEx('General');

  FStartTimeLabel := TLabel.Create(FTabGeneral);
  FStartTimeLabel.Width := 100;
  FStartTimeLabel.WordWrap := False;
  FStartTimeLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FStartTimeLabel.Text := TranslateTextEx(sTMSFNCPlannerStartTime);
  FStartTimeLabel.Position.X := 10;
  FStartTimeLabel.Position.Y := 10;
  FStartTimeLabel.FontColor := gcDarkgray;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FStartTimeLabel.Caption := TranslateTextEx(sTMSFNCPlannerStartTime);
  FStartTimeLabel.Left := 10;
  FStartTimeLabel.Top := 10;
  FStartTimeLabel.Font.Color := gcDarkgray;
  {$ENDIF}
  FStartTimeLabel.Parent := FTabGeneral;

  FStartTimeEdit := TTMSFNCPlannerTimeEdit.Create(FTabGeneral);
  FStartTimeEdit.OnChange := StartTimeEditChanged;
  FStartTimeEdit.Width := 100;
  {$IFDEF FMXLIB}
  FStartTimeEdit.Position.X := FContentPanel.Width - FStartTimeEdit.Width - 10;
  FStartTimeEdit.Position.Y := FStartTimeLabel.Position.Y + int((FStartTimeLabel.Height - FStartTimeEdit.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FStartTimeEdit.Kind := dtkTime;
  FStartTimeEdit.Left := FContentPanel.Width - FStartTimeEdit.Width - 10;
  FStartTimeEdit.Top := FStartTimeLabel.Top + (FStartTimeLabel.Height - FStartTimeEdit.Height) div 2;
  {$ENDIF}
  FStartTimeEdit.Parent := FTabGeneral;

  FStartDateEdit := TTMSFNCPlannerDateEdit.Create(FTabGeneral);
  FStartDateEdit.OnChange := StartDateEditChanged;
  FStartDateEdit.Width := 100;
  {$IFDEF FMXLIB}
  FStartDateEdit.Position.X := FStartTimeEdit.Position.X - FStartDateEdit.Width - 10;
  FStartDateEdit.Position.Y := FStartTimeEdit.Position.Y + int((FStartTimeEdit.Height - FStartDateEdit.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FStartDateEdit.Left := FStartTimeEdit.Left - FStartDateEdit.Width - 10;
  FStartDateEdit.Top := FStartTimeEdit.Top + (FStartTimeEdit.Height - FStartDateEdit.Height) div 2;
  {$ENDIF}
  FStartDateEdit.Parent := FTabGeneral;

  FEndTimeLabel := TLabel.Create(FTabGeneral);
  FEndTimeLabel.Width := 100;
  FEndTimeLabel.WordWrap := False;
  FEndTimeLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FEndTimeLabel.Text := TranslateTextEx(sTMSFNCPlannerEndTime);
  FEndTimeLabel.Position.X := 10;
  FEndTimeLabel.Position.Y := FStartTimeLabel.Position.Y + FStartTimeLabel.Height + 15;
  FEndTimeLabel.FontColor := gcDarkgray;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FEndTimeLabel.Caption := TranslateTextEx(sTMSFNCPlannerEndTime);
  FEndTimeLabel.Left := 10;
  FEndTimeLabel.Top := FStartTimeLabel.Top + FStartTimeLabel.Height + 15;
  FEndTimeLabel.Font.Color := gcDarkgray;
  {$ENDIF}
  FEndTimeLabel.Parent := FTabGeneral;

  FEndTimeEdit := TTMSFNCPlannerTimeEdit.Create(FTabGeneral);
  FEndTimeEdit.OnChange := EndTimeEditChanged;
  FEndTimeEdit.Width := 100;
  {$IFDEF FMXLIB}
  FEndTimeEdit.Position.X := FContentPanel.Width - FEndTimeEdit.Width - 10;
  FEndTimeEdit.Position.Y := FEndTimeLabel.Position.Y + int((FEndTimeLabel.Height - FEndTimeEdit.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FEndTimeEdit.Kind := dtkTime;
  FEndTimeEdit.Left := FContentPanel.Width - FEndTimeEdit.Width - 10;
  FEndTimeEdit.Top := FEndTimeLabel.Top + (FEndTimeLabel.Height - FEndTimeEdit.Height) div 2;
  {$ENDIF}
  FEndTimeEdit.Parent := FTabGeneral;

  FEndDateEdit := TTMSFNCPlannerDateEdit.Create(FTabGeneral);
  FEndDateEdit.OnChange := EndDateEditChanged;
  FEndDateEdit.Width := 100;
  {$IFDEF FMXLIB}
  FEndDateEdit.Position.X := FEndTimeEdit.Position.X - FEndDateEdit.Width - 10;
  FEndDateEdit.Position.Y := FEndTimeEdit.Position.Y + int((FEndTimeEdit.Height - FEndDateEdit.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FEndDateEdit.Left := FEndTimeEdit.Left - FEndDateEdit.Width - 10;
  FEndDateEdit.Top := FEndTimeEdit.Top + (FEndTimeEdit.Height - FEndDateEdit.Height) div 2;
  {$ENDIF}
  FEndDateEdit.Parent := FTabGeneral;

  FResourceLabel := TLabel.Create(FTabGeneral);
  FResourceLabel.Width := 100;
  FResourceLabel.WordWrap := False;
  FResourceLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FResourceLabel.Text := TranslateTextEx(sTMSFNCPlannerResource);
  FResourceLabel.Position.X := 10;
  FResourceLabel.Position.Y := FEndTimeLabel.Position.Y + FEndTimeLabel.Height + 15;
  FResourceLabel.FontColor := gcDarkgray;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FResourceLabel.Caption := TranslateTextEx(sTMSFNCPlannerResource);
  FResourceLabel.Left := 10;
  FResourceLabel.Top := FEndTimeLabel.Top + FEndTimeLabel.Height + 15;
  FResourceLabel.Font.Color := gcDarkgray;
  {$ENDIF}
  FResourceLabel.Parent := FTabGeneral;

  FResourcesComboBox := TComboBox.Create(FTabGeneral);
  {$IFDEF FMXLIB}
  FResourcesComboBox.Width := FContentPanel.Width - FStartDateEdit.Position.X - 10;
  FResourcesComboBox.Position.X := FStartDateEdit.Position.X;
  FResourcesComboBox.Position.Y := FResourceLabel.Position.Y + Int((FResourceLabel.Height - FResourcesComboBox.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FResourcesComboBox.Width := 415 - FStartDateEdit.Left - 10;
  FResourcesComboBox.Left := FStartDateEdit.Left;
  FResourcesComboBox.Top := FResourceLabel.Top + (FResourceLabel.Height - FResourcesComboBox.Height) div 2;
  {$ENDIF}
  FResourcesComboBox.Parent := FTabGeneral;

  FTitleLabel := TLabel.Create(FTabGeneral);
  FTitleLabel.Width := 100;
  FTitleLabel.WordWrap := False;
  FTitleLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FTitleLabel.Text := TranslateTextEx(sTMSFNCPlannerTitle);
  FTitleLabel.Position.X := 10;
  FTitleLabel.Position.Y := FResourceLabel.Position.Y + FResourceLabel.Height + 15;
  FTitleLabel.FontColor := gcDarkgray;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTitleLabel.Caption := TranslateTextEx(sTMSFNCPlannerTitle);
  FTitleLabel.Left := 10;
  FTitleLabel.Top := FResourceLabel.Top + FResourceLabel.Height + 15;
  FTitleLabel.Font.Color := gcDarkgray;
  {$ENDIF}
  FTitleLabel.Parent := FTabGeneral;

  FTitleEdit := TEdit.Create(FTabGeneral);
  {$IFDEF FMXLIB}
  FTitleEdit.Width := FContentPanel.Width - FStartDateEdit.Position.X - 10;
  FTitleEdit.Position.X := FStartDateEdit.Position.X;
  FTitleEdit.Position.Y := FTitleLabel.Position.Y + Int((FTitleLabel.Height - FTitleEdit.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTitleEdit.Width := 415 - FStartDateEdit.Left - 10;
  FTitleEdit.Left := FStartDateEdit.Left;
  FTitleEdit.Top := FTitleLabel.Top + (FTitleLabel.Height - FTitleEdit.Height) div 2;
  {$ENDIF}
  FTitleEdit.Parent := FTabGeneral;

  FTextLabel := TLabel.Create(FTabGeneral);
  FTextLabel.Width := 100;
  FTextLabel.WordWrap := False;
  FTextLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FTextLabel.Text := TranslateTextEx(sTMSFNCPlannerText);
  FTextLabel.Position.X := 10;
  FTextLabel.Position.Y := FTitleLabel.Position.Y + FTitleLabel.Height + 15;
  FTextLabel.FontColor := gcDarkgray;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTextLabel.Caption := TranslateTextEx(sTMSFNCPlannerText);
  FTextLabel.Left := 10;
  FTextLabel.Top := FTitleLabel.Top + FTitleLabel.Height + 15;
  FTextLabel.Font.Color := gcDarkgray;
  {$ENDIF}
  FTextLabel.Parent := FTabGeneral;

  FTextMemo := TMemo.Create(FTabGeneral);
  {$IFDEF FMXLIB}
  FTextMemo.Align := TAlignLayout.Client;
  FTextMemo.Margins.Top := FTextLabel.Position.Y + FTextLabel.Height + 15;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FTextMemo.AlignWithMargins := True;
  {$ENDIF}
  FTextMemo.Align := alClient;
  {$IFDEF VCLLIB}
  FTextMemo.Margins.Top := FTextLabel.Top + FTextLabel.Height + 15;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FTextMemo.BorderSpacing.Top := FTextLabel.Top + FTextLabel.Height + 15;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FTextMemo.Margins.Right := 10;
  FTextMemo.Margins.Bottom := 10;
  FTextMemo.Margins.Left := 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FTextMemo.BorderSpacing.Right := 10;
  FTextMemo.BorderSpacing.Bottom := 10;
  FTextMemo.BorderSpacing.Left := 10;
  {$ENDIF}
  FTextMemo.Parent := FTabGeneral;

  FTabRecurrency := TTMSFNCPlannerItemEditorTabItem.Create(FTabControl);
  {$IFDEF FMXLIB}
  FTabRecurrency.Text := TranslateTextEx('Recurrency');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabRecurrency.Caption := TranslateTextEx('Recurrency');
  {$ENDIF}

  FTabControlRecurrency := TTMSFNCPlannerItemEditorTabControl.Create(FTabRecurrency);
  {$IFDEF FMXLIB}
  FTabControlRecurrency.Align := TAlignLayout.Client;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabControlRecurrency.Align := alClient;
  {$ENDIF}
  FTabControlRecurrency.Parent := FTabRecurrency;

  FTabRecurrencySettings := TTMSFNCPlannerItemEditorTabItem.Create(FTabControlRecurrency);
  {$IFDEF FMXLIB}
  FTabRecurrencySettings.Text := TranslateTextEx('Settings');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabRecurrencySettings.Caption := TranslateTextEx('Settings');
  {$ENDIF}

  FTabRecurrencyExceptions := TTMSFNCPlannerItemEditorTabItem.Create(FTabControlRecurrency);
  {$IFDEF FMXLIB}
  FTabRecurrencyExceptions.Text := TranslateTextEx('Exceptions');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabRecurrencyExceptions.Caption := TranslateTextEx('Exceptions');
  {$ENDIF}

  FPatternLayout := TTMSFNCPlannerItemEditorLayout.Create(FTabRecurrencySettings);
  FPatternLayout.Parent := FTabRecurrencySettings;
  {$IFDEF FMXLIB}
  FPatternLayout.Align := TAlignLayout.Top;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FPatternLayout.AlignWithMargins := True;
  {$ENDIF}
  FPatternLayout.Align := alTop;
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FPatternLayout.Margins.Left := 10;
  FPatternLayout.Margins.Top := 10;
  FPatternLayout.Margins.Right := 10;
  FPatternLayout.Margins.Bottom := 5;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FPatternLayout.BorderSpacing.Left := 10;
  FPatternLayout.BorderSpacing.Top := 10;
  FPatternLayout.BorderSpacing.Right := 10;
  FPatternLayout.BorderSpacing.Bottom := 5;
  {$ENDIF}

  {$IFDEF FMXMOBILE}
  FPatternLayout.Height := 200;
  {$ELSE}
  FPatternLayout.Height := 150;
  {$ENDIF}

  FFreq := TTMSFNCPlannerItemEditorRadioGroup.Create(FPatternLayout);
  FFreq.OnClick := FreqChanged;
  FFreq.Name := 'rgPattern';
  {$IFDEF FMXLIB}
  FFreq.Text := TranslateTextEx('Pattern');
  FFreq.Align := TAlignLayout.Left;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FFreq.Caption := TranslateTextEx('Pattern');
  FFreq.Align := alLeft;
  {$ENDIF}
  FFreq.Parent := FPatternLayout;

  {$IFDEF FMXMOBILE}
  FFreq.Width := 175;
  FFreq.Columns := 2;
  {$ELSE}
  FFreq.Width := 150;
  {$ENDIF}

  FRange := TTMSFNCPlannerItemEditorRadioGroup.Create(FTabRecurrencySettings);
  FRange.Name := 'rgRange';
  {$IFDEF FMXLIB}
  FRange.Text := TranslateTextEx('Range');
  FRange.Align := TAlignLayout.Client;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FRange.Caption := TranslateTextEx('Range');
  FRange.Align := alClient;
  {$IFDEF VCLLIB}
  FRange.AlignWithMargins := True;
  {$ENDIF}
  {$ENDIF}
  FRange.Parent := FTabRecurrencySettings;
  {$IFNDEF LCLLIB}
  FRange.Margins.Left := 10;
  FRange.Margins.Bottom := 10;
  FRange.Margins.Right := 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FRange.BorderSpacing.Left := 10;
  FRange.BorderSpacing.Bottom := 10;
  FRange.BorderSpacing.Right := 10;
  {$ENDIF}

  FDate := TTMSFNCPlannerDateEdit.Create(FRange);
  FDate.Width := 100;
  {$IFDEF FMXLIB}
  FDate.Position.X := 100;
  FDate.Position.Y := 75;
  {$ENDIF}
  {$IFDEF VCLLIB}
  FDate.Left := 100;
  FDate.Top := 60;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FDate.Left := 100;
  FDate.Top := 50;
  {$ENDIF}
  FDate.Parent := FRange;

  FOccur := TEdit.Create(FRange);
  {$IFDEF FMXLIB}
  FOccur.Position.X := 100;
  FOccur.Position.Y := 70 - FOccur.Height;
  {$ENDIF}
  {$IFDEF VCLLIB}
  FOccur.Left := 100;
  FOccur.Top := 55 - FOccur.Height;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FOccur.Left := 100;
  FOccur.Top := 45 - FOccur.Height;
  {$ENDIF}
  FOccur.Parent := FRange;

  FOccurLabel := TLabel.Create(FRange);
  FOccurLabel.Width := 100;
  FOccurLabel.WordWrap := False;
  FOccurLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FOccurLabel.Position.X := FOccur.Position.X + FOccur.Width + 5;
  FOccurLabel.Text := TranslateTextEx('Occurences');
  FOccurLabel.Position.Y := Int(FOccur.Position.Y + (FOccur.Height - FOccurLabel.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FOccurLabel.Left := FOccur.Left + FOccur.Width + 5;
  FOccurLabel.Caption := TranslateTextEx('Occurences');
  FOccurLabel.Top := FOccur.Top + (FOccur.Height - FOccurLabel.Height) div 2;
  {$ENDIF}
  FOccurLabel.Parent := FRange;

  FPatternDetails := TGroupBox.Create(FPatternLayout);
  {$IFDEF FMXLIB}
  FPatternDetails.Align := TAlignLayout.Client;
  FPatternDetails.Text := TranslateTextEx('Pattern details');
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FPatternDetails.AlignWithMargins := True;
  {$ENDIF}
  FPatternDetails.Align := alClient;
  FPatternDetails.Caption  := TranslateTextEx('Pattern details');
  {$ENDIF}
  FPatternDetails.Parent := FPatternLayout;
  {$IFNDEF LCLLIB}
  FPatternDetails.Margins.Left := 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FPatternDetails.BorderSpacing.Left := 10;
  {$ENDIF}

  FIntervalLabel := TLabel.Create(FPatternDetails);
  FIntervalLabel.Width := 100;
  FIntervalLabel.WordWrap := False;
  FIntervalLabel.AutoSize := True;
  {$IFDEF FMXLIB}
  FIntervalLabel.Position.X := 10;
  FIntervalLabel.Position.Y := 20;
  FIntervalLabel.Text := TranslateTextEx('Interval');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FIntervalLabel.Left := 10;
  {$IFDEF VCLLIB}
  FIntervalLabel.Top := 20;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FIntervalLabel.Top := 10;
  {$ENDIF}
  FIntervalLabel.Caption := TranslateTextEx('Interval');
  {$ENDIF}
  FIntervalLabel.Parent := FPatternDetails;

  FInterval := TEdit.Create(FPatternDetails);
  {$IFDEF FMXLIB}
  FInterval.Position.Y := FIntervalLabel.Position.Y + Int((FIntervalLabel.Height - FInterval.Height) / 2);
  FInterval.Position.X := FIntervalLabel.Position.X + FIntervalLabel.Width + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FInterval.Top := FIntervalLabel.Top + (FIntervalLabel.Height - FInterval.Height) div 2;
  FInterval.Left := FIntervalLabel.Left + FIntervalLabel.Width + 10;
  {$ENDIF}
  FInterval.Parent := FPatternDetails;

  FDayL := TTMSFNCPlannerItemEditorLayout.Create(FPatternDetails);
  {$IFDEF FMXLIB}
  FDayL.Align := TAlignLayout.Client;
  FDayL.Margins.Top := FInterval.Position.Y + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FDayL.AlignWithMargins := True;
  {$ENDIF}
  FDayL.Align := alClient;
  {$IFDEF VCLLIB}
  FDayL.Margins.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FDayL.BorderSpacing.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FDayL.Margins.Left := 5;
  FDayl.Margins.Right := 5;
  FDayL.Margins.Bottom := 5;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FDayL.BorderSpacing.Left := 5;
  FDayl.BorderSpacing.Right := 5;
  FDayL.BorderSpacing.Bottom := 5;
  {$ENDIF}
  FDayL.Parent := FPatternDetails;
  FDayL.Visible := False;

  FMon := TCheckBox.Create(FDayL);
  FMon.Parent := FDayL;
  {$IFDEF FMXLIB}
  FMon.Text := TranslateTextEx('Mon');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FMon.Caption := TranslateTextEx('Mon');
  {$ENDIF}
  FMon.Width := 50;

  FTue := TCheckBox.Create(FDayL);
  FTue.Parent := FDayL;
  {$IFDEF FMXLIB}
  FTue.Position.X := FMon.Position.X + FMon.Width + 5;
  FTue.Position.Y := FMon.Position.Y;
  FTue.Text := TranslateTextEx('Tue');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTue.Left := FMon.Left + FMon.Width + 5;
  FTue.Top := FMon.Top;
  FTue.Caption := TranslateTextEx('Tue');
  {$ENDIF}
  FTue.Width := 50;

  FWed := TCheckBox.Create(FDayL);
  FWed.Parent := FDayL;
  {$IFDEF FMXLIB}
  FWed.Position.X := FTue.Position.X + FTue.Width + 5;
  FWed.Position.Y := FMon.Position.Y;
  FWed.Text := TranslateTextEx('Wed');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FWed.Left := FTue.Left + FTue.Width + 5;
  FWed.Top := FMon.Top;
  FWed.Caption := TranslateTextEx('Wed');
  {$ENDIF}
  FWed.Width := 50;

  FThu := TCheckBox.Create(FDayL);
  FThu.Parent := FDayL;
  {$IFDEF FMXLIB}
  FThu.Position.X := FWed.Position.X + FWed.Width + 5;
  FThu.Position.Y := FMon.Position.Y;
  FThu.Text := TranslateTextEx('Thu');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FThu.Left := FWed.Left + FWed.Width + 5;
  FThu.Top := FMon.Top;
  FThu.Caption := TranslateTextEx('Thu');
  {$ENDIF}
  FThu.Width := 50;

  FFri := TCheckBox.Create(FDayL);
  FFri.Parent := FDayL;
  {$IFDEF FMXLIB}
  FFri.Text := TranslateTextEx('Fri');
  FFri.Position.Y := FMon.Position.Y + FMon.Height + 5;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FFri.Caption := TranslateTextEx('Fri');
  FFri.Top := FMon.Top + FMon.Height + 5;
  {$ENDIF}
  FFri.Width := 50;

  FSat := TCheckBox.Create(FDayL);
  FSat.Parent := FDayL;
  {$IFDEF FMXLIB}
  FSat.Position.X := FFri.Position.X + FFri.Width + 5;
  FSat.Position.Y := FFri.Position.Y;
  FSat.Text := TranslateTextEx('Sat');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FSat.Left := FFri.Left + FFri.Width + 5;
  FSat.Top := FFri.Top;
  FSat.Caption := TranslateTextEx('Sat');
  {$ENDIF}
  FSat.Width := 50;

  FSun := TCheckBox.Create(FDayL);
  FSun.Parent := FDayL;
  {$IFDEF FMXLIB}
  FSun.Position.X := FSat.Position.X + FSat.Width + 5;
  FSun.Position.Y := FFri.Position.Y;
  FSun.Text := TranslateTextEx('Sun');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FSun.Left := FSat.Left + FSat.Width + 5;
  FSun.Top := FFri.Top;
  FSun.Caption := TranslateTextEx('Sun');
  {$ENDIF}
  FSun.Width := 50;

  FDayWeekL := TTMSFNCPlannerItemEditorLayout.Create(FPatternDetails);
  {$IFDEF FMXLIB}
  FDayWeekL.Align := TAlignLayout.Client;
  FDayWeekL.Margins.Top := FInterval.Position.Y + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FDayWeekL.AlignWithMargins := True;
  {$ENDIF}
  FDayWeekL.Align := alClient;
  {$IFDEF VCLLIB}
  FDayWeekL.Margins.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FDayWeekL.BorderSpacing.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FDayWeekL.Margins.Left := 5;
  FDayWeekL.Margins.Right := 5;
  FDayWeekL.Margins.Bottom := 5;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FDayWeekL.BorderSpacing.Left := 5;
  FDayWeekL.BorderSpacing.Right := 5;
  FDayWeekL.BorderSpacing.Bottom := 5;
  {$ENDIF}
  FDayWeekL.Parent := FPatternDetails;
  FDayWeekL.Visible := False;

  FDay := TRadioButton.Create(FDayWeekL);
  FDay.Width := 200;
  FDay.Parent := FDayWeekL;
  {$IFDEF FMXLIB}
  FDay.GroupName := '1';
  FDay.Text := TranslateTextEx('Every day');
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FDay.GroupName := '1';
  FDay.Caption := TranslateTextEx('Every day');
  {$ENDIF}

  FWeekDay := TRadioButton.Create(FDayWeekL);
  FWeekDay.Width := 200;
  FWeekDay.Parent := FDayWeekL;
  {$IFDEF FMXLIB}
  FWeekDay.GroupName := '1';
  FWeekDay.Text := TranslateTextEx('Every weekday');
  FWeekDay.Position.Y := FDay.Position.Y + FDay.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FWeekDay.GroupName := '1';
  FWeekDay.Caption := TranslateTextEx('Every weekday');
  FWeekDay.Top := FDay.Top + FDay.Height + 10;
  {$ENDIF}

  FWeekL := TTMSFNCPlannerItemEditorLayout.Create(FPatternDetails);
  {$IFDEF FMXLIB}
  FWeekL.Align := TAlignLayout.Client;
  FWeekL.Margins.Top := FInterval.Position.Y + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FWeekL.AlignWithMargins := True;
  {$ENDIF}
  FWeekL.Align := alClient;
  {$IFDEF VCLLIB}
  FWeekL.Margins.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FWeekL.BorderSpacing.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FWeekL.Margins.Left := 5;
  FWeekL.Margins.Right := 5;
  FWeekL.Margins.Bottom := 5;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FWeekL.BorderSpacing.Left := 5;
  FWeekL.BorderSpacing.Right := 5;
  FWeekL.BorderSpacing.Bottom := 5;
  {$ENDIF}
  FWeekL.Parent := FPatternDetails;
  FWeekL.Visible := False;

  FMonthDay := TRadioButton.Create(FWeekL);
  FMonthDay.Width := 200;
  FMonthDay.Parent := FWeekL;
  {$IFDEF FMXLIB}
  FMonthDay.GroupName := '2';
  FMonthDay.Text := TranslateTextEx('Every same day of the month');
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FMonthDay.GroupName := '2';
  FMonthDay.Caption := TranslateTextEx('Every same day of the month');
  {$ENDIF}

  FSpecialDay := TRadioButton.Create(FWeekL);
  FSpecialDay.Width := 200;
  FSpecialDay.Parent := FWeekL;
  {$IFDEF FMXLIB}
  FSpecialDay.GroupName := '2';
  FSpecialDay.Text := TranslateTextEx('Every');
  FSpecialDay.Position.Y := FMonthDay.Position.Y + FMonthDay.Height + 5;
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FSpecialDay.GroupName := '2';
  FSpecialDay.Caption := TranslateTextEx('Every');
  FSpecialDay.Top := FMonthDay.Top + FMonthDay.Height + 5;
  {$ENDIF}
  FSpecialDay.Width := 50;

  FCWeekNum := TComboBox.Create(FWeekL);
  FCWeekNum.Width := 75;
  FCWeekNum.Parent := FWeekL;
  {$IFDEF FMXLIB}
  FCWeekNum.Position.X := FSpecialDay.Position.X + FSpecialDay.Width + 10;
  FCWeekNum.Position.Y := FSpecialDay.Position.Y + Int((FSpecialDay.Height - FCWeekNum.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FCWeekNum.Left := FSpecialDay.Left + FSpecialDay.Width + 10;
  FCWeekNum.Top := FSpecialDay.Top + (FSpecialDay.Height - FCWeekNum.Height) div 2;
  {$ENDIF}

  FCDay := TComboBox.Create(FWeekL);
  FCDay.Parent := FWeekL;
  FCDay.Width := 100;
  {$IFDEF FMXLIB}
  FCDay.Position.X := FCWeekNum.Position.X + FCWeekNum.Width + 10;
  FCDay.Position.Y := FSpecialDay.Position.Y + Int((FSpecialDay.Height - FCDay.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FCDay.Left := FCWeekNum.Left + FCWeekNum.Width + 10;
  FCDay.Top := FSpecialDay.Top + (FSpecialDay.Height - FCDay.Height) div 2;
  {$ENDIF}

  FYearL := TTMSFNCPlannerItemEditorLayout.Create(FPatternDetails);
  {$IFDEF FMXLIB}
  FYearL.Align := TAlignLayout.Client;
  FYearL.Margins.Top := FInterval.Position.Y + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FYearL.AlignWithMargins := True;
  {$ENDIF}
  FYearL.Align := alClient;
  {$IFDEF VCLLIB}
  FYearL.Margins.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FYearL.BorderSpacing.Top := FInterval.Top + FInterval.Height + 10;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FYearL.Margins.Left := 5;
  FYearL.Margins.Right := 5;
  FYearL.Margins.Bottom := 5;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FYearL.BorderSpacing.Left := 5;
  FYearL.BorderSpacing.Right := 5;
  FYearL.BorderSpacing.Bottom := 5;
  {$ENDIF}
  FYearL.Parent := FPatternDetails;
  FYearL.Visible := False;

  FYearDay := TRadioButton.Create(FYearL);
  FYearDay.Width := 200;
  FYearDay.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYearDay.GroupName := '3';
  FYearDay.Text := TranslateTextEx('Every same day of the year');
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FYearDay.GroupName := '3';
  FYearDay.Caption := TranslateTextEx('Every same day of the year');
  {$ENDIF}

  FYearSpecialDay := TRadioButton.Create(FYearL);
  FYearSpecialDay.Width := 200;
  FYearSpecialDay.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYearSpecialDay.GroupName := '3';
  FYearSpecialDay.Text := TranslateTextEx('Every');
  FYearSpecialDay.Position.Y := FYearDay.Position.Y + FYearDay.Height + 5;
  {$ENDIF}
  {$IFDEF CMNLIB}
//  FYearSpecialDay.GroupName := '3';
  FYearSpecialDay.Caption := TranslateTextEx('Every');
  FYearSpecialDay.Top := FYearDay.Top + FYearDay.Height + 5;
  {$ENDIF}
  FYearSpecialDay.Width := 50;

  FCYearWeekNum := TComboBox.Create(FYearL);
  FCYearWeekNum.Width := 75;
  FCYearWeekNum.Parent := FYearL;
  {$IFDEF FMXLIB}
  FCYearWeekNum.Position.X := FSpecialDay.Position.X + FSpecialDay.Width + 10;
  FCYearWeekNum.Position.Y := FSpecialDay.Position.Y + Int((FSpecialDay.Height - FCYearWeekNum.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FCYearWeekNum.Left := FSpecialDay.Left + FSpecialDay.Width + 10;
  FCYearWeekNum.Top := FSpecialDay.Top + (FSpecialDay.Height - FCYearWeekNum.Height) div 2;
  {$ENDIF}

  FCYearDay := TComboBox.Create(FYearL);
  FCYearDay.Parent := FYearL;
  FCYearDay.Width := 100;
  {$IFDEF FMXLIB}
  FCYearDay.Position.X := FCWeekNum.Position.X + FCWeekNum.Width + 10;
  FCYearDay.Position.Y := FYearSpecialDay.Position.Y + Int((FYearSpecialDay.Height - FCYearDay.Height) / 2);
  {$ENDIF}
  {$IFDEF CMNLIB}
  FCYearDay.Left := FCWeekNum.Left + FCWeekNum.Width + 10;
  FCYearDay.Top := FYearSpecialDay.Top + (FYearSpecialDay.Height - FCYearDay.Height) div 2;
  {$ENDIF}

  FYCK1 := TCheckBox.Create(FYearL);
  FYCK1.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK1.Text := 'J';
  FYCk1.Position.Y := FCYearDay.Position.Y + FCYearDay.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK1.Caption := 'J';
  FYCk1.Top := FCYearDay.Top + FCYearDay.Height + 10;
  {$ENDIF}
  FYCk1.Width := 35;

  FYCK2 := TCheckBox.Create(FYearL);
  FYCK2.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK2.Text := 'F';
  FYCk2.Position.X := FYCK1.Position.X + FYCK1.Width + 5;
  FYCk2.Position.Y := FYCK1.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK2.Caption := 'F';
  FYCk2.Left := FYCK1.Left + FYCK1.Width + 5;
  FYCk2.Top := FYCK1.Top;
  {$ENDIF}
  FYCk2.Width := 35;

  FYCK3 := TCheckBox.Create(FYearL);
  FYCK3.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK3.Text := 'M';
  FYCk3.Position.X := FYCK2.Position.X + FYCK2.Width + 5;
  FYCk3.Position.Y := FYCK2.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK3.Caption := 'M';
  FYCk3.Left := FYCK2.Left + FYCK2.Width + 5;
  FYCk3.Top := FYCK2.Top;
  {$ENDIF}
  FYCk3.Width := 35;

  FYCK4 := TCheckBox.Create(FYearL);
  FYCK4.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK4.Text := 'A';
  FYCk4.Position.X := FYCK3.Position.X + FYCK3.Width + 5;
  FYCk4.Position.Y := FYCK3.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK4.Caption := 'A';
  FYCk4.Left := FYCK3.Left + FYCK3.Width + 5;
  FYCk4.Top := FYCK3.Top;
  {$ENDIF}
  FYCk4.Width := 35;

  FYCK5 := TCheckBox.Create(FYearL);
  FYCK5.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK5.Text := 'M';
  FYCk5.Position.X := FYCK4.Position.X + FYCK4.Width + 5;
  FYCk5.Position.Y := FYCK4.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK5.Caption := 'M';
  FYCk5.Left := FYCK4.Left + FYCK4.Width + 5;
  FYCk5.Top := FYCK4.Top;
  {$ENDIF}
  FYCk5.Width := 35;

  FYCK6 := TCheckBox.Create(FYearL);
  FYCK6.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK6.Text := 'J';
  FYCk6.Position.X := FYCK5.Position.X + FYCK5.Width + 5;
  FYCk6.Position.Y := FYCK5.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK6.Caption := 'J';
  FYCk6.Left := FYCK5.Left + FYCK5.Width + 5;
  FYCk6.Top := FYCK5.Top;
  {$ENDIF}
  FYCk6.Width := 35;

  FYCK7 := TCheckBox.Create(FYearL);
  FYCK7.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK7.Text := 'J';
  FYCk7.Position.Y := FYCK1.Position.Y + FYCK1.Height + 5;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK7.Caption := 'J';
  FYCk7.Top := FYCK1.Top + FYCK1.Height + 5;
  {$ENDIF}
  FYCk7.Width := 35;

  FYCK8 := TCheckBox.Create(FYearL);
  FYCK8.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK8.Text := 'A';
  FYCk8.Position.X := FYCK7.Position.X + FYCK7.Width + 5;
  FYCk8.Position.Y := FYCK7.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK8.Caption := 'A';
  FYCk8.Left := FYCK7.Left + FYCK7.Width + 5;
  FYCk8.Top := FYCK7.Top;
  {$ENDIF}
  FYCk8.Width := 35;

  FYCK9 := TCheckBox.Create(FYearL);
  FYCK9.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK9.Text := 'S';
  FYCk9.Position.X := FYCK8.Position.X + FYCK8.Width + 5;
  FYCk9.Position.Y := FYCK8.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK9.Caption := 'S';
  FYCk9.Left := FYCK8.Left + FYCK8.Width + 5;
  FYCk9.Top := FYCK8.Top;
  {$ENDIF}
  FYCk9.Width := 35;

  FYCK10 := TCheckBox.Create(FYearL);
  FYCK10.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK10.Text := 'O';
  FYCk10.Position.X := FYCK9.Position.X + FYCK9.Width + 5;
  FYCk10.Position.Y := FYCK9.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK10.Caption := 'O';
  FYCk10.Left := FYCK9.Left + FYCK9.Width + 5;
  FYCk10.Top := FYCK9.Top;
  {$ENDIF}
  FYCk10.Width := 35;

  FYCK11 := TCheckBox.Create(FYearL);
  FYCK11.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK11.Text := 'N';
  FYCk11.Position.X := FYCK10.Position.X + FYCK10.Width + 5;
  FYCk11.Position.Y := FYCK10.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK11.Caption := 'N';
  FYCk11.Left := FYCK10.Left + FYCK10.Width + 5;
  FYCk11.Top := FYCK10.Top;
  {$ENDIF}
  FYCk11.Width := 35;

  FYCK12 := TCheckBox.Create(FYearL);
  FYCK12.Parent := FYearL;
  {$IFDEF FMXLIB}
  FYCK12.Text := 'D';
  FYCk12.Position.X := FYCK11.Position.X + FYCK11.Width + 5;
  FYCk12.Position.Y := FYCK11.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FYCK12.Caption := 'D';
  FYCk12.Left := FYCK11.Left + FYCK11.Width + 5;
  FYCk12.Top := FYCK11.Top;
  {$ENDIF}
  FYCk12.Width := 35;

  FExsd := TTMSFNCPlannerDateEdit.Create(FTabRecurrencyExceptions);
  FExsd.Width := 100;
  FExsd.Parent := FTabRecurrencyExceptions;
  {$IFDEF FMXLIB}
  FExsd.Position.X := 10;
  FExsd.Position.Y := 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FExsd.Left := 10;
  FExsd.Top := 10;
  {$ENDIF}

  FExst := TTMSFNCPlannerTimeEdit.Create(FTabRecurrencyExceptions);
  FExst.ShowCheckBox := True;
  FExst.Width := 100;
  FExst.Parent := FTabRecurrencyExceptions;
  {$IFDEF FMXLIB}
  FExst.Position.X := FExsd.Position.X + FExsd.Width + 10;
  FExst.Position.Y := FExsd.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FExst.Kind := dtkTime;
  FExst.Left := FExsd.Left + FExsd.Width + 10;
  FExst.Top := FExsd.Top;
  {$ENDIF}

  FExed := TTMSFNCPlannerDateEdit.Create(FTabRecurrencyExceptions);
  FExed.Width := 100;
  FExed.Parent := FTabRecurrencyExceptions;
  {$IFDEF FMXLIB}
  FExed.Position.X := 10;
  FExed.Position.Y := FExsd.Position.Y + FExsd.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FExed.Left := 10;
  FExed.Top := FExsd.Top + FExsd.Height + 10;
  {$ENDIF}

  FExet := TTMSFNCPlannerTimeEdit.Create(FTabRecurrencyExceptions);
  FExet.ShowCheckBox := True;
  FExet.Width := 100;
  FExet.Parent := FTabRecurrencyExceptions;
  {$IFDEF FMXLIB}
  FExet.Position.X := FExed.Position.X + FExed.Width + 10;
  FExet.Position.Y := FExed.Position.Y;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FExet.Kind := dtkTime;
  FExet.Left := FExed.Left + FExed.Width + 10;
  FExet.Top := FExed.Top;
  {$ENDIF}

  FExList := TListBox.Create(FTabRecurrencyExceptions);
  FExList.Parent := FTabRecurrencyExceptions;
  {$IFDEF FMXLIB}
  FExList.Align := TAlignLayout.Client;
  FExList.Margins.Top := FExet.Position.Y + FExet.Height + 10;
  {$ENDIF}
  {$IFDEF CMNLIB}
  {$IFDEF VCLLIB}
  FExList.AlignWithMargins := True;
  {$ENDIF}
  FExList.Align := alClient;
  {$IFDEF VCLLIB}
  FExList.Margins.Top := FExet.Top + FExet.Height + 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FExList.BorderSpacing.Top := FExet.Top + FExet.Height + 10;
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LCLLIB}
  FExList.Margins.Left := 10;
  FExList.Margins.Right := 100;
  FExList.Margins.Bottom := 10;
  {$ENDIF}
  {$IFDEF LCLLIB}
  FExList.BorderSpacing.Left := 10;
  FExList.BorderSpacing.Right := 100;
  FExList.BorderSpacing.Bottom := 10;
  {$ENDIF}

  FBtnAdd := TButton.Create(FTabRecurrencyExceptions);
  {$IFDEF FMXLIB}
  FBtnAdd.Position.X := FExList.Position.X + 350;
  FBtnAdd.Position.Y := FExList.Position.Y;
  FBtnAdd.Text := TranslateTextEx('Add');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FBtnAdd.Left := FExList.Left + 350;
  FBtnAdd.Top := 75;
  FBtnAdd.Caption := TranslateTextEx('Add');
  {$ENDIF}
  FBtnAdd.Parent := FTabRecurrencyExceptions;
  FBtnAdd.OnClick := BtnAddClicked;

  FBtnDelete := TButton.Create(FTabRecurrencyExceptions);
  {$IFDEF FMXLIB}
  FBtnDelete.Position.X := FBtnAdd.Position.X;
  FBtnDelete.Position.Y := FBtnAdd.Position.Y + FBtnAdd.Height + 10;
  FBtnDelete.Text := TranslateTextEx('Delete');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FBtnDelete.Left := FBtnAdd.Left;
  FBtnDelete.Top := FBtnAdd.Top + FBtnAdd.Height + 10;
  FBtnDelete.Caption := TranslateTextEx('Delete');
  {$ENDIF}
  FBtnDelete.Parent := FTabRecurrencyExceptions;
  FBtnDelete.OnClick := BtnDeleteClicked;

  FBtnClear := TButton.Create(FTabRecurrencyExceptions);
  {$IFDEF FMXLIB}
  FBtnClear.Position.X := FBtnDelete.Position.X;
  FBtnClear.Position.Y := FBtnDelete.Position.Y + FBtnDelete.Height + 10;
  FBtnClear.Text := TranslateTextEx('Clear');
  {$ENDIF}
  {$IFDEF CMNLIB}
  FBtnClear.Left := FBtnDelete.Left;
  FBtnClear.Top := FBtnDelete.Top + FBtnDelete.Height + 10;
  FBtnClear.Caption := TranslateTextEx('Clear');
  {$ENDIF}
  FBtnClear.Parent := FTabRecurrencyExceptions;
  FBtnClear.OnClick := BtnClearClicked;

  FCustomContentPanelCreated := True;
end;

function TTMSPlannerCustomItemEditor_VCL.CreateInstance: TTMSFNCPlannerCustomItemEditor;
begin
  Result := TTMSPlannerCustomItemEditor_VCL.Create(nil);
end;

procedure TTMSPlannerCustomItemEditor_VCL.BtnAddClicked(Sender: TObject);
var
  ed: string;
begin
  {$IFDEF FMXLIB}
  if FExst.IsChecked and FExet.IsChecked then
  {$ENDIF}
  {$IFDEF CMNLIB}
  if FExst.Checked and FExet.Checked then
  {$ENDIF}
    ed := DateToStr(FExsd.Date)+ ' - ' + TimeToStr(FExst.Time) + ' to ' + DateToStr(FExed.date)+ ' - ' + TimeToStr(FExet.Time)
  else
    ed := DateToStr(Fexsd.Date)+ ' to ' + DateToStr(FExed.date);

  if FExList.Items.IndexOf(ed) = -1 then
  begin
    FExList.Items.Add(ed);
    with FExDates.Add do
    begin
      {$IFDEF FMXLIB}
      if FExst.IsChecked and FExet.IsChecked then
      {$ENDIF}
      {$IFDEF CMNLIB}
      if FExst.Checked and FExet.Checked then
      {$ENDIF}
      begin
        StartDate := Int(FExsd.Date) + Frac(FExst.Time);
        EndDate := Int(FExed.Date) + Frac(FExet.Time);
      end
      else
      begin
        StartDate := int(FExsd.Date);
        EndDate := int(FExed.Date);
      end;
    end;
  end;
end;

procedure TTMSPlannerCustomItemEditor_VCL.BtnClearClicked(Sender: TObject);
begin
  FExList.Items.Clear;
  FExDates.Clear;
end;

procedure TTMSPlannerCustomItemEditor_VCL.BtnDeleteClicked(Sender: TObject);
var
  idx: Integer;
begin
  idx := FExList.ItemIndex;
  if idx >= 0 then
  begin
    FExList.Items.Delete(idx);
    FExDates.Delete(idx);
  end;
end;

constructor TTMSPlannerCustomItemEditor_VCL.Create(AOwner: TComponent);
begin
  inherited;
  FExDates := TTMSFNCRecurrencyDateItems.Create;
  FContentPanelSet := False;
end;

function TTMSPlannerCustomItemEditor_VCL.CustomContentPanel: TTMSFNCPlannerItemEditorRecurrencyContentPanel;
begin
  CreateCustomContentPanel;
  FContentPanelSet := True;
  Result.ContentPanel := FContentPanel;
  Result.TabControl := FTabControl;
  Result.TabControlRecurrency := FTabControlRecurrency;
  Result.TabGeneral := FTabGeneral;
  Result.StartTimeLabel := FStartTimeLabel;
  Result.EndTimeLabel := FEndTimeLabel;
  Result.StartTimeEdit := FStartTimeEdit;
  Result.EndTimeEdit := FEndTimeEdit;
  Result.StartDateEdit := FStartDateEdit;
  Result.EndDateEdit := FEndDateEdit;
  Result.ResourceLabel := FResourceLabel;
  Result.ResourceComboBox := FResourcesComboBox;
  Result.TitleLabel := FTitleLabel;
  Result.TextLabel := FTextLabel;
  Result.TitleEdit := FTitleEdit;
  Result.TextMemo := FTextMemo;
  Result.TabRecurrency := FTabRecurrency;
  Result.TabRecurrencySettings := FTabRecurrencySettings;
  Result.TabRecurrencyExceptions := FTabRecurrencyExceptions;
  Result.PatternLayout := FPatternLayout;
  Result.Frequency := FFreq;
  Result.Range := FRange;
  Result.PatternDetails := FPatternDetails;
  Result.Date := FDate;
  Result.Occur := FOccur;
  Result.OccurLabel := FOccurLabel;
  Result.Interval := FInterval;
  Result.IntervalLabel := FIntervalLabel;
  Result.DayLayout := FDayL;
  Result.DayWeekLayout := FDayWeekL;
  Result.Day := FDay;
  Result.WeekLayout := FWeekL;
  Result.YearLayout := FYearL;
  Result.Mon := FMon;
  Result.Tue := FTue;
  Result.Wed := FWed;
  Result.Thu := FThu;
  Result.Fri := FFri;
  Result.Sat := FSat;
  Result.Sun := FSun;
  Result.WeekDay := FWeekDay;
  Result.MonthDay := FMonthDay;
  Result.YearDay :=  FYearDay;
  Result.SpecialDay := FSpecialDay;
  Result.YearSpecialDay := FYearSpecialDay;
  Result.ComboWeekNum := FCWeekNum;
  Result.ComboDay := FCDay;
  Result.ComboYearWeekNum := FCYearWeekNum;
  Result.ComboYearDay := FCYearDay;
  Result.YearCheck1 := FYCK1;
  Result.YearCheck2 := FYCK2;
  Result.YearCheck3 := FYCK3;
  Result.YearCheck4 := FYCK4;
  Result.YearCheck5 := FYCK5;
  Result.YearCheck6 := FYCK6;
  Result.YearCheck7 := FYCK7;
  Result.YearCheck8 := FYCK8;
  Result.YearCheck9 := FYCK9;
  Result.YearCheck10 := FYCK10;
  Result.YearCheck11 := FYCK11;
  Result.YearCheck12 := FYCK12;
  Result.ExceptionStartDate := FExsd;
  Result.ExceptionEndDate := FExed;
  Result.ExceptionStartTime := FExst;
  Result.ExceptionEndTime := FExet;
  Result.ExceptionList := FExList;
  Result.ExceptionAdd := FBtnAdd;
  Result.ExceptionDelete := FBtnDelete;
  Result.ExceptionClear := FBtnClear;
end;

procedure TTMSPlannerCustomItemEditor_VCL.CustomContentPanelToItem(
  AContentPanel: TTMSFNCPlannerEditingDialogContentPanel; AItem: TTMSFNCPlannerItem);
var
  sta, ste: TDateTime;
  t, n: String;
  res: Integer;
  p: TTMSFNCCustomPlannerOpen;
begin
  if not Assigned(Planner) then
    Exit;

  p := TTMSFNCCustomPlannerOpen(Planner);

  if Assigned(AItem) then
  begin
    sta := Int(Now);
    if Assigned(FStartDateEdit) then
      sta := Int(FStartDateEdit.Date);

    if Assigned(FStartTimeEdit) then
      sta := sta + Frac(FStartTimeEdit.Time);

    ste := Int(Now);
    if Assigned(FEndDateEdit) then
      ste := Int(FEndDateEdit.Date);

    if Assigned(FEndTimeEdit) then
      ste := ste + Frac(FEndTimeEdit.Time);

    t := p.DefaultItem.Title;
    if Assigned(FTitleEdit) then
      t := FTitleEdit.Text;

    n := p.DefaultItem.Text;
    if Assigned(FTextMemo) then
      n := FTextMemo.Text;

    res := FInsertResource;
    if Assigned(FResourcesComboBox) then
      res := FResourcesComboBox.ItemIndex;

    AItem.StartTime := sta;
    AItem.EndTime := ste;
    AItem.Title := t;
    AItem.Text := n;
    AItem.Resource := res;

    RecurrencyToItem(AItem);
  end;
end;

destructor TTMSPlannerCustomItemEditor_VCL.Destroy;
begin
  FExDates.Free;
  if not FContentPanelSet then
    FContentPanel.Free;
  inherited;
end;

procedure TTMSPlannerCustomItemEditor_VCL.GetCustomContentPanel(
  AItem: TTMSFNCPlannerItem; var AContentPanel: TTMSFNCPlannerEditingDialogContentPanel);
begin
  AContentPanel := FContentPanel;
  FContentPanelSet := True;
end;

function TTMSPlannerCustomItemEditor_VCL.GetRecurrency: String;
var
  it: TTMSFNCPlannerItem;
begin
  it := TTMSFNCPlannerItem.Create(nil);
  RecurrencyToItem(it);
  Result := it.Recurrency;
  it.Free;
end;

function TTMSPlannerCustomItemEditor_VCL.GetVersion: String;
begin
  Result := GetVersionNumber(MAJ_VER, MIN_VER, REL_VER, BLD_VER);
end;

procedure TTMSPlannerCustomItemEditor_VCL.InitializeCustomContentPanel;
begin
  {$IFDEF FMXLIB}
  FTabGeneral.Parent := FTabControl;
  FTabRecurrency.Parent := FTabControl;
  FTabRecurrencySettings.Parent := FTabControlRecurrency;
  FTabRecurrencyExceptions.Parent := FTabControlRecurrency;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FTabGeneral.PageControl := FTabControl;
  FTabRecurrency.PageControl := FTabControl;
  FTabRecurrencySettings.PageControl := FTabControlRecurrency;
  FTabRecurrencyExceptions.PageControl := FTabControlRecurrency;
  {$ENDIF}

  FCYearWeekNum.Items.Add(TranslateTextEx('First'));
  FCYearWeekNum.Items.Add(TranslateTextEx('Second'));
  FCYearWeekNum.Items.Add(TranslateTextEx('Third'));
  FCYearWeekNum.Items.Add(TranslateTextEx('Fourth'));
  FCYearWeekNum.ItemIndex := 0;
  FCDay.Items.Add(TranslateTextEx('Weekday'));
  FCDay.Items.Add(TranslateTextEx('Weekend day'));
  FCDay.Items.Add(TranslateTextEx('Monday'));
  FCDay.Items.Add(TranslateTextEx('Tuesday'));
  FCDay.Items.Add(TranslateTextEx('Wednesday'));
  FCDay.Items.Add(TranslateTextEx('Thursday'));
  FCDay.Items.Add(TranslateTextEx('Friday'));
  FCDay.Items.Add(TranslateTextEx('Saturday'));
  FCDay.Items.Add(TranslateTextEx('Sunday'));
  FCDay.ItemIndex := 0;
  FCWeekNum.Items.Add(TranslateTextEx('First'));
  FCWeekNum.Items.Add(TranslateTextEx('Second'));
  FCWeekNum.Items.Add(TranslateTextEx('Third'));
  FCWeekNum.Items.Add(TranslateTextEx('Fourth'));
  FCWeekNum.ItemIndex := 0;
  FRange.Items.Add(TranslateTextEx('Infinite'));
  FRange.Items.Add(TranslateTextEx('For'));
  FRange.Items.Add(TranslateTextEx('Until date'));
  FOccur.BringToFront;
  FOccurLabel.BringToFront;
  FDate.BringToFront;
  FRange.ItemIndex := 0;
  FFreq.Items.Add(TranslateTextEx('None'));
  FFreq.Items.Add(TranslateTextEx('Hourly'));
  FFreq.Items.Add(TranslateTextEx('Daily'));
  FFreq.Items.Add(TranslateTextEx('Weekly'));
  FFreq.Items.Add(TranslateTextEx('Monthly'));
  FFreq.Items.Add(TranslateTextEx('Yearly'));
  FFreq.ItemIndex := 0;
  FCYearDay.Items.Add(TranslateTextEx('Weekday'));
  FCYearDay.Items.Add(TranslateTextEx('Weekend day'));
  FCYearDay.Items.Add(TranslateTextEx('Monday'));
  FCYearDay.Items.Add(TranslateTextEx('Tuesday'));
  FCYearDay.Items.Add(TranslateTextEx('Wednesday'));
  FCYearDay.Items.Add(TranslateTextEx('Thursday'));
  FCYearDay.Items.Add(TranslateTextEx('Friday'));
  FCYearDay.Items.Add(TranslateTextEx('Saturday'));
  FCYearDay.Items.Add(TranslateTextEx('Sunday'));
  FCYearDay.ItemIndex := 0;
end;

procedure TTMSPlannerCustomItemEditor_VCL.ItemToCustomContentPanel(
  AItem: TTMSFNCPlannerItem; AContentPanel: TTMSFNCPlannerEditingDialogContentPanel);
var
  s: TStringList;
  fr: Boolean;
  dst, det: TDateTime;
  res: Integer;
  t, n: String;
  p: TTMSFNCCustomPlannerOpen;
  recurr: String;
begin
  if not Assigned(Planner) then
    Exit;

  p := TTMSFNCCustomPlannerOpen(Planner);

  if Assigned(AItem) then
  begin
    dst := AItem.StartTime;
    det := AItem.EndTime;
    res := AItem.Resource;
    t := AItem.Title;
    n := AItem.Text;
    recurr := AItem.Recurrency;
  end
  else
  begin
    dst := p.SelectedStartDateTime;
    det := p.SelectedEndDateTime;
    res := p.SelectedResource;
    t := '';
    n := '';
    recurr := '';
  end;

  FInsertResource := res;
  FDialogStartDate := Int(dst);
  FDialogEndDate := Int(det);
  FDialogStartTime := Frac(dst);
  FDialogEndTime := Frac(det);

  if Assigned(FStartDateEdit) then
    FStartDateEdit.Date := FDialogStartDate;

  if Assigned(FEndDateEdit) then
    FEndDateEdit.Date := FDialogEndDate;

  if Assigned(FStartTimeEdit) then
    FStartTimeEdit.Time := FDialogStartTime;

  if Assigned(FEndTimeEdit) then
    FEndTimeEdit.Time := FDialogEndTime;

  if Assigned(FTitleEdit) then
    FTitleEdit.Text := t;

  if Assigned(FTextMemo) then
    FTextMemo.Text := n;

  fr := false;
  if Assigned(AItem) then
    fr := AItem.FixedResource;

  if Assigned(FResourcesComboBox) then
  begin
    FResourcesComboBox.Clear;
    s := p.GetResources;
    FResourcesComboBox.Items.Assign(s);
    s.Free;
    FResourcesComboBox.ItemIndex := p.PositionToResource(res);
    FResourcesComboBox.Visible := p.GetResourcesAvailable and not fr;
  end;

  if Assigned(FResourceLabel) then
    FResourceLabel.Visible := p.GetResourcesAvailable and not fr;

  if Assigned(FTitleLabel) then
  begin
    {$IFDEF FMXLIB}
    if Assigned(FResourceLabel) and FResourceLabel.Visible then
      FTitleLabel.Position.Y := FResourceLabel.Position.Y + FResourceLabel.Height + 15
    else if Assigned(FStartTimeLabel) then
      FTitleLabel.Position.Y := FEndTimeLabel.Position.Y + FEndTimeLabel.Height + 15;

    if Assigned(FTitleEdit) then
      FTitleEdit.Position.Y := FTitleLabel.Position.Y + Int((FTitleLabel.Height - FTitleEdit.Height) / 2);

    if Assigned(FTextLabel) then
      FTextLabel.Position.Y := FTitleLabel.Position.Y + FTitleLabel.Height + 15;

    if Assigned(FTextMemo) then
      FTextMemo.Margins.Top := FTextLabel.Position.Y + FTextLabel.Height + 15;
    {$ENDIF}
    {$IFDEF CMNLIB}
    if Assigned(FResourceLabel) and FResourceLabel.Visible then
      FTitleLabel.Top := FResourceLabel.Top + FResourceLabel.Height + 15
    else if Assigned(FStartTimeLabel) then
      FTitleLabel.Top := FEndTimeLabel.Top + FEndTimeLabel.Height + 15;

    if Assigned(FTitleEdit) then
      FTitleEdit.Top := FTitleLabel.Top + (FTitleLabel.Height - FTitleEdit.Height) div 2;

    if Assigned(FTextLabel) then
      FTextLabel.Top := FTitleLabel.Top + FTitleLabel.Height + 15;

    {$IFDEF VCLLIB}
    if Assigned(FTextMemo) then
      FTextMemo.Margins.Top := FTextLabel.Top + FTextLabel.Height + 15;
    {$ENDIF}

    {$IFDEF LCLLIB}
    if Assigned(FTextMemo) then
      FTextMemo.BorderSpacing.Top := FTextLabel.Top + FTextLabel.Height + 15;
    {$ENDIF}

    {$ENDIF}
  end;

  ItemToRecurrency(recurr);

  {$IFDEF FMXLIB}
  if Assigned(FTabControl) and Assigned(FTabGeneral) then
    FTabControl.ActiveTab := FTabGeneral;

  if Assigned(FTabControlRecurrency) and Assigned(FTabRecurrencySettings) then
    FTabControlRecurrency.ActiveTab := FTabRecurrencySettings;
  {$ENDIF}

  {$IFDEF CMNLIB}
  if Assigned(FTabControl) and Assigned(FTabGeneral) then
    FTabControl.ActivePage := FTabGeneral;

  if Assigned(FTabControlRecurrency) and Assigned(FTabRecurrencySettings) then
    FTabControlRecurrency.ActivePage := FTabRecurrencySettings;
  {$ENDIF}
end;

procedure TTMSPlannerCustomItemEditor_VCL.ItemToRecurrency(
  ARecurrency: String);
var
  rh: TTMSFNCRecurrencyHandler;
  i: Integer;
  dn: Boolean;
  ed: string;
begin
  FExDates.Clear;
  FExList.Clear;
  FDate.Date := Now;
  FRange.ItemIndex := 0;
  FExsd.Date := Now;
  FExed.Date := Now;
  FOccur.Text := '10';
  FInterval.Text := '1';
  {$IFDEF FMXLIB}
  FExst.IsChecked := false;
  FExet.IsChecked := false;
  FMon.IsChecked := false;
  FTue.IsChecked := false;
  FWed.IsChecked := false;
  FThu.IsChecked := false;
  FFri.IsChecked := false;
  FSat.IsChecked := false;
  FSun.IsChecked := false;
  FDay.IsChecked := True;
  FYearDay.IsChecked := True;
  FMonthDay.IsChecked := True;
  FWeekDay.IsChecked := False;
  FSpecialDay.IsChecked := False;
  FYearSpecialDay.IsChecked := False;
  FYCK1.IsChecked := False;
  FYCK2.IsChecked := False;
  FYCK3.IsChecked := False;
  FYCK4.IsChecked := False;
  FYCK5.IsChecked := False;
  FYCK6.IsChecked := False;
  FYCK7.IsChecked := False;
  FYCK8.IsChecked := False;
  FYCK9.IsChecked := False;
  FYCK10.IsChecked := False;
  FYCK11.IsChecked := False;
  FYCK12.IsChecked := False;
  {$ENDIF}
  {$IFDEF CMNLIB}
  FExst.Checked := false;
  FExet.Checked := false;
  FMon.Checked := false;
  FTue.Checked := false;
  FWed.Checked := false;
  FThu.Checked := false;
  FFri.Checked := false;
  FSat.Checked := false;
  FSun.Checked := false;
  FDay.Checked := True;
  FYearDay.Checked := True;
  FMonthDay.Checked := True;
  FWeekDay.Checked := False;
  FSpecialDay.Checked := False;
  FYearSpecialDay.Checked := False;
  FYCK1.Checked := False;
  FYCK2.Checked := False;
  FYCK3.Checked := False;
  FYCK4.Checked := False;
  FYCK5.Checked := False;
  FYCK6.Checked := False;
  FYCK7.Checked := False;
  FYCK8.Checked := False;
  FYCK9.Checked := False;
  FYCK10.Checked := False;
  FYCK11.Checked := False;
  FYCK12.Checked := False;
  {$ENDIF}

  FCWeekNum.ItemIndex := 0;
  FCDay.ItemIndex := 0;
  FCYearWeekNum.ItemIndex := 0;
  FCYearDay.ItemIndex := 0;
  FRecurrency := ARecurrency;

  if FRecurrency = '' then
  begin
    if Assigned(FFreq) then
      FFreq.ItemIndex := 0;

    Exit;
  end;

  rh := TTMSFNCRecurrencyHandler.Create;

  rh.Recurrency := FRecurrency;
  rh.Parse;

  FFreq.ItemIndex := Integer(rh.Frequency) + 1;
  FInterval.Text := IntToStr(rh.Interval);

  FRange.ItemIndex := 0;
  if (rh.RepeatCount = 0) and (rh.RepeatUntil = 0) then
    FRange.ItemIndex := 0;

  if (rh.RepeatCount > 0) and (rh.RepeatUntil = 0) then
    FRange.ItemIndex := 1;

  if (rh.RepeatUntil > 0) then
    FRange.ItemIndex := 2;

  FOccur.Text := IntToStr(rh.RepeatCount);

  if FRange.ItemIndex = 2 then
    FDate.Date := rh.RepeatUntil
  else
    FDate.Date := Now;

  case Integer(rh.Frequency) of
      {$IFDEF FMXLIB}
  1:begin
      if (2 in rh.Days) and (3 in rh.Days) and (4 in rh.Days) and (5 in rh.Days) and (6 in rh.Days) then
      begin
        FWeekDay.IsChecked := True;
        FDay.IsChecked := False;
      end
      else
      begin
        FDay.IsChecked := True;
        FWeekDay.IsChecked := False;
      end;
    end;
  2:begin
      FMon.IsChecked := 2 in rh.Days;
      FTue.IsChecked := 3 in rh.Days;
      FWed.IsChecked := 4 in rh.Days;
      FThu.IsChecked := 5 in rh.Days;
      FFri.IsChecked := 6 in rh.Days;
      FSat.IsChecked := 7 in rh.Days;
      FSun.IsChecked := 1 in rh.Days;
    end;
    {$ENDIF}
    {$IFDEF CMNLIB}
  1:begin
      if (2 in rh.Days) and (3 in rh.Days) and (4 in rh.Days) and (5 in rh.Days) and (6 in rh.Days) then
      begin
        FWeekDay.Checked := True;
        FDay.Checked := False;
      end
      else
      begin
        FDay.Checked := True;
        FWeekDay.Checked := False;
      end;
    end;
  2:begin
      FMon.Checked := 2 in rh.Days;
      FTue.Checked := 3 in rh.Days;
      FWed.Checked := 4 in rh.Days;
      FThu.Checked := 5 in rh.Days;
      FFri.Checked := 6 in rh.Days;
      FSat.Checked := 7 in rh.Days;
      FSun.Checked := 1 in rh.Days;
    end;
    {$ENDIF}
  3:begin
      dn := false;
      for i := 1 to 7 do
        if rh.DayNum[i] <> 0 then
          dn := true;

      if (dn) then
      begin
        {$IFDEF FMXLIB}
        FSpecialDay.IsChecked := True;
        FMonthDay.IsChecked := False;
        {$ENDIF}
        {$IFDEF CMNLIB}
        FSpecialDay.Checked := True;
        FMonthDay.Checked := False;
        {$ENDIF}

        if (2 in rh.Days) and (3 in rh.Days) and (4 in rh.Days) and (5 in rh.Days) and (6 in rh.Days) then
        begin
          FCDay.ItemIndex := 0;
          FCWeekNum.ItemIndex := rh.DayNum[2] - 1;
        end
        else
          if (1 in rh.Days) and (7 in rh.Days) then
          begin
            FCDay.ItemIndex := 1;
            FCWeekNum.ItemIndex := rh.DayNum[1] - 1;
          end
          else
          begin
            if 1 in rh.Days then
            begin
              FCDay.ItemIndex := 8;
              FCWeekNum.ItemIndex := rh.DayNum[1] - 1;
            end;
            if 2 in rh.Days then
            begin
              FCDay.ItemIndex := 2;
              FCWeekNum.ItemIndex := rh.DayNum[2] - 1;
            end;
            if 3 in rh.Days then
            begin
              FCDay.ItemIndex := 3;
              FCWeekNum.ItemIndex := rh.DayNum[3] - 1;
            end;
            if 4 in rh.Days then
            begin
              FCDay.ItemIndex := 4;
              FCWeekNum.ItemIndex := rh.DayNum[4] - 1;
            end;
            if 5 in rh.Days then
            begin
              FCDay.ItemIndex := 5;
              FCWeekNum.ItemIndex := rh.DayNum[5] - 1;
            end;
            if 6 in rh.Days then
            begin
              FCDay.ItemIndex := 6;
              FCWeekNum.ItemIndex := rh.DayNum[6] - 1;
            end;
            if 7 in rh.Days then
            begin
              FCDay.ItemIndex := 7;
              FCWeekNum.ItemIndex := rh.DayNum[7] - 1;
            end;
          end;
      end;
    end;
  4:begin
      dn := false;
      for i := 1 to 7 do
        if rh.DayNum[i] <> 0 then
          dn := true;

      if (dn) then
      begin
        {$IFDEF FMXLIB}
        FYearSpecialDay.IsChecked := true;
        FYearDay.IsChecked := False;
        {$ENDIF}

        {$IFDEF CMNLIB}
        FYearSpecialDay.Checked := true;
        FYearDay.Checked := False;
        {$ENDIF}


        if (2 in rh.Days) and (3 in rh.Days) and (4 in rh.Days) and (5 in rh.Days) and (6 in rh.Days) then
        begin
          FCYearDay.ItemIndex := 0;
          FCYearWeekNum.ItemIndex := rh.DayNum[2] - 1;
        end
        else
          if (1 in rh.Days) and (7 in rh.Days) then
          begin
            FCYearDay.ItemIndex := 1;
            FcYearWeekNum.ItemIndex := rh.DayNum[1] - 1;
          end
          else
          begin
            if 1 in rh.Days then
            begin
              FCYearDay.ItemIndex := 8;
              FCYearWeekNum.ItemIndex := rh.DayNum[1] - 1;
            end;
            if 2 in rh.Days then
            begin
              FCYearDay.ItemIndex := 2;
              FCYearWeekNum.ItemIndex := rh.DayNum[2] - 1;
            end;
            if 3 in rh.Days then
            begin
              FCYearDay.ItemIndex := 3;
              FCYearWeekNum.ItemIndex := rh.DayNum[3] - 1;
            end;
            if 4 in rh.Days then
            begin
              FCYearDay.ItemIndex := 4;
              FCYearWeekNum.ItemIndex := rh.DayNum[4] - 1;
            end;
            if 5 in rh.Days then
            begin
              FCYearDay.ItemIndex := 5;
              FCYearWeekNum.ItemIndex := rh.DayNum[5] - 1;
            end;
            if 6 in rh.Days then
            begin
              FCYearDay.ItemIndex := 6;
              FCYearWeekNum.ItemIndex := rh.DayNum[6] - 1;
            end;
            if 7 in rh.Days then
            begin
              FCYearDay.ItemIndex := 7;
              FCYearWeekNum.ItemIndex := rh.DayNum[7] - 1;
            end;
          end;
      end;

      {$IFDEF FMXLIB}
      FYCK1.IsChecked := rh.Months.HasValue(1);
      FYCK2.IsChecked := rh.Months.HasValue(2);
      FYCK3.IsChecked := rh.Months.HasValue(3);
      FYCK4.IsChecked := rh.Months.HasValue(4);
      FYCK5.IsChecked := rh.Months.HasValue(5);
      FYCK6.IsChecked := rh.Months.HasValue(6);
      FYCK7.IsChecked := rh.Months.HasValue(7);
      FYCK8.IsChecked := rh.Months.HasValue(8);
      FYCK9.IsChecked := rh.Months.HasValue(9);
      FYCK10.IsChecked := rh.Months.HasValue(10);
      FYCK11.IsChecked := rh.Months.HasValue(11);
      FYCK12.IsChecked := rh.Months.HasValue(12);
      {$ENDIF}
      {$IFDEF CMNLIB}
      FYCK1.Checked := rh.Months.HasValue(1);
      FYCK2.Checked := rh.Months.HasValue(2);
      FYCK3.Checked := rh.Months.HasValue(3);
      FYCK4.Checked := rh.Months.HasValue(4);
      FYCK5.Checked := rh.Months.HasValue(5);
      FYCK6.Checked := rh.Months.HasValue(6);
      FYCK7.Checked := rh.Months.HasValue(7);
      FYCK8.Checked := rh.Months.HasValue(8);
      FYCK9.Checked := rh.Months.HasValue(9);
      FYCK10.Checked := rh.Months.HasValue(10);
      FYCK11.Checked := rh.Months.HasValue(11);
      FYCK12.Checked := rh.Months.HasValue(12);
      {$ENDIF}
    end;
  end;

  FExList.Clear;
  FExDates.Clear;
  for i := 1 to rh.ExDates.Count do
  begin
    if (Frac(rh.ExDates.Items[i - 1].StartDate) = 0) and (Frac(rh.ExDates.Items[i - 1].EndDate) = 0) then
      ed := DateToStr(rh.ExDates.Items[i - 1].StartDate)+ ' to ' +DateToStr(rh.ExDates.Items[i - 1].EndDate)
    else
      ed := DateToStr(rh.ExDates.Items[i - 1].StartDate)+ ' - ' + TimeToStr(rh.ExDates.Items[i - 1].StartDate) + ' to ' +DateToStr(rh.ExDates.Items[i - 1].EndDate)+ ' - ' + TimeToStr(rh.ExDates.Items[i - 1].EndDate);

    FExList.Items.Add(ed);
    with FExDates.Add do
    begin
      StartDate := rh.ExDates.Items[i - 1].StartDate;
      EndDate := rh.ExDates.Items[i - 1].EndDate;
    end;
  end;

  rh.Free;

  ShowLayouts(FFreq.ItemIndex);
end;

function TTMSPlannerCustomItemEditor_VCL.Panel: TPanel;
begin
  Result := CustomContentPanel.ContentPanel;
end;

procedure TTMSPlannerCustomItemEditor_VCL.RecurrencyToItem(AItem: TTMSFNCPlannerItem);
var
  rh: TTMSFNCRecurrencyHandler;
  i,{%H-}e: Integer;
  dn: TTMSFNCRecurrencyHandlerDayArray;
begin
  if FFreq.ItemIndex = 0 then
  begin
    FRecurrency := '';
    if Assigned(AItem) then
    begin
      AItem.Recurrency := FRecurrency;
      AItem.Recurrent := False;
    end;
    Exit;
  end;

  rh := TTMSFNCRecurrencyHandler.Create;
  rh.Frequency := TTMSFNCRecurrencyFrequency(FFreq.ItemIndex - 1);

  for i := 1 to FExDates.Count do
  begin
    with rh.ExDates.Add do
    begin
      StartDate := FExDates.Items[i - 1].StartDate;
      EndDate := FExDates.Items[i - 1].EndDate;
    end;
  end;

  e := 0;
  val(FInterval.Text,i,e);
  rh.Interval := i;
  val(FOccur.Text, i, e);

  if FRange.ItemIndex = 0 then
  begin
    rh.RepeatCount := 0;
    rh.RepeatUntil := 0;
  end
  else
    if FRange.ItemIndex = 1 then
      rh.RepeatCount := i
    else
      rh.RepeatCount := 0;

  if FRange.ItemIndex = 2 then
    rh.RepeatUntil := Int(FDate.Date)
  else
    rh.RepeatUntil := 0;

  rh.Days := [];

  if rh.Frequency = rfDaily then
  begin
    {$IFDEF FMXLIB}
    if FWeekDay.IsChecked then
    {$ENDIF}
    {$IFDEF CMNLIB}
    if FWeekDay.Checked then
    {$ENDIF}
      rh.Days := [2,3,4,5,6];
  end;

  if rh.Frequency = rfWeekly then
  begin
    {$IFDEF FMXLIB}
    if FMon.IsChecked then
      rh.Days := rh.Days + [2];
    if FTue.IsChecked then
      rh.Days := rh.Days + [3];
    if FWed.IsChecked then
      rh.Days := rh.Days + [4];
    if FThu.IsChecked then
      rh.Days := rh.Days + [5];
    if FFri.IsChecked then
      rh.Days := rh.Days + [6];
    if FSat.IsChecked then
      rh.Days := rh.Days + [7];
    if FSun.IsChecked then
      rh.Days := rh.Days + [1];
    {$ENDIF}
    {$IFDEF CMNLIB}
    if FMon.Checked then
      rh.Days := rh.Days + [2];
    if FTue.Checked then
      rh.Days := rh.Days + [3];
    if FWed.Checked then
      rh.Days := rh.Days + [4];
    if FThu.Checked then
      rh.Days := rh.Days + [5];
    if FFri.Checked then
      rh.Days := rh.Days + [6];
    if FSat.Checked then
      rh.Days := rh.Days + [7];
    if FSun.Checked then
      rh.Days := rh.Days + [1];
    {$ENDIF}
  end;

  if rh.Frequency = rfMonthly then
  begin
    {$IFDEF FMXLIB}
    if (FSpecialDay.IsChecked) then
    {$ENDIF}
    {$IFDEF CMNLIB}
    if (FSpecialDay.Checked) then
    {$ENDIF}
    begin
      for i := 1 to 7 do
        dn[i] := 0;

      case FCDay.ItemIndex of
      0:begin
          rh.Days := [2,3,4,5,6];
          dn[2] := FCWeekNum.ItemIndex + 1;
          dn[3] := FCWeekNum.ItemIndex + 1;
          dn[4] := FCWeekNum.ItemIndex + 1;
          dn[5] := FCWeekNum.ItemIndex + 1;
          dn[6] := FCWeekNum.ItemIndex + 1;
        end;
      1:begin
          rh.Days := [1,7];
          dn[1] := FCWeekNum.ItemIndex + 1;
          dn[7] := FCWeekNum.ItemIndex + 1;
        end;
      2:begin
          rh.Days := [2];
          dn[2] := FCWeekNum.ItemIndex + 1;
        end;
      3:begin
          rh.Days := [3];
          dn[3] := FCWeekNum.ItemIndex + 1;
        end;
      4:begin
          rh.Days := [4];
          dn[4] := FCWeekNum.ItemIndex + 1;
        end;
      5:begin
          rh.Days := [5];
          dn[5] := FCWeekNum.ItemIndex + 1;
        end;
      6:begin
          rh.Days := [6];
          dn[6] := FCWeekNum.ItemIndex + 1;
        end;
      7:begin
          rh.Days := [7];
          dn[7] := FCWeekNum.ItemIndex + 1;
        end;
      8:begin
          rh.Days := [1];
          dn[1] := FCWeekNum.ItemIndex + 1;
        end;

      end;
      for i := 1 to 7 do
        rh.DayNum[i] := dn[i];
    end;
  end;

  if rh.Frequency = rfYearly then
  begin
    {$IFDEF FMXLIB}
    if (FYearSpecialDay.IsChecked) then
    {$ENDIF}
    {$IFDEF CMNLIB}
    if (FYearSpecialDay.Checked) then
    {$ENDIF}
    begin
      for i := 1 to 7 do dn[i] := 0;

      case FCYearDay.ItemIndex of
      0:begin
          rh.Days := [2,3,4,5,6];
          dn[2] := FCYearWeekNum.ItemIndex + 1;
          dn[3] := FCYearWeekNum.ItemIndex + 1;
          dn[4] := FCYearWeekNum.ItemIndex + 1;
          dn[5] := FCYearWeekNum.ItemIndex + 1;
          dn[6] := FCYearWeekNum.ItemIndex + 1;
        end;
      1:begin
          rh.Days := [1,7];
          dn[1] := FCYearWeekNum.ItemIndex + 1;
          dn[7] := FCYearWeekNum.ItemIndex + 1;
        end;
      2:begin
          rh.Days := [2];
          dn[2] := FCYearWeekNum.ItemIndex + 1;
        end;
      3:begin
          rh.Days := [3];
          dn[3] := FCYearWeekNum.ItemIndex + 1;
        end;
      4:begin
          rh.Days := [4];
          dn[4] := FCYearWeekNum.ItemIndex + 1;
        end;
      5:begin
          rh.Days := [5];
          dn[5] := FCYearWeekNum.ItemIndex + 1;
        end;
      6:begin
          rh.Days := [6];
          dn[6] := FCYearWeekNum.ItemIndex + 1;
        end;
      7:begin
          rh.Days := [7];
          dn[7] := FCYearWeekNum.ItemIndex + 1;
        end;
      8:begin
          rh.Days := [1];
          dn[1] := FCYearWeekNum.ItemIndex + 1;
        end;

      end;
      for i := 1 to 7 do
        rh.DayNum[i] := dn[i];
    end;

    rh.Months.Clear;
    {$IFDEF FMXLIB}
    if FYCK1.IsChecked then
      rh.Months.Add(1);
    if FYCK2.IsChecked then
      rh.Months.Add(2);
    if FYCK3.IsChecked then
      rh.Months.Add(3);
    if FYCK4.IsChecked then
      rh.Months.Add(4);
    if FYCK5.IsChecked then
      rh.Months.Add(5);
    if FYCK6.IsChecked then
      rh.Months.Add(6);
    if FYCK7.IsChecked then
      rh.Months.Add(7);
    if FYCK8.IsChecked then
      rh.Months.Add(8);
    if FYCK9.IsChecked then
      rh.Months.Add(9);
    if FYCK10.IsChecked then
      rh.Months.Add(10);
    if FYCK11.IsChecked then
      rh.Months.Add(11);
    if FYCK12.IsChecked then
      rh.Months.Add(12);
    {$ENDIF}
    {$IFDEF CMNLIB}
    if FYCK1.Checked then
      rh.Months.Add(1);
    if FYCK2.Checked then
      rh.Months.Add(2);
    if FYCK3.Checked then
      rh.Months.Add(3);
    if FYCK4.Checked then
      rh.Months.Add(4);
    if FYCK5.Checked then
      rh.Months.Add(5);
    if FYCK6.Checked then
      rh.Months.Add(6);
    if FYCK7.Checked then
      rh.Months.Add(7);
    if FYCK8.Checked then
      rh.Months.Add(8);
    if FYCK9.Checked then
      rh.Months.Add(9);
    if FYCK10.Checked then
      rh.Months.Add(10);
    if FYCK11.Checked then
      rh.Months.Add(11);
    if FYCK12.Checked then
      rh.Months.Add(12);
    {$ENDIF}
  end;

  FRecurrency := rh.Compose;

  if Assigned(AItem) then
  begin
    AItem.Recurrency := FRecurrency;
    AItem.Recurrent := AItem.Recurrency <> '';
  end;
  rh.Free;
end;

procedure TTMSPlannerCustomItemEditor_VCL.RegisterRuntimeClasses;
begin
  inherited;
  RegisterClass(TTMSPlannerCustomItemEditor_VCL);
end;

procedure TTMSPlannerCustomItemEditor_VCL.EndDateEditChanged(Sender: TObject);
var
  dt, dte: TDateTime;
begin
  if Assigned(FStartDateEdit) and Assigned(FStartTimeEdit) and Assigned(FEndDateEdit) and Assigned(FStartDateEdit) then
  begin
    dt := FStartDateEdit.Date + FStartTimeEdit.Time;
    dte := FEndDateEdit.Date + FEndTimeEdit.Time;
    if CompareDateTime(dt + IncMilliSecond(0, 1), dte) = 1 then
      FEndDateEdit.Date := FDialogEndDate
    else
      FDialogEndDate := FEndDateEdit.Date;
  end;
end;

procedure TTMSPlannerCustomItemEditor_VCL.EndTimeEditChanged(Sender: TObject);
var
  dt, dte: TDateTime;
begin
  if Assigned(FStartDateEdit) and Assigned(FStartTimeEdit) and Assigned(FEndDateEdit) and Assigned(FStartDateEdit) then
  begin
    dt := FStartDateEdit.Date + FStartTimeEdit.Time;
    dte := FEndDateEdit.Date + FEndTimeEdit.Time;
    if CompareDateTime(dt + IncMilliSecond(0, 1), dte) = 1 then
      FEndTimeEdit.Time := FDialogEndTime
    else
      FDialogEndTime := FEndTimeEdit.Time;
  end;
end;

procedure TTMSPlannerCustomItemEditor_VCL.FreqChanged(Sender: TObject);
begin
  if Assigned(FFreq) then
    ShowLayouts(FFreq.ItemIndex);
end;

procedure TTMSPlannerCustomItemEditor_VCL.SetRecurrency(ARecurrency: String);
begin
  ItemToRecurrency(ARecurrency);
end;

procedure TTMSPlannerCustomItemEditor_VCL.ShowLayouts(AIndex: Integer);
begin
  FWeekL.Visible := False;
  FDayL.Visible := False;
  FYearL.Visible := False;
  FDayWeekL.Visible := False;

  case AIndex of
  2: FDayWeekL.Visible := True;
  3: FDayL.Visible := True;
  4: FWeekL.Visible := True;
  5: FYearL.Visible := True;
  end;
end;

procedure TTMSPlannerCustomItemEditor_VCL.StartDateEditChanged(Sender: TObject);
var
  dt, dte: TDateTime;
begin
  if Assigned(FStartDateEdit) and Assigned(FStartTimeEdit) and Assigned(FEndDateEdit) and Assigned(FStartDateEdit) then
  begin
    dt := FStartDateEdit.Date + FStartTimeEdit.Time;
    dte := FEndDateEdit.Date + FEndTimeEdit.Time;
    if CompareDateTime(dt + IncMilliSecond(0, 1), dte) = 1 then
      FStartDateEdit.Date := FDialogStartDate
    else
      FDialogStartDate := FStartDateEdit.Date;
  end;
end;

procedure TTMSPlannerCustomItemEditor_VCL.StartTimeEditChanged(Sender: TObject);
var
  dt, dte: TDateTime;
begin
  if Assigned(FStartDateEdit) and Assigned(FStartTimeEdit) and Assigned(FEndDateEdit) and Assigned(FStartDateEdit) then
  begin
    dt := FStartDateEdit.Date + FStartTimeEdit.Time;
    dte := FEndDateEdit.Date + FEndTimeEdit.Time;
    if CompareDateTime(dt + IncMilliSecond(0, 1), dte) = 1 then
      FStartTimeEdit.Time := FDialogStartTime
    else
      FDialogStartTime := FStartTimeEdit.Time;
  end;
end;

{ TTMSFNCPlannerItemEditorRadioGroup }

constructor TTMSFNCPlannerItemEditorRadioGroup.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FButtons := TTMSFNCPlannerItemEditorRadioGroupButtons.Create;
  FItems := TStringList.Create;
  TStringList(FItems).OnChange := ItemsChange;
  FItemIndex := -1;
  FColumns := 1;
end;

destructor TTMSFNCPlannerItemEditorRadioGroup.Destroy;
begin
  SetButtonCount(0);
  FItems.Free;
  FButtons.Free;
  inherited Destroy;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.ArrangeButtons;
var
  ButtonsPerCol, I: Integer;
  ButtonWidth, ButtonHeight, TopMargin: Single;
  g: TTMSFNCGraphics;
  sz: TSizeF;
  btn: TRadioButton;
begin
  g := TTMSFNCGraphics.CreateBitmapCanvas;
  try
    if Assigned(FButtons) and (FButtons.Count <> 0) and not FReading then
    begin
    {$ifdef USING_FMX}
      g.SetFontName(Font.Family);
    {$else NOT USING_FMX}
      g.SetFontName(Font.Name);
    {$endif NOT USING_FMX}
      g.SetFontSize(Round(Font.Size));
      sz := g.CalculateTextSize('gh');

      ButtonsPerCol := (FButtons.Count + FColumns - 1) div FColumns;
      ButtonWidth := (Width - 10) / FColumns;
      I := Round(Height - sz.Height) - 5;
      ButtonHeight := I div ButtonsPerCol;
      TopMargin := sz.Height + 1 + (I mod ButtonsPerCol) div 2;

      {$IFDEF LCLLIB}
      TopMargin := TopMargin - 15;
      {$ENDIF}

      for I := 0 to FButtons.Count - 1 do
      begin
        btn := TRadioButton(FButtons[I]);
        {$IFDEF FMXLIB}
        btn.GroupName := Name;
        btn.SetBounds((I div ButtonsPerCol) * ButtonWidth + 8, (I mod ButtonsPerCol) * ButtonHeight + TopMargin, ButtonWidth, ButtonHeight);
        {$ENDIF}
        {$IFDEF CMNLIB}
        btn.SetBounds(Round((I div ButtonsPerCol) * ButtonWidth + 8), Round((I mod ButtonsPerCol) * ButtonHeight + TopMargin), Round(ButtonWidth), Round(ButtonHeight));
        {$ENDIF}
        btn.SendToBack;
      end;
    end;
  finally
    g.Free;
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.ButtonClick(Sender: TObject);
begin
  if not FUpdating then
  begin
    if Sender is TRadioButton then
      FItemIndex := FButtons.IndexOf(Sender as TRadioButton);

    Click;
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.ItemsChange(Sender: TObject);
begin
  if not FReading then
  begin
    if FItemIndex >= FItems.Count then
      FItemIndex := FItems.Count - 1;
    UpdateButtons;
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.Loaded;
begin
  inherited Loaded;
  ArrangeButtons;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.SetButtonCount(Value: Integer);
var
  btn: TRadioButton;
begin
  if csDestroying in ComponentState then
    Exit;

  while FButtons.Count < Value do
  begin
    btn := TRadioButton.Create(Self);
    btn.OnClick := ButtonClick;
    btn.Parent := Self;
    FButtons.Add(btn);
  end;

  while FButtons.Count > Value do
  begin
    btn := FButtons.Last;
    btn.Free;
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.SetColumns(Value: Integer);
begin
  if Value < 1 then
    Value := 1;

  if Value > 16 then
    Value := 16;

  if FColumns <> Value then
  begin
    FColumns := Value;
    ArrangeButtons;
  {$ifdef USING_FMX}
    Repaint;
  {$else NOT USING_FMX}
    Invalidate;
  {$endif USING_FMX}
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.SetItemIndex(Value: Integer);
begin
  if FReading then FItemIndex := Value else
  begin
    if Value < -1 then
      Value := -1;

    if Value >= FButtons.Count then
      Value := FButtons.Count - 1;

    if FItemIndex <> Value then
    begin
      {$IFDEF FMXLIB}
      if FItemIndex >= 0 then
        TRadioButton(FButtons[FItemIndex]).IsChecked := False;
      FItemIndex := Value;
      if FItemIndex >= 0 then
        TRadioButton(FButtons[FItemIndex]).IsChecked := True;
      {$ENDIF}
      {$IFDEF CMNLIB}
      if FItemIndex >= 0 then
        TRadioButton(FButtons[FItemIndex]).Checked := False;
      FItemIndex := Value;
      if FItemIndex >= 0 then
        TRadioButton(FButtons[FItemIndex]).Checked := True;
      {$ENDIF}
    end;
  end;
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.SetItems(Value: TStrings);
begin
  FItems.Assign(Value);
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.UpdateButtons;
var
  I: Integer;
begin
  SetButtonCount(FItems.Count);
  for I := 0 to FButtons.Count - 1 do
  begin
    {$IFDEF FMXLIB}
    TRadioButton(FButtons[I]).Text := FItems[I];
    {$ENDIF}
    {$IFDEF CMNLIB}
    TRadioButton(FButtons[I]).Caption := FItems[I];
    {$ENDIF}
  end;
  if FItemIndex >= 0 then
  begin
    FUpdating := True;
    {$IFDEF FMXLIB}
    TRadioButton(FButtons[FItemIndex]).IsChecked := True;
    {$ENDIF}
    {$IFDEF CMNLIB}
    TRadioButton(FButtons[FItemIndex]).Checked := True;
    {$ENDIF}
    FUpdating := False;
  end;
  ArrangeButtons;
{$ifdef USING_FMX}
  Repaint;
{$else NOT USING_FMX}
  Invalidate;
{$endif USING_FMX}
end;

procedure TTMSFNCPlannerItemEditorRadioGroup.Resize;
begin
  inherited;
  {$IFNDEF LCLLIB}
  ArrangeButtons;
  {$ENDIF}
end;

function TTMSFNCPlannerItemEditorRadioGroup.GetButtons(Index: Integer): TRadioButton;
begin
  Result := TRadioButton(FButtons[Index]);
end;

{$IFDEF LCLLIB}
procedure TTMSFNCPlannerItemEditorRadioGroup.DoSetBounds(ALeft, ATop, AWidth,
  AHeight: Integer);
begin
  inherited DoSetBounds(ALeft, ATop, AWidth, AHeight);
  ArrangeButtons;
end;
{$ENDIF}

{$IFDEF CMNLIB}
constructor TTMSFNCPlannerItemEditorLayout.Create(AOwner: TComponent);
begin
  inherited;
  BorderStyle := bsNone;
  BevelOuter := bvNone;
end;
{$ENDIF}


end.
