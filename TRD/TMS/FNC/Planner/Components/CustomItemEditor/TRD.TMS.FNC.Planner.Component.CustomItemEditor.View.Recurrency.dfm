inherited TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency: TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency
  Width = 430
  Height = 350
  ExplicitWidth = 430
  ExplicitHeight = 350
  object gbPattern: TGroupBox
    Left = 16
    Top = 16
    Width = 97
    Height = 185
    Caption = 'Pattern'
    TabOrder = 0
    object rbNone: TRadioButton
      Left = 8
      Top = 24
      Width = 65
      Height = 19
      Caption = 'None'
      TabOrder = 0
    end
    object rbHourly: TRadioButton
      Left = 8
      Top = 48
      Width = 73
      Height = 19
      Caption = 'Hourly'
      TabOrder = 1
    end
    object rbDaily: TRadioButton
      Left = 8
      Top = 72
      Width = 65
      Height = 19
      Caption = 'Daily'
      TabOrder = 2
    end
    object rbWeekly: TRadioButton
      Left = 8
      Top = 96
      Width = 73
      Height = 19
      Caption = 'Weekly'
      TabOrder = 3
    end
    object rbMonthly: TRadioButton
      Left = 8
      Top = 120
      Width = 73
      Height = 19
      Caption = 'Monthly'
      TabOrder = 4
    end
    object rbYearly: TRadioButton
      Left = 8
      Top = 144
      Width = 65
      Height = 19
      Caption = 'Yearly'
      TabOrder = 5
    end
  end
  object gbPDetails: TGroupBox
    Left = 128
    Top = 16
    Width = 289
    Height = 185
    Caption = 'Pattern Details'
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 38
      Height = 13
      Caption = 'Interval'
    end
    object pnlYearly: TPanel
      Left = 8
      Top = 64
      Width = 273
      Height = 113
      TabOrder = 0
      Visible = False
      object cbSep: TCheckBox
        Left = 88
        Top = 94
        Width = 49
        Height = 19
        Caption = 'Sep'
        TabOrder = 0
      end
      object cbOct: TCheckBox
        Left = 136
        Top = 94
        Width = 49
        Height = 19
        Caption = 'Oct'
        TabOrder = 1
      end
      object cbNov: TCheckBox
        Left = 184
        Top = 94
        Width = 49
        Height = 19
        Caption = 'Nov'
        TabOrder = 2
      end
      object cbMar: TCheckBox
        Left = 88
        Top = 70
        Width = 49
        Height = 19
        Caption = 'Mar'
        TabOrder = 3
      end
      object cbMay: TCheckBox
        Left = 184
        Top = 70
        Width = 49
        Height = 19
        Caption = 'May'
        TabOrder = 4
      end
      object cbJun: TCheckBox
        Left = 232
        Top = 70
        Width = 41
        Height = 19
        Caption = 'Jun'
        TabOrder = 5
      end
      object cbJul: TCheckBox
        Left = 0
        Top = 94
        Width = 41
        Height = 19
        Caption = 'Jul'
        TabOrder = 6
      end
      object cbJan: TCheckBox
        Left = 0
        Top = 70
        Width = 41
        Height = 19
        Caption = 'Jan'
        TabOrder = 7
      end
      object cbFeb: TCheckBox
        Left = 40
        Top = 70
        Width = 41
        Height = 19
        Caption = 'Feb'
        TabOrder = 8
      end
      object cbDec: TCheckBox
        Left = 232
        Top = 94
        Width = 49
        Height = 19
        Caption = 'Dec'
        TabOrder = 9
      end
      object cbAug: TCheckBox
        Left = 40
        Top = 94
        Width = 49
        Height = 19
        Caption = 'Aug'
        TabOrder = 10
      end
      object cbApr: TCheckBox
        Left = 136
        Top = 70
        Width = 41
        Height = 19
        Caption = 'Apr'
        TabOrder = 11
      end
      object rbEveryYear: TRadioButton
        Left = 8
        Top = 32
        Width = 65
        Height = 19
        Caption = 'Every'
        TabOrder = 13
        object cmbWeekNumYearly: TComboBox
          Left = 68
          Top = 0
          Width = 85
          Height = 21
          TabOrder = 0
          Items.Strings = (
            'First'
            'Second'
            'Third'
            'Fourth'
            'Fifth')
        end
        object cmbDayofWeekYearly: TComboBox
          Left = 172
          Top = 0
          Width = 85
          Height = 21
          TabOrder = 1
          Items.Strings = (
            'Weekday'
            'Weekend Day'
            'Monday'
            'Tuesday'
            'Wednesday'
            'Thursday'
            'Friday'
            'Saturday'
            'Sunday')
        end
      end
      object rbSameDayYear: TRadioButton
        Left = 8
        Top = 8
        Width = 201
        Height = 19
        Caption = 'Every same day of the year'
        TabOrder = 12
      end
    end
    object edtInterval: TEdit
      Left = 72
      Top = 24
      Width = 161
      Height = 21
      TabOrder = 1
    end
    object pnlMonthly: TPanel
      Left = 8
      Top = 64
      Width = 273
      Height = 113
      TabOrder = 2
      Visible = False
      object rbSameDayMonth: TRadioButton
        Left = 8
        Top = 16
        Width = 193
        Height = 19
        Caption = 'Every same day of the month'
        TabOrder = 0
      end
      object rbEveryMonth: TRadioButton
        Left = 8
        Top = 40
        Width = 65
        Height = 19
        Caption = 'Every'
        TabOrder = 1
      end
      object cmbWeekNumMonthly: TComboBox
        Left = 72
        Top = 40
        Width = 86
        Height = 21
        TabOrder = 3
        Items.Strings = (
          'First'
          'Second'
          'Third'
          'Fourth'
          'Fifth')
      end
      object cmbDayofWeekMonthly: TComboBox
        Left = 176
        Top = 40
        Width = 86
        Height = 21
        TabOrder = 2
        Items.Strings = (
          'Weekday'
          'Weekend Day'
          'Monday'
          'Tuesday'
          'Wednesday'
          'Thursday'
          'Friday'
          'Saturday'
          'Sunday')
      end
    end
    object pnlWeekly: TPanel
      Left = 8
      Top = 64
      Width = 273
      Height = 113
      TabOrder = 4
      Visible = False
      object cbMon: TCheckBox
        Left = 8
        Top = 16
        Width = 49
        Height = 19
        Caption = 'Mon'
        TabOrder = 0
      end
      object cbTue: TCheckBox
        Left = 64
        Top = 16
        Width = 57
        Height = 19
        Caption = 'Tues'
        TabOrder = 1
      end
      object cbWed: TCheckBox
        Left = 120
        Top = 16
        Width = 49
        Height = 19
        Caption = 'Wed'
        TabOrder = 2
      end
      object cbThu: TCheckBox
        Left = 176
        Top = 16
        Width = 49
        Height = 19
        Caption = 'Thu'
        TabOrder = 3
      end
      object cbFri: TCheckBox
        Left = 8
        Top = 48
        Width = 41
        Height = 19
        Caption = 'Fri'
        TabOrder = 4
      end
      object cbSat: TCheckBox
        Left = 64
        Top = 48
        Width = 49
        Height = 19
        Caption = 'Sat'
        TabOrder = 6
      end
      object cbSun: TCheckBox
        Left = 120
        Top = 48
        Width = 49
        Height = 19
        Caption = 'Sun'
        TabOrder = 5
      end
    end
    object pnlDaily: TPanel
      Left = 8
      Top = 64
      Width = 273
      Height = 113
      TabOrder = 3
      Visible = False
      object rbEveryDay: TRadioButton
        Left = 16
        Top = 16
        Width = 113
        Height = 17
        Caption = 'Every day'
        TabOrder = 1
      end
      object rbEveryWeekday: TRadioButton
        Left = 16
        Top = 40
        Width = 113
        Height = 17
        Caption = 'Every weekday'
        TabOrder = 0
      end
    end
  end
  object gbRange: TGroupBox
    Left = 16
    Top = 216
    Width = 401
    Height = 121
    Caption = 'Range'
    TabOrder = 2
    object Label1: TLabel
      Left = 208
      Top = 56
      Width = 56
      Height = 13
      Caption = 'Occurences'
    end
    object rbInfinite: TRadioButton
      Left = 16
      Top = 24
      Width = 113
      Height = 17
      Caption = 'Infinite'
      TabOrder = 0
    end
    object rbOcurrences: TRadioButton
      Left = 16
      Top = 56
      Width = 57
      Height = 19
      Caption = 'For'
      TabOrder = 1
      object edtOccur: TEdit
        Left = 80
        Top = 0
        Width = 105
        Height = 21
        TabOrder = 0
      end
    end
    object rbUntilDate: TRadioButton
      Left = 16
      Top = 88
      Width = 81
      Height = 19
      Caption = 'Until date'
      TabOrder = 2
    end
    object deUntil: TDateTimePicker
      Left = 96
      Top = 88
      Width = 105
      Height = 22
      Date = 42572.194322696760000000
      Time = 42572.194322696760000000
      TabOrder = 3
    end
  end
end
