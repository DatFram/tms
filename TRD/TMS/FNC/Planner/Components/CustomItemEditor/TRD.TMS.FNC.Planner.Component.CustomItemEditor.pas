unit TRD.TMS.FNC.Planner.Component.CustomItemEditor;
// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. Concerning creating a custom editor for TMS Planner item, see:
//   - http://tmssoftware.be/site/forum/forum_posts.asp?TID=2891&OB=ASC
//   - https://www.tmssoftware.com/site/forum/forum_posts.asp?TID=458&title=create-own-planner-item-editor
//   - http://www.tmssoftware.biz/Download/Manuals/TMS%20TPLANNER.pdf (Pages 29 to 31)
//
//  2. Concerning the VERY ANNOYING error "Control" has no parent running under VCL, see:
//   - https://stackoverflow.com/questions/3776028/the-control-xxx-has-no-parent-window
//
//  3. Concerning leaving dates blank, TPlannerDatePicker allows entry to blank dates
//     as discussed here:
//   - http://tmssoftware.be/site/forum/forum_posts.asp?TID=7546&KW=planner+custom+editor&PID=29447&title=inline-editor-eddateedit#29447
//
//  4. Conerning retrieving application version information, see:
//   - https://delphihaven.wordpress.com/2012/12/08/retrieving-the-applications-version-string/
//
// -----------------------------------------------------------------------------
// TODO (Last updated 2016-07-22)
//
//  1. Drag & Drop between ListBoxes in Tasks Frame
//  2. Implement Exceptions Frame
//
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}{$modeswitch advancedrecords}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.Classes
, System.SysUtils
  {$ifdef USING_VCL}
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.Controls
//, Vcl.Memo
//
, VCL.TMSFNCUtils
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCColorPicker
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.StdCtrls
, Fmx.ExtCtrls
, Fmx.Controls
, Fmx.Memo
//
, FMX.TMSFNCUtils
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCColorPicker
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
, StdCtrls
, ExtCtrls
, Controls
, Memo
//
, LCLTMSFNCUtils
, LCLTMSFNCCustomComponent
, LCLTMSFNCColorPicker
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif}
// Rework into Kennedy's & Sarah's 2017 Summer efforts
, Sample.View.PlannerItem
// Third party extensions - TMS FNC Planner...
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View             // for base class
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General     // for TTMSCustomItemEditorGeneral
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks       // for TTMSCustomItemEditorTasks
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency  // for TTMSCustomItemEditorRecurrency
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions  // for TTMSCustomItemEditorExceptions
;

type

  TTRD_TMS_FNC_Planner_Component_CustomItemEditor = class( TTMSFNCPlannerCustomItemEditor )
    private
    //
      fContentPanelCreated        : Boolean;
      fContentPanelSet            : Boolean;
      fContentPanel               : TPanel;
    //
      fTabControl                 : TTMSFNCPlannerItemEditorTabControl;
      fTabControlRecurrency       : TTMSFNCPlannerItemEditorTabControl;
    //
      fTabGeneral                 : TTMSFNCPlannerItemEditorTabItem;
      fTabTasks                   : TTMSFNCPlannerItemEditorTabItem;
      fTabRecurrency              : TTMSFNCPlannerItemEditorTabItem;
    //
      fTabRecurrencySettings      : TTMSFNCPlannerItemEditorTabItem;
      fTabRecurrencyExceptions    : TTMSFNCPlannerItemEditorTabItem;
    //
      fView_ItemDetail            : TTMSCustomItemEditorView_General;
      fView_ItemTasks             : TTMSCustomItemEditorView_Tasks;
      fView_RecurrencySettings    : TTMSCustomItemEditorView_Recurrency;
      fView_RecurrencyExceptions  : TTMSCustomItemEditorView_Exceptions;
    //
      //fRecurrency                 : String;
      //fExDates                    : TTMSFNCRecurrencyDateItems;
//      fMemo                       : TMemo;
    protected
      //procedure DoOnShow( aSender: TObject );
    public
    // existential methods
      constructor Create( aOwner: TComponent ); override;
      destructor Destroy; override;
    // called to create singleton instance???
      function CreateInstance: TTMSFNCPlannerCustomItemEditor; override;
      procedure RegisterRuntimeClasses; override;
      function GetVersion: String; override;
    // Called once to create editor View
      procedure CreateCustomContentPanel; override;
    // Called repeatedly to syn location of PlannerItem
      procedure InitializeCustomContentPanel; override;
    // Return custom editor to edit event for a particular PlannerItem
      procedure GetCustomContentPanel( {%H-}aItem: TTMSFNCPlannerItem;
        var AContentPanel: TTMSFNCPlannerEditingDialogContentPanel ); override;
    // Populate controls within custom editor with the PlannerItem property settings
      procedure ItemToCustomContentPanel( aItem: TTMSFNCPlannerItem; aContentPanel:
        TTMSFNCPlannerEditingDialogContentPanel ); override;
    // Update PlannerItem properties with values from controls within custom editor
      procedure CustomContentPanelToItem( {%H-}aContentPanel:
        TTMSFNCPlannerEditingDialogContentPanel; aItem: TTMSFNCPlannerItem ); override;
  end;

// Short name alias
  TTMSPlannerCustomItemEditor = TTRD_TMS_FNC_Planner_Component_CustomItemEditor;

implementation

uses
  CodeSiteLogging
;

type
  TTMSFNCCustomPlannerOpen = class( TTMSFNCCustomPlanner );

const
  MAJ_VER = 1; // Major version nr.
  MIN_VER = 0; // Minor version nr.
  REL_VER = 0; // Release nr.
  BLD_VER = 0; // Build nr.

  // version history
  // v1.0.0.0 : first release - RFI edition (2017-08-21 - Day of Solar Eclipse)


// =============================================================================

{ TTRD_TMS_FNC_Planner_Component_CustomItemEditor }

// -----------------------------------------------------------------------------
// existential methods

constructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor.Create( aOwner: TComponent );
begin
  CodeSite.Clear;
  CodeSite.TraceMethod( Format( '_CustomItemEditor.Create [Instance of %s]',
    [ Self.QualifiedClassName ] ), TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  inherited;
  fContentPanelSet := False;
end;

destructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor.Destroy;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.Destroy', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
// free ContentPanel if NOT assigned to planner as inplace/pop-up iutem editor
  if NOT fContentPanelSet then
    fContentPanel.Free;
  inherited;
end;

function TTRD_TMS_FNC_Planner_Component_CustomItemEditor.CreateInstance: TTMSFNCPlannerCustomItemEditor;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.CreateInstance', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  Result := TTMSPlannerCustomItemEditor.Create( nil );
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.RegisterRuntimeClasses;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.RegisterRuntimeClasses', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  inherited;
  RegisterClass( TTMSPlannerCustomItemEditor );
end;

function TTRD_TMS_FNC_Planner_Component_CustomItemEditor.GetVersion: String;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.GetVersionNumber', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  Result := GetVersionNumber( MAJ_VER, MIN_VER, REL_VER, BLD_VER );
end;


// -----------------------------------------------------------------------------
// existential helper methods

// Step 1: Called first...
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.CreateCustomContentPanel;
begin
  if fContentPanelCreated then
    Exit;

  CodeSite.TraceMethod( '_CustomItemEditor.CreateCustomContentPanel', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  try
    try
    // create panel as main view for CustomItemEditor
      fContentPanel := TPanel.Create( Planner );
      with fContentPanel do begin
        fContentPanel.Name := 'RFIContentPanel';
      {$ifdef FMXMOBILE}
        fContentPanel.Height := 550;
        fContentPanel.Width := 500;
      {$else}
        fContentPanel.Height := 425;
        fContentPanel.Width := 450;
      {$endif}

      // Assign parent to fContentPanel
        if NOT Assigned( fContentPanel.Parent ) then begin
          fContentPanel.Parent := Planner;
        end;
      end;

    {.$define TUTORIAL}

    {$ifdef TUTORIAL}

      with TMemo.Create( fContentPanel ) do begin
        Name := 'DemoMemo';
        Align :=
        {$ifdef USING_FMX}
          TAlignLayout.Client;
        {$else NOT USING_FMX}
          TAlign.alClient;
        {$endif USING_FMX}
        WordWrap := True;
        Text :=
          'Here''s a starting point for your custom item editor (it''s defined ' +
          'in the unit Sample.View.ItemEditor.pas). To see TMS'' own customized ' +
          'for recurrency, open "Demos.FNC.Planner.Editor.View.VCL/FMX/LCL.pas" ' +
          'and unrem (remove "//" from) the line: ' + #10#13 +
          '"// TMSFNCPlanner1.ItemEditor := TMSFNCPlannerItemEditorRecurrency1" ';
        Parent := fContentPanel;
      end;

    {$else NOT TUTORIAL}

    // create tab control as view container to host collection of editor subviews
      fTabControl := TTMSFNCPlannerItemEditorTabControl.Create( fContentPanel );
      with fTabControl do begin
        fTabControl.Name := 'RFITopMostViewContainer';
        fTabControl.Align :=
      {$ifdef USING_FMX}
        TAlignLayout.Client;
      {$else NOT USING_FMX}
        TAlign.alClient;
      {$endif NOT USING_FMX}
        fTabControl.Parent := fContentPanel;
      end;

    // ToDo: ITERATE AND INSTANTIATE composite subviews...
    // like an RFI View conatiner, iterate through some list classes of ItemEditor
    // SubView Classes create SubView instances of which the ItemEditor is comprised

    // Subview "General" within main view container
      fTabGeneral := TTMSFNCPlannerItemEditorTabItem.Create( fTabControl );
      with fTabGeneral do begin
        fTabGeneral.Name := 'RFIGeneralViewContainer';
      {$ifdef USING_FMX}
        fTabGeneral.Text := TranslateTextEx( 'General' );
        fTabGeneral.Parent := fTabControl;
      {$else NOT USING_FMX}
        fTabGeneral.Caption := TranslateTextEx( 'General' );
        fTabGeneral.PageControl := fTabControl;
      {$endif NOT USING_FMX}
      end;

    // Subview "Tasks" within main view container
      fTabTasks := TTMSFNCPlannerItemEditorTabItem.Create( fTabControl );
      with fTabTasks do begin
        fTabTasks.Name := 'RFITasksViewContainer';
      {$ifdef USING_FMX}
        fTabTasks.Text := TranslateTextEx( 'Tasks' );
        fTabTasks.Parent := fTabControl;
      {$else NOT USING_FMX}
        fTabTasks.Caption := TranslateTextEx( 'Tasks' );
        fTabTasks.PageControl := fTabControl;
      {$endif NOT USING_FMX}
      end;

    // Subview "Recurrency" within main view container
      fTabRecurrency := TTMSFNCPlannerItemEditorTabItem.Create( fTabControl );
      with fTabRecurrency do begin
        fTabRecurrency.Name := 'RFIRecurrencyViewContainer';
      {$ifdef USING_FMX}
        fTabRecurrency.Text := TranslateTextEx( 'Recurrency' );
        fTabRecurrency.Parent := fTabControl;
      {$else NOT USING_FMX}
        fTabRecurrency.Caption := TranslateTextEx( 'Recurrency' );
        fTabRecurrency.PageControl := fTabControl;
      {$endif NOT USING_FMX}
      end;

    // Subview subcontainer for Recurrency
      fTabControlRecurrency := TTMSFNCPlannerItemEditorTabControl.Create( fTabRecurrency );
      with fTabControlRecurrency do begin
        fTabControlRecurrency.Name := 'RFIRecurrencySubViewContainer';
      {$ifdef USING_FMX}
        fTabControlRecurrency.Align := TAlignLayout.Client;
      {$else NOT USING_FMX}
        fTabControlRecurrency.Align := alClient;
      {$endif NOT USING_FMX}
        fTabControlRecurrency.Parent := fTabRecurrency;
      end;

    // Subview "Settings" within "Recurrency" subcontainer
      fTabRecurrencySettings := TTMSFNCPlannerItemEditorTabItem.Create( fTabControlRecurrency );
      with fTabRecurrencySettings do begin
        fTabRecurrencySettings.Name := 'RFIRecurrencySettingsSubViewContainer';
      {$ifdef USING_FMX}
        fTabRecurrencySettings.Text := TranslateTextEx( 'Settings' );
        fTabRecurrencySettings.Parent := fTabControlRecurrency;
      {$else NOT USING_FMX}
        fTabRecurrencySettings.Caption := TranslateTextEx( 'Settings' );
        fTabRecurrencySettings.PageControl := fTabControlRecurrency;
      {$endif NOT USING_FMX}
      end;

    // Subview "Exceptions" within "Recurrency" subcontainer
      fTabRecurrencyExceptions := TTMSFNCPlannerItemEditorTabItem.Create( fTabControlRecurrency );
      with fTabRecurrencyExceptions do begin
        fTabRecurrencyExceptions.Name := 'RFIRecurrencyExceptionsSubViewContainer';
      {$ifdef USING_FMX}
        fTabRecurrencyExceptions.Text := TranslateTextEx( 'Exceptions' );
        fTabRecurrencyExceptions.Parent := fTabControlRecurrency;
      {$else NOT USING_FMX}
        fTabRecurrencyExceptions.Caption := TranslateTextEx( 'Exceptions' );
        fTabRecurrencyExceptions.PageControl := fTabControlRecurrency;
      {$endif NOT USING_FMX}
      end;

    {$endif NOT TUTORIAL}

    // Experimental...
      //fContentPanel.Parent := nil;

    except
      raise Exception.Create( 'ERROR: Cannot create custom planner item editor' );
    end;
  finally
  // flag already created
    fContentPanelCreated := True;
  end;
end;

// Step 2: Called Second and last...
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.GetCustomContentPanel(
  aItem: TTMSFNCPlannerItem; var aContentPanel: TTMSFNCPlannerEditingDialogContentPanel );
begin
  CodeSite.TraceMethod( '_CustomItemEditor.GetCustomContentPanel', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );
  //inherited;

  try
    if Assigned( fContentPanel ) AND ( aContentPanel <> fContentPanel ) then begin
      aContentPanel := fContentPanel;
      fContentPanelSet := True;
    // https://www.raize.com/devtools/codesite/LoggingClasses.asp
      //CodeSite.SendParents( '< ContentPanel Parenting >', TControl( fContentPanel ) );
      //CodeSite.SendControls( '< ContentPanel Hierarchy >', TControl( fContentPanel ) );
    end;

    if Assigned( aItem ) {AND  aItem <> Empty} then begin
      CodeSite.Send(
        Format(
          'NOTICE: Editor invoked on planner item entitled: "%s" [DBKey:%s]',
          [ aItem.Title, aItem.DBKey ]
        )
      );
    end;

    Assert( Assigned( aContentPanel ), 'ERROR: Content panel not assigned' );
    Assert( Assigned( aContentPanel.Parent ), 'ERROR: Content panel parent not assigned' );
  except
    on E: Exception do
      CodeSite.SendException( E );
  end;
end;

// Step 3: Called Third...
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.InitializeCustomContentPanel;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.InitializeCustomContentPanel', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );
  inherited;

  if ( NOT Assigned( fView_ItemDetail ) ) AND Assigned( fTabGeneral ) then begin
  // create general item detail view as required
    fView_ItemDetail := TTMSCustomItemEditorView_General.Create( fTabGeneral, Planner );
  end;

  if ( NOT Assigned( fView_ItemTasks ) ) AND Assigned( fTabTasks ) then begin
  // create item tasks detail view as required
    fView_ItemTasks := TTMSCustomItemEditorView_Tasks.Create( fTabTasks, Planner );
  end;

  if ( NOT Assigned( fView_RecurrencySettings ) ) AND Assigned( fTabRecurrencySettings ) then begin
  // create item tasks detail view as required
    fView_RecurrencySettings := TTMSCustomItemEditorView_Recurrency.Create( fTabRecurrencySettings, Planner  );
  end;

  if ( NOT Assigned( fView_RecurrencyExceptions ) ) AND Assigned( fTabRecurrencyExceptions ) then begin
  // create item tasks detail view as required
    fView_RecurrencyExceptions := TTMSCustomItemEditorView_Exceptions.Create( fTabRecurrencyExceptions, Planner );
    fView_RecurrencySettings.ViewRecurrencyExceptions := fView_RecurrencyExceptions;
  end;
end;

// Step 4: Called Fourth...
// Set View control values from planner item
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.ItemToCustomContentPanel(
  aItem: TTMSFNCPlannerItem; aContentPanel: TTMSFNCPlannerEditingDialogContentPanel );
begin
  CodeSite.TraceMethod( '_CustomItemEditor.ItemToCustomContentPanel', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );
  //inherited; // its abstract!!!

  try
  // Ensure planner item exists with which to initialize te Views
    if Assigned( aItem ) then begin
      if Assigned( Self.fView_ItemDetail ) then
         Self.fView_ItemDetail.BindData_ItemToView( aItem );
      if Assigned( Self.fView_ItemTasks ) then
         Self.fView_ItemTasks.BindData_ItemToView( aItem );
      if Assigned( Self.fView_RecurrencySettings ) then
         Self.fView_RecurrencySettings.BindData_ItemToView( aItem );
    //  if Assigned( Self.fView_RecurrencyExceptions ) then
    //     Self.fView_RecurrencyExceptions.BindData_ItemToView( aItem );
    end
    else begin
      raise Exception.Create( 'ERROR: No planner item assigned to edit' );
    end;
  except
    on E: Exception do
      CodeSite.SendException( E );
  end;
end;

// Set planner item properties from View control values
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor.CustomContentPanelToItem(
  aContentPanel: TTMSFNCPlannerEditingDialogContentPanel; aItem: TTMSFNCPlannerItem );
var
  p: TTMSFNCCustomPlannerOpen;
//  lContentPanelParented: Boolean;
//  lContentPanelVisible: Boolean;
begin
  CodeSite.TraceMethod( '_CustomItemEditor.CustomContentPanelToItem', TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );
  //inherited; // its abstract!!!

  if NOT Assigned( fContentPanel.Parent ) then begin
    CodeSite.SendWarning( 'ContentPanel has no parenting' );
    p := TTMSFNCCustomPlannerOpen( Planner );
    if Assigned( p.ItemEditor ) then begin
      TTMSFNCPlannerItemEditor( p.ItemEditor ).InsertComponent( fContentPanel );
    end
    else begin
      p.ItemEditor := Self;
    end;
    fContentPanel.Parent := p.ItemEditor;
    fContentPanel.Visible := True;
    //CodeSite.SendParents( '< ContentPanel Parenting >', fContentPanel );
  end;

  try
  // So long as this custom editor has been assigned to a planner, continue
    if Assigned( aItem ) then begin
      if Assigned( Self.fView_ItemDetail ) then
        Self.fView_ItemDetail.BindData_ViewToItem( aItem );
      if Assigned( Self.fView_ItemTasks ) then
         Self.fView_ItemTasks.BindData_ViewToItem( aItem );
      if Assigned( Self.fView_RecurrencySettings ) then
         Self.fView_RecurrencySettings.BindData_ViewToItem( aItem );
    //  if Assigned( Self.fView_RecurrencyExceptions ) then
    //     Self.fView_RecurrencyExceptions.BindData_ViewToItem( aItem );
    end
    else begin
      raise Exception.Create( 'ERROR: No planner item assigned to update' );
    end;
  except
    on E: Exception do
      CodeSite.SendException( E );
  end;
end;


end.
