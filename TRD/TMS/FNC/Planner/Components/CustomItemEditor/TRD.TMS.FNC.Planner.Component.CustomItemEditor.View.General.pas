unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General;
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.SysUtils
, System.Types
, System.UITypes
, System.Classes
, System.Variants
  {$ifdef USING_VCL}
, Vcl.Graphics
, Vcl.Controls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.ComCtrls
//
, VCL.TMSFNCTypes
, VCL.TMSFNCUtils
, VCL.TMSFNCGraphics
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCCustomControl
, VCL.TMSFNCCustomPicker
, VCL.TMSFNCColorPicker
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Graphics
, Fmx.Controls
, Fmx.Forms
, Fmx.Dialogs
, Fmx.StdCtrls
, Fmx.ScrollBox
, Fmx.Memo
, Fmx.Edit
, Fmx.Controls.Presentation
, Fmx.DateTimeCtrls
, Fmx.ListBox
//
, FMX.TMSFNCTypes
, FMX.TMSFNCUtils
, FMX.TMSFNCGraphics
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCCustomControl
, FMX.TMSFNCCustomPicker
, FMX.TMSFNCColorPicker
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
  SysUtils
, Types
, UITypes
, Classes
, Variants
//
, Graphics
, Controls
, Forms
, Dialogs
, StdCtrls
, ExtCtrls
, ComCtrls
//
, LCLTMSFNCTypes
, LCLTMSFNCUtils
, LCLTMSFNCGraphics
, LCLTMSFNCGraphicsTypes
, LCLTMSFNCCustomControl
, LCLTMSFNCCustomPicker
, LCLTMSFNCColorPicker
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View
;

type

  TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General = class( TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View )
  {$ifdef USING_VCL}
    deStartDate: TDateTimePicker;
    deEndDate: TDateTimePicker;
  {$endif USING_VCL}
  {$ifdef USING_FMX}
    deStartDate: TDateEdit;
    deEndDate: TDateEdit;
  {$endif USING_FMX}
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    mmNotes: TMemo;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
  {$ifdef USING_VCL}
    teStartTime: TDateTimePicker;
    teEndTime: TDateTimePicker;
  {$endif USING_VCL}
  {$ifdef USING_FMX}
    teStartTime: TTimeEdit;
    teEndTime: TTimeEdit;
  {$endif USING_FMX}
    cmbEmployee: TComboBox;
    cmbResource: TComboBox;
    cmbGender: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    cpColor: TTMSFNCColorPicker;
    cpTitleColor: TTMSFNCColorPicker;
    cmbTimeBlock: TComboBox;
  private
    procedure FillCmbEmployee;
    procedure FillCmbTimeBlock;
    procedure FillCmbGender;
  protected
    //procedure CallOnParentShow( aSender: TObject ); override;
    // reset controls within View
      procedure ResetControls; override;
  public
    //constructor Create( aOwner: TComponent ); override;
    // update View controls with the planner item property values
      procedure BindData_ItemToView( const aItem: TTMSFNCPlannerItem ); override;
    // update planner item property values with View's current control values
      procedure BindData_ViewToItem( {var} aItem: TTMSFNCPlannerItem ); override;
  end;

// Short name alias
  TTMSCustomItemEditorView_General = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General;

implementation

{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}
{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}

uses
// Rework into Kennedy's & Sarah's 2017 Summer efforts
  Sample.View.PlannerItem
;

const
// ToDo: Make this array of Employee Objects with emails, availability, etc.
  ArrayEmployees: Array[ 0..5 ] of String = (
    'Open',
    'Person A',
    'Person B',
    'Person C',
    'Person D',
    'Person E'
  );

// ToDo: Make this array of TimeBlock objects, each with specific start/end times
  ArrayTimeBlocks: Array[ 0..5 ] of String = (
    'None Selected',
    'CP Instruction',
    'Game Hour',
    'TP Instruction',
    'Admin',
    'Phones-Outcalls'
  );

// ToDo: Link to datastore
  ArrayGenders: Array[ 0..2 ] of String = (
    'None Selected',
    'Male',
    'Female'
  );

// -----------------------------------------------------------------------------
//

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.ResetControls;
begin
  Name := 'Settings';
//
  FillCmbEmployee;
  FillCmbTimeBlock;
  FillCmbGender;
end;


// -----------------------------------------------------------------------------
// data binding to/from View controls from/to planner item

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.BindData_ItemToView(
  const aItem: TTMSFNCPlannerItem );
var
  lCustomPlannerItem: TSampleViewModelPlannerItem;
begin
  //inherited; // its abstract!!!

  if Assigned( aItem ) then begin
  //
    teStartTime.Time := aItem.StartTime;
    teEndTime.Time := aItem.EndTime;

  //
    deStartDate.Date := aItem.StartTime;
    deEndDate.Date := aItem.EndTime;

  // NOTE: if an item is added to the planner, then saved to the database, it's
  // title property gets cut off. Therefore this line will fail and the
  // combobox in the editor will not be able to read the title of the planner item
    if cmbTimeBlock.Items.IndexOf( aItem.Title ) > -1 then begin
      cmbTimeBlock.ItemIndex := cmbTimeBlock.Items.IndexOf( aItem.Title );
    end
    else begin
      cmbTimeBlock.ItemIndex := 0;
    end;

    mmNotes.Text := aItem.Text;
    cmbResource.ItemIndex := aItem.Resource;
    cpColor.SelectedColor := aItem.Color;
    cpTitleColor.SelectedColor := aItem.TitleColor;

  // To get the other properties of the planner item
    if ( Assigned( aItem.DataObject ) ) AND
       ( aItem.DataObject.InheritsFrom( TSampleViewModelPlannerItem ) ) then begin
      lCustomPlannerItem := aItem.DataObject as TSampleViewModelPlannerItem;
      if lCustomPlannerItem.Employee = 'Person A' then begin
        cmbEmployee.ItemIndex := 1;
      end;
      cmbGender.ItemIndex := lCustomPlannerItem.Gender;
    end
    else begin
      cmbEmployee.ItemIndex := 0;
      cmbGender.ItemIndex := 0;
    end;
  end
  else begin
  // is this correct? (Shouldn't planner have passed in a new EditorItem IFF none was clicked upon??
    deStartDate.Date        := Planner.SelectedStartDateTime;
    deEndDate.Date          := Planner.SelectedEndDateTime;
    teStartTime.Time        := Planner.SelectedStartDateTime;
    teEndTime.Time          := Planner.SelectedEndDateTime;
    cmbTimeBlock.ItemIndex  := 0;
    mmNotes.Text            := '';
    cmbResource.ItemIndex   := planner.SelectedResource;
    cmbEmployee.ItemIndex   := 0;
    cmbGender.ItemIndex     := 0;
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.BindData_ViewToItem(
  {var} aItem: TTMSFNCPlannerItem );
var
  lGUID: TGUID;
  lTitle: String;
  lCustomPlannerItem: TSampleViewModelPlannerItem;
begin
  inherited;
// *****************************************************************************
// REMOVE TO PLANNER'S DATASOURCE/DATASET EVENT HANDLER???
// *****************************************************************************
  if ( aItem.DBKey = EmptyStr ) OR ( aItem.DBKey = GUIDtoString( TGUID.Empty ) ) then begin
    CreateGUID( lGUID );
    aItem.DBKey := GUIDtoString( lGUID );
  end;
// *****************************************************************************
{$ifdef USING_FMX}
  if Assigned( cmbTimeBlock.Selected ) then begin
    lTitle := cmbTimeBlock.Selected.Text;
  end;
{$else NOT USING_FMX}
  lTitle := cmbTimeBlock.SelText;
{$endif NOT USING_FMX}
  if lTitle = EmptyStr then
    lTitle := 'Not defined';
  aItem.StartTime   := deStartDate.Date + teStartTime.Time;
  aItem.EndTime     := deEndDate.Date + teEndTime.Time;
  aItem.Title       := lTitle;
  aItem.Text        := mmNotes.Text;
  aItem.Resource    := cmbResource.ItemIndex;
  aItem.Color       := cpColor.SelectedColor;
  aItem.TitleColor  := cpTitleColor.SelectedColor;

// Set recurrency of item according to changes made
  //RecurrencyToItem( aItem );

  if Assigned ( aItem.DataObject ) then begin
  // Skips this loop if item extension is ISampleViewModelPlannerItem
  // and not TSampeViewModelPlannerItem - only sees AItem.DataObject is TObject
    if aItem.DataObject is TSampleViewModelPlannerItem then begin
      lCustomPlannerItem := aItem.DataObject as TSampleViewModelPlannerItem;
      if Self.cmbEmployee.ItemIndex = 1 then begin
        lCustomPlannerItem.Employee := 'Person A';
      end;
      lCustomPlannerItem.Gender := Self.cmbGender.ItemIndex;
    end;
  end
  else begin
    lCustomPlannerItem := TSampleViewModelPlannerItem.Create;
    lCustomPlannerItem.Gender := Self.cmbGender.ItemIndex;
    if Self.cmbEmployee.ItemIndex = 1 then begin
      lCustomPlannerItem.Employee := 'Person A';
    end;
    aItem.DataObject := lCustomPlannerItem;
  end;
end;


// -----------------------------------------------------------------------------
// local control methods

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.FillCmbEmployee;
var
  lEmployee: String;
begin
  for lEmployee in ArrayEmployees do begin
    if ( cmbEmployee.Items.IndexOf( lEmployee ) = -1 ) then begin
      cmbEmployee.Items.Add( lEmployee );
    end;
  end;
  cmbEmployee.ItemIndex := 0;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.FillCmbTimeBlock;
var
  I: Integer;
  TimeBlock: string;
begin
  for I := 0 to High(ArrayTimeBlocks) do begin
    TimeBlock := ArrayTimeBlocks[I];
    cmbTimeBlock.Items.Add(TimeBlock);
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General.FillCmbGender;
var
  I: Integer;
  Gender: string;
begin
  for I := 0 to High(ArrayGenders) do begin
    Gender := ArrayGenders[I];
    cmbGender.Items.Add(Gender);
  end;
end;


end.
