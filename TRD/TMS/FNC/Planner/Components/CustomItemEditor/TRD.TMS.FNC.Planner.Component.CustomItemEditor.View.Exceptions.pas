unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions;
// -----------------------------------------------------------------------------
// TODO (Last updated 2017-08-20)
//
//  1. Implement all TButton OnClick events and their enablements under TAction
//
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.Classes
, System.SysUtils
, System.Types
, System.UITypes
, System.Variants
, System.Actions
  {$ifdef USING_VCL}
//, VCL.Types
, Vcl.Graphics
, Vcl.Controls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.StdCtrls
, Vcl.ComCtrls
, Vcl.ActnList
//
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Graphics
, Fmx.Layouts
, Fmx.Controls
, Fmx.Controls.Presentation
, Fmx.Forms
, Fmx.Dialogs
, Fmx.StdCtrls
, Fmx.DateTimeCtrls
, Fmx.ListBox
, Fmx.ActnList
//
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
, Classes
, StdCtrls
, Types
//
, Controls
, StdCtrls
, ExtCtrls
, ComCtrls
, ActnList
//
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View
;

type

  TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions = class( TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View )
    {$ifdef USING_FMX}
      deExStartDate: TDateEdit;
      deExEndDate: TDateEdit;
      teExStartTime: TTimeEdit;
      teExEndTime: TTimeEdit;
    {$endif USING_FMX}
    {$if defined( USING_LCL ) or defined( USING_VCL)}
      deExStartDate: TDateTimePicker;
      deExEndDate: TDateTimePicker;
      teExStartTime: TDateTimePicker;
      teExEndTime: TDateTimePicker;
    {$endif defined( USING_LCL ) or defined( USING_VCL)}
      Label1: TLabel;
      Label2: TLabel;
      lbEx: TListBox;
      btnAdd: TButton;
      btnDelete: TButton;
      btnClear: TButton;
      ActionList: TActionList;
    private
      fExceptionDates: TTMSFNCRecurrencyDateItems;
    protected
    // local methods - events
      procedure Event_ExceptionAdd( aSender: TObject );
      procedure Event_ExceptionRemoveSelected( aSender: TObject );
      procedure Event_ExceptionClearAll( aSender: TObject );
    protected
     // reset controls within View
      procedure ResetControls; override;
   public
      constructor Create( aOwner: TComponent; const aPlanner: TTMSFNCCustomPlanner ); override;
      destructor Destroy( ); override;
    // update View controls with the planner item property values
      procedure BindData_ItemToView( const aItem: TTMSFNCPlannerItem ); override;
    // update planner item property values with View's current control values
      procedure BindData_ViewToItem( {var} aItem: TTMSFNCPlannerItem ); override;
    // local methods - business logic
      procedure ReadRecurrencyHandlerFromView( var aRecurrencyHandler: TTMSFNCRecurrencyHandler );
      procedure WriteRecurrencyHandlerToView( const aRecurrencyHandler: TTMSFNCRecurrencyHandler );
    // ToDo: Use common ViewModel to share one instance of TTMSFNCRecurrencyDateItems (a TCollection)
    // ToDo: Write one on more helper methods for TTMSFNCRecurrencyDateItems (a TCollection)
      property ExceptionDates: TTMSFNCRecurrencyDateItems
        read fExceptionDates;
  end;

// Short name alias
  TTMSCustomItemEditorView_Exceptions = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions;

implementation

{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}
{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}

// -----------------------------------------------------------------------------
// existential methods

constructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.Create(
  aOwner: TComponent; const aPlanner: TTMSFNCCustomPlanner );
begin
  Self.fExceptionDates := TTMSFNCRecurrencyDateItems.Create;

  inherited Create( aOwner, aPlanner ); // calls align & ResetControls
//
  Self.Name := 'RecurrencyExceptions';
{$ifndef USING_VCL}
  //Self.Caption := 'Recurrency Exceptions';
{$else NOT USING_FMX}
  Self.Caption := 'Recurrency Exceptions';
{$endif NOT USING_FMX}
//
  Self.btnAdd.OnClick     := Event_ExceptionAdd;
  Self.btnClear.OnClick   := Event_ExceptionClearAll;
  Self.btnDelete.OnClick  := Event_ExceptionRemoveSelected;
end;

destructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.Destroy;
begin
  Self.fExceptionDates.Free;
  inherited;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.ResetControls;
begin
  if Assigned( Self.fExceptionDates ) then
    Self.fExceptionDates.Clear;
  Self.lbEx.Clear;
  Self.deExStartDate.Date      := Now;
  Self.deExEndDate.Date        := Now;
{$ifdef USING_FMX}
  Self.teExStartTime.IsChecked := False;
  Self.teExEndTime.IsChecked   := False;
{$else NOT USING_FMX}
  Self.teExStartTime.Checked   := False;
  Self.teExEndTime.Checked     := False;
{$endif USING_FMX}
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.BindData_ItemToView(
  const aItem: TTMSFNCPlannerItem );
begin
  inherited;
  // NOT USED
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.BindData_ViewToItem(
  {var} aItem: TTMSFNCPlannerItem );
begin
  inherited;
  // NOT USED
end;


// -----------------------------------------------------------------------------
// local methods - business logic

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.ReadRecurrencyHandlerFromView(
  var aRecurrencyHandler: TTMSFNCRecurrencyHandler );
var
  i: Integer;
begin
  if Assigned( aRecurrencyHandler ) then begin
    for i := 1 to Self.ExceptionDates.Count do begin
      with aRecurrencyHandler.ExDates.Add do begin
        StartDate := Self.ExceptionDates.Items[ i - 1 ].StartDate;
        EndDate := Self.ExceptionDates.Items[ i - 1 ].EndDate;
      end;
    end;
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.WriteRecurrencyHandlerToView(
  const aRecurrencyHandler: TTMSFNCRecurrencyHandler );
var
  i: Integer;
  lEdit: String;
begin
  ResetControls;
  if Assigned( aRecurrencyHandler ) then begin
    for i := 1 to aRecurrencyHandler.ExDates.Count do begin
      if ( Frac( aRecurrencyHandler.ExDates.Items[ i - 1 ].StartDate ) = 0 ) and
         ( Frac( aRecurrencyHandler.ExDates.Items[ i - 1 ].EndDate) = 0 ) then begin
        lEdit := DateToStr( aRecurrencyHandler.ExDates.Items[ i - 1 ].StartDate ) + ' to ' +
                 DateToStr(aRecurrencyHandler.ExDates.Items[i - 1].EndDate )
      end
      else begin
        lEdit := DateToStr( aRecurrencyHandler.ExDates.Items[ i - 1 ].StartDate ) + ' - '  +
                 TimeToStr( aRecurrencyHandler.ExDates.Items[ i - 1 ].StartDate ) + ' to ' +
                 DateToStr( aRecurrencyHandler.ExDates.Items[ i - 1 ].EndDate )   + ' - '  +
                 TimeToStr( aRecurrencyHandler.ExDates.Items[ i - 1 ].EndDate );
      end;

      Self.lbEx.Items.Add( lEdit );
      with fExceptionDates.Add do begin
        StartDate := aRecurrencyHandler.ExDates.Items[ i - 1 ].StartDate;
        EndDate := aRecurrencyHandler.ExDates.Items[ i - 1 ].EndDate;
      end;
    end;
  end;
end;


// -----------------------------------------------------------------------------
// local methods - events

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.Event_ExceptionAdd(
  aSender: TObject );
var
  lEdit: String;
  lHasStartTime, lHasEndTime: Boolean;
begin
{$ifdef USING_FMX}
  lHasStartTime := teExStartTime.IsChecked;
  lHasEndTime := teExEndTime.IsChecked;
{$else NOT USING_FMX}
  lHasStartTime := teExStartTime.Checked;
  lHasEndTime := teExEndTime.Checked;
{$endif NOT USING_FMX}
  if lHasStartTime and lHasEndTime then begin
    lEdit := DateToStr( deExStartDate.Date ) + ' - '  +
             TimeToStr( teExStartTime.Time ) + ' to ' +
             DateToStr( deExEndDate.date )   + ' - '  +
             TimeToStr( teExEndTime.Time )
  end
  else begin
    lEdit := DateToStr( deExStartDate.Date ) + ' to ' +
             DateToStr( deExEndDate.Date );
  end;

  if lbEx.Items.IndexOf( lEdit ) = -1 then begin
    lbEx.Items.Add( lEdit );
    with ( fExceptionDates.Add ) do begin
      if lHasStartTime and lHasEndTime then begin
        StartDate := Int( deExStartDate.Date ) +
                     Frac( teExStartTime.Time );
        EndDate   := Int( deExEndDate.Date ) +
                     Frac( teExEndTime.Time );
      end else
      begin
        StartDate := Int( deExStartDate.Date );
        EndDate   := Int( deExEndDate.Date );
      end;
    end;
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.Event_ExceptionClearAll(
  aSender: TObject );
begin
  ResetControls;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions.Event_ExceptionRemoveSelected( aSender: TObject );
var
  lSelectedIndex: Integer;
begin
  lSelectedIndex := Self.lbEx.ItemIndex;
  if lSelectedIndex > -1 then begin
    Self.lbEx.Items.Delete( lSelectedIndex );
    fExceptionDates.Delete( lSelectedIndex );
  end;
end;


end.
