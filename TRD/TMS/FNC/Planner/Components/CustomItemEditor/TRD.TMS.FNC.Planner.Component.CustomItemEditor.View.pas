unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.View;
// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. Concerning VCL's requirement of a screen object Handle for some contained
//     controls to function, see:
//    - https://stackoverflow.com/questions/3037379/accessing-and-inheriting-windows-message-for-other-windows-message-in-delphi?rq=1
//    - https://stackoverflow.com/questions/3776028/the-control-xxx-has-no-parent-window
//    - http://www.delphigroups.info/2/c1/527133.html
//    - http://docwiki.embarcadero.com/Libraries/en/Vcl.Controls.TWinControl.HandleNeeded
//    - http://docwiki.embarcadero.com/Libraries/en/Winapi.Messages.TWMShowWindow
//    - https://stackoverflow.com/questions/28195671/how-do-i-program-a-resize-handle-on-a-delphi-tframe
//
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.Classes
, System.SysUtils
, System.Types
, System.UITypes
, System.Variants
  {$ifdef USING_VCL}
// Windows API dependencies
, WinApi.Windows
, Winapi.Messages
//, Vcl.Types
, Vcl.Graphics
, Vcl.Controls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.StdCtrls
//
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Graphics
, Fmx.Controls
, Fmx.Forms
, Fmx.Dialogs
, Fmx.StdCtrls
//
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
, Classes
, StdCtrls
, Types
, ExtCtrls
, Controls
//
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
;


type

  TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View = class( TFrame )
    private
      fPlanner: TTMSFNCCustomPlanner;
      fOnShow: TNotifyEvent;
      //fParentOnPaint: TOnPaintEvent;
    protected
    {$ifdef USING_VCL}
    // http://docwiki.embarcadero.com/Libraries/en/Winapi.Messages.TWMShowWindow
    // to handle the WM_SHOWWINDOW method of TFrame
      procedure wmShowWindow( var aMsg: TWMShowWindow ); message WM_SHOWWINDOW;
    // http://www.delphigroups.info/2/7a/303325.html
      procedure cmVisibleChanged( var aMsg: TMessage ); message CM_VISIBLECHANGED;
      procedure cmShowingChanged( var aMsg: TMessage ); message CM_SHOWINGCHANGED;
    {$endif USING_VCL}
      procedure CallOnParentShow( aSender: TObject ); virtual;
      //procedure DoOnParentPaint( aSender: TObject; aCanvas: TCanvas; const aRect: TRectF ); virtual; final;
    // reset controls within View
      procedure ResetControls; virtual; abstract;
    public
    {$ifdef USING_VCL}
    // USAGE: Call CreateWithParentHandle( aOwnerWindow: HWND ) constructor --and
    //        NOT using Create( aOwner:TComponent ) --and pass in owner's HWND
    //        to parent this TFrame derivative (for use within a DLL, etc)
      class function CreateWithParentHandle( aOwnerWindow: HWND ): HWND;
    {$endif USING_VCL}
      constructor Create( aOwner: TComponent ); overload; override; deprecated 'ERROR: Default construction deprecated for all planner editor views';
      constructor Create( aOwner: TComponent; const aPlanner: TTMSFNCCustomPlanner ); reintroduce; overload; virtual;
    // update View controls with the planner item property values
      procedure BindData_ItemToView( const aItem: TTMSFNCPlannerItem ); virtual; abstract;
    // update planner item property values with View's current control values
      procedure BindData_ViewToItem( {var} aItem: TTMSFNCPlannerItem ); virtual; abstract;
      property OnShow: TNotifyEvent read fOnShow write fOnShow;
      property Planner: TTMSFNCCustomPlanner read fPlanner;
  end;

// Short name alias
  TTMSCustomItemEditorView = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View;

implementation

{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}
{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}

uses
// CodeSite Logging
  CodeSiteLogging
;

constructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.Create(
  aOwner: TComponent );
begin
  raise Exception.CreateFmt( 'ERROR: Default construction deprecated for "%s"', [ Self.QualifiedClassName ] );
end;

constructor TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.Create(
  aOwner: TComponent; const aPlanner: TTMSFNCCustomPlanner );
begin
  CodeSite.TraceMethod( Format( '_CustomItemEditor_View.Create [Instance of %s]', [ Self.QualifiedClassName ] ), TCodeSiteTraceMethodOption.tmoTiming, TCodeSiteTimingFormat.tfMilliseconds  );  //inherited; // its abstract!!!
  inherited Create( aOwner );
  fPlanner := aPlanner;
  // ToDo: Use RTTI to lookup whether aOwner has OnShow method and, if so,
  //       hook its OnShow to additionally call this instance's DoOnParentShow

{$ifdef USING_FMX}
//  if Assigned( Parent ) AND ( Parent.InheritsFrom( TControl ) ) then begin
//    if Assigned( ( Parent as TControl ).OnPaint ) then begin
//      fParentOnPaint := ( aOwner as TControl ).OnPaint;
//      ( aOwner as TControl ).OnPaint := DoOnParentPaint;
//    end;
//  end;
{$endif USING_FMX}

{$ifdef USING_VCL}
//  Self.HandleNeeded;
//  Self.OnResize := DoOnParentShow;
{$endif USING_VCL}

{$ifdef USING_FMX}
  Self.Align := TAlignLayout.Client;
  if Assigned( aOwner ) AND aOwner.InheritsFrom( TControl ) then begin
    Self.Parent := ( aOwner as TControl );
  end;
{$else NOT USING_FMX}
  Self.Align := TAlign.alClient;
  if Assigned( aOwner ) AND aOwner.InheritsFrom( TWinControl ) then begin
    Self.Parent := ( aOwner as TWinControl );
  end;
{$endif USING_FMX}
  ResetControls;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.CallOnParentShow(
  aSender: TObject );
begin
  if {( NOT Visible ) AND} Assigned( fOnShow ) then begin
    fOnshow( aSender );
  end;
end;

{$ifdef USING_FMX}
//procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.DoOnParentPaint(
//  aSender: TObject; aCanvas: TCanvas; const aRect: TRectF );
//begin
//  if Assigned( fParentOnPaint ) then begin
//    fParentOnPaint( aSender, aCanvas, aRect );
//  end;
//  DoOnParentShow( aSender );
//end;
{$endif USING_FMX}

{$ifdef USING_VCL}
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.wmShowWindow( var aMsg: TWMShowWindow );
begin
  inherited;
  CallOnParentShow( nil );
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.cmShowingChanged(
  var aMsg: TMessage);
begin
  inherited;
  CallOnParentShow( nil );
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.cmVisibleChanged(
  var aMsg: TMessage);
begin
  inherited;
  CallOnParentShow( nil );
end;

// See https://stackoverflow.com/questions/3776028/the-control-xxx-has-no-parent-window
class function TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View.CreateWithParentHandle( aOwnerWindow: HWND ): HWND;
begin
  Result := CreateParented( aOwnerWindow ).Handle;
end;
{$endif USING_VCL}


end.
