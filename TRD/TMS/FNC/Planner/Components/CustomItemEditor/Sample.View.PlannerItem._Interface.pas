unit Sample.View.PlannerItem._Interface;
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
 {$define USING_LCL}
 {$undef USING_VCL}
 {$undef USING_FMX}
{$else NOT fpc}
 {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses

{$ifndef USING_LCL}
  System.Classes
{$else USING_LCL}
  Classes
{$endif USING_LCL}
;

type

  ISampleViewModelPlannerItem = interface
    ['{BADBE7B9-D70E-4979-9DCE-CBB1693AF087}']
    // private/protected accessor
      procedure SetTasks(const ATasks: TStringList);
      function GetTasks: TStringList;
      procedure SetEmployee(const AEmployee: string);
      function GetEmployee: string;
      procedure SetGender(const AGender: Integer);
      function GetGender: Integer;
      procedure SetGCalendarID(const AGCalendarID: string);
      function GetGCalendarID: string;
    // piblic properties
      property Tasks: TStringList read GetTasks write SetTasks;
      property Employee: string read GetEmployee write SetEmployee;
      property Gender: Integer read GetGender write SetGender;
      property GCalendarID: string read GetGCalendarID write SetGCalendarID;
  end;

implementation

end.
