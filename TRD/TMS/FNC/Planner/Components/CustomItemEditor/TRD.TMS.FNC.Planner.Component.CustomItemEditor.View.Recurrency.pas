unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency;
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.SysUtils
, System.Types
, System.UITypes
, System.Classes
, System.Variants
  {$ifdef USING_VCL}
, Vcl.Graphics
, Vcl.Controls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.ComCtrls
//
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Graphics
, Fmx.Controls
, Fmx.Forms
, Fmx.Dialogs
, Fmx.StdCtrls
, Fmx.ListBox
, Fmx.Edit
, Fmx.DateTimeCtrls
, Fmx.Controls.Presentation
//
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
  SysUtils
, Types
, UITypes
, Classes
, Variants
//
, Graphics
, Controls
, Forms
, Dialogs
, StdCtrls
, ExtCtrls
, ComCntrls
//
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions, FMX.Types
;

type
  TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency = class( TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View )
      gbPattern: TGroupBox;
      gbPDetails: TGroupBox;
      gbRange: TGroupBox;
      rbInfinite: TRadioButton;
      rbOcurrences: TRadioButton;
      rbUntilDate: TRadioButton;
    {$ifdef USING_VCL}
      deUntil: TDateTimePicker;
    {$endif USING_VCL}
    {$ifdef USING_FMX}
      deUntil: TDateEdit;
    {$endif USING_FMX}
      edtOccur: TEdit;
      Label1: TLabel;
      rbNone: TRadioButton;
      rbHourly: TRadioButton;
      rbDaily: TRadioButton;
      rbWeekly: TRadioButton;
      rbMonthly: TRadioButton;
      rbYearly: TRadioButton;
      edtInterval: TEdit;
      rbSameDayYear: TRadioButton;
      rbEveryYear: TRadioButton;
      Label2: TLabel;
      cmbWeekNumYearly: TComboBox;
      cmbDayofWeekYearly: TComboBox;
      cbJan: TCheckBox;
      cbFeb: TCheckBox;
      cbMar: TCheckBox;
      cbApr: TCheckBox;
      cbMay: TCheckBox;
      cbJun: TCheckBox;
      cbJul: TCheckBox;
      cbAug: TCheckBox;
      cbSep: TCheckBox;
      cbOct: TCheckBox;
      cbNov: TCheckBox;
      cbDec: TCheckBox;
      pnlYearly: TPanel;
      pnlMonthly: TPanel;
      rbSameDayMonth: TRadioButton;
      rbEveryMonth: TRadioButton;
      cmbWeekNumMonthly: TComboBox;
      cmbDayofWeekMonthly: TComboBox;
      pnlWeekly: TPanel;
      cbMon: TCheckBox;
      cbTue: TCheckBox;
      cbWed: TCheckBox;
      cbThu: TCheckBox;
      cbFri: TCheckBox;
      cbSat: TCheckBox;
      cbSun: TCheckBox;
      pnlDaily: TPanel;
      rbEveryDay: TRadioButton;
      rbEveryWeekday: TRadioButton;
      procedure rbNoneChange(Sender: TObject);
      procedure rbHourlyChange(Sender: TObject);
      procedure rbDailyChange(Sender: TObject);
      procedure rbWeeklyChange(Sender: TObject);
      procedure rbMonthlyChange(Sender: TObject);
      procedure rbYearlyChange(Sender: TObject);
    private
      fView_RecurrencyExceptions: TTMSCustomItemEditorView_Exceptions;
    // local methods - business logic
      function ReadRecurrencyAsStringFromView: String;
      procedure WriteRecurrencyAsStringToView( const aRecurrencyAsString: String );
      procedure ShowLayouts( aIndex: Integer );
    protected
    // reset controls within View
      procedure ResetControls; override;
    public
      //constructor Create( aOwner: TComponent ); override;
    // update View controls with the planner item property values
      procedure BindData_ItemToView( const aItem: TTMSFNCPlannerItem ); override;
    // update planner item property values with View's current control values
      procedure BindData_ViewToItem( {var} aItem: TTMSFNCPlannerItem ); override;
    // local methods - public
      function GetRecurrencyAsString: String; // NOT NEEDED!?!
    // ToDo: Use common ViewModel to share one instance of TTMSFNCRecurrencyDateItems (a TCollection)
    // ToDo: Write one on more helper methods for TTMSFNCRecurrencyDateItems (a TCollection)
      property RecurrencyAsString: String
        read ReadRecurrencyAsStringFromView
        write WriteRecurrencyAsStringToView;
      property ViewRecurrencyExceptions: TTMSCustomItemEditorView_Exceptions
        read fView_RecurrencyExceptions
        write fView_RecurrencyExceptions;
  end;

  TTMSCustomItemEditorView_Recurrency = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency;

implementation

{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}
{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}

// =============================================================================

type
  TTMSFNCCustomPlannerOpen = class( TTMSFNCCustomPlanner );


// =============================================================================

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.ResetControls;
begin
//
  Name := 'RecurrencySettings';
//
  Self.cmbWeekNumMonthly.ItemIndex := 0;
  Self.cmbDayofWeekMonthly.ItemIndex := 0;
  Self.cmbWeekNumYearly.ItemIndex := 0;
  Self.cmbDayofWeekYearly.ItemIndex := 0;
  Self.deUntil.Date := Now;
  Self.edtOccur.Text := '10';
  Self.edtInterval.Text := '1';
{$ifdef USING_FMX}
  Self.cbMon.IsChecked := False;
  Self.cbTue.IsChecked := False;
  Self.cbWed.IsChecked := False;
  Self.cbThu.IsChecked := False;
  Self.cbFri.IsChecked := False;
  Self.cbSat.IsChecked := False;
  Self.cbSun.IsChecked := False;
  Self.rbEveryDay.IsChecked := True;
  Self.rbSameDayYear.IsChecked := True;
  Self.rbSameDayMonth.IsChecked := True;
  Self.rbEveryWeekday.IsChecked := False;
  Self.rbEveryMonth.IsChecked := False;
  Self.rbEveryYear.IsChecked := False;
  Self.cbJan.IsChecked := False;
  Self.cbFeb.IsChecked := False;
  Self.cbMar.IsChecked := False;
  Self.cbApr.IsChecked := False;
  Self.cbMay.IsChecked := False;
  Self.cbJun.IsChecked := False;
  Self.cbJul.IsChecked := False;
  Self.cbAug.IsChecked := False;
  Self.cbSep.IsChecked := False;
  Self.cbOct.IsChecked := False;
  Self.cbNov.IsChecked := False;
  Self.cbDec.IsChecked := False;
  Self.rbInfinite.IsChecked := True;
  Self.rbOcurrences.IsChecked := False;
  Self.rbUntilDate.IsChecked := False;
{$else NOT USING_FMX}
  Self.cbMon.Checked := False;
  Self.cbTue.Checked := False;
  Self.cbWed.Checked := False;
  Self.cbThu.Checked := False;
  Self.cbFri.Checked := False;
  Self.cbSat.Checked := False;
  Self.cbSun.Checked := False;
  Self.rbEveryDay.Checked := True;
  Self.rbSameDayYear.Checked := True;
  Self.rbSameDayMonth.Checked := True;
  Self.rbEveryWeekday.Checked := False;
  Self.rbEveryMonth.Checked := False;
  Self.rbEveryYear.Checked := False;
  Self.cbJan.Checked := False;
  Self.cbFeb.Checked := False;
  Self.cbMar.Checked := False;
  Self.cbApr.Checked := False;
  Self.cbMay.Checked := False;
  Self.cbJun.Checked := False;
  Self.cbJul.Checked := False;
  Self.cbAug.Checked := False;
  Self.cbSep.Checked := False;
  Self.cbOct.Checked := False;
  Self.cbNov.Checked := False;
  Self.cbDec.Checked := False;
  Self.rbInfinite.Checked := True;
  Self.rbOcurrences.Checked := False;
  Self.rbUntilDate.Checked := False;
{$endif}
end;

function TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.GetRecurrencyAsString: String;
var
  lPlannerItem: TTMSFNCPlannerItem;
begin
// Why can't we now just use this View's RecurrencyAsString property???
  lPlannerItem := TTMSFNCPlannerItem.Create( nil ); // create as converter
  try
    Self.BindData_ViewToItem( lPlannerItem );
    Result := lPlannerItem.Recurrency;
  finally
    lPlannerItem.Free;
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.BindData_ItemToView(
  const aItem: TTMSFNCPlannerItem );
var
  lRecurrencyAsStringToWriteToView: String;
  p: TTMSFNCCustomPlannerOpen;
begin
  if Assigned( aItem ) then begin
  // read "recurrency-encoded" string from planner item
    lRecurrencyAsStringToWriteToView := aItem.Recurrency;
  // update View with "recurrency-encoded" string
    WriteRecurrencyAsStringToView( lRecurrencyAsStringToWriteToView );
  end
  else begin
    p := TTMSFNCCustomPlannerOpen( Planner );
  // read "recurrency-encoded" string from default planner item (or set to EmptyStr)
    lRecurrencyAsStringToWriteToView := p.DefaultItem.Recurrency;
  // update View with "recurrency-encoded" string
    WriteRecurrencyAsStringToView( lRecurrencyAsStringToWriteToView );
  end;
end;

// Update planner item property values with View's current recurrency values
procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.BindData_ViewToItem(
  {var} aItem: TTMSFNCPlannerItem );
var
  lRecurrencyAsStringAsReadFromView: String;
begin
  if Assigned( aItem ) then begin
  // read View's control values as "recurrency-encoded" string
    lRecurrencyAsStringAsReadFromView := ReadRecurrencyAsStringFromView;
  // update item with recurrency as string
    aItem.Recurrency := lRecurrencyAsStringAsReadFromView;
    aItem.Recurrent := ( aItem.Recurrency <> '' );
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.WriteRecurrencyAsStringToView( const aRecurrencyAsString: String );
var
  lIsRecurrent: Boolean;
  rh: TTMSFNCRecurrencyHandler;
  i: Integer;
  dn: Boolean;
begin
  // fExceptionDates.Clear;

  lIsRecurrent := ( aRecurrencyAsString = EmptyStr );

  if lIsRecurrent then begin
    ResetControls;
    // ToDo: Call ResetControls on RecurrencyExceptions herein as well

    rh := TTMSFNCRecurrencyHandler.Create;
    try
      rh.Recurrency := aRecurrencyAsString;
      rh.Parse;

    //  FFreq.ItemIndex := Integer(rh.Frequency) + 1;
      case rh.Frequency of
      {$ifdef USING_FMX}
        rfNone:     Self.rbNone.IsChecked     := True;
        rfHourly:   Self.rbHourly.IsChecked   := True;
        rfDaily:    Self.rbDaily.IsChecked    := True;
        rfWeekly:   Self.rbWeekly.IsChecked   := True;
        rfMonthly:  Self.rbMonthly.IsChecked  := True;
        rfYearly:   Self.rbYearly.IsChecked   := True;
      {$else NOT USING_FMX}
        rfNone:     Self.rbNone.Checked       := True;
        rfHourly:   Self.rbHourly.Checked     := True;
        rfDaily:    Self.rbDaily.Checked      := True;
        rfWeekly:   Self.rbWeekly.Checked     := True;
        rfMonthly:  Self.rbMonthly.Checked    := True;
        rfYearly:   Self.rbYearly.Checked     := True;
      {$endif NOT USING_FMX}
      end;

      Self.edtInterval.Text := IntToStr( rh.Interval );

    // Set to proper radio button in Range Groupbox
    {$ifdef USING_FMX}
      Self.rbInfinite.IsChecked     := True;
      if (rh.RepeatCount = 0) and (rh.RepeatUntil = 0) then
        Self.rbInfinite.IsChecked   := True;;
      if (rh.RepeatCount > 0) and (rh.RepeatUntil = 0) then
        Self.rbOcurrences.IsChecked := True;
      if (rh.RepeatUntil > 0) then
        Self.rbUntilDate.IsChecked  := True;
    {$else NOT USING_FMX}
      Self.rbInfinite.Checked       := True;
      if (rh.RepeatCount = 0) and (rh.RepeatUntil = 0) then
        Self.rbInfinite.Checked     := True;;
      if (rh.RepeatCount > 0) and (rh.RepeatUntil = 0) then
        Self.rbOcurrences.Checked   := True;
      if (rh.RepeatUntil > 0) then
        Self.rbUntilDate.Checked    := True;
    {$endif NOT USING_FMX}

      Self.edtOccur.Text := IntToStr( rh.RepeatCount );
    {$ifdef USING_FMX}
      if Self.rbUntilDate.IsChecked then begin
        Self.deUntil.Date := rh.RepeatUntil
      end
      else begin
        Self.deUntil.Date := Now;
      end;
    {$else NOT USING_FMX}
      if Self.rbUntilDate.Checked then begin
        Self.deUntil.Date := rh.RepeatUntil
      end
      else begin
        Self.deUntil.Date := Now;
      end;
    {$endif NOT USING_FMX}

      case Integer(rh.Frequency) of
    {$ifdef USING_FMX}
      1:begin
          if (2 in rh.Days) and
             (3 in rh.Days) and
             (4 in rh.Days) and
             (5 in rh.Days) and
             (6 in rh.Days) then begin
            Self.rbEveryWeekday.IsChecked := True;
            Self.rbEveryDay.IsChecked := False;
          end
          else begin
            Self.rbEveryWeekday.IsChecked := False;
            Self.rbEveryDay.IsChecked := True;
          end;
        end;
      2:begin
          Self.cbMon.IsChecked := 2 in rh.Days;
          Self.cbTue.IsChecked := 3 in rh.Days;
          Self.cbWed.IsChecked := 4 in rh.Days;
          Self.cbThu.IsChecked := 5 in rh.Days;
          Self.cbFri.IsChecked := 6 in rh.Days;
          Self.cbSat.IsChecked := 7 in rh.Days;
          Self.cbSun.IsChecked := 1 in rh.Days;
        end;
    {$else NOT USING_FMX}
      1:begin
          if (2 in rh.Days) and
             (3 in rh.Days) and
             (4 in rh.Days) and
             (5 in rh.Days) and
             (6 in rh.Days) then begin
            Self.rbEveryWeekday.Checked := True;
            Self.rbEveryDay.Checked := False;
          end
          else begin
            Self.rbEveryWeekday.Checked := False;
            Self.rbEveryDay.Checked := True;
          end;
        end;
      2:begin
          Self.cbMon.Checked := 2 in rh.Days;
          Self.cbTue.Checked := 3 in rh.Days;
          Self.cbWed.Checked := 4 in rh.Days;
          Self.cbThu.Checked := 5 in rh.Days;
          Self.cbFri.Checked := 6 in rh.Days;
          Self.cbSat.Checked := 7 in rh.Days;
          Self.cbSun.Checked := 1 in rh.Days;
        end;
    {$endif NOT USING_FMX}
      3:begin
          dn := false;
          for i := 1 to 7 do
            if rh.DayNum[i] <> 0 then
              dn := true;

          if (dn) then begin
            {$ifdef USING_FMX}
            Self.rbSameDayMonth.IsChecked := False;
            Self.rbEveryMonth.IsChecked := True;
            {$else NOT USING_FMX}
            Self.rbSameDayMonth.Checked := False;
            Self.rbEveryMonth.Checked := True;
            {$endif NOT USING_FMX}

            if (2 in rh.Days) and
               (3 in rh.Days) and
               (4 in rh.Days) and
               (5 in rh.Days) and
               (6 in rh.Days) then begin
              Self.cmbDayofWeekMonthly.ItemIndex := 0;
              Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[2] - 1;
            end
            else
              if (1 in rh.Days) and
                 (7 in rh.Days) then begin
                Self.cmbDayofWeekMonthly.ItemIndex := 1;
                Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[1] - 1;
              end
              else
              begin
                if 1 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 8;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[1] - 1;
                end;
                if 2 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 2;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[2] - 1;
                end;
                if 3 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 3;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[3] - 1;
                end;
                if 4 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 4;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[4] - 1;
                end;
                if 5 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 5;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[5] - 1;
                end;
                if 6 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 6;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[6] - 1;
                end;
                if 7 in rh.Days then begin
                  Self.cmbDayofWeekMonthly.ItemIndex := 7;
                  Self.cmbWeekNumMonthly.ItemIndex := rh.DayNum[7] - 1;
                end;
              end;
          end;
        end;
      4:begin
          dn := false;
          for i := 1 to 7 do
            if rh.DayNum[i] <> 0 then
              dn := true;

          if (dn) then begin
          {$ifdef USING_FMX}
            Self.rbSameDayYear.IsChecked := False;
            Self.rbEveryYear.IsChecked := True;
          {$else NOT USING_FMX}
            Self.rbSameDayYear.Checked := False;
            Self.rbEveryYear.Checked := True;
          {$ENDIF}

            if (2 in rh.Days) and
               (3 in rh.Days) and
               (4 in rh.Days) and
               (5 in rh.Days) and
               (6 in rh.Days) then begin
              Self.cmbDayofWeekYearly.ItemIndex := 0;
              Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[2] - 1;
            end
            else
            if (1 in rh.Days) and (7 in rh.Days) then begin
              Self.cmbDayofWeekYearly.ItemIndex := 1;
              Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[1] - 1;
            end
            else begin
              if 1 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 8;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[1] - 1;
              end;
              if 2 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 2;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[2] - 1;
              end;
              if 3 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 3;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[3] - 1;
              end;
              if 4 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 4;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[4] - 1;
              end;
              if 5 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 5;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[5] - 1;
              end;
              if 6 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 6;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[6] - 1;
              end;
              if 7 in rh.Days then begin
                Self.cmbDayofWeekYearly.ItemIndex := 7;
                Self.cmbWeekNumYearly.ItemIndex := rh.DayNum[7] - 1;
              end;
            end;
          end;

          {$ifdef USING_FMX}
          Self.cbJan.IsChecked := rh.Months.HasValue(1);
          Self.cbFeb.IsChecked := rh.Months.HasValue(2);
          Self.cbMar.IsChecked := rh.Months.HasValue(3);
          Self.cbApr.IsChecked := rh.Months.HasValue(4);
          Self.cbMay.IsChecked := rh.Months.HasValue(5);
          Self.cbJun.IsChecked := rh.Months.HasValue(6);
          Self.cbJul.IsChecked := rh.Months.HasValue(7);
          Self.cbAug.IsChecked := rh.Months.HasValue(8);
          Self.cbSep.IsChecked := rh.Months.HasValue(9);
          Self.cbOct.IsChecked := rh.Months.HasValue(10);
          Self.cbNov.IsChecked := rh.Months.HasValue(11);
          Self.cbDec.IsChecked := rh.Months.HasValue(12);
          {$else NOT USING_FMX}
          Self.cbJan.Checked := rh.Months.HasValue(1);
          Self.cbFeb.Checked := rh.Months.HasValue(2);
          Self.cbMar.Checked := rh.Months.HasValue(3);
          Self.cbApr.Checked := rh.Months.HasValue(4);
          Self.cbMay.Checked := rh.Months.HasValue(5);
          Self.cbJun.Checked := rh.Months.HasValue(6);
          Self.cbJul.Checked := rh.Months.HasValue(7);
          Self.cbAug.Checked := rh.Months.HasValue(8);
          Self.cbSep.Checked := rh.Months.HasValue(9);
          Self.cbOct.Checked := rh.Months.HasValue(10);
          Self.cbNov.Checked := rh.Months.HasValue(11);
          Self.cbDec.Checked := rh.Months.HasValue(12);
          {$ENDIF}
        end;
      end;

    // interpret exceptions within assigned View
      if Assigned( fView_RecurrencyExceptions ) then
        fView_RecurrencyExceptions.WriteRecurrencyHandlerToView( rh );
    finally
      rh.Free;
    end;
  end
  else begin
  // if Assigned( fFreq ) then
  //  fFreq.ItemIndex := 0;
  {$ifdef USING_FMX}
    Self.rbNone.IsChecked := True;
  {$else NOT USING_FMX}
    Self.rbNone.Checked := True;
  {$endif USING_FMX}
  // clear exceptions within assigned View
    if Assigned( fView_RecurrencyExceptions ) then
      fView_RecurrencyExceptions.WriteRecurrencyHandlerToView( nil );
  //end;
  end;
end;

// Update View with values read from planner item property values
function TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.ReadRecurrencyAsStringFromView: String;
var
  lIsRecurrent: Boolean;
  rh: TTMSFNCRecurrencyHandler;
  i,e: Integer;
  dn: TTMSFNCRecurrencyHandlerDayArray;
begin
  Result := EmptyStr;
{$ifdef USING_FMX}
  lIsRecurrent := NOT Self.rbNone.IsChecked;
{$else NOT USING_FMX}
  lIsRecurrent := NOT Self.rbNone.Checked;
{$endif NOT USING_FMX}

  if lIsRecurrent then begin
    rh := TTMSFNCRecurrencyHandler.Create;
    try
    {$ifdef USING_FMX}
      if Self.rbHourly.IsChecked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfHourly;
      end
      else
      if Self.rbDaily.IsChecked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfDaily;
      end
      else
      if Self.rbWeekly.IsChecked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfWeekly;
      end
      else
      if Self.rbMonthly.IsChecked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfMonthly;
      end
      else
      if Self.rbYearly.IsChecked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfYearly;
      end;
    {$else NOT USING_FMX}
      if Self.rbHourly.Checked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfHourly;
      end
      else
      if Self.rbDaily.Checked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfDaily;
      end
      else
      if Self.rbWeekly.Checked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfWeekly;
      end
      else
      if Self.rbMonthly.Checked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfMonthly;
      end
      else
      if Self.rbYearly.Checked then begin
        rh.Frequency := TTMSFNCRecurrencyFrequency.rfYearly;
      end;
    {$endif NOT USING_FMX}

      if Assigned( fView_RecurrencyExceptions ) then
        fView_RecurrencyExceptions.ReadRecurrencyHandlerFromView( rh );

      val(Self.edtInterval.Text,i,e);
      rh.Interval := i;
      val(Self.edtOccur.Text, i, e);

    {$ifdef USING_FMX}
      if Self.rbInfinite.IsChecked then begin
        rh.RepeatCount := 0;
        rh.RepeatUntil := 0;
      end else
      if Self.rbOcurrences.IsChecked then begin
          rh.RepeatCount := i
      end else begin
          rh.RepeatCount := 0;
      end;

      if Self.rbUntilDate.IsChecked then begin
        rh.RepeatUntil := Int(Self.deUntil.Date)
      end else begin
        rh.RepeatUntil := 0;
      end;
    {$else NOT USING_FMX}
      if Self.rbInfinite.Checked then begin
        rh.RepeatCount := 0;
        rh.RepeatUntil := 0;
      end else
      if Self.rbOcurrences.Checked then begin
          rh.RepeatCount := i
      end else begin
          rh.RepeatCount := 0;
      end;

      if Self.rbUntilDate.Checked then begin
        rh.RepeatUntil := Int(Self.deUntil.Date)
      end else begin
        rh.RepeatUntil := 0;
      end;
    {$endif NOT USING_FMX}

      rh.Days := [ ];

      if rh.Frequency = rfDaily then begin
      {$ifdef USING_FMX}
        if Self.rbEveryWeekday.IsChecked then
          rh.Days := [2,3,4,5,6];
      {$else NOT USING_FMX}
        if Self.rbEveryWeekDay.Checked then
          rh.Days := [2,3,4,5,6];
      {$endif NOT USING_FMX}
      end;

      if rh.Frequency = rfWeekly then
      begin
      {$ifdef USING_FMX}
        if Self.cbMon.IsChecked then
          rh.Days := rh.Days + [2];
        if Self.cbTue.IsChecked then
          rh.Days := rh.Days + [3];
        if Self.cbWed.IsChecked then
          rh.Days := rh.Days + [4];
        if Self.cbThu.IsChecked then
          rh.Days := rh.Days + [5];
        if Self.cbFri.IsChecked then
          rh.Days := rh.Days + [6];
        if Self.cbSat.IsChecked then
          rh.Days := rh.Days + [7];
        if Self.cbSun.IsChecked then
          rh.Days := rh.Days + [1];
      {$else NOT USING_FMX}
        if Self.cbMon.Checked then
          rh.Days := rh.Days + [2];
        if Self.cbTue.Checked then
          rh.Days := rh.Days + [3];
        if Self.cbWed.Checked then
          rh.Days := rh.Days + [4];
        if Self.cbThu.Checked then
          rh.Days := rh.Days + [5];
        if Self.cbFri.Checked then
          rh.Days := rh.Days + [6];
        if Self.cbSat.Checked then
          rh.Days := rh.Days + [7];
        if Self.cbSun.Checked then
          rh.Days := rh.Days + [1];
      {$endif NOT USING_FMX}
      end;

      if rh.Frequency = rfMonthly then begin
      {$ifdef USING_FMX}
        if Self.rbEveryMonth.IsChecked then
      {$else NOT USING_FMX}
        if Self.rbEveryMonth.Checked then
      {$endif NOT USING_FMX}
        begin
          for i := 1 to 7 do
            dn[i] := 0;

          case Self.cmbDayofWeekMonthly.ItemIndex of
          0:begin
              rh.Days := [2,3,4,5,6];
              dn[2] := Self.cmbWeekNumMonthly.ItemIndex + 1;
              dn[3] := Self.cmbWeekNumMonthly.ItemIndex + 1;
              dn[4] := Self.cmbWeekNumMonthly.ItemIndex + 1;
              dn[5] := Self.cmbWeekNumMonthly.ItemIndex + 1;
              dn[6] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          1:begin
              rh.Days := [1,7];
              dn[1] := Self.cmbWeekNumMonthly.ItemIndex + 1;
              dn[7] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          2:begin
              rh.Days := [2];
              dn[2] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          3:begin
              rh.Days := [3];
              dn[3] :=Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          4:begin
              rh.Days := [4];
              dn[4] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          5:begin
              rh.Days := [5];
              dn[5] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          6:begin
              rh.Days := [6];
              dn[6] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          7:begin
              rh.Days := [7];
              dn[7] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;
          8:begin
              rh.Days := [1];
              dn[1] := Self.cmbWeekNumMonthly.ItemIndex + 1;
            end;

          end;
          for i := 1 to 7 do
            rh.DayNum[i] := dn[i];
        end;
      end;

      if rh.Frequency = rfYearly then begin
      {$ifdef USING_FMX}
        if Self.rbEveryYear.IsChecked then
      {$else NOT USING_FMX}
        if Self.rbEveryYear.Checked then
      {$endif NOT USING_FMX}
        begin
          for i := 1 to 7 do dn[i] := 0;

          case Self.cmbDayofWeekYearly.ItemIndex of
          0:begin
              rh.Days := [2,3,4,5,6];
              dn[2] := Self.cmbWeekNumYearly.ItemIndex + 1;
              dn[3] := Self.cmbWeekNumYearly.ItemIndex + 1;
              dn[4] := Self.cmbWeekNumYearly.ItemIndex + 1;
              dn[5] := Self.cmbWeekNumYearly.ItemIndex + 1;
              dn[6] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          1:begin
              rh.Days := [1,7];
              dn[1] := Self.cmbWeekNumYearly.ItemIndex + 1;
              dn[7] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          2:begin
              rh.Days := [2];
              dn[2] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          3:begin
              rh.Days := [3];
              dn[3] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          4:begin
              rh.Days := [4];
              dn[4] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          5:begin
              rh.Days := [5];
              dn[5] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          6:begin
              rh.Days := [6];
              dn[6] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          7:begin
              rh.Days := [7];
              dn[7] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;
          8:begin
              rh.Days := [1];
              dn[1] := Self.cmbWeekNumYearly.ItemIndex + 1;
            end;

          end;
          for i := 1 to 7 do
            rh.DayNum[i] := dn[i];
        end;

        rh.Months.Clear;
      {$ifdef USING_FMX}
        if Self.cbJan.IsChecked then
          rh.Months.Add(1);
        if Self.cbFeb.IsChecked then
          rh.Months.Add(2);
        if Self.cbMar.IsChecked then
          rh.Months.Add(3);
        if Self.cbApr.IsChecked then
          rh.Months.Add(4);
        if Self.cbMay.IsChecked then
          rh.Months.Add(5);
        if Self.cbJun.IsChecked then
          rh.Months.Add(6);
        if Self.cbJul.IsChecked then
          rh.Months.Add(7);
        if Self.cbAug.IsChecked then
          rh.Months.Add(8);
        if Self.cbSep.IsChecked then
          rh.Months.Add(9);
        if Self.cbOct.IsChecked then
          rh.Months.Add(10);
        if Self.cbNov.IsChecked then
          rh.Months.Add(11);
        if Self.cbDec.IsChecked then
          rh.Months.Add(12);
      {$else NOT USING_FMX}
        if Self.cbJan.Checked then
          rh.Months.Add(1);
        if Self.cbFeb.Checked then
          rh.Months.Add(2);
        if Self.cbMar.Checked then
          rh.Months.Add(3);
        if Self.cbApr.Checked then
          rh.Months.Add(4);
        if Self.cbMay.Checked then
          rh.Months.Add(5);
        if Self.cbJun.Checked then
          rh.Months.Add(6);
        if Self.cbJul.Checked then
          rh.Months.Add(7);
        if Self.cbAug.Checked then
          rh.Months.Add(8);
        if Self.cbSep.Checked then
          rh.Months.Add(9);
        if Self.cbOct.Checked then
          rh.Months.Add(10);
        if Self.cbNov.Checked then
          rh.Months.Add(11);
        if Self.cbDec.Checked then
          rh.Months.Add(12);
      {$endif NOT USING_FMX}
      end;

    // Cache last recurrency as String
      Result := rh.Compose;
    finally
      rh.Free;
    end;
  end;
end;


// -----------------------------------------------------------------------------
// local control methods

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbDailyChange(Sender: TObject);
begin
  pnlYearly.Visible := False;
  pnlMonthly.Visible := False;
  pnlWeekly.Visible := False;
  pnlDaily.Visible := True;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbHourlyChange(Sender: TObject);
begin
  pnlYearly.Visible := False;
  pnlMonthly.Visible := False;
  pnlWeekly.Visible := False;
  pnlDaily.Visible := False;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbMonthlyChange(Sender: TObject);
begin
  pnlYearly.Visible := False;
  pnlMonthly.Visible := True;
  pnlWeekly.Visible := False;
  pnlDaily.Visible := False;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbNoneChange(Sender: TObject);
begin
  pnlYearly.Visible := False;
  pnlMonthly.Visible := False;
  pnlWeekly.Visible := False;
  pnlDaily.Visible := False;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbWeeklyChange(Sender: TObject);
begin
  pnlYearly.Visible := False;
  pnlMonthly.Visible := False;
  pnlWeekly.Visible := True;
  pnlDaily.Visible := False;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.rbYearlyChange(Sender: TObject);
begin
  pnlYearly.Visible := True;
  pnlMonthly.Visible := False;
  pnlWeekly.Visible := False;
  pnlDaily.Visible := False;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency.ShowLayouts( aIndex: Integer );
begin
//  FWeekL.Visible := False;
//  FDayL.Visible := False;
//  FYearL.Visible := False;
//  FDayWeekL.Visible := False;
//
//  case AIndex of
//  2: FDayWeekL.Visible := True;
//  3: FDayL.Visible := True;
//  4: FWeekL.Visible := True;
//  5: FYearL.Visible := True;
//  end;
end;


end.
