unit TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks;
// -----------------------------------------------------------------------------
// REFERNCES
//
//  1. Concerning Drag & Drop under VCL versus FMX, see:
//    - https://github.com/chrisrolliston/CCR.Clipboard/wiki/Drag-and-Drop
//    - http://blog.vdisasm.com/2015/04/drop-files-on-form-fmx-vs-vcl.html
//    - https://newsgroups.embarcadero.com/message.jspa?messageID=691386
//    - https://stackoverflow.com/questions/29364143/move-tabcontrol-tabitems-using-drag-and-drop
//    - http://www.swissdelphicenter.com/de/showcode.php?id=963
//
// -----------------------------------------------------------------------------
interface

{$ifdef fpc}
  {$define USING_LCL}
  {$undef USING_VCL}
  {$undef USING_FMX}
{$else NOT fpc}
  {$undef USING_LCL}
{$endif}

{$ifdef USING_LCL}
  {$mode objfpc}{$H+}
{$endif USING_LCL}

uses
{$ifndef USING_LCL}
  System.SysUtils
, System.Types
, System.UITypes
, System.Classes
, System.Variants
  {$ifdef USING_VCL}
, Vcl.Graphics
, Vcl.Controls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.ComCtrls
//
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlannerItemEditor
, VCL.TMSFNCPlannerItemEditorRecurrency
, VCL.TMSFNCRecurrencyHandler
  {$endif USING_VCL}
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Layouts
, Fmx.Controls
, Fmx.Forms
, Fmx.Dialogs
, Fmx.StdCtrls
, Fmx.ListBox
, Fmx.Edit
, Fmx.DateTimeCtrls
, Fmx.Controls.Presentation
//
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlannerItemEditor
, FMX.TMSFNCPlannerItemEditorRecurrency
, FMX.TMSFNCRecurrencyHandler
  {$endif USING_FMX}
{$else USING_LCL}
  SysUtils
, Types
, UITypes
, Classes
, Variants
//
, Graphics
, Controls
, Forms
, Dialogs
, StdCtrls
, ExtCtrls
, ComCntrls
//
, LCLTMSFNCPlanner
, LCLTMSFNCPlannerData
, LCLTMSFNCPlannerItemEditor
, LCLTMSFNCPlannerItemEditorRecurrency
, LCLTMSFNCRecurrencyHandler
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.View
;

type

{$ifndef USING_FMX}
  TDragOperation = ( Copy, None, Move, Link );
{$endif USING_FMX}

  TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks = class( TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View )
      ListBox1: TListBox;
      ListBox2: TListBox;
      Label1: TLabel;
      Label2: TLabel;
      procedure ListBox1DragOver( aSender: TObject; const aData: TDragObject;
        const aPoint: TPointF; var aOperation: TDragOperation );
      procedure ListBox2DragOver( aSender: TObject; const aData: TDragObject;
        const aPoint: TPointF; var aOperation: TDragOperation );
      procedure ListBox1DragDrop( aSender: TObject; const aData: TDragObject;
        const aPoint: TPointF );
      procedure ListBox2DragDrop( aSender: TObject; const aData: TDragObject;
        const aPoint: TPointF );
    private
    // local methods
      procedure FillListBox1;
      procedure CheckLB1ToLB2;
      //procedure CheckLB2toLB1;
    protected
    // reset controls within View
      procedure ResetControls; override;
    public
      //constructor Create( aOwner: TComponent ); override;
    // update View controls with the planner item property values
      procedure BindData_ItemToView( const aItem: TTMSFNCPlannerItem ); override;
    // update planner item property values with View's current control values
      procedure BindData_ViewToItem( {var} aItem: TTMSFNCPlannerItem ); override;
  end;

  TTMSCustomItemEditorView_Tasks = TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks;

implementation

{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}
{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}

uses
// Rework into Kennedy's & Sarah's 2017 Summer efforts
  Sample.View.PlannerItem
;

const
  ArrayTasks: array[0..4] of String = (
    'Cleaning',
    'Door Monitor',
    'Orient New Students',
    'Paperwork/New Binders',
    'Print interim'
  );

// -----------------------------------------------------------------------------
// existential methods

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.ResetControls;
begin
  Name := 'Tasks';
//
  Self.FillListBox1;
end;


// -----------------------------------------------------------------------------
// aData binding to/from View controls from/to planner item

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.BindData_ItemToView(
  const aItem: TTMSFNCPlannerItem );
var
  I: Integer;
  lCustomPlannerItem: TSampleViewModelPlannerItem;
begin
  //inherited; // its abstract!!!

  if Assigned( aItem ) then begin
  // To get the other properties of the planner item
    if ( Assigned( aItem.DataObject ) ) AND
       ( aItem.DataObject.InheritsFrom( TSampleViewModelPlannerItem ) ) then begin
      lCustomPlannerItem := aItem.DataObject as TSampleViewModelPlannerItem;

      Self.ListBox2.Items.Clear;
      Self.FillListBox1;

      for I := 0 to lCustomPlannerItem.Tasks.Count-1 do begin
        Self.ListBox2.Items.Add( lCustomPlannerItem.Tasks[ I ] );
      end;
      Self.CheckLB1ToLB2;
    end;
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.BindData_ViewToItem(
  {var} aItem: TTMSFNCPlannerItem );
var
  lCustomPlannerItem: TSampleViewModelPlannerItem;
  lListItemIndex: Integer;
begin
  inherited;
  if Assigned ( aItem.DataObject ) then begin
  // Skip if NOT TSampeViewModelPlannerItem (other types yet not supported)
    if aItem.DataObject is TSampleViewModelPlannerItem then begin
      lCustomPlannerItem := aItem.DataObject as TSampleViewModelPlannerItem;

      lCustomPlannerItem.Tasks.Clear;
      for lListItemIndex := 0 to Self.ListBox2.Items.Count-1 do begin
        lCustomPlannerItem.Tasks.Add( Self.ListBox2.Items[ lListItemIndex ] );
      end;
    end;
  end;
end;


// -----------------------------------------------------------------------------
// local control methods

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.FillListBox1;
var
  I: Integer;
{$ifdef USING_VCL}
  lLBItem: TListBoxItemData; // REWORK: Their is NO TListBoxItem in the VCL!!
{$endif USING_VCL}
{$ifdef USING_FMX}
  lLBItem: TListBoxItem;
{$endif USING_FMX}
begin
  for I := Low( ArrayTasks ) to High( ArrayTasks ) do begin
  {$ifdef USING_VCL}
    if Self.ListBox1.Items.IndexOf( ArrayTasks[I] ) = -1 then begin
      Self.ListBox1.Items.Add( ArrayTasks[I] );
    end;
  {$endif USING_VCL}
  {$ifdef USING_FMX}
    lLBItem := TListBoxItem.Create( Self );
    lLBItem.Text := ArrayTasks[I];
    if Self.ListBox1.Items.IndexOf( lLBItem.Text ) = -1 then begin
      Self.ListBox1.Items.AddObject( lLBItem.Text, lLBItem );
    end;
  {$endif USING_FMX}
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.CheckLB1ToLB2;
var
  I, Index: Integer;
  lTaskName: String;
begin
  for I := 0 to Self.ListBox2.Items.Count-1 do begin
    lTaskName := Self.ListBox2.Items[I];
    if Self.ListBox1.Items.IndexOf( lTaskName ) <> -1 then begin
      Index := Self.ListBox1.Items.IndexOf( lTaskName );
      Self.ListBox1.Items.Delete( Index );
    end;
  end;
end;

//procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.CheckLB2toLB1;
//var
//  I, Index: Integer;
//  lTaskName: string;
//begin
//  for I := 0 to Self.ListBox1.Items.Count-1 do begin
//    lTaskName := Self.ListBox1.Items[ I ];
//    if Self.ListBox2.Items.IndexOf( lTaskName ) <> -1 then begin
//      Index := Self.ListBox2.Items.IndexOf( lTaskName );
//      Self.ListBox2.Items.Delete( Index );
//    end;
//  end;
//end;


// -----------------------------------------------------------------------------
// drag & drop methods

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.ListBox1DragDrop( aSender: TObject;
  const aData: TDragObject; const aPoint: TPointF );
var
  lListBox: TListBox;
{$ifdef USING_VCL}
  lDragItem: TListBoxItemData;
{$endif USING_VCL}
{$ifdef USING_FMX}
  lDragItem: TListBoxItem;
{$endif USING_FMX}
  lItemIndex: Integer;
begin
  if aSender is TListBox then begin
    lListBox := aSender as TListBox;
{$ifdef USING_VCL}
//    if aData.Source is TListBoxItemData then begin
//      lDragItem := aData.Source as TListBoxItemData;
//    end;
{$endif USING_VCL}
{$ifdef USING_FMX}
    if aData.Source is TListBoxItem then begin
      lDragItem := aData.Source as TListBoxItem;
      if lListBox.Items.IndexOf( lDragItem.Text ) = -1 then begin
        lItemIndex := lListBox.Items.Add( lDragItem.Text );
        if lItemIndex <> -1 then begin
          // If item was successfully moved, then remove item in other listbox
          Self.ListBox2.RemoveObject( aData.Source as TFmxObject );
        end;
      end;
    end;
{$endif USING_FMX}
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.ListBox1DragOver(
  aSender: TObject; const aData: TDragObject; const aPoint: TPointF; var aOperation: TDragOperation);
begin
  if aSender is TListBox then begin
    aOperation := TDragOperation.Move;
    // Can't assign to Data, Data.Source or Data.Data
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.ListBox2DragDrop(
  aSender: TObject; const aData: TDragObject; const aPoint: TPointF);
var
  lListBox: TListBox;
{$ifdef USING_VCL}
  lDragItem: TListBoxItemData;
{$endif USING_VCL}
{$ifdef USING_FMX}
  lDragItem: TListBoxItem;
{$endif USING_FMX}
  lItemIndex: Integer;
  //lString: string;
begin
  if aSender is TListBox then begin
    lListBox := aSender as TListBox;
{$ifdef USING_FMX}
    if aData.Source is TListBoxItem then begin
      lDragItem := aData.Source as TListBoxItem;
      if lListBox.Items.IndexOf( lDragItem.Text ) = -1 then begin
        lItemIndex := lListBox.Items.Add( lDragItem.Text );
        if lItemIndex <> -1 then begin
          // If item was successfully moved, then remove item in other listbox
          Self.ListBox1.RemoveObject(aData.Source as TFmxObject);
        end;
      end;
    end;
{$endif USING_FMX}
  end;
end;

procedure TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks.ListBox2DragOver(
  aSender: TObject; const aData: TDragObject; const aPoint: TPointF; var aOperation: TDragOperation );
begin
  if aSender is TListBox then begin
    aOperation := TDragOperation.Move;
  end;
end;


end.
