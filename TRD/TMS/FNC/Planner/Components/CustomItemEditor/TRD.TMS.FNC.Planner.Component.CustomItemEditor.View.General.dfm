inherited TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General: TTRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General
  Width = 425
  Height = 401
  ExplicitWidth = 425
  ExplicitHeight = 401
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 49
    Height = 13
    Caption = 'Start Time'
  end
  object Label2: TLabel
    Left = 8
    Top = 40
    Width = 43
    Height = 13
    Caption = 'End Time'
  end
  object Label3: TLabel
    Left = 8
    Top = 72
    Width = 49
    Height = 13
    Caption = 'Time Block'
  end
  object Label4: TLabel
    Left = 8
    Top = 256
    Width = 28
    Height = 13
    Caption = 'Notes'
  end
  object Label5: TLabel
    Left = 8
    Top = 104
    Width = 46
    Height = 13
    Caption = 'Employee'
  end
  object Label6: TLabel
    Left = 8
    Top = 136
    Width = 45
    Height = 13
    Caption = 'Resource'
  end
  object Label7: TLabel
    Left = 8
    Top = 168
    Width = 35
    Height = 13
    Caption = 'Gender'
  end
  object Label8: TLabel
    Left = 8
    Top = 200
    Width = 25
    Height = 13
    Caption = 'Color'
  end
  object Label9: TLabel
    Left = 8
    Top = 232
    Width = 48
    Height = 13
    Caption = 'Title Color'
  end
  object deStartDate: TDateTimePicker
    Left = 72
    Top = 8
    Width = 186
    Height = 21
    Date = 42572.098039699070000000
    Time = 42572.098039699070000000
    TabOrder = 0
  end
  object deEndDate: TDateTimePicker
    Left = 72
    Top = 40
    Width = 186
    Height = 21
    Date = 42572.098039699070000000
    Time = 42572.098039699070000000
    TabOrder = 1
  end
  object mmNotes: TMemo
    Left = 8
    Top = 280
    Width = 297
    Height = 105
    TabOrder = 5
  end
  object teStartTime: TDateTimePicker
    Left = 200
    Top = 8
    Width = 186
    Height = 21
    Date = 42966.409027777780000000
    Time = 42966.409027777780000000
    Kind = dtkTime
    TabOrder = 10
  end
  object teEndTime: TDateTimePicker
    Left = 200
    Top = 40
    Width = 186
    Height = 21
    Date = 42966.409027777780000000
    Time = 42966.409027777780000000
    Kind = dtkTime
    TabOrder = 2
  end
  object cmbEmployee: TComboBox
    Left = 72
    Top = 104
    Width = 225
    Height = 21
    TabOrder = 3
  end
  object cmbResource: TComboBox
    Left = 72
    Top = 136
    Width = 113
    Height = 21
    TabOrder = 4
    Items.Strings = (
      'Resource 0'
      'Resource 1'
      'Resource 2'
      'Resource 3'
      'Resource 4'
      'Resource 5'
      'Resource 6'
      'Resource 7')
  end
  object cmbGender: TComboBox
    Left = 72
    Top = 168
    Width = 113
    Height = 21
    TabOrder = 6
  end
  object cpColor: TTMSFNCColorPicker
    Left = 72
    Top = 200
    Width = 60
    Height = 22
    ParentDoubleBuffered = False
    DoubleBuffered = True
    TabOrder = 7
    Appearance.Stroke.Color = 11119017
    Appearance.Fill.Color = 15329769
    Appearance.StrokeHover.Color = 10061943
    Appearance.FillHover.Color = 13419707
    Appearance.StrokeDown.Color = 9470064
    Appearance.FillDown.Color = 13156536
    Appearance.StrokeSelected.Color = 5197615
    Appearance.FillSelected.Color = 13156536
    Appearance.StrokeDisabled.Color = 11119017
    Appearance.FillDisabled.Color = clSilver
    Appearance.SeparatorStroke.Color = 11119017
    Appearance.Font.Charset = DEFAULT_CHARSET
    Appearance.Font.Color = clWindowText
    Appearance.Font.Height = -11
    Appearance.Font.Name = 'Segoe UI'
    Appearance.Font.Style = []
    Items = <
      item
        Color = clBlack
      end
      item
        Color = clMaroon
      end
      item
        Color = clGreen
      end
      item
        Color = clOlive
      end
      item
        Color = clNavy
      end
      item
        Color = clPurple
      end
      item
        Color = clTeal
      end
      item
        Color = clSilver
      end
      item
        Color = clGray
      end
      item
        Color = clRed
      end
      item
        Color = clLime
      end
      item
        Color = clYellow
      end
      item
        Color = clBlue
      end
      item
        Color = clFuchsia
      end
      item
        Color = clAqua
      end
      item
        Color = clWhite
      end>
  end
  object cpTitleColor: TTMSFNCColorPicker
    Left = 72
    Top = 232
    Width = 60
    Height = 22
    ParentDoubleBuffered = False
    DoubleBuffered = True
    TabOrder = 8
    Appearance.Stroke.Color = 11119017
    Appearance.Fill.Color = 15329769
    Appearance.StrokeHover.Color = 10061943
    Appearance.FillHover.Color = 13419707
    Appearance.StrokeDown.Color = 9470064
    Appearance.FillDown.Color = 13156536
    Appearance.StrokeSelected.Color = 5197615
    Appearance.FillSelected.Color = 13156536
    Appearance.StrokeDisabled.Color = 11119017
    Appearance.FillDisabled.Color = clSilver
    Appearance.SeparatorStroke.Color = 11119017
    Appearance.Font.Charset = DEFAULT_CHARSET
    Appearance.Font.Color = clWindowText
    Appearance.Font.Height = -11
    Appearance.Font.Name = 'Segoe UI'
    Appearance.Font.Style = []
    Items = <
      item
        Color = clBlack
      end
      item
        Color = clMaroon
      end
      item
        Color = clGreen
      end
      item
        Color = clOlive
      end
      item
        Color = clNavy
      end
      item
        Color = clPurple
      end
      item
        Color = clTeal
      end
      item
        Color = clSilver
      end
      item
        Color = clGray
      end
      item
        Color = clRed
      end
      item
        Color = clLime
      end
      item
        Color = clYellow
      end
      item
        Color = clBlue
      end
      item
        Color = clFuchsia
      end
      item
        Color = clAqua
      end
      item
        Color = clWhite
      end>
  end
  object cmbTimeBlock: TComboBox
    Left = 72
    Top = 72
    Width = 225
    Height = 21
    TabOrder = 9
  end
end
