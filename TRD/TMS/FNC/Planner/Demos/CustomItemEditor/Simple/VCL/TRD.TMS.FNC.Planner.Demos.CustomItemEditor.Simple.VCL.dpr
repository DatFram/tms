program TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Simple.VCL;

uses
  VCL.Forms,
  VCL.Dialogs,
  TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Simple.View in 'TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Simple.View.pas' {Form130},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View: TFrame},
  Sample.View.PlannerItem._Interface in '..\..\..\..\Component\CustomItemEditor\Sample.View.PlannerItem._Interface.pas',
  Sample.View.PlannerItem in '..\..\..\..\Component\CustomItemEditor\Sample.View.PlannerItem.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.FMX in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.FMX.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks in '..\..\..\..\Component\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks: TFrame};

{$R *.res}

begin
  Application.MainFormOnTaskbar := True;
//
  Application.Initialize;
  Application.CreateForm(TForm130, Form130);
  try
    Application.Run;
  except
    ShowMessage( 'ERROR: Unhandled exception raised in application' );
  end;
end.
