unit TRD.TMS.FNC.Planner.CustomItem;

interface

uses
{$ifndef USING_LCL}
// For all Delphi-based compilations
  System.SysUtils
, System.Classes
, System.Generics.Collections
//
, Data.DB
  {$ifdef USING_FMX}
//
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCPlannerData
  {$endif USING_FMX}
  {$ifdef USING_VCL}
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCPlannerData
  {$endif USING_VCL}
{$else USING_LCL}
// For all Lazarus-based compilations
  SysUtils
, Classes
//
, DB
//
, LCLTMSFNCGraphicsTypes
, LCLTMSFNCPlannerData
{$endif USING_LCL}
;


type

  ITRD_TMS_Types_Structured_PlannerItem = interface( IInterface )
    // item adapter event handlers
      procedure DoOnItemToFields( aSender: TObject; aItem: TTMSFNCPlannerItem; aFields: TFields ); //virtual; //abstract;
      procedure DoOnFieldsToItem( aSender: TObject; aFields: TFields; aItem: TTMSFNCPlannerItem ); //virtual; //abstract;
      procedure DoOnCreateDBKey( aSender: TObject; aItem: TTMSFNCPlannerItem; var aDBKey: string ); //virtual; //abstract;
      procedure DoOnItemCreated( aSender: TObject; aItem: TTMSFNCPlannerItem ); //virtual; //abstract; // inserted
      procedure DoOnItemRead( aSender: TObject; aItem: TTMSFNCPlannerItem ); //virtual; //abstract;
      procedure DoOnItemUpdated( aSender: TObject; aItem: TTMSFNCPlannerItem ); //virtual; //abstract;
      procedure DoOnItemsLoaded( aSender: TObject ); //virtual; //abstract;
    // data field accessors required by IRFI_Types_Structured_PlannerItem
      function  GetDBKey( ): TGUID;
      procedure SetDBKey( const aDBKey: TGUID );
      function  GetID( ): Integer;
      procedure SetID( const aID: Integer );
      function  GetTitle( ): String;
      procedure SetTitle( const aTitle: String );
      function  GetStartTime( ): TDateTime;
      procedure SetStartTime( const aStartTime: TDateTime );
      function  GetEndTime( ): TDateTime;
      procedure SetEndTime( const aEndTime: TDateTime );
      function  GetText( ): String;
      procedure SetText( const aText: String );
      function  GetColor( ): LongWord;
      procedure SetColor( const aColor: LongWord );
      function  GetResource( ): Integer;
      procedure SetResource( const aResource: Integer );
      // string array to enable quick lookup on Item
      function  GetResourcesAsString( ): TArray< String >;
      procedure SetResourcesAsString( const aResourcesAsString: TArray< String > );
      function  GetRecurrency( ): String;
      procedure SetRecurrency( const aRecurrency: String );
    // public properties exposed by IRFI_Types_Structured_PlannerItem
      property DBKey: TGUID read GetDBKey write SetDBKey;
      property ID: Integer read GetID write SetID;
      property Title: String read GetTitle write SetTitle;
      property StartTime: TDateTime read GetStartTime write SetStartTime;
      property EndTime: TDateTime read GetEndTime write SetEndTime;
      property Text: String read GetText write SetText;
      property Color: LongWord read GetColor write SetColor;
      property Resource: Integer read GetResource write SetResource;
      property ResourcesAsString: TArray< String > read GetResourcesAsString write SetResourcesAsString;
      property Recurrency: String read GetRecurrency write SetRecurrency;
  end;

  TObjectMapper< ObjectType, DataSetType: class > = class
  // ToDo: use Daniele Teti's ObjectMapper unit from the DMVCFramework
    public
      procedure ClassInstancesToDataset( const aObject: TList< ObjectType >; out aDataSet: DataSetType ); virtual; abstract;
      procedure DataSetToClassInstances( out aDataSet: DataSetType; out aObject: TList< ObjectType > ); virtual; abstract;
  end;

// unit's public API
  function Create_PlannerItemInterface( const aPlannerItem: TTMSFNCPlannerItem ): ITRD_TMS_Types_Structured_PlannerItem;

implementation

//const PlannerItems: Array[ 0..10 ] of TTRD_TMS_Types_Structured_PlannerItem = ;


// =============================================================================

{ TTRD_TMS_Types_Structured_PlannerItem }

type

// ToDo: BEGIN: Move to own module *********************************************
  TTRD_TMS_Types_Structured_PlannerItem = class( {TTMSFNCPlannerItem}TInterfacedObject, ITRD_TMS_Types_Structured_PlannerItem )
    private
      fPlannerItem: TTMSFNCPlannerItem;
      fPlannerItemOwned: Boolean;
      fResourcesAsString: TArray< String >; // To enable quick lookup on Item
    protected
    // item adapter event handlers
      procedure DoOnItemToFields( aSender: TObject; aItem: TTMSFNCPlannerItem; aFields: TFields ); virtual; //abstract;
      procedure DoOnFieldsToItem( aSender: TObject; aFields: TFields; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnCreateDBKey( aSender: TObject; aItem: TTMSFNCPlannerItem; var aDBKey: string ); virtual; //abstract;
      procedure DoOnItemCreated( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract; // inserted
      procedure DoOnItemRead( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnItemUpdated( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnItemsLoaded( aSender: TObject ); virtual; //abstract;
    // data field accessors required by IRFI_Types_Structured_PlannerItem
      function  GetDBKey( ): TGUID;
      procedure SetDBKey( const aDBKey: TGUID );
      function  GetID( ): Integer;
      procedure SetID( const aID: Integer );
      function  GetTitle( ): String;
      procedure SetTitle( const aTitle: String );
      function  GetStartTime( ): TDateTime;
      procedure SetStartTime( const aStartTime: TDateTime );
      function  GetEndTime( ): TDateTime;
      procedure SetEndTime( const aEndTime: TDateTime );
      function  GetText( ): String;
      procedure SetText( const aText: String );
      function  GetColor( ): LongWord;
      procedure SetColor( const aColor: LongWord );
      function  GetResource( ): Integer;
      procedure SetResource( const aResource: Integer );
      // string array to enable quick lookup on Item
      function  GetResourcesAsString( ): TArray< String >;
      procedure SetResourcesAsString( const aResourcesAsString: TArray< String > );
      function  GetRecurrency( ): String;
      procedure SetRecurrency( const aRecurrency: String );
    public
    // existential methods
      constructor Create( const aPlannerItem: TTMSFNCPlannerItem ); overload;
      constructor Create( const aGUID: TGUID ); overload;
      constructor Create( const aGUIDAsString: String ); overload;
      destructor Destroy( ); override;
    // public properties exposed by IRFI_Types_Structured_PlannerItem
      property DBKey: TGUID read GetDBKey write SetDBKey;
      property ID: Integer read GetID write SetID;
      property Title: String read GetTitle write SetTitle;
      property StartTime: TDateTime read GetStartTime write SetStartTime;
      property EndTime: TDateTime read GetEndTime write SetEndTime;
      property Text: String read GetText write SetText;
      property Color: LongWord read GetColor write SetColor;
      property Resource: Integer read GetResource write SetResource;
      property ResourcesAsString: TArray< String > read GetResourcesAsString write SetResourcesAsString;
      property Recurrency: String read GetRecurrency write SetRecurrency;
  end;
// ToDo: END: Move to own module ***********************************************


// -----------------------------------------------------------------------------
// existential methods

constructor TTRD_TMS_Types_Structured_PlannerItem.Create( const aPlannerItem: TTMSFNCPlannerItem );
begin
  fPlannerItemOwned := False;
  fPlannerItem := aPlannerItem;
  if NOT Assigned( aPlannerItem ) then begin
    raise Exception.Create( 'ERROR: aPlannerItem not defined' );
  end;
  fPlannerItemOwned := True;
end;

constructor TTRD_TMS_Types_Structured_PlannerItem.Create( const aGUID: TGUID );
var
  lGUID: TGUID;
begin
  if aGUID = TGUID.Empty then begin
    CreateGUID( lGUID );
  end
  else begin
    lGUID := aGUID;
  end;
  fPlannerItem := TTMSFNCPlannerItem.Create( nil );
  fPlannerItemOwned := True;
  DBKey             := lGUID;
  Title             := 'A Title';
  StartTime         := Int( Now ) + (12.0 / 24.0 ); // Redo as EncodeTime( Hour( Now )
  EndTime           := Int( Now ) + (13.0 / 24.0 );
  Text              := 'Some text';
  Color             := gcRed;
  Resource          := -1; // out of range!
  ResourcesAsString := [ ]; // out of range!
  Recurrency        := EmptyStr; // none
end;

constructor TTRD_TMS_Types_Structured_PlannerItem.Create( const aGUIDAsString: String );
var
  lGUID: TGUID;
begin
  if aGUIDAsString.IsEmpty {ToDO: OR invalid} then begin
    CreateGUID( lGUID );
  end
  else begin
    lGUID := StringToGUID( aGUIDAsString );
  end;
  fPlannerItem := TTMSFNCPlannerItem.Create( nil );
  fPlannerItemOwned := True;
  DBKey             := lGUID;
  Title             := 'A Title';
  StartTime         := Int( Now ) + (12.0 / 24.0 ); // Redo as EncodeTime( Hour( Now )
  EndTime           := Int( Now ) + (13.0 / 24.0 );
  Text              := 'Some text';
  Color             := gcRed;
  Resource          := -1; // out of range!
  ResourcesAsString := [ ]; // out of range!
  Recurrency        := EmptyStr; // none
end;

destructor TTRD_TMS_Types_Structured_PlannerItem.Destroy( );
begin
  if fPlannerItemOwned then begin
    fPlannerItem.Free;
  end;
end;


// -----------------------------------------------------------------------------
// data field accessors

function TTRD_TMS_Types_Structured_PlannerItem.GetDBKey: TGUID;
begin
  Result := StringToGUID( fPlannerItem.DBKey );
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetDBKey( const aDBKey: TGUID );
begin
  fPlannerItem.DBKey := GUIDtoString( aDBKey );
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetID: Integer;
begin
  Result := fPlannerItem.ID;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetID( const aID: Integer );
begin
  //fPlannerItem.ID := aID;
  raise Exception.Create( 'ERROR: ID is read-only' );
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetTitle: String;
begin
  Result := fPlannerItem.Title;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetTitle( const aTitle: String );
begin
  fPlannerItem.Title := aTitle;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetStartTime: TDateTime;
begin
  Result := fPlannerItem.StartTime;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetStartTime( const aStartTime: TDateTime );
begin
  fPlannerItem.StartTime := aStartTime;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetEndTime: TDateTime;
begin
  Result := fPlannerItem.EndTime;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetEndTime( const aEndTime: TDateTime );
begin
  fPlannerItem.EndTime := aEndTime;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetText: String;
begin
  Result := fPlannerItem.Text;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetText( const aText: String );
begin
  fPlannerItem.Text := aText;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetColor: LongWord;
begin
  Result := fPlannerItem.Color;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetColor( const aColor: LongWord );
begin
  fPlannerItem.Color := aColor;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetResource: Integer;
begin
  Result := fPlannerItem.Resource;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetResource( const aResource: Integer );
begin
  fPlannerItem.Resource := aResource;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetResourcesAsString: TArray<String>;
begin
  Result := fResourcesAsString;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetResourcesAsString( const aResourcesAsString: TArray<String> );
begin
  fResourcesAsString := aResourcesAsString;
end;

function TTRD_TMS_Types_Structured_PlannerItem.GetRecurrency: String;
begin
  Result := fPlannerItem.Recurrency;
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.SetRecurrency( const aRecurrency: String );
begin
  fPlannerItem.Recurrency := aRecurrency;
end;


// -----------------------------------------------------------------------------
// event handlers for item conversion and update

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnCreateDBKey( aSender: TObject;
  aItem: TTMSFNCPlannerItem; var aDBKey: String );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnFieldsToItem( aSender: TObject; aFields: TFields;
  aItem: TTMSFNCPlannerItem );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnItemCreated( aSender: TObject;
  aItem: TTMSFNCPlannerItem );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnItemRead( aSender: TObject;
  aItem: TTMSFNCPlannerItem );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnItemsLoaded( aSender: TObject );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnItemToFields( aSender: TObject;
  aItem: TTMSFNCPlannerItem; aFields: TFields );
begin
  //TBD
end;

procedure TTRD_TMS_Types_Structured_PlannerItem.DoOnItemUpdated( aSender: TObject;
  aItem: TTMSFNCPlannerItem );
begin
  //TBD
end;


// =============================================================================
// IRFI_Types_Structured_PlannerItem;

function Create_PlannerItemInterface( const aPlannerItem: TTMSFNCPlannerItem ): ITRD_TMS_Types_Structured_PlannerItem;
begin
  Result := TTRD_TMS_Types_Structured_PlannerItem.Create( aPlannerItem );
end;


end.
