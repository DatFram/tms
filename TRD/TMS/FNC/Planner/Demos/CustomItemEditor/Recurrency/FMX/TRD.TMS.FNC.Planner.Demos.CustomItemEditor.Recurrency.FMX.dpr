program TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.FMX;

uses
  System.StartUpCopy,
  FMX.Forms,
  Sample.View.PlannerItem._Interface in '..\..\..\..\Components\CustomItemEditor\Sample.View.PlannerItem._Interface.pas',
  Sample.View.PlannerItem in '..\..\..\..\Components\CustomItemEditor\Sample.View.PlannerItem.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.FMX in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.FMX.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL.pas',
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Exceptions.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Exceptions: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.General.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_General: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Recurrency.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Recurrency: TFrame},
  TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks in '..\..\..\..\Components\CustomItemEditor\TRD.TMS.FNC.Planner.Component.CustomItemEditor.View.Tasks.pas' {TRD_TMS_FNC_Planner_Component_CustomItemEditor_View_Tasks: TFrame},
  TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.Model in '..\TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.Model.pas' {Model: TDataModule},
  TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.ViewModel in '..\TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.ViewModel.pas' {ViewModel: TDataModule},
  TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View in '..\TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View.pas' {Form14: TDataModule};

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := True;
  Application.Initialize;
  Application.CreateForm(TForm14, Form14);
  Application.Run;
end.
