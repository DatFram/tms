unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.DataAccess;
// -----------------------------------------------------------------------------
// PURPOSE
//
//  This unit provides dataset-centric access to underlying datastore (that is,
//  it provides record-oriented C/R/U/D access to file, database, and/or
//  cloud-based datastores through a name-based registry of TDataSet datastores.
//
//  For each dataset placed onto the design surface associated with this class
//  at design-time OR created at runtime BEFORE (a TDataregistered within the underlying DataAccess layer, a
//  TObjectList of type TEntity<DataStoreName> is constructed via an
//  Object-Relationship Mapper (ORM) for object-orientated access to datasets.
//
//
// -----------------------------------------------------------------------------
// NOTICE
//
//  NO PRESENTATION-LAYER DEPENDENCIES ARE PERMITTED HEREIN.
//
// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. Concerning smart records (like smart pointers), and:
//    - http://www.thedelphigeek.com/2015/01/implementing-destructor-for-record.html
//
// -----------------------------------------------------------------------------
interface

uses
{$ifndef USING_LCL}
// For all Delphi-based compilations
  System.SysUtils
, System.Classes
, System.Generics.Collections
//
, Data.DB
{$else USING_LCL}
// For all Lazarus-based compilations
  SysUtils
, Classes
//
, DB
{$endif USING_LCL}
;

const
  gkMockDatastore = 'DataAccess_MockDatastore';
  gkMockDatastores: Array[ 0..0 ] of String = ( gkMockDatastore );

type

  TConnectionParameter = TPair< String, String >; // Parameter|Key := Value
  TConnectionParameters = TArray< TConnectionParameter >; // Array of Parameter|Key := Value

  TNameValueCriterion = TPair< String, String >; // Field|Key := Value
  TNameValueCriteria = TArray< TNameValueCriterion >; // Array of Field|Key := Value

  TDataSetRegistry = TObjectDictionary< String, TDataSet >;

  TDataAccess = class( TDataModule )
    procedure DoOnDataAccessCreate( aSender: TObject );
    procedure DoOnDataAccessDestroy( aSender: TObject );
    private
      fHasMockDatastore: Boolean;
      fDatastores: TDataSetRegistry;
    // `--- ToDo: Turn each dataset into a record with a field map translator
    //            interface as paired to-from methods for use by Model layer's
    //            entity classes as ORM) <-- will need DatastoreName to Class map first
    protected
    //
      function mGetDatastore: TDataSetRegistry;
    //
      function Datastore_FindByName( const aDatastoreName: String; const aRaiseExceptions: Boolean = True ): TDataset;
      function Datastore_TryFindByName( const aDatastoreName: String; out aDatastore: TDataSet ): Boolean;
    public
    // existential methods
      constructor Create( aOwner: TComponent ); override;
      destructor Destroy( ); override;
    // public API methods - ToDo: to be added via helper class to TDataModule
      function  Datastore_AllAvailable_ListByClass: TArray< TDataSetClass >;
      function  Datastore_AllAvailable_ListByName: TArray< String >;
    // public API methods
      function  Datastore_AllConnected_ListByName: TArray< String >;
      // singular
      function  Datastore_Add( const aDatastoreName: String = gkMockDatastore ): TDataSet; overload; virtual;
      procedure Datastore_Connect( const aDatastoreName: String = gkMockDatastore; const aConnectionParameters: TConnectionParameters = [ ] ); overload; virtual;
      procedure Datastore_Disconnect( const aDatastoreName: String = gkMockDatastore; const aFlushAllUpdates: Boolean = True ); overload; virtual;
      function  Datastore_Read( const aDatastoreName: String = gkMockDatastore; const aNameValueCriteria: TNameValueCriteria = [ ] ): TDataSet; overload; virtual;
      procedure Datastore_Write( const aDatastoreName: String = gkMockDatastore; const aRecordSet: TDataSet = nil ); overload; virtual;
      // plural (ToDo: could make each final SO LONG AS implementation details is in the singular version)
      function  Datastore_Add( const aDatastoreNames: TArray< String > {= [ ]} ): TArray< TDataSet >; overload; virtual;
      procedure Datastore_Connect( const aDatastoreNames: TArray< String > {= [ ]} ); overload; virtual;
      procedure Datastore_Disconnect( const aDatastoreNames: TArray< String > {= [ ]}; const aFlushAllUpdates: Boolean = True ); overload; virtual;
//      function  Datastore_Read( const aDatastoreNames: TArray< String > {= [ ]}; const aNameValueCriteria: TNameValueCriteria = [ ] ): TArray< TDataSet >; overload; virtual;
//      procedure Datastore_Write( const aDatastoreNames: TArray< String > {= [ ]}; const aRecordSet: TDataSet = nil ); overload; virtual;
    // public properties
      property HasMockDatastore: Boolean read fHasMockDatastore;
      property Datastores: TDataSetRegistry read mGetDatastore;
  end;

implementation

// *****************************************************************************
// For more on ClassGroup pseudo-property of TDataModule and its framework-neutral
// configuration (namely, 'System.Classes.TPersistent') see:
// - http://docwiki.embarcadero.com/RADStudio/en/ClassGroup_pseudo-property_of_TDataModule
// *****************************************************************************
{%CLASSGROUP 'System.Classes.TPersistence'}
{$R *.dfm}
// *****************************************************************************

uses
//
{$ifndef USING_LCL}
  System.IOUtils                          // for TPath
//
, DataSnap.DBClient                       // for TClientDataSet
, FireDAC.Comp.Client                     // for TFDMemTable
{$else USING_LCL}
//, ????                                  // for TBufDataSet
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.DataAccess.MockDatastore
;

// =============================================================================

{ TDataAccess }

// -----------------------------------------------------------------------------
// existential methods

constructor TDataAccess.Create( aOwner: TComponent );
begin
  fHasMockDatastore := False;
// inject OR create any required dependencies BEFORE finishing construction of
// this data access layer in such are required by the DoOnDataAccessCreate
  // TBD
// call into construction lineage
  inherited Create( aOwner );
end;

procedure TDataAccess.DoOnDataAccessCreate( aSender: TObject );
begin
  Self.Datastore_Add(); // adds a "mock" database
end;

procedure TDataAccess.DoOnDataAccessDestroy( aSender: TObject );
begin
  Self.Datastore_Disconnect( Self.Datastore_AllConnected_ListByName );
end;

destructor TDataAccess.Destroy;
begin
  fDatastores.Free;
  inherited;
end;


// -----------------------------------------------------------------------------
// internal methods

function TDataAccess.mGetDatastore: TDataSetRegistry;
begin
  if NOT Assigned( fDatastores ) then begin
  // lazy create/load datastores
    fDatastores := TDataSetRegistry.Create( [ {NO OWNERSHIPS} ] );
  end;
  Result := fDatastores;
end;

function TDataAccess.Datastore_FindByName( const aDatastoreName: String; const aRaiseExceptions: Boolean ): TDataset;
var
  lDatastoreInstance: TComponent;
  lParameter: TConnectionParameter;
begin
  Result := nil;
  if NOT aDatastoreName.IsEmpty then begin
    if ( Self.ComponentCount > 0 ) then begin
    // search for TDataSet instance contained by this TDataModule derivative
    // (for example, any TFDQuery, etc dataset dropped at design-time)
      lDatastoreInstance := Self.FindComponent( aDatastoreName );
      if Assigned( lDatastoreInstance ) AND ( lDatastoreInstance.InheritsFrom( TDataSet ) ) then begin
      // return contained TDataSet instance newly registered to aDatastoreName
        Result := lDatastoreInstance as TDataSet;
      end;
    end;
  end
  else
  if aRaiseExceptions then begin
    raise Exception.Create( 'ERROR: Datastore name cannot be empty' );
  end;

  if aRaiseExceptions AND ( NOT Assigned( Result ) ) then begin
    raise Exception.CreateFmt( 'ERROR: Datastore "%s" not owned by "%s"', [ aDatastoreName, Self.Name ] );
  end;
end;

function TDataAccess.Datastore_TryFindByName( const aDatastoreName: String;
  out aDatastore: TDataSet ): Boolean;
begin
  aDatastore := Datastore_FindByName( aDatastoreName, False );
  Result := ( aDatastore <> nil );
end;


// -----------------------------------------------------------------------------
// public API methods - ToDo: to be added via helper class to TDataModule
// -----------------------------------------------------------------------------

function TDataAccess.Datastore_AllAvailable_ListByClass(  ): TArray< TDataSetClass >;
var
  lComponentIndex: Integer;
  lComponent: TComponent;
begin
  Result := [ ];
  for lComponentIndex := ( 0 ) to ( Self.ComponentCount -1 ) do begin
    lComponent := Self.Components[ lComponentIndex ];
    if ( lComponent.InheritsFrom( TDataSet ) ) then begin
    // increase size of dynamic string array to accommodate new name
      SetLength( Result, ( Length( Result ) + 1 ) );
    // add component name ( DatastoreName ) to dynamic string array
      Result[ Length( Result ) - 1 ] := TDataSetClass( lComponent.ClassType );
    end;
  end;
end;

function TDataAccess.Datastore_AllAvailable_ListByName(  ): TArray< String >;
var
  lComponentIndex: Integer;
  lComponent: TComponent;
begin
  Result := [ ];
  for lComponentIndex := ( 0 ) to ( Self.ComponentCount -1 ) do begin
    lComponent := Self.Components[ lComponentIndex ];
    if ( lComponent.InheritsFrom( TDataSet ) ) then begin
    // increase size of dynamic string array to accommodate new name
      SetLength( Result, ( Length( Result ) + 1 ) );
    // add component name ( DatastoreName ) to dynamic string array
      Result[ Length( Result ) - 1 ] := lComponent.Name;
    end;
  end;
end;


// -----------------------------------------------------------------------------
// public API methods
// -----------------------------------------------------------------------------

function TDataAccess.Datastore_AllConnected_ListByName: TArray< String >;
begin
  Result := [ ];
  if ( Datastores.Count > 0 ) then begin
  // list all keys ( Datastore names ) as string array
    Result := Datastores.Keys.ToArray;
  end;
end;


// -----------------------------------------------------------------------------
// singular access to 'datastore'

function TDataAccess.Datastore_Add( const aDatastoreName: String ): TDataSet;
begin
  Result := Datastore_FindByName( aDatastoreName, False );
  if NOT Assigned( Result ) then begin
    if ( aDatastoreName.IsEmpty ) OR ( aDatastoreName = gkMockDatastore ) then begin
    // create mock datastore
      Result := TDataAccess_MockDatastore.Create( Self, gkMockDatastore );
      fHasMockDatastore := True;
    end
    else begin
    // ToDo: look up name in some class registry and instance via factory method/class
      //TBD
      //Result := ...;
    end;
  end;
end;

// takes any named TDataSet component instance registered with this TDataModule
// instance and registers as a DataStore then connects it to its "real" datastore.
procedure TDataAccess.Datastore_Connect( const aDatastoreName: String; const aConnectionParameters: TConnectionParameters );
var
  lDatastoreInstance: TDataset;
  lParameter: TConnectionParameter;
begin
  try
    if ( Datastores.Count > 0 ) AND ( Datastores.ContainsKey( aDatastoreName ) ) then begin
    // return contained TDataSet instance already registered to aDatastoreName
      lDatastoreInstance := Datastores.Items[ aDatastoreName ];
    end
    else begin
    // add TDataSet instance to TDataModule
      lDatastoreInstance := Datastore_Add( aDatastoreName );
    end;

    if Assigned( lDatastoreInstance ) then begin

      if Length( aConnectionParameters ) > 0 then begin
      // apply connection parameters as appropriate to lDatastore
        for lParameter in aConnectionParameters do begin
        // ToDo: Process connection parameters

          //if lParameter.Key = 'Name' then begin
          // ...
          //end;

          // ...

        end;
      end;

    // Activate the dataset
      lDatastoreInstance.Active := True;

    // register connection-configured, activate dataset by aDatastoreName
      Datastores.Add( aDatastoreName, lDatastoreInstance );
    end;
  except
    raise;
  end;
end;

procedure TDataAccess.Datastore_Disconnect( const aDatastoreName: String;
  const aFlushAllUpdates: Boolean );
var
  lDatastoreInstance: TDataSet;
begin
  if ( Datastores.Count > 0 ) AND ( Datastores.ContainsKey( aDatastoreName ) ) then begin
  // return contained TDataSet instance already registered to aDatastoreName
    lDatastoreInstance := Datastores.Items[ aDatastoreName ];
  // now conditionally handle all known in-memory dataset sets able that can
  // track changes internally (e.g., TClientDataSet, TFDMemTable, TBufDataset, etc)
    if lDatastoreInstance.InheritsFrom( TClientDataSet ) then begin
    // with TClientDataSet you can add, delete, change records and then process
    // TClientDataSet.Delta using (ToDo: explain...)
      with lDatastoreInstance as TClientDataSet do begin
    // at the final stage, before updating the Datastore associated with this
    // dataset, set StatusFilter so that only the records that you want to take
    // action on should be showing. For instance; ClientDataSet1.StatusFilter := [usDeleted];
    // See: https://stackoverflow.com/questions/3027852/to-track-the-modified-rows-and-manually-update-from-the-tclientdatasets-delta
        // TBD
        if ChangeCount > 0  then begin
          if FileExists( FileName ) then begin
          // process as file-based
            if LogChanges then begin
              MergeChangeLog;
            end;
          end
          else
          if NOT ProviderName.IsEmpty then begin
          // process as remote datastore via its DataSetProvider
            ApplyUpdates( 0 );
          end;
        end;
      end;
    end;

  // TFDMemTable has similar functionality, but it follows the Cached Updates model.
  // When CachedUpdates property value is True, MemTable will record changes in the updates log.
    // TBD
  end
end;

function TDataAccess.Datastore_Read( const aDatastoreName: String; const aNameValueCriteria: TNameValueCriteria ): TDataSet;
begin
  Result := nil;

  if ( Datastores.Count > 0 ) AND ( Datastores.ContainsKey( aDatastoreName ) ) then begin
  // return contained TDataSet instance already registered to aDatastoreName
    Result := Datastores.Items[ aDatastoreName ];
  end
  else begin
  // test whether TDataSet instance is already contained by this TDataModule
  // derivative (for example, any TFDQuery, etc dataset dropped at design-time)
    // return contained TDataSet instance newly registered to aDatastoreName
    if NOT ( Datastore_TryFindByName( aDatastoreName, Result ) ) then begin
    // ToDo: lookup aDatastoreName and, IFF TDataSet derivative, create and
    //       register it under by aDatastoreName (
    end;
  end;

  if Assigned( Result ) then begin
    try
      if ( Length( aNameValueCriteria ) > 0 ) then begin
      // apply aNameValueCriteria: TNameValueCriteria hereunder
        // TBD
      end;
      if Result is TDataAccess_MockDataStore then begin
        TDataAccess_MockDataStore( Result ).Open( '' );
      end
      else Result.Open;
    except
      raise;
    end;
  end
  else begin
    raise Exception.Create( 'ERROR: Not connected' );
  end;

  // using the resulting dataset returned here, we'll convert its records to a
  // dynamic array or TList of TEntity<...> instances within the model for
  // manipluation by the ViewModel (ViewModel, in turn, will mostly stringify
  // the TEntity's field name-values for display and editing within the View layer
end;

procedure TDataAccess.Datastore_Write( const aDatastoreName: String; const aRecordSet: TDataSet );
var
  lDatastoreInstance: TDataSet;
begin
  if ( Datastores.Count > 0 ) AND ( Datastores.ContainsKey( aDatastoreName ) ) then begin
  // lookup contained TDataSet instance already registered to aDatastoreName
    lDatastoreInstance := Datastores.Items[ aDatastoreName ];
  // convert dynamic array or TList of TEntity<...> instances to dataset records
  // (selectively creating OR updating based on DBKey GUID's existence within DAL)
  end
  else begin
    raise Exception.Create( 'ERROR: Not connected' );
  end;
end;


// -----------------------------------------------------------------------------
// plural access to 'datastore"s"'

function TDataAccess.Datastore_Add( const aDatastoreNames: TArray< String > ): TArray< TDataSet >;
var
  lDatastore: String;
begin
  Result := [ ];
// if list of datastores passed contains names
  if ( Length( aDatastoreNames ) > 0 ) then begin
  // ToDo -oJamesR -cForRelease : Make configurable / select via the ViewModel | View layers
  // TEMPORARILY HARDCODED
  // for each available datastore, connect to physical datastore
    for lDatastore in aDatastoreNames do begin
      SetLength( Result, ( Length( Result ) + 1 ) );
      Result[ ( Length( Result ) - 1 ) ] := Self.Datastore_Add( lDatastore );
    end;
  end;
end;

procedure TDataAccess.Datastore_Connect( const aDatastoreNames: TArray< String > );
var
  lDatastore: String;
begin
// if list of datastores passed contains names
  if ( Length( aDatastoreNames ) > 0 ) then begin
  // ToDo -oJamesR -cForRelease : Make configurable / select via the ViewModel | View layers
  // TEMPORARILY HARDCODED
  // for each available datastore, connect to physical datastore
    for lDatastore in aDatastoreNames do begin
      Self.Datastore_Connect( lDatastore );
    end;
  end;
end;

procedure TDataAccess.Datastore_Disconnect( const aDatastoreNames: TArray< String >; const aFlushAllUpdates: Boolean );
var
  lDatastore: String;
begin
// if list of datastores passed contains names
  if ( Length( aDatastoreNames ) > 0 ) then begin
  // ToDo -oJamesR -cForRelease : Make configurable / select via the ViewModel | View layers
  // TEMPORARILY HARDCODED
  // for each available datastore, disconnect from physical datastore
    for lDatastore in aDatastoreNames do begin
      Self.Datastore_Disconnect( lDatastore, aFlushAllUpdates );
    end;
  end;
end;


end.
