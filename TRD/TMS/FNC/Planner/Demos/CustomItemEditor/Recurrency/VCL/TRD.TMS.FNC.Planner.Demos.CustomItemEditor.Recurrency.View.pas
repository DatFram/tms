unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View;

interface

uses
{$ifndef USING_LCL}
// All Delphi-based compilations
  System.SysUtils
, System.Types
, System.UITypes
, System.Classes
, System.Variants
//
, Data.DB
, Datasnap.DBClient
//
  {$ifdef USING_FMX}
, FMX.Types
, FMX.Graphics
, FMX.Controls
, FMX.Controls.Presentation
, FMX.StdCtrls
, FMX.Forms
, FMX.Dialogs
//
, FMX.TMSFNCTypes
, FMX.TMSFNCUtils
, FMX.TMSFNCGraphics
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCCustomControl
, FMX.TMSFNCPlannerBase
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerDatabaseAdapter
, FMX.TMSFNCPlannerItemEditorRecurrency
  {$endif USING_FMX}
  {$ifdef USING_VCL}
, VCL.Graphics
, VCL.Controls
, VCL.StdCtrls
, VCL.ExtCtrls
, VCL.Forms
, VCL.Dialogs
//
, VCL.TMSFNCTypes
, VCL.TMSFNCUtils
, VCL.TMSFNCGraphics
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCCustomControl
, VCL.TMSFNCPlannerBase
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerDatabaseAdapter
, VCL.TMSFNCPlannerItemEditorRecurrency
  {$endif USING_VCL}
{$else USING_LCL}
// All Lazarus-based compilations
  SysUtils
, Types
, UITypes
, Classes
, Variants
//
, DB
//, Datasnap.DBClient
{$endif USING_LCL}
//
;

type
  TForm14 = class(TForm)
    ClientDataSet1: TClientDataSet;
    Panel1: TPanel;
    Button1: TButton;
    DataSource1: TDataSource;
    TMSFNCPlanner1: TTMSFNCPlanner;
    TMSFNCPlannerDatabaseAdapter1: TTMSFNCPlannerDatabaseAdapter;
    TMSFNCPlannerItemEditorRecurrency1: TTMSFNCPlannerItemEditorRecurrency;
    procedure FormCreate(Sender: TObject);
    procedure TMSFNCPlanner1AfterNavigateToDateTime(Sender: TObject;
      ADirection: TTMSFNCPlannerNavigationDirection; ACurrentDateTime,
      ANewDateTime: TDateTime);
    procedure TMSFNCPlanner1IsDateTimeSub(Sender: TObject; ADateTime: TDateTime;
      var AIsSub: Boolean);
    procedure TMSFNCPlannerDatabaseAdapter1FieldsToItem(Sender: TObject;
      AFields: TFields; AItem: TTMSFNCPlannerItem);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form14: TForm14;

implementation

// select/enable ONLY while designing
  {.$R *.fmx}
  {.$R *.lfm}
  {.$R *.dfm}

{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}
{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}

uses
  DateUtils;

procedure TForm14.Button1Click(Sender: TObject);
begin
  ClientDataSet1.Active := not ClientDataSet1.Active;

{$ifdef USING_FMX}
  if ClientDataSet1.Active then
    Button1.Text := 'Disconnect'
  else
    Button1.Text := 'Connect';
{$else NOT USING_FMX}
  if ClientDataSet1.Active then
    Button1.Caption := 'Disconnect'
  else
    Button1.Caption := 'Connect';
{$endif USING_VCL}
end;

procedure TForm14.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  TMSFNCPlanner1.BeginUpdate;

  TMSFNCPlanner1.ItemEditor := TMSFNCPlannerItemEditorRecurrency1;

  TMSFNCPlanner1.Interaction.UpdateMode := pumDialog;
  TMSFNCPlanner1.Interaction.TopNavigationButtons := [pnbPrevious, pnbNext];
  TMSFNCPlanner1.Interaction.MouseInsertMode := pmimAfterSelection;
  TMSFNCPlanner1.Interaction.KeyboardInsertMode := pkimSelection;
  TMSFNCPlanner1.DefaultItem.FontColor :=  gcWhite;
  TMSFNCPlanner1.DefaultItem.TitleFontColor := gcWhite;
  TMSFNCPlanner1.ModeSettings.StartTime := Now;
  TMSFNCPlanner1.Mode := pmDay;
  TMSFNCPlanner1.TimeLine.DisplayUnit := 240;
  TMSFNCPlanner1.TimeLine.DisplayUnitFormat := 'dddd dd/mm';
  TMSFNCPlanner1.TimeLine.DisplaySubUnitFormat := 'hh:nn';
  TMSFNCPlanner1.TimeLine.DisplayEnd := Round((MinsPerDay * 7) / TMSFNCPlanner1.TimeLine.DisplayUnit) - 1;
  TMSFNCPlanner1.TimeLineAppearance.LeftSize := 200;
  TMSFNCPlanner1.PositionsAppearance.TopSize := 120;
  TMSFNCPlanner1.Resources.Clear;
  for I := 0 to 7 do
    TMSFNCPlanner1.Resources.Add;

  TMSFNCPlanner1.PositionsAppearance.TopFont.Size := 18;

  TMSFNCPlanner1.Positions.Count := TMSFNCPlanner1.Resources.Count;
  TMSFNCPlanner1.Adapter := TMSFNCPlannerDatabaseAdapter1;
  TMSFNCPlanner1.EndUpdate;

  TMSFNCPlannerDatabaseAdapter1.Item.AutoIncrementDBKey := False;
  TMSFNCPlannerDatabaseAdapter1.Item.DataSource := DataSource1;
  TMSFNCPlannerDatabaseAdapter1.Item.DBKey := 'Id';
  TMSFNCPlannerDatabaseAdapter1.Item.StartTime := 'StartTime';
  TMSFNCPlannerDatabaseAdapter1.Item.EndTime := 'EndTime';
  TMSFNCPlannerDatabaseAdapter1.Item.Title := 'Title';
  TMSFNCPlannerDatabaseAdapter1.Item.Text := 'Text';
  TMSFNCPlannerDatabaseAdapter1.Item.Resource := 'Resource';
  TMSFNCPlannerDatabaseAdapter1.Item.Recurrency := 'Recurrency';

  ClientDataSet1.FieldDefs.Add('Id', ftString, 255);
  ClientDataSet1.FieldDefs.Add('Resource', ftString, 10);
  ClientDataSet1.FieldDefs.Add('Title', ftString, 10);
  ClientDataSet1.FieldDefs.Add('Text', ftString, 255);
  ClientDataSet1.FieldDefs.Add('StartTime', ftDateTime);
  ClientDataSet1.FieldDefs.Add('EndTime', ftDateTime);
  ClientDataSet1.FieldDefs.Add('Recurrency', ftString, 255);
  ClientDataSet1.FieldDefs.Add('Color', ftLongWord);
  ClientDataSet1.CreateDataSet;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(8, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 0;
  ClientDataSet1.FieldByName('Title').AsString := 'Miami';
  ClientDataSet1.FieldByName('Text').AsString := 'Dialy shoot at the beach';
  ClientDataSet1.FieldByName('Recurrency').AsString := 'RRULE:FREQ=DAILY';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcOrange;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(12, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 1;
  ClientDataSet1.FieldByName('Title').AsString := 'New York';
  ClientDataSet1.FieldByName('Text').AsString := 'Shoe model';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcDarkgray;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 4 + EncodeTime(12, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 5 + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 1;
  ClientDataSet1.FieldByName('Title').AsString := 'New York';
  ClientDataSet1.FieldByName('Text').AsString := 'Shoe model';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcDarkgray;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 1 + EncodeTime(11, 30, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 2 + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 1;
  ClientDataSet1.FieldByName('Title').AsString := 'Barcelona';
  ClientDataSet1.FieldByName('Text').AsString := 'Audition for photoshoot';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcDarkgray;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(10, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 2;
  ClientDataSet1.FieldByName('Title').AsString := 'TV Ad';
  ClientDataSet1.FieldByName('Text').AsString := 'Advertisement for toothpaste';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcGhostwhite;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 5 + EncodeTime(10, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 6 + EncodeTime(10, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 2;
  ClientDataSet1.FieldByName('Title').AsString := 'TV Ad';
  ClientDataSet1.FieldByName('Text').AsString := 'Advertisement for toothpaste';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcGhostwhite;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 2 + EncodeTime(4, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 3 + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 2;
  ClientDataSet1.FieldByName('Title').AsString := 'Barcelona';
  ClientDataSet1.FieldByName('Text').AsString := 'Meet with Daniel Harris for audition';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcGhostwhite;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 3 + EncodeTime(11, 30, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 3 + EncodeTime(21, 30, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 3;
  ClientDataSet1.FieldByName('Title').AsString := 'Clothes';
  ClientDataSet1.FieldByName('Text').AsString := 'New clothes line presentation in Milan';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcSeagreen;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(4, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(22, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 4;
  ClientDataSet1.FieldByName('Title').AsString := 'Photoshoot';
  ClientDataSet1.FieldByName('Text').AsString := 'Photoshoot for bikini magazine';
  ClientDataSet1.FieldByName('Recurrency').AsString := 'RRULE:FREQ=DAILY;BYDAY=MO,WE,FR,SU';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcSkyblue;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 1 + EncodeTime(12, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 2 + EncodeTime(20, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 5;
  ClientDataSet1.FieldByName('Title').AsString := 'Catwalk';
  ClientDataSet1.FieldByName('Text').AsString := 'Catwalk in Paris';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcPlum;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(4, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 1 + EncodeTime(16, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 6;
  ClientDataSet1.FieldByName('Title').AsString := 'TV Ad';
  ClientDataSet1.FieldByName('Text').AsString := 'Dinner with friends at the seafood restaurant while shooting a new advertisement';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcLightpink;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 4 + EncodeTime(4, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 4 + EncodeTime(20, 30, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 6;
  ClientDataSet1.FieldByName('Title').AsString := 'Catwalk';
  ClientDataSet1.FieldByName('Text').AsString := 'Catwalk in Barcelona';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcLightpink;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 1;
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now) + 1 + EncodeTime(12, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 7;
  ClientDataSet1.FieldByName('Title').AsString := 'Test shoot';
  ClientDataSet1.FieldByName('Text').AsString := 'Test shoot at the market in Phuket';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcDarkkhaki;

  ClientDataSet1.Insert;
  ClientDataSet1.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
  ClientDataSet1.FieldByName('StartTime').AsDateTime := Int(Now) + 3.5 + EncodeTime(4, 0, 0, 0);
  ClientDataSet1.FieldByName('EndTime').AsDateTime := Int(Now)+ 3.5 + EncodeTime(22, 0, 0, 0);
  ClientDataSet1.FieldByName('Resource').AsInteger := 7;
  ClientDataSet1.FieldByName('Title').AsString := 'Test shoot 2';
  ClientDataSet1.FieldByName('Text').AsString := 'Second Test shoot at the market in Phuket';
  ClientDataSet1.FieldByName('Color').AsLongWord := gcDarkkhaki;

  ClientDataSet1.Post;

  TMSFNCPlannerDatabaseAdapter1.Active := True;
{$ifdef USING_FMX}
  Button1.Text := 'Disconnect';
{$else NOT USING_FMX}
  Button1.Caption := 'Disconnect';
{$endif NOT USING_FMX}
end;

procedure TForm14.TMSFNCPlanner1AfterNavigateToDateTime(Sender: TObject;
  ADirection: TTMSFNCPlannerNavigationDirection; ACurrentDateTime,
  ANewDateTime: TDateTime);
begin
  TMSFNCPlannerDatabaseAdapter1.LoadItems;
end;

procedure TForm14.TMSFNCPlanner1IsDateTimeSub(Sender: TObject;
  ADateTime: TDateTime; var AIsSub: Boolean);
begin
  AIsSub := HourOf(ADateTime) + MinuteOf(ADateTime) + SecondOf(ADateTime) + MilliSecondOf(ADateTime) > 0;
end;

procedure TForm14.TMSFNCPlannerDatabaseAdapter1FieldsToItem(Sender: TObject;
  AFields: TFields; AItem: TTMSFNCPlannerItem);
var
  c: TAlphaColor;
begin
  c := AFields.FieldByName('Color').AsLongWord;
  if c <> 0 then
    AItem.Color := c
  else
    AItem.Color := TMSFNCPlanner1.DefaultItem.Color;

  if AItem.Color = gcGhostwhite then
  begin
    AItem.TitleFontColor := gcDarkGray;
    AItem.FontColor := gcDarkgray;
  end;
end;

end.
