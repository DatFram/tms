unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.DataAccess.MockDatastore;

interface

uses
{$ifndef USING_LCL}
// For all Delphi-based compilations
  System.SysUtils
, System.Classes
//
, Data.DB
, Datasnap.DBClient
{$else USING_LCL}
// For all Lazarus-based compilations
  SysUtils
, Classes
//
, DB
//, Datasnap.DBClient
{$endif USING_LCL}
;

type

// *****************************************************************************
// For test purposes only...
// *****************************************************************************
  TDataAccess_MockDatastore = class( TClientDataSet )
//    protected
//      procedure mWipe( const aClassName: String );
//      procedure mLoad( const aFilePathAndName: String = '' );
//      procedure mSave( const aFilePathAndName: String = ''; const aOverwrite: Boolean = False );
    public
      constructor Create( aOwner: TComponent; const aName: String = 'DataAccess_MockDataStore' ); reintroduce;
      procedure Open( const aFilePathAndName: String ); reintroduce;
      procedure Close( const aFilePathAndName: String; const aOverwrite: Boolean = False ); reintroduce;
  end;
// *****************************************************************************


implementation

uses
{$ifndef USING_LCL}
  {$ifdef USING_FMX}
//
  FMX.TMSFNCGraphicsTypes
  {$endif USING_FMX}
  {$ifdef USING_VCL}
  VCL.TMSFNCGraphicsTypes
  {$endif USING_VCL}
{$else USING_LCL}
// For all Lazarus-based compilations
  LCLTMSFNCGraphicsTypes
{$endif USING_LCL}
;

// =============================================================================

{ TDataAccess_Mock }

constructor TDataAccess_MockDataStore.Create( aOwner: TComponent; const aName: String );
begin
  inherited Create( aOwner );
  try
    // define field | type | size - iterate through array passed in?
    // ToDo: Read field & types using RTTI on class retrieved via Class_FindByName
    Self.FieldDefs.Add( 'Id',         ftString,   255 );
    Self.FieldDefs.Add( 'Resource',   ftString,   10  );
    Self.FieldDefs.Add( 'Title',      ftString,   10  );
    Self.FieldDefs.Add( 'Text',       ftString,   255 );
    Self.FieldDefs.Add( 'StartTime',  ftDateTime      );
    Self.FieldDefs.Add( 'EndTime',    ftDateTime      );
    Self.FieldDefs.Add( 'Recurrency', ftString,   255 );
    Self.FieldDefs.Add( 'Color',      ftLongWord      );
    // create in-memory dataset
    Self.CreateDataSet;
    Self.Name := aName;
  except
    Exception.CreateFmt( 'ERROR: Cannot create mock datastore named "%s"', [ ClassName ] );
  end;
end;

procedure TDataAccess_MockDataStore.Open( const aFilePathAndName: String );
begin
  if Assigned( Self ) then begin
    if NOT Self.Active then begin
      inherited Open();
    end;

    if ( NOT aFilePathAndName.IsEmpty ) AND ( FileExists( Self.FileName ) ) then begin
      try
        Self.LoadFromFile( aFilePathAndName );
      except
        // eat file load exception for now (will load mock data hereafter)
      end;
    end;

    if NOT ( Self.RecordCount > 0 ) then begin
    // insert some mock data for now (ToDo: iterate through an array of raw item data)
      Self.Insert;
      Self.FieldByName( 'Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName( 'StartTime').AsDateTime := Int(Now) + EncodeTime(8, 0, 0, 0);
      Self.FieldByName( 'EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
      Self.FieldByName( 'Resource').AsInteger := 0;
      Self.FieldByName( 'Title').AsString := 'Miami';
      Self.FieldByName( 'Text').AsString := 'Dialy shoot at the beach';
      Self.FieldByName( 'Recurrency').AsString := 'RRULE:FREQ=DAILY';
      Self.FieldByName( 'Color').AsLongWord := gcOrange;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(12, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 1;
      Self.FieldByName('Title').AsString := 'New York';
      Self.FieldByName('Text').AsString := 'Shoe model';
      Self.FieldByName('Color').AsLongWord := gcDarkgray;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 4 + EncodeTime(12, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 5 + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 1;
      Self.FieldByName('Title').AsString := 'New York';
      Self.FieldByName('Text').AsString := 'Shoe model';
      Self.FieldByName('Color').AsLongWord := gcDarkgray;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 1 + EncodeTime(11, 30, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 2 + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 1;
      Self.FieldByName('Title').AsString := 'Barcelona';
      Self.FieldByName('Text').AsString := 'Audition for photoshoot';
      Self.FieldByName('Color').AsLongWord := gcDarkgray;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(10, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 2;
      Self.FieldByName('Title').AsString := 'TV Ad';
      Self.FieldByName('Text').AsString := 'Advertisement for toothpaste';
      Self.FieldByName('Color').AsLongWord := gcGhostwhite;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 5 + EncodeTime(10, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 6 + EncodeTime(10, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 2;
      Self.FieldByName('Title').AsString := 'TV Ad';
      Self.FieldByName('Text').AsString := 'Advertisement for toothpaste';
      Self.FieldByName('Color').AsLongWord := gcGhostwhite;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 2 + EncodeTime(4, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 3 + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 2;
      Self.FieldByName('Title').AsString := 'Barcelona';
      Self.FieldByName('Text').AsString := 'Meet with Daniel Harris for audition';
      Self.FieldByName('Color').AsLongWord := gcGhostwhite;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 3 + EncodeTime(11, 30, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 3 + EncodeTime(21, 30, 0, 0);
      Self.FieldByName('Resource').AsInteger := 3;
      Self.FieldByName('Title').AsString := 'Clothes';
      Self.FieldByName('Text').AsString := 'New clothes line presentation in Milan';
      Self.FieldByName('Color').AsLongWord := gcSeagreen;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(4, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + EncodeTime(22, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 4;
      Self.FieldByName('Title').AsString := 'Photoshoot';
      Self.FieldByName('Text').AsString := 'Photoshoot for bikini magazine';
      Self.FieldByName('Recurrency').AsString := 'RRULE:FREQ=DAILY;BYDAY=MO,WE,FR,SU';
      Self.FieldByName('Color').AsLongWord := gcSkyblue;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 1 + EncodeTime(12, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 2 + EncodeTime(20, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 5;
      Self.FieldByName('Title').AsString := 'Catwalk';
      Self.FieldByName('Text').AsString := 'Catwalk in Paris';
      Self.FieldByName('Color').AsLongWord := gcPlum;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + EncodeTime(4, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 1 + EncodeTime(16, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 6;
      Self.FieldByName('Title').AsString := 'TV Ad';
      Self.FieldByName('Text').AsString := 'Dinner with friends at the seafood restaurant while shooting a new advertisement';
      Self.FieldByName('Color').AsLongWord := gcLightpink;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 4 + EncodeTime(4, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 4 + EncodeTime(20, 30, 0, 0);
      Self.FieldByName('Resource').AsInteger := 6;
      Self.FieldByName('Title').AsString := 'Catwalk';
      Self.FieldByName('Text').AsString := 'Catwalk in Barcelona';
      Self.FieldByName('Color').AsLongWord := gcLightpink;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 1;
      Self.FieldByName('EndTime').AsDateTime := Int(Now) + 1 + EncodeTime(12, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 7;
      Self.FieldByName('Title').AsString := 'Test shoot';
      Self.FieldByName('Text').AsString := 'Test shoot at the market in Phuket';
      Self.FieldByName('Color').AsLongWord := gcDarkkhaki;

      Self.Insert;
      Self.FieldByName('Id').AsString := TGuid.NewGuid.ToString;
      Self.FieldByName('StartTime').AsDateTime := Int(Now) + 3.5 + EncodeTime(4, 0, 0, 0);
      Self.FieldByName('EndTime').AsDateTime := Int(Now)+ 3.5 + EncodeTime(22, 0, 0, 0);
      Self.FieldByName('Resource').AsInteger := 7;
      Self.FieldByName('Title').AsString := 'Test shoot 2';
      Self.FieldByName('Text').AsString := 'Second Test shoot at the market in Phuket';
      Self.FieldByName('Color').AsLongWord := gcDarkkhaki;

      Self.Post;
    end;
  end;
end;

procedure TDataAccess_MockDatastore.Close( const aFilePathAndName: String; const aOverwrite: Boolean );
var
  lFilePathAndName: String;
begin
//var
//  lFilePersisted: Boolean;
      // https://stackoverflow.com/questions/3783202/is-there-any-way-to-validate-a-filename
        // lFilePersisted := TPath.HasValidFileNameChars( AFileName, UseWildcards);
        //lFilePersisted := True;
  if Assigned( Self ) then begin
    lFilePathAndName := aFilePathAndName;
    if lFilePathAndName.IsEmpty then begin
      lFilePathAndName := Self.FileName
    end;

    if ( NOT lFilePathAndName.IsEmpty ) AND FileExists( lFilePathAndName ) then begin
      // don't overwrite to update unless explicitly requested
      if NOT aOverwrite then
        Exit;
    end;

    try
      Self.SaveToFile( lFilePathAndName );
    except
      // eat file load exception for now (will load mock data hereafter)
    end;
  end;
end;

//    if fMockDatastore.Active then begin
//      fMockDatastore.Close;
//      fMockDatastore.FieldDefs.Clear;
//    end;


end.
