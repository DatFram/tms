unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.Model;
// -----------------------------------------------------------------------------
// PURPOSE
//
//  This unit provides entity-centric access to underlying datastore (that is,
//  it provides class-based C/R/U/D access to the underlying datastores using
//  each entity class (or record) registered herein to create/read/update/delete
//  corresponding records within the underlying DataAccess layer.
//
//  For each datastore registered within the underlying DataAccess layer, a
//  TObjectList of type TEntity<DataStoreName> is constructed via an
//  Object-Relationship Mapper (ORM) for object-orientated access to datasets.
//
//  To support data-aware controls within the View layer and attached through
//  the ViewModel to this Model, pass-through access to the DataAccess layer
//  (with caching using Dr. Bob's .SetProvider or without by bypassing Model) is
//  provided herein as well.
//
// -----------------------------------------------------------------------------
// NOTICE
//
//  NO PRESENTATION-LAYER DEPENDENCIES ARE PERMITTED HEREIN.
//
// -----------------------------------------------------------------------------
// REFERENCES
//
//  1. For a comparison of TClientDatSet and TFDMemTable, see:
//    - https://forums.embarcadero.com/thread.jspa?threadID=119145
//
//  2. For a backgrounder on TBufDataSet, see:
//    - http://wiki.freepascal.org/How_to_write_in-memory_database_applications_in_Lazarus/FPC
//
//  3. For info on copying any TDataSet to TClientDataSet (using a TDataSetProvider), see:
//    - https://stackoverflow.com/questions/23345365/how-can-i-move-data-from-tdataset-to-tclientdataset-i-need-xml-representation-o
//
//  4. For info on copying any TDataSet to a TFDMemTable, see:
//    - http://docwiki.embarcadero.com/RADStudio/en/TFDMemTable_Questions
//    - http://docwiki.embarcadero.com/CodeExamples/en/FireDAC.TFDMemTable.Main_Sample
//    - https://sourceforge.net/p/radstudiodemos/code/HEAD/tree/branches/RADStudio_Tokyo/Object%20Pascal/Database/FireDAC/Samples/Comp%20Layer/TFDMemTable/Main/fMainDemo.pas#l261
//
//  5. For information on cloning dataset cursors, see:
//    - http://caryjensen.blogspot.com/2011/10/cloning-clientdataset-cursors.html
//
// -----------------------------------------------------------------------------
// TODO (Last updated 2017-08-24)
//
//  1. Employ Daniele Teti's ObjectMapper to implement basic ORM herein per:
//    - https://github.com/danieleteti/delphimvcframework/blob/master/sources/ObjectsMappers.pas
//
// -----------------------------------------------------------------------------
interface

uses
{$ifndef USING_LCL}
// For all Delphi-based compilations
  System.SysUtils
, System.Classes
, System.Generics.Collections
//
, Data.DB
, DataSnap.DBClient                       // for TClientDataSet
, FireDAC.Stan.Intf
, FireDAC.Stan.Option
, FireDAC.Stan.Param
, FireDAC.Stan.Error
, FireDAC.DatS
, FireDAC.Phys.Intf
, FireDAC.DApt.Intf
, FireDAC.Comp.DataSet
, FireDAC.Comp.Client                     // for TFDMemTable
{$ifdef USING_FMX}
//
, FMX.TMSFNCGraphicsTypes
  {$endif USING_FMX}
  {$ifdef USING_VCL}
, VCL.TMSFNCGraphicsTypes
  {$endif USING_VCL}
{$else USING_LCL}
// For all Lazarus-based compilations
  SysUtils
, Classes
//
, DB
//, ????                                    // for TBufDatSet
//
, LCLTMSFNCGraphicsTypes
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.DataAccess
;

type

  TModel = class( TDataModule )
    FDMemTable1: TFDMemTable;
      procedure DoOnModelCreate( aSender: TObject );
    private
      fDataAccess: TDataAccess;
      fUserCacheClass: TDataSetClass;
      fUserCaches: TDataSetRegistry;
//      function ReadItemsFromDAL: TArray<TPlannerItem>;
//      procedure WriteItemsToDAL(const aPlannerItems: TArray<TPlannerItem>);
    protected
      function  mGetUserCacheClass( ): TDataSetClass;
      procedure mSetUserCacheClass( const aUserCacheClass: TDataSetClass );
      function mGetUserCaches: TDataSetRegistry;
//      function mGetUserCache( const aDALDataStoreName: String );
//      procedure mSetUserCache( const aDALDataStoreName: String; const aUserCache: TDataSet );
    public
    // existential methods
      constructor Create( aOwner: TComponent ); override;
      destructor Destroy( ); override;
    // public API methods - ToDo: to be added via helper class to TDataModule
      function  UserCache_AllAvailable_ListByClass: TArray< TDataSetClass >;
      function  UserCache_AllAvailable_ListByName: TArray< String >;
    // public API methods
      function  UserCache_AllInstantiated_ListByName: TArray< String >;
      // singular
      function  UserCache_Add( const aDALDatastoreName: String ): TDataSet; overload; virtual;
      procedure UserCache_Connect( const aDALDatastoreName: String; const aConnectionParameters: TConnectionParameters = [ ] ); overload; virtual;
      procedure UserCache_Disconnect( const aDALDatastoreName: String; const aFlushAllUpdates: Boolean = True ); overload; virtual;
      function  UserCache_Read( const aDALDatastoreName: String; const aNameValueCriteria: TNameValueCriteria = [ ] ): TDataSet; overload; virtual;
      procedure UserCache_Write( const aDALDatastoreName: String; const aRecordSet: TDataSet = nil ); overload; virtual;
    // ToDo: C/R/U/D operator methods
//      procedure ConnectToDAL( {Connection Info} ); virtual; // creates a cache for each DAL datastore
//      function UserCache_ReadFromDAL( const aDALDataStoreName: String; const
//        aNameValueCriteria: TNameValueCriteria = [ ] ): TDataSet; virtual;
//      procedure UserCache_WriteToDAL( const aDALDataStoreName: String );  virtual;
    // public properties
      property DataAccess: TDataAccess read fDataAccess;
      property UserCacheClass: TDataSetClass read mGetUserCacheClass write mSetUserCacheClass;
      property UserCaches: TDataSetRegistry read mGetUserCaches;
//      property UserCache[ const aDALDatastoreName ]: TDataSet read mGetUserCache;
  end;

implementation

// *****************************************************************************
// For more on ClassGroup pseudo-property of TDataModule and its framework-neutral
// configuration (namely, 'System.Classes.TPersistent') see:
// - http://docwiki.embarcadero.com/RADStudio/en/ClassGroup_pseudo-property_of_TDataModule
// *****************************************************************************
{%CLASSGROUP 'System.Classes.TPersistence'}
{$R *.dfm}
// *****************************************************************************


// =============================================================================

{ TModel }

// -----------------------------------------------------------------------------
// existential methods

constructor TModel.Create(aOwner: TComponent);
begin
// inject dependencies AND/OR create data access layer BEFORE completing the
// construction of this model layer in case such are required by the DoOnModelCreate
  fDataAccess := TDataAccess.Create( Self );
// call into construction lineage
  inherited Create( aOwner );
end;

procedure TModel.DoOnModelCreate( aSender: TObject );
begin
// query the names of what datastores are available from the data access layer
// and connect all of them (for now)
  fDataAccess.Datastore_Connect( fDataAccess.Datastore_AllAvailable_ListByName );
// `--- ToDo -oJamesR -cForRelease : Make configurable / select via the ViewModel | View layers

  Self.UserCache_Connect( fDataAccess.Datastore_Read.Name );

// for each connected datastore, create a local cache in the form of an
// in-memory dataset based on the type of TDataSet dropped on the design surface
// for this TDataModule derivative (TClientDataSet, TFDMemTable, TBufDataSet, etc)

// IFF the associated cache is a TClientDataSet, then call TDataSetProvider to
// connect "briefly" to the datastore like Dr. Bob does to fill it; then, before
// departing this unit (or upon calling method TBD), call TDataSetProvider to
// post back changes to associated datastore via the DataAccess layer
//  UserCache.SetProvider( fDataAccess.Datastore_Read );
//  UserCache.SetProvider( nil );
//  UserCache.MergeChangeLog;
//  UserCache.LogChanges := True;
end;

destructor TModel.Destroy;
begin
// TBD
  fUserCaches.Free;
  inherited;
end;


// -----------------------------------------------------------------------------
// private/proteced data field accessors OR class internal methods

function TModel.mGetUserCacheClass: TDataSetClass;
begin
  Result := fUserCacheClass;
end;

procedure TModel.mSetUserCacheClass( const aUserCacheClass: TDataSetClass );
var
  lUserCacheClasses: TArray< TDataSetClass >;
begin
  if NOT Assigned( aUserCacheClass ) then begin
  // determine type of local cache as class of in-memory dataset based on the
  // type of TDataSet dropped on the design surface for this TDataModule
  // derivative (TClientDataSet, TFDMemTable, TBufDataSet, etc)
    lUserCacheClasses := Self.UserCache_AllAvailable_ListByClass;
    if Length( lUserCacheClasses ) > 0 then begin
      fUserCacheClass := lUserCacheClasses[ 0 ]
    // `--- ToDo: selectively use class types??? (for now, use first found)
    end
    else begin
      fUserCacheClass := TClientDataSet;
    end;
  end
  else begin
    fUserCacheClass := aUserCacheClass;
  end;
end;

function TModel.mGetUserCaches: TDataSetRegistry;
begin
  if ( NOT Assigned( fUserCaches ) ) then begin
  // lazy create/load datastores
    fUserCaches := TDataSetRegistry.Create( [ doOwnsValues ] );
  end;
  Result := fUserCaches;
end;


// -----------------------------------------------------------------------------
// public API methods - ToDo: to be added via helper class to TDataModule
// -----------------------------------------------------------------------------

function TModel.UserCache_AllAvailable_ListByClass: TArray< TDataSetClass >;
var
  lComponentIndex: Integer;
  lComponent: TComponent;
begin
  //Result := [ TClientDataSet ];
  Result := [ ];
  for lComponentIndex := ( 0 ) to ( Self.ComponentCount -1 ) do begin
    lComponent := Self.Components[ lComponentIndex ];
    if ( lComponent.InheritsFrom( TDataSet ) ) then begin
    // increase size of dynamic string array to accommodate new name
      SetLength( Result, ( Length( Result ) + 1 ) );
    // add component class type ( UserCacheClass ) to dynamic string array
      Result[ Length( Result ) - 1 ] := TDataSetClass( lComponent.ClassType );
    end;
  end;
end;

function TModel.UserCache_AllAvailable_ListByName: TArray< String >;
var
  lComponentIndex: Integer;
  lComponent: TComponent;
begin
  //Result := [ 'TClientDataSet' ];
  Result := [ ];
  for lComponentIndex := ( 0 ) to ( Self.ComponentCount -1 ) do begin
    lComponent := Self.Components[ lComponentIndex ];
    if ( lComponent.InheritsFrom( TDataSet ) ) then begin
    // increase size of dynamic string array to accommodate new name
      SetLength( Result, ( Length( Result ) + 1 ) );
    // add component name ( UserCacheName ) to dynamic string array
      Result[ Length( Result ) - 1 ] := lComponent.Name;
    end;
  end;
end;


// -----------------------------------------------------------------------------
// public API methods
// -----------------------------------------------------------------------------

function TModel.UserCache_AllInstantiated_ListByName: TArray< String >;
begin
  Result := [ ];
  if ( UserCaches.Count > 0 ) then begin
  // list all keys ( UserCache name = DAL's Datastore name ) as string array
    Result := UserCaches.Keys.ToArray;
  end;
end;


// -----------------------------------------------------------------------------
// singular access to 'UserCache'

function TModel.UserCache_Add( const aDALDatastoreName: String ): TDataSet;
begin
  Result := nil;

  if NOT ( aDALDatastoreName.IsEmpty ) then begin
    if NOT ( UserCaches.ContainsKey( aDALDatastoreName ) ) then begin
      if NOT Assigned( UserCacheClass ) then begin
      // reset to lookup UserCache class within Self
        UserCacheClass := nil;
      // class type is determined as type of in-memory dataset placed on the
      // on the design surface of this TDataModule derivative at design time
      // ToDo: Minimally support TClientDataSet, TFDMemTable, and TBufDataSet
      end;

      if Assigned( UserCacheClass ) then begin
        Result := UserCacheClass.Create( nil );
        Result.Name := aDALDatastoreName;
        UserCaches.Add( Result.Name, Result );
      end;
    end
//    else begin
//    // return TDataSet instance of UserCache already registered for aDALDatastoreName
//      Result := UserCaches.Items[ aDALDatastoreName ];
//    end
  end;
end;

// takes any named TDataSet component instance registered with this TDataModule
// instance and registers as a DataStore then connects it to its "real" datastore.
procedure TModel.UserCache_Connect( const aDALDatastoreName: String; const aConnectionParameters: TConnectionParameters );
var
  lUserCacheInstance: TDataSet;
  lSourceDatastore: TDataSet;
  lParameter: TConnectionParameter;
begin
  try
    if ( UserCaches.ContainsKey( aDALDatastoreName ) ) then begin
    // return TDataSet UserCache instance already registered for aDALDatastoreName
      lUserCacheInstance := UserCaches.Items[ aDALDatastoreName ];
    end
    else begin
    // add new UserCache instance to UserCaches reqistry by aDALDatastoreName
      lUserCacheInstance := UserCache_Add( aDALDatastoreName );
    end;

    if Assigned( lUserCacheInstance ) then begin

      if Length( aConnectionParameters ) > 0 then begin
      // apply connection parameters as appropriate to lDatastore
        for lParameter in aConnectionParameters do begin
        // ToDo: Process connection parameters

          //if lParameter.Key = 'Name' then begin
          // ...
          //end;

          // ...

        end;
      end;

      lSourceDatastore := fDataAccess.Datastore_Read( lUserCacheInstance.Name );

      if Assigned( lSourceDatastore ) then begin
        if NOT lSourceDatastore.Active then begin
        // open source dataset for copying
          lSourceDatastore.Open;
        end;

        if lUserCacheInstance.InheritsFrom( TClientDataSet ) then begin
        // IFF this UserCache instance is of type TClientDataSet, then call its
        // TDataSetProvider to connect "briefly" to the datastore (per Dr. Bob)
        // to fill it; then, before departing this unit (or upon calling method
        // TBD), call its TDataSetProvider again to post back changes to
        // associated datastore via the DataAccess layer
          with ( lUserCacheInstance as TClientDataSet ) do begin
            SetProvider( lSourceDatastore );
            if NOT Active then
              Open;
            MergeChangeLog;
            LogChanges := True;
            SetProvider( nil );
          end;
        end;

        if lUserCacheInstance.InheritsFrom( TFDMemTable ) then begin
        // IFF this UserCache instance is of type TFDMemTable, then ... TBD
          with ( lUserCacheInstance as TFDMemTable ) do begin
            CopyDataSet( lSourceDatastore, [coStructure, coRestart, coAppend] );
//            CopyDataSet(
//              lSourceDatastore,
//              [
//                coStructure,
//                coIndexesCopy,
//                coIndexesReset,
//                coConstraintsReset,
//                coAggregatesReset,
//                coRestart,
//                coAppend
//              ]
//            );
          end;
        end;

      // Activate the dataset
        lUserCacheInstance.Active := True;
      end
      else begin
        raise Exception.CreateFmt( 'ERROR: Cannot locate/open datastore "%s"', [ aDALDatastoreName ] );
      end;
    end
    else begin
      raise Exception.CreateFmt( 'ERROR: Cannot connect user cache "%s"', [ aDALDatastoreName ] );
    end;
  except
    raise;
  end;
end;

procedure TModel.UserCache_Disconnect( const aDALDatastoreName: String;
  const aFlushAllUpdates: Boolean );
var
  lUserCacheInstance: TDataSet;
begin
  if ( UserCaches.Count > 0 ) AND ( UserCaches.ContainsKey( aDALDatastoreName ) ) then begin
  // return contained TDataSet instance already registered to aDALDatastoreName
    lUserCacheInstance := UserCaches.Items[ aDALDatastoreName ];

  // now conditionally handle all known in-memory dataset sets able that can
  // track changes internally (e.g., TClientDataSet, TFDMemTable, TBufDataset, etc)
    if lUserCacheInstance.InheritsFrom( TClientDataSet ) then begin
    // with TClientDataSet you can add, delete, change records and then process
    // TClientDataSet.Delta using (ToDo: explain...)
      with lUserCacheInstance as TClientDataSet do begin
    // at the final stage, before updating the Datastore associated with this
    // dataset, set StatusFilter so that only the records that you want to take
    // action on should be showing. For instance; ClientDataSet1.StatusFilter := [usDeleted];
    // See: https://stackoverflow.com/questions/3027852/to-track-the-modified-rows-and-manually-update-from-the-tclientdatasets-delta
        // TBD
        if ChangeCount > 0  then begin
          if FileExists( FileName ) then begin
          // process as file-based
            if LogChanges then begin
              MergeChangeLog;
            end;
          end
          else
          if NOT ProviderName.IsEmpty then begin
          // process as remote datastore via its DataSetProvider
            ApplyUpdates( 0 );
          end;
        end;
      end;
    end;

    if lUserCacheInstance.InheritsFrom( TFDMemTable ) then begin
    // TFDMemTable has similar functionality, but it follows the Cached Updates model.
    // When CachedUpdates property value is True, MemTable will record changes in the updates log.
    // TBD
    end;
  end
end;

function TModel.UserCache_Read( const aDALDatastoreName: String; const aNameValueCriteria: TNameValueCriteria ): TDataSet;
begin
  Result := nil;
  if ( UserCaches.Count > 0 ) AND ( UserCaches.ContainsKey( aDALDatastoreName ) ) then begin
  // lookup TDataSet UserCache instance already registered for aDALDatastoreName
    Result := UserCaches.Items[ aDALDatastoreName ];

    if Assigned( Result ) then begin
      try
        if ( Length( aNameValueCriteria ) > 0 ) then begin
        // apply aNameValueCriteria: TNameValueCriteria hereunder
          // TBD
        end;

        Result.Open;
      except
        raise;
      end;
    end;

  // ToDo:
    // using the resulting dataset returned here, we'll convert its records to a
    // dynamic array or TList of TEntity<...> instances within the model for
    // manipluation by the ViewModel (ViewModel, in turn, will mostly stringify
    // the TEntity's field name-values for display and editing within the View layer
  end
  else begin
    raise Exception.Create( 'ERROR: Not connected' );
  end;
end;

procedure TModel.UserCache_Write( const aDALDatastoreName: String; const aRecordSet: TDataSet );
var
  lUserCacheInstance: TDataSet;
begin
  if ( UserCaches.Count > 0 ) AND ( UserCaches.ContainsKey( aDALDatastoreName ) ) then begin
  // lookup TDataSet UserCache instance already registered for aDALDatastoreName
    lUserCacheInstance := UserCaches.Items[ aDALDatastoreName ];

  // ToDo:
    // convert dynamic array or TList of TEntity<...> instances to dataset records
    // (selectively creating OR updating based on DBKey GUID's existence within DAL)
  end
  else begin
    raise Exception.Create( 'ERROR: Not connected' );
  end;
end;


// -----------------------------------------------------------------------------
// TBD

//function TModel.ReadItemsFromDAL( {criteria} ): TArray< TPlannerItem >;
//begin
//// Convert dataset records to dynamic array of TPlannerItem
//end;
//
//procedure TModel.WriteItemsToDAL( const aPlannerItems: TArray< TPlannerItem > );
//begin
//// convert dynamic array of TPlannerItem to dataset records (selectively
//// creating OR updating based on DBKey GUID's existence within DAL
//end;


end.
