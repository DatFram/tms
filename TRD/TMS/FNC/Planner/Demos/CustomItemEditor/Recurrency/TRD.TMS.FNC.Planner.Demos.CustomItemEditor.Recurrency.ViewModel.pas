unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.ViewModel;

interface

uses
{$ifndef USING_LCL}
// For all Delphi-based compilations
  System.SysUtils
, System.Classes
//
, Data.DB
, Datasnap.DBClient
  {$ifdef USING_FMX}
//
, FMX.TMSFNCTypes
, FMX.TMSFNCUtils
, FMX.TMSFNCGraphics
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCPlannerDatabaseAdapter
  {$endif USING_FMX}
  {$ifdef USING_VCL}
, VCL.TMSFNCTypes
, VCL.TMSFNCUtils
, VCL.TMSFNCGraphics
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCPlannerData                   // TTMSFNCPlannerItem
, VCL.TMSFNCPlannerDatabaseAdapter
  {$endif USING_VCL}
{$else USING_LCL}
// For all Lazarus-based compilations
  SysUtils
, Classes
//
, DB
//, Datasnap.DBClient
//
, LCLTMSFNCTypes
, LCLTMSFNCUtils
, LCLTMSFNCGraphics
, LCLTMSFNCGraphicsTypes
, LCLTMSFNCPlannerDatabaseAdapter
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.Model
;

type

  TViewModel = class( TDataModule )
    dt_fDefaultDataSource: TDataSource;
      procedure DataModuleCreate( aSender: TObject );
    private
    // ...
      fModel: TModel;
    // strangely, unlike TDataSource, we cannot create/drop a framework-neutral
    // version of the adapter on this units associate datamodule at design-time...
      fDefaultPlannerDatabaseAdapter: TTMSFNCPlannerDatabaseAdapter;
    // selected
      fSelectedDataSourceName: String;
      fSelectedDataSource: TDataSource;
      fSelectedPlannerDatabaseAdapter: TTMSFNCPlannerDatabaseAdapter;
    protected
    // ...
      procedure LoadDataSources( );
      function GetSelectedDataSource: TDataSource;
      function GetSelectedPlannerDatabaseApdater: TTMSFNCPlannerDatabaseAdapter;
    // override and/or replace internals based on which datasource is selected (ToDo: add custom mappers)
      procedure DoOnItemToFields( aSender: TObject; aItem: TTMSFNCPlannerItem; aFields: TFields ); virtual; //abstract;
      procedure DoOnFieldsToItem( aSender: TObject; aFields: TFields; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnCreateDBKey( aSender: TObject; aItem: TTMSFNCPlannerItem; var aDBKey: string ); virtual; //abstract;
      procedure DoOnItemCreated( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract; // inserted
      procedure DoOnItemRead( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnItemUpdated( aSender: TObject; aItem: TTMSFNCPlannerItem ); virtual; //abstract;
      procedure DoOnItemsLoaded( aSender: TObject ); virtual; //abstract;
    public
      { Public declarations }
      function SelectDataSource( const aDataSourceName: String = '';
        const aPlannerDatabaseAdapter: TTMSFNCPlannerDatabaseAdapter = nil ): TDataSource; // ToDo: Lookup in dictionary
      function SelectPlannerDatabaseAdapter( const aPlannerDatabaseAdapter:
        TTMSFNCPlannerDatabaseAdapter = nil; const aDataSouerceName: String = '' ): TTMSFNCPlannerDatabaseAdapter;
      property SelectedDataSource: TDataSource
        read GetSelectedDataSource
        write fSelectedDataSource;
      property SelectedPlannerDatabaseAdapter: TTMSFNCPlannerDatabaseAdapter
        read GetSelectedPlannerDatabaseApdater
        write fSelectedPlannerDatabaseAdapter;
  end;

implementation

// *****************************************************************************
// For more on ClassGroup pseudo-property of TDataModule and its framework-neutral
// configuration (namely, 'System.Classes.TPersistent') see:
// - http://docwiki.embarcadero.com/RADStudio/en/ClassGroup_pseudo-property_of_TDataModule
// *****************************************************************************
{%CLASSGROUP 'System.Classes.TPersistence'}
{$R *.dfm}
// *****************************************************************************

// =============================================================================

{ TViewModel }

procedure TViewModel.DataModuleCreate( aSender: TObject );
begin
// initialize data fileds
  Self.fModel := TModel.Create( Self );
  Self.fSelectedDataSource := nil;
  Self.fSelectedPlannerDatabaseAdapter := nil;

// attach/load collection of datasources read from fModel and/or its DataAccess layer
  LoadDataSources;

// create planner database adapter for View containiing FNC planner
  fDefaultPlannerDatabaseAdapter := TTMSFNCPlannerDatabaseAdapter.Create( Self );
  with ( fDefaultPlannerDatabaseAdapter ) do begin
    Name := 'dt_fDefaultPlannerDatabaseAdapter';
    Item.AutoIncrementDBKey := False;
    Item.DBKey              := 'Id';
    Item.StartTime          := 'StartTime';
    Item.EndTime            := 'EndTime';
    Item.Title              := 'Title';
    Item.Text               := 'Text';
    Item.Resource           := 'Resource';
    Item.Recurrency         := 'Recurrency';
  end;

// select default datasource for initializing the asociated View(s)
  SelectDataSource( '', fDefaultPlannerDatabaseAdapter );
end;

procedure TViewModel.DoOnItemToFields( aSender: TObject; aItem: TTMSFNCPlannerItem;
  aFields: TFields );
begin
  //
end;

procedure TViewModel.DoOnFieldsToItem( aSender: TObject; aFields: TFields;
  aItem: TTMSFNCPlannerItem );
begin
  //
end;

procedure TViewModel.DoOnCreateDBKey( aSender: TObject; aItem: TTMSFNCPlannerItem;
  var aDBKey: string );
begin
  //
end;

procedure TViewModel.DoOnItemCreated( aSender: TObject; aItem: TTMSFNCPlannerItem );
begin
  //
end;

procedure TViewModel.DoOnItemRead( aSender: TObject; aItem: TTMSFNCPlannerItem );
begin
  //
end;

procedure TViewModel.DoOnItemUpdated( aSender: TObject; aItem: TTMSFNCPlannerItem );
begin
  //
end;

procedure TViewModel.DoOnItemsLoaded( aSender: TObject );
begin
  //
end;

function TViewModel.GetSelectedDataSource: TDataSource;
begin
  Result := SelectDataSource( '', nil );
end;

function TViewModel.GetSelectedPlannerDatabaseApdater: TTMSFNCPlannerDatabaseAdapter;
begin
  Result := SelectPlannerDatabaseAdapter( nil, '' );
end;

procedure TViewModel.LoadDataSources;
begin
// ToDo: Load up collection (TObjectDictionary) of data sources (one for each
//       dataset read via the Model from its fDataAccess DataAccess layer)

// for now, the following bypasses any caching done in the Model layer...
  //dt_fDefaultDataSource.DataSet := fModel.DataAccess.Datastore_Read;
// or, using the following, we can access a cached version
  dt_fDefaultDataSource.DataSet := fModel.UserCache_Read( 'DataAccess_MockDatastore' );
end;

function TViewModel.SelectDataSource( const aDataSourceName: String;
  const aPlannerDatabaseAdapter: TTMSFNCPlannerDatabaseAdapter ): TDataSource;
begin
  Result := nil;
  try
    if NOT aDataSourceName.IsEmpty then begin
      if Assigned( fSelectedDataSource ) AND ( aDataSourceName <> fSelectedDataSource.Name ) then begin
        fSelectedDataSource.Enabled := False;
        fSelectedDataSource := nil;
      end;
      if ( {collection contains name} FALSE ) then begin
      // get dataSource from collection
        // fSelectedDataSource := TBD
      // for now we'll use just the one built in datasource
        fSelectedDataSource := dt_fDefaultDataSource;
      end;
    end;

  // Guard against nil assignment
    if NOT Assigned( fSelectedDataSource ) then begin
    // IFF aDataSourcerName=EmptyStr, use built in/design-time DataSource
      fSelectedDataSource := dt_fDefaultDataSource;
    end;
    fSelectedDataSourceName := dt_fDefaultDataSource.Name;
    fSelectedDataSource.Enabled := True;

    if Assigned( aPlannerDatabaseAdapter ) then
      SelectPlannerDatabaseAdapter( aPlannerDatabaseAdapter );

    Result := fSelectedDataSource;
  except
    raise Exception.Create( 'ERROR: Cannot assign planner data source' );
  end;
end;

function TViewModel.SelectPlannerDatabaseAdapter( const aPlannerDatabaseAdapter:
  TTMSFNCPlannerDatabaseAdapter = nil; const aDataSouerceName: String = '' ): TTMSFNCPlannerDatabaseAdapter;
begin
  Result := nil;
  try
    if ( Assigned( aPlannerDatabaseAdapter ) ) then begin
      if ( aPlannerDatabaseAdapter <> fSelectedPlannerDatabaseAdapter ) then begin
        if Assigned( fSelectedPlannerDatabaseAdapter ) then begin
          fSelectedPlannerDatabaseAdapter.Active := False;
          fSelectedPlannerDatabaseAdapter := nil;
        end;
        fSelectedPlannerDatabaseAdapter := aPlannerDatabaseAdapter;
      end;
    end;

  // Guard against nil assignment
    if NOT Assigned( fSelectedPlannerDatabaseAdapter ) then begin
    // IFF aPlannerDatabaseAdapter=nil, use built in planner database adapter
      fSelectedPlannerDatabaseAdapter := fDefaultPlannerDatabaseAdapter;
    end;

    if NOT aDataSouerceName.IsEmpty then
      SelectDataSource( aDataSouerceName );

  // update fPlannerAdapter
    if Assigned( fSelectedDataSource ) then
      fSelectedPlannerDatabaseAdapter.Item.DataSource := fSelectedDataSource;
    //fSelectedPlannerDatabaseAdapter.Active := True; // do

    Result := fSelectedPlannerDatabaseAdapter;
  except
    raise Exception.Create( 'ERROR: Cannot assign planner database adapter' );
  end;

end;


end.
