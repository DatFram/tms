unit TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View;
// NOTE: .\$(Config)\$(Platform) set in build environment
interface

uses
{$ifndef USING_LCL}
// All Delphi-based compilations
  System.SysUtils
, System.Types
, System.UITypes
, System.Classes
, System.Variants
, System.Actions
//
, Data.DB
, Datasnap.DBClient
//
  {$ifdef USING_FMX}
, Fmx.Types
, Fmx.Graphics
, Fmx.Controls
, Fmx.Controls.Presentation
, Fmx.StdCtrls
, Fmx.Forms
, Fmx.Dialogs
, Fmx.ActnList
//
, FMX.TMSFNCTypes
, FMX.TMSFNCUtils
, FMX.TMSFNCGraphics
, FMX.TMSFNCGraphicsTypes
, FMX.TMSFNCCustomComponent
, FMX.TMSFNCCustomControl
, FMX.TMSFNCPlannerBase
, FMX.TMSFNCPlannerData
, FMX.TMSFNCPlanner
, FMX.TMSFNCPlannerDatabaseAdapter
, FMX.TMSFNCPlannerItemEditorRecurrency
  {$endif USING_FMX}
  {$ifdef USING_VCL}
, Vcl.Graphics
, Vcl.Controls
, Vcl.StdCtrls
, Vcl.ExtCtrls
, Vcl.Forms
, Vcl.Dialogs
, Vcl.ActnList
//
, VCL.TMSFNCTypes
, VCL.TMSFNCUtils
, VCL.TMSFNCGraphics
, VCL.TMSFNCGraphicsTypes
, VCL.TMSFNCCustomComponent
, VCL.TMSFNCCustomControl
, VCL.TMSFNCPlannerBase
, VCL.TMSFNCPlannerData
, VCL.TMSFNCPlanner
, VCL.TMSFNCPlannerItemEditorRecurrency
  {$endif USING_VCL}
{$else USING_LCL}
// All Lazarus-based compilations
  SysUtils
, Types
, UITypes
, Classes
, Variants
//
, DB
//, Datasnap.DBClient
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.ViewModel
;

type

  TForm14 = class( TForm )
      Panel1: TPanel;
      Button1: TButton;
      TMSFNCPlanner1: TTMSFNCPlanner;
      TMSFNCPlannerItemEditorRecurrency1: TTMSFNCPlannerItemEditorRecurrency;
      ActionList1: TActionList;
      actConnect: TAction;
      procedure FormCreate( aSender: TObject );
      procedure TMSFNCPlanner1AfterNavigateToDateTime( aSender: TObject;
        aDirection: TTMSFNCPlannerNavigationDirection; aCurrentDateTime,
        aNewDateTime: TDateTime );
      procedure TMSFNCPlanner1IsDateTimeSub( aSender: TObject; aDateTime: TDateTime;
        var aIsSub: Boolean );
      procedure TMSFNCPlannerDatabaseAdapter1FieldsToItem( aSender: TObject;
        aFields: TFields; aItem: TTMSFNCPlannerItem );
    procedure actConnectExecute(Sender: TObject);
    procedure actConnectUpdate(Sender: TObject);
    private
      { Private declarations }
      fViewModel: TViewModel;
    public
      { Public declarations }
  end;

var
  Form14: TForm14;

implementation

// select/enable ONE-AND-ONLY-ONE while designing
  {.$R TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View.fmx}
  {.$R TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View.lfm}
  {.$R TRD.TMS.FNC.Planner.Demos.CustomItemEditor.Recurrency.View.dfm}

{$ifdef USING_FMX}
  {$R *.fmx}
{$endif USING_FMX}
{$ifdef USING_LCL}
  {$R *.lfm}
{$endif USING_LCL}
{$ifdef USING_VCL}
  {$R *.dfm}
{$endif USING_VCL}

uses
{$ifndef USING_LCL}
  System.DateUtils
{$else USING_LCL}
  DateUtils
{$endif USING_LCL}
//
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.VCL  // for TMS VCL-based editor
, TRD.TMS.FNC.Planner.Component.CustomItemEditor.FMX  // for TMS FMX-based editor
, TRD.TMS.FNC.Planner.Component.CustomItemEditor
;

procedure TForm14.FormCreate( aSender: TObject );
var
  I: Integer;
begin
  fViewModel := TViewModel.Create( Self );
  //fViewModel.SelectDataSource( );

  TMSFNCPlanner1.BeginUpdate;
  try
  // ToDo: BEGIN: Load ItemEditor based on Resource being edited
//    TMSFNCPlanner1.ItemEditor := TMSFNCPlannerItemEditorRecurrency1; // Recurrency editor as delivered from TMS
//    TMSFNCPlanner1.ItemEditor := TTMSPlannerCustomItemEditor_VCL.Create( Self ); // TMS editor code from VCL module
//    TMSFNCPlanner1.ItemEditor := TTMSPlannerCustomItemEditor_FMX.Create( Self ); // TMS editor code from FMX module
//    TMSFNCPlanner1.ItemEditor := TTMSPlannerCustomItemEditor.Create( Self ); // Home-grown editor
  // ToDo: END: Load ItemEditor based on Resource being edited

    TMSFNCPlanner1.Interaction.UpdateMode := pumDialog;
    TMSFNCPlanner1.Interaction.TopNavigationButtons := [pnbPrevious, pnbNext];
    TMSFNCPlanner1.Interaction.MouseInsertMode := pmimAfterSelection;
    TMSFNCPlanner1.Interaction.KeyboardInsertMode := pkimSelection;
    TMSFNCPlanner1.DefaultItem.FontColor :=  gcWhite;
    TMSFNCPlanner1.DefaultItem.TitleFontColor := gcWhite;
    TMSFNCPlanner1.ModeSettings.StartTime := Now;
    TMSFNCPlanner1.Mode := pmDay;
    TMSFNCPlanner1.TimeLine.DisplayUnit := 240;
    TMSFNCPlanner1.TimeLine.DisplayUnitFormat := 'dddd dd/mm';
    TMSFNCPlanner1.TimeLine.DisplaySubUnitFormat := 'hh:nn';
    TMSFNCPlanner1.TimeLine.DisplayEnd := Round((MinsPerDay * 7) / TMSFNCPlanner1.TimeLine.DisplayUnit) - 1;
    TMSFNCPlanner1.TimeLineAppearance.LeftSize := 200;
    TMSFNCPlanner1.PositionsAppearance.TopSize := 120;
    TMSFNCPlanner1.PositionsAppearance.TopFont.Size := 18;

  // ToDo: BEGIN: Load Resources
    TMSFNCPlanner1.Resources.Clear;
    for I := 0 to 7 do
      TMSFNCPlanner1.Resources.Add;
    TMSFNCPlanner1.Positions.Count := TMSFNCPlanner1.Resources.Count;
  // ToDo: END: Load Resources

  finally
    TMSFNCPlanner1.EndUpdate;
  end;
end;


// -----------------------------------------------------------------------------
// event handlers

procedure TForm14.actConnectExecute(Sender: TObject);
begin
// ToDo: Move this logic into the ViewModel
  if Assigned( fViewModel.SelectedPlannerDatabaseAdapter ) then begin
  // Connect planner to adapter
    TMSFNCPlanner1.Adapter := fViewModel.SelectedPlannerDatabaseAdapter;
  // Toggle
    TMSFNCPlanner1.Adapter.Active := NOT TMSFNCPlanner1.Adapter.Active;
  end;
end;

procedure TForm14.actConnectUpdate(Sender: TObject);
begin
  actConnect.Enabled := Assigned( fViewModel.SelectedPlannerDatabaseAdapter );
  if ( actConnect.Enabled ) then begin
  {$ifdef USING_FMX}
    if ( Button1.Text <> 'Disconnect' ) AND
       ( fViewModel.SelectedPlannerDatabaseAdapter.Active = True ) then begin
      Button1.Text := 'Disconnect'
    end
    else
    if ( Button1.Text <> 'Connect' ) AND NOT
       ( fViewModel.SelectedPlannerDatabaseAdapter.Active = True ) then begin
      Button1.Text := 'Connect';
    end;
  {$else NOT USING_FMX}
    if ( Button1.Caption <> 'Disconnect' ) AND
       ( fViewModel.SelectedPlannerDatabaseAdapter.Active = True ) then begin
      Button1.Caption := 'Disconnect'
    end
    else
    if ( Button1.Caption <> 'Connect' ) AND NOT
       ( fViewModel.SelectedPlannerDatabaseAdapter.Active = True ) then begin
      Button1.Caption := 'Connect';
    end;
  {$endif NOT USING_FMX}
  end
  else begin
  {$ifdef USING_FMX}
    if ( Button1.Text <> 'Connect' ) then
      Button1.Text := 'Connect';
  {$else NOT USING_FMX}
    if ( Button1.Caption <> 'Connect' ) then
      Button1.Caption := 'Connect';
  {$endif NOT USING_FMX}
  end;
end;

procedure TForm14.TMSFNCPlanner1AfterNavigateToDateTime( aSender: TObject;
  aDirection: TTMSFNCPlannerNavigationDirection; aCurrentDateTime,
  aNewDateTime: TDateTime );
begin
  fViewModel.SelectedPlannerDatabaseAdapter.LoadItems;
end;

procedure TForm14.TMSFNCPlanner1IsDateTimeSub( aSender: TObject;
  aDateTime: TDateTime; var aIsSub: Boolean );
begin
  aIsSub := HourOf(aDateTime) + MinuteOf(aDateTime) + SecondOf(aDateTime) + MilliSecondOf(aDateTime) > 0;
end;

procedure TForm14.TMSFNCPlannerDatabaseAdapter1FieldsToItem( aSender: TObject;
  aFields: TFields; aItem: TTMSFNCPlannerItem );
var
  c: TTMSFNCGraphicsColor;
begin
  c := aFields.FieldByName( 'Color' ).AsLongWord;
  if c <> 0 then
    aItem.Color := c
  else
    aItem.Color := TMSFNCPlanner1.DefaultItem.Color;

  if aItem.Color = gcGhostwhite then
  begin
    aItem.TitleFontColor := gcDarkGray;
    aItem.FontColor := gcDarkgray;
  end;
end;


end.
