/* Copy and run these 3 statements into FlameRobin */
/* NOTE: The comments within the second chunk that define a block that you can copy and paste separately to see how many records will be created */


/* Paste and run this first! */
ALTER TRIGGER TRIG_ENTITY_BASE INACTIVE;


/* Paste and run this second! */
SET TERM ^ ;
EXECUTE BLOCK
AS
DECLARE VARIABLE id GUID;
DECLARE VARIABLE child_id GUID;
DECLARE VARIABLE parent_id GUID;
DECLARE VARIABLE pos Integer;
BEGIN

FOR
/* Start copying from here... */
SELECT GEN_UUID(), a.ID, a2.ID, 5
-- , a.CONTENT, ap.STREET1, ap.CITY, ap.POSTAL_CODE
FROM ADDRESS a
JOIN ADDRESS_TYPE t ON t.ID = a.ADDRESS_TYPE_ID AND t.NAME = 'ZIP_CODE'
JOIN ADDRESS_PHYSICAL ap ON TRIM(ap.POSTAL_CODE) = TRIM(a.CONTENT)
JOIN ADDRESS a2 ON a2.ID = ap.ID
/* ... to here to get the select query that will show all records that will be created. Uncomment the second line of it to get a better view */
INTO :id, :child_id, :parent_id, :pos
DO BEGIN
    INSERT INTO ENTITY_BASE (ID, TABLE_NAME, POSITION_INDEX)
    VALUES (:id, 'ADDRESS_HIERARCHY', GEN_ID(SEQ_ENTITY_BASE,1) );
    INSERT ADDRESS_HIERARCHY (ID, CHILD_ADDRESS_ID, PARENT_ADDRESS_ID, POSITION_INDEX)
    VALUES (:id, :child_id, :parent_id, :pos);
END

END ^ ;


/* Paste and run this third! */
ALTER TRIGGER TRIG_ENTITY_BASE ACTIVE;