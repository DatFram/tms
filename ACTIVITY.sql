    /* ************************************************************

    ACTIVITY METHODS DDL

    Change Log
    ----------
	2017-12-18
		- Updated the following METHODs to respect latest schema updates the the general hierarchical structure
			- createActivity - creates top-level ACTIVITY record
			- updateAttribute - creates attribute level ACTIVITY record and associates it with the indicated top-level ACTIVITY record (will check for existing attribute-level ACTIVITY records with identical values and use instead of creating a new one)
		- Added the following METHOD drafts:
			- addContainerType - creates new top-level ACTIVITY_TYPE
			- addAttributeType - creates new attribute level ACTIVITY_TYPE
			- addAttributeToActivityType - associates an attribute level ACTIVITY_TYPE to a top-level ACTIVITY_TYPE
			- addToEnum - creates an Enumeration placeholder record for a specific ACTIVITY_TYPE
			- createEnum - adds enumeration values for a specific ACTIVITY_TYPE's Enumeration
    2017-11-02
        - Created File
        
    ************************************************************ */
    
    /* createActivity */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('C679F58B-189C-46E5-854B-B059960ED449'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('C679F58B-189C-46E5-854B-B059960ED449')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('C679F58B-189C-46E5-854B-B059960ED449') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('C679F58B-189C-46E5-854B-B059960ED449')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'createActivity', 
        DESCRIPTION = 'Creates a new event of the specified container type.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ID:GUID,ACTIVITY_TYPE_NAME:Varchar(35)', 
        OUTPUTS = 'ACTIVITY_ID:GUID', 
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.createActivity
    Sequencing:
    - Check to make sure the given :activity_type_name is a container level activity type (aka KIND)
    - Create a new record in the ACTIVITY table of the matching type
    - Return the GUID generated for the ACTIVITY record
    
    TO DO: allow an optional input of the GUID to use for the new record so that the GUID can be generated outside of the function call.
    */
    EXECUTE BLOCK (
        ID GUID = :id,
		ACTIVITY_TYPE_NAME Varchar(35) = :activity_type_name )
    RETURNS (
        activity_id GUID )
    AS
    DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE child_id GUID;
	DECLARE VARIABLE pos Integer;
    DECLARE VARIABLE is_column BOOLEAN;
    BEGIN
        SELECT t.ID FROM ACTIVITY_TYPE t 
        WHERE t.NAME = :activity_type_name
        INTO :type_id;
        
		SELECT eb.ID FROM ENTITY_BASE eb WHERE eb.ID = :id
		INTO :activity_id;
		
        IF (type_id IS NULL OR activity_id IS NOT NULL) THEN EXIT;
        
        /* create the parent/container activity */
		/* Use the input :id if provided */
		IF (id IS NOT NULL) THEN activity_id = id; ELSE activity_id = GEN_UUID();
        INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:activity_id, ''ACTIVITY'');
        INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID) VALUES( :activity_id, :type_id);
		
		/* Now that the master record is created, create placeholder child records to be filled in with attribute values later  */
		FOR
		SELECT t.NAME, ah.CHILD_ACTIVITY_ID, ah.POS
		FROM ACTIVITY_HIERARCHY ah
		JOIN ACTIVITY a ON a.ID = ah.PARENT_ACTIVITY_ID
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		WHERE t.ID = :type_id
		AND ah.IS_TEMPLATE = ''1''
		INTO :activity_type_name, :child_id, :pos
		DO BEGIN
            /* first check to see if the attribute is actually just a field in the table */
            SELECT COUNT(a.RDB$FIELD_NAME) FROM RDB$RELATION_FIELDS a
            WHERE TRIM(a.RDB$RELATION_NAME) = ''ACTIVITY''
            AND TRIM(a.RDB$FIELD_NAME) = :activity_type_name
            INTO :is_column;
			
            IF (is_column = 0) THEN BEGIN
                id = GEN_UUID();
                INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
                INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, POS)
                VALUES (:id, :activity_id, :child_id, :pos);
            END
		END
		
        SUSPEND;
    END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('C679F58B-189C-46E5-854B-B059960ED449');
    
    /* updateAttribute */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('508E3FDE-8FF1-4CE6-898E-9F4FE4E64DE1'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('508E3FDE-8FF1-4CE6-898E-9F4FE4E64DE1')
    WHERE eb.ID IS NULL;
	
    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('508E3FDE-8FF1-4CE6-898E-9F4FE4E64DE1') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('508E3FDE-8FF1-4CE6-898E-9F4FE4E64DE1')
    WHERE m.ID IS NULL;
	
    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'updateAttribute', 
        DESCRIPTION = 'Updates an existing attribute with the content provided.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_ID:GUID,ATTRIBUTE_TYPE_NAME:Varchar(35),CONTENT_STRING:Varchar(255),CONTENT_DATETIME:Timestamp,CONTENT_NUMERIC:Decimal(10,8),CONTENT_BOOLEAN:Boolean,CONTENT_BLOB:BLOB,CONTENT_GUID:GUID', 
        OUTPUTS = 'ATTRIBUTE_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.updateAttribute 
    Sequencing:
    - Check to make sure the input activity exists
    - Update the values of the content in the attribute
    */
    EXECUTE BLOCK (
        ACTIVITY_ID GUID = :activity_id,
		ATTRIBUTE_TYPE_NAME Varchar(35) = :attribute_type_name,
        CONTENT_STRING Varchar(255) = :content_string,
		CONTENT_DATETIME Timestamp = :content_datetime,
		CONTENT_NUMERIC Decimal(10,8) = :content_numeric,
		CONTENT_BOOLEAN BOOLEAN = :content_boolean,
		CONTENT_BLOB BLOB = :content_blob,
		CONTENT_GUID GUID = :content_guid
        )
    RETURNS (
        attribute_id GUID )
    AS
    DECLARE VARIABLE is_column BOOLEAN;
    DECLARE VARIABLE stmt BLOB;
    DECLARE VARIABLE id GUID;
    DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE data_type Varchar(35);
	DECLARE VARIABLE pos Integer;
	DECLARE VARIABLE old_id GUID;
    BEGIN
        /* first check to see if the attribute is actually just a field in the table */
        SELECT COUNT(a.RDB$FIELD_NAME) FROM RDB$RELATION_FIELDS a
        WHERE TRIM(a.RDB$RELATION_NAME) = ''ACTIVITY''
        AND TRIM(a.RDB$FIELD_NAME) = :attribute_type_name
        INTO :is_column;
        
        IF (is_column = 1) THEN BEGIN
            stmt = ''UPDATE ACTIVITY a SET a.'' || :attribute_type_name || '' = :content WHERE a.ID = :activity_id;'';
            EXECUTE STATEMENT (stmt) (content := COALESCE(content_string,content_datetime,content_numeric,content_boolean,content_blob,content_guid), activity_id := activity_id);
            
            attribute_id = activity_id;
            SUSPEND;
            EXIT;
        END
        
		/* check to make sure the attribute type is valid for the specified activity */
		SELECT t.ID, t.DATA_TYPE, ah.POS, ah.ID
		FROM ACTIVITY_HIERARCHY ah
		JOIN ENTITY_BASE eb ON eb.ID = ah.ID AND eb.IS_ACTIVE = ''1''
		JOIN ACTIVITY a ON a.ID = ah.CHILD_ACTIVITY_ID
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		WHERE t.NAME = :attribute_type_name
		AND ah.PARENT_ACTIVITY_ID = :activity_id
		INTO :type_id, :data_type, :pos, :old_id;
		
		IF (type_id IS NULL) THEN EXIT;
		
		/* Check to see if an attribute with the same content already exists */
		attribute_id = NULL;
		IF (data_type = ''STRING'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_STRING = :content_string OR (a.CONTENT_STRING IS NULL AND :content_string IS NULL))
			INTO :attribute_id;
		END ELSE IF (data_type = ''DATETIME'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_DATETIME = :content_datetime OR (a.CONTENT_DATETIME IS NULL AND :content_datetime IS NULL))
			INTO :attribute_id;
		END ELSE IF (data_type = ''NUMERIC'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_NUMERIC = :content_numeric OR (a.CONTENT_NUMERIC IS NULL AND :content_numeric IS NULL))
			INTO :attribute_id;
		END ELSE IF (data_type = ''BOOLEAN'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_BOOLEAN = :content_boolean OR (a.CONTENT_BOOLEAN IS NULL AND :content_boolean IS NULL))
			INTO :attribute_id;
		END ELSE IF (data_type = ''GUID'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_GUID = :content_guid OR (a.CONTENT_GUID IS NULL AND :content_guid IS NULL))
			INTO :attribute_id;
		END
        
		/* If the attribute does not already exists, create one now */
		IF (attribute_id IS NULL) THEN BEGIN
			attribute_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:attribute_id, ''ACTIVITY'');
			INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID, CONTENT_STRING, CONTENT_DATETIME, CONTENT_NUMERIC, CONTENT_BOOLEAN, CONTENT_BLOB, CONTENT_GUID)
			VALUES (:attribute_id, :type_id, :content_string, :content_datetime, :content_numeric, :content_boolean, :content_blob, :content_guid);
		END
        
        /* relate the attribute to the parent activity */
        UPDATE ACTIVITY_HIERARCHY ah SET ah.CHILD_ACTIVITY_ID = :attribute_id WHERE ah.ID = :old_id;
        
        /* The method below does not work when updating a value with something it was before, e.g. NULL */
        
		/* Deactive the old association
		UPDATE ENTITY_BASE eb SET eb.IS_ACTIVE = 0, eb.UPDATED_BY = CURRENT_USER, eb.UPDATED_AT = CURRENT_TIMESTAMP
		WHERE eb.ID = :old_id;
		*/
		
        /* Create the new association
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
		INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, POS)
		VALUES (:id, :activity_id, :attribute_id, :pos);
		*/
         
		SUSPEND;
    END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('508E3FDE-8FF1-4CE6-898E-9F4FE4E64DE1');
    
	/* addContainerType */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('5C02F9A7-328A-49CE-B78A-72C17FBA58A6'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('5C02F9A7-328A-49CE-B78A-72C17FBA58A6')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('5C02F9A7-328A-49CE-B78A-72C17FBA58A6') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('5C02F9A7-328A-49CE-B78A-72C17FBA58A6')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'addContainerType', 
        DESCRIPTION = 'Adds a new container ACTIVITY_TYPE record.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'NAME:Varchar(35),DESCRIPTION:Varchar(255),FORMAT_MASK:Varchar(255),VALIDATION:Varchar(255),DATA_TYPE:Varchar(35),DATA_SIZE:Integer', 
        OUTPUTS = 'ACTIVITY_TYPE_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.addContainerType */
	EXECUTE BLOCK (
        NAME Varchar(35) = :name,
        DESCRIPTION Varchar(255) = :description,
		FORMAT_MASK Varchar(255) = :format_mask,
		VALIDATION Varchar(255) = :validation,
		DATA_TYPE Varchar(35) = :data_type,
		DATA_SIZE Integer = :data_size
        )
    RETURNS (
        ACTIVITY_TYPE_ID GUID )
    AS
    DECLARE VARIABLE id GUID;
	DECLARE VARIABLE pos Integer;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
    BEGIN
		/* Make sure the type does not already exist */
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :name
		INTO :activity_type_id;
		IF (activity_type_id IS NOT NULL) THEN EXIT;
		
		activity_type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:activity_type_id, ''ACTIVITY_TYPE'');
		INSERT INTO ACTIVITY_TYPE (ID, NAME, DESCRIPTION, FORMAT_MASK, VALIDATION, DATA_TYPE, DATA_SIZE)
		VALUES (:activity_type_id, :name, :description, :format_mask, :validation, :data_type, :data_size);
		
		child_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:child_id, ''ACTIVITY'');
		INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID) VALUES (:child_id, :activity_type_id);
		
		/* Add the new activity type to the container list */
		SELECT a.ID, MAX(ah.POS) + 1
		FROM ACTIVITY_HIERARCHY ah
		JOIN ACTIVITY a ON a.ID = ah.PARENT_ACTIVITY_ID
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		WHERE t.NAME = ''UNASSIGNED''
		AND ah.IS_TEMPLATE = ''1''
		AND a.CONTENT_GUID = t.ID
		GROUP BY a.ID
		INTO :parent_id, :pos;
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
		INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, ''1'', :pos);
		
		SUSPEND;
	END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('5C02F9A7-328A-49CE-B78A-72C17FBA58A6');
    
	/* addAttributeType */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('6DEAF422-D945-41E7-B812-0ED00138B547'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('6DEAF422-D945-41E7-B812-0ED00138B547')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('6DEAF422-D945-41E7-B812-0ED00138B547') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('6DEAF422-D945-41E7-B812-0ED00138B547')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'addAttributeType', 
        DESCRIPTION = 'Adds a new attribute ACTIVITY_TYPE record.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'NAME:Varchar(35),DESCRIPTION:Varchar(255),FORMAT_MASK:Varchar(255),VALIDATION:Varchar(255),DATA_TYPE:Varchar(35),DATA_SIZE:Integer', 
        OUTPUTS = 'ACTIVITY_TYPE_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.addAttributeType */
	EXECUTE BLOCK (
        NAME Varchar(35) = :name,
        DESCRIPTION Varchar(255) = :description,
		FORMAT_MASK Varchar(255) = :format_mask,
		VALIDATION Varchar(255) = :validation,
		DATA_TYPE Varchar(35) = :data_type,
		DATA_SIZE Integer = :data_size
        )
    RETURNS (
        ACTIVITY_TYPE_ID GUID )
    AS
    DECLARE VARIABLE id GUID;
	DECLARE VARIABLE pos Integer;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
    BEGIN
		/* Make sure the type does not already exist */
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :name
		INTO :activity_type_id;
		IF (activity_type_id IS NOT NULL) THEN EXIT;
		
		activity_type_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:activity_type_id, ''ACTIVITY_TYPE'');
		INSERT INTO ACTIVITY_TYPE (ID, NAME, DESCRIPTION, FORMAT_MASK, VALIDATION, DATA_TYPE, DATA_SIZE)
		VALUES (:activity_type_id, :name, :description, :format_mask, :validation, :data_type, :data_size);
		
		SUSPEND;
	END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('6DEAF422-D945-41E7-B812-0ED00138B547');
    
	/* addAttributeToActivityType */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('930AE11F-6ADD-421B-B0B5-413D12B53FFB'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('930AE11F-6ADD-421B-B0B5-413D12B53FFB')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('930AE11F-6ADD-421B-B0B5-413D12B53FFB') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('930AE11F-6ADD-421B-B0B5-413D12B53FFB')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'addAttributeToActivityType', 
        DESCRIPTION = 'Associates the input attribute ACTIVITY_TYPE record to the input container ACTIVITY_TYPE record.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_TYPE_ID:GUID,ATTRIBUTE_TYPE_ID:GUID', 
        OUTPUTS = 'RESULT:BOOLEAN',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.addAttributeToActivityType  */
	EXECUTE BLOCK (
        ACTIVITY_TYPE_NAME Varchar(35) = :activity_type_name,
		ATTRIBUTE_TYPE_NAME Varchar(35) = :attribute_type_name )
    RETURNS (
        RESULT BOOLEAN )
    AS
	DECLARE VARIABLE activity_type_id GUID;
	DECLARE VARIABLE attribute_type_id GUID;
    DECLARE VARIABLE id GUID;
	DECLARE VARIABLE pos Integer;
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE child_id GUID;
    BEGIN
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :activity_type_name
		INTO :activity_type_id;
		SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :attribute_type_name
		INTO :attribute_type_id;
		
		IF (activity_type_id IS NULL OR attribute_type_id IS NULL) THEN EXIT;
		
		/* Make sure the relationship does not already exist */
		SELECT ah.ID
		FROM ACTIVITY_HIERARCHY ah
		JOIN ACTIVITY a ON a.ID = ah.PARENT_ACTIVITY_ID
		JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
		WHERE ah.IS_TEMPLATE = ''1''
		AND a.ACTIVITY_TYPE_ID = :activity_type_id
		AND a2.ACTIVITY_TYPE_ID = :attribute_type_id
		INTO :id;
		IF (id IS NOT NULL) THEN EXIT;
		
		/* Fetch parent shell ACTIVITY record to relate */
		SELECT a2.ID, COALESCE(MAX(ah2.POS)+1,1)
		FROM ACTIVITY_HIERARCHY ah
		JOIN ACTIVITY a ON a.ID = ah.PARENT_ACTIVITY_ID
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
        LEFT JOIN ACTIVITY_HIERARCHY ah2 ON ah2.PARENT_ACTIVITY_ID = a2.ID
			AND ah2.IS_TEMPLATE = ''1''
		WHERE ah.IS_TEMPLATE = ''1''
		AND t.NAME = ''UNASSIGNED''
        AND a.CONTENT_GUID = t.ID
		AND a2.ACTIVITY_TYPE_ID = :activity_type_id
		GROUP BY a2.ID
		INTO :parent_id, :pos;
		
		/* Fetch or create the child shell ACTIVITY record to relate */
		SELECT a2.ID
		FROM ACTIVITY_HIERARCHY ah
		JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
		WHERE ah.IS_TEMPLATE = ''1''
		AND a2.ACTIVITY_TYPE_ID = :attribute_type_id
		GROUP BY a2.ID
		INTO :child_id;
		
		IF (child_id IS NULL) THEN BEGIN
			child_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:child_id, ''ACTIVITY'');
			INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID) VALUES (:child_id, :attribute_type_id);
		END
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
		INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :child_id, ''1'', :pos);
		
		SUSPEND;
	END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('930AE11F-6ADD-421B-B0B5-413D12B53FFB');
    
	/* createEnum */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('23033970-5238-440D-AC5B-98C2487A7E09'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('23033970-5238-440D-AC5B-98C2487A7E09')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('23033970-5238-440D-AC5B-98C2487A7E09') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('23033970-5238-440D-AC5B-98C2487A7E09')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'createEnum', 
        DESCRIPTION = 'Creates enumeration container ACTIVITY record of type UNASSIGNED that enum values can be added to using addEnum', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_TYPE_ID:GUID', 
        OUTPUTS = 'ACTIVITY_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.createEnum  */
	EXECUTE BLOCK (
        ACTIVITY_TYPE_NAME Varchar(35) = :activity_type_name
        )
    RETURNS (
        ACTIVITY_ID GUID )
    AS
    DECLARE VARIABLE ACTIVITY_TYPE_ID GUID;
    DECLARE VARIABLE id GUID;
    BEGIN
        SELECT t.ID FROM ACTIVITY_TYPE t WHERE t.NAME = :activity_type_name
        INTO :activity_type_id;
        IF (activity_type_id IS NULL) THEN EXIT;
        
		/* Make sure the enum type does not already exist */
		SELECT a.ID
		FROM ACTIVITY a
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		WHERE t.NAME = ''UNASSIGNED''
		AND a.CONTENT_GUID = :activity_type_id
		INTO :activity_id;
		IF (activity_id IS NOT NULL) THEN EXIT;
		
		/* create the enumeration record */
		activity_id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:activity_id, ''ACTIVITY'');
		INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID, CONTENT_GUID)
		SELECT :activity_id, t.ID, :activity_type_id
		FROM ACTIVITY_TYPE t WHERE t.NAME = ''UNASSIGNED'';
	
		SUSPEND;
	END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('23033970-5238-440D-AC5B-98C2487A7E09');
    
	/* addToEnum */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('43BEB525-8D8E-4ADC-9939-170E6BD99F22'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('43BEB525-8D8E-4ADC-9939-170E6BD99F22')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('43BEB525-8D8E-4ADC-9939-170E6BD99F22') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('43BEB525-8D8E-4ADC-9939-170E6BD99F22')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'addToEnum', 
        DESCRIPTION = 'Adds indicated value to input enumeration', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_TYPE_ID:GUID, ', 
        OUTPUTS = 'ACTIVITY_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.addToEnum  */
	EXECUTE BLOCK (
        ACTIVITY_TYPE_NAME Varchar(35) = :activity_type_name,
        CONTENT_STRING Varchar(255) = :content_string,
		CONTENT_DATETIME Timestamp = :content_datetime,
		CONTENT_NUMERIC Decimal(10,8) = :content_numeric,
		CONTENT_BOOLEAN BOOLEAN = :content_boolean,
		CONTENT_BLOB BLOB = :content_blob,
		CONTENT_GUID GUID = :content_guid
        )
    RETURNS (
        ACTIVITY_ID GUID )
    AS
	DECLARE VARIABLE parent_id GUID;
	DECLARE VARIABLE type_id GUID;
	DECLARE VARIABLE pos Integer;
    DECLARE VARIABLE id GUID;
	DECLARE VARIABLE data_type Varchar(35);
    BEGIN
		/* make sure the type is valid */
		SELECT t.ID, t.DATA_TYPE FROM ACTIVITY_TYPE t WHERE t.NAME = :activity_type_name
		INTO :type_id, :data_type;
		IF (data_type IS NULL OR type_id IS NULL) THEN EXIT;
		
		/* Make sure the enum type exists */
		SELECT a.ID, COALESCE(MAX(ah.POS) + 1 ,1)
		FROM ACTIVITY a
		JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
		LEFT JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID AND ah.IS_TEMPLATE = ''1''
		WHERE t.NAME = ''UNASSIGNED''
		AND a.CONTENT_GUID = :type_id
        GROUP BY a.ID
		INTO :parent_id, :pos;
		IF (parent_id IS NULL) THEN EXIT;
		
		/* check for an existing record with the same content */
		IF (data_type = ''STRING'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_STRING = :content_string OR (a.CONTENT_STRING IS NULL AND :content_string IS NULL))
			INTO :activity_id;
		END ELSE IF (data_type = ''DATETIME'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_DATETIME = :content_datetime OR (a.CONTENT_DATETIME IS NULL AND :content_datetime IS NULL))
			INTO :activity_id;
		END ELSE IF (data_type = ''NUMERIC'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_NUMERIC = :content_numeric OR (a.CONTENT_NUMERIC IS NULL AND :content_numeric IS NULL))
			INTO :activity_id;
		END ELSE IF (data_type = ''BOOLEAN'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_BOOLEAN = :content_boolean OR (a.CONTENT_BOOLEAN IS NULL AND :content_boolean IS NULL))
			INTO :activity_id;
		END ELSE IF (data_type = ''GUID'') THEN BEGIN
			SELECT a.ID FROM ACTIVITY a WHERE a.ACTIVITY_TYPE_ID = :type_id
			AND (a.CONTENT_GUID = :content_guid OR (a.CONTENT_GUID IS NULL AND :content_guid IS NULL))
			INTO :activity_id;
		END
		
		/* if no existing identical record, create a new one */
		IF (activity_id IS NULL) THEN BEGIN
			activity_id = GEN_UUID();
			INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:activity_id, ''ACTIVITY'');
			INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID, CONTENT_STRING, CONTENT_DATETIME, CONTENT_NUMERIC, CONTENT_BOOLEAN, CONTENT_BLOB, CONTENT_GUID)
			VALUES (:activity_id, :type_id, :content_string, :content_datetime, :content_numeric, :content_boolean, :content_blob, :content_guid);
		END
		
		id = GEN_UUID();
		INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
		INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, IS_TEMPLATE, POS)
		VALUES (:id, :parent_id, :activity_id, ''1'', :pos);
		
		SUSPEND;
	END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('43BEB525-8D8E-4ADC-9939-170E6BD99F22');
    
    
    
    
    
    
    
UNDER CONSTRUCTION:    
    
    
    
    /* Generate a SELECT query dynamically to fetch all attributes of an ACTIVITY as columns */
    SET TERM ^ ;
    EXECUTE BLOCK
    RETURNS (parent_type_name Varchar(35), stmt Varchar(8000))
    AS
    DECLARE VARIABLE parent_id GUID;
    DECLARE VARIABLE parent_type_id GUID;
    DECLARE VARIABLE pos Integer;
    DECLARE VARIABLE child_id GUID;
    DECLARE VARIABLE child_type_id GUID;
    DECLARE VARIABLE child_type_name Varchar(35);
    DECLARE VARIABLE data_type Varchar(35);
    DECLARE VARIABLE temp Varchar(4000);
    DECLARE VARIABLE is_column BOOLEAN;
    DECLARE VARIABLE temp_type_name Varchar(35);
    BEGIN
        parent_type_name = NULL;
        pos = 0;
        FOR
        SELECT a.ID, t.ID, t.NAME, ah.POS, a2.ID, t2.ID, t2.NAME, t2.DATA_TYPE
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
        JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
        JOIN ACTIVITY_TYPE t2 ON t2.ID = a2.ACTIVITY_TYPE_ID
        WHERE ah.IS_TEMPLATE = '1'
        AND t.DATA_TYPE IS NULL
        AND t.NAME <> 'UNASSIGNED'
        ORDER BY t.NAME, ah.POS
        --AND ah.IS_SINGLETON = '1' /* filters out properties that can appear as collections */
        INTO :parent_id, :parent_type_id, :temp_type_name, :pos, :child_id, :child_type_id, :child_type_name, :data_type
        DO BEGIN
            IF (parent_type_name IS NULL) THEN BEGIN
                parent_type_name = temp_type_name;
            END IF (parent_type_name <> temp_type_name) THEN BEGIN
                stmt = stmt || ' FROM ACTIVITY a';
                SUSPEND;
                parent_type_name = temp_type_name;
                pos = 0;
            END
            pos = pos + 1;
            
            /* first check to see if the attribute is actually just a field in the table */
            SELECT COUNT(a.RDB$FIELD_NAME) FROM RDB$RELATION_FIELDS a
            WHERE TRIM(a.RDB$RELATION_NAME) = 'ACTIVITY'
            AND TRIM(a.RDB$FIELD_NAME) = :child_type_name
            INTO :is_column;
            
            /* generate sub-query for each attribute */
            IF (is_column = 1) THEN BEGIN
                temp = 'a.' || child_type_name;
            END ELSE BEGIN
                IF DATA_TYPE = STRING THEN
                CAST( AS DATA_SIZE)
                temp = '(SELECT a2.CONTENT_' || data_type || ' AS "' || :child_type_name || '" FROM ACTIVITY a2 JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a2.ID AND ah.PARENT_ACTIVITY_ID = a.ID AND a2.ACTIVITY_TYPE_ID = CHAR_TO_UUID(''' || UUID_TO_CHAR(child_type_id) || ''')) AS "' || child_type_name || '"';
            END
            
            IF (pos = 1) THEN BEGIN
                stmt = 'SELECT ' || temp;
            END ELSE BEGIN
                stmt = stmt || ', ' || temp;
            END
            --'SELECT a2.CONTENT_' || :data_type || ' FROM ' || :table_name || ' a2 JOIN ' || :table_name || '_HIERARCHY ah ON ah.PARENT_' || :table_name || '_ID = :parent_id WHERE a2.' || :table_name || '_TYPE_ID = :child_type_id'
            
            
        END
        
        parent_type_name = temp_type_name;
        
        SUSPEND;
    END^
    SET TERM ; ^
    
    
    
    
/* Drafted work for outputing a query with all attributes as rows */
SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, a2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, a2.CONTENT_STRING, a2.CONTENT_DATETIME, a2.CONTENT_NUMERIC, a2.CONTENT_BOOLEAN, a2.CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID
JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
JOIN ACTIVITY_TYPE t2 ON t2.ID = a2.ACTIVITY_TYPE_ID
WHERE ah.IS_TEMPLATE = '0'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, a.START_AT AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'START_AT'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, a.END_AT AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'END_AT'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, a.TITLE AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'TITLE'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, a.IS_COMPLETE AS CONTENT_BOOLEAN, NULL AS CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'IS_COMPLETE'

UNION ALL

SELECT
    a.ID AS ACTIVITY_ID, t.NAME AS ACTIVITY_TYPE_NAME, t2.ID AS ATTRIBUTE_ID, t2.NAME AS ATTRIBUTE_TYPE_NAME, NULL AS CONTENT_STRING, NULL AS CONTENT_DATETIME, NULL AS CONTENT_NUMERIC, NULL AS CONTENT_BOOLEAN, a.NOTE AS CONTENT_BLOB
FROM ACTIVITY a
JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
JOIN ACTIVITY_TYPE t2 ON t2.NAME = 'NOTE'

ORDER BY 1

    
    
    
    
    
    
    
    
    
    
    
    
    /* addAttribute */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('ED3E7320-C021-4EE4-91FC-6A725ADBDC00'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('ED3E7320-C021-4EE4-91FC-6A725ADBDC00')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('ED3E7320-C021-4EE4-91FC-6A725ADBDC00') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('ED3E7320-C021-4EE4-91FC-6A725ADBDC00')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'addAttribute', 
        DESCRIPTION = 'Adds a new attribute of the specified type to the referenced existing activity record.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_ID:GUID,ACTIVITY_TYPE_NAME:Varchar(35),CONTENT_BLOB:BLOB,CONTENT_STRING:Varchar(255),CONTENT_DATETIME:Timestamp,CONTENT_NUMERIC:Decimal(10,8)', 
        OUTPUTS = 'ATTRIBUTE_ID:GUID',
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.addAttribute 
    Sequencing:
    - Check to make sure the input parent activity exists
    - Check to make sure the given :activity_type_name is a detail level activity type (aka ATTRIBUTE)
    - Create a new record in the ACTIVITY table of the matching type, with the input content as its value
    - Create a new record in the ACTIVITY_HIERARCHY table to relate the new record with the input parent record
    - Return the GUID generated for the ACTIVITY record
    
    TO DO: allow an optional input of the GUID to use for the new record so that the GUID can be generated outside of the function call.
    */
    EXECUTE BLOCK (
        ACTIVITY_ID GUID = :activity_id,
        ACTIVITY_TYPE_NAME Varchar(35) = :activity_type_name,
        CONTENT_BLOB BLOB = :content_blob,
        CONTENT_STRING Varchar(255) = :content_string,
        CONTENT_DATETIME Timestamp = :content_datetime,
        CONTENT_NUMERIC Decimal(10,8) = :content_numeric
        )
    RETURNS (
        attribute_id GUID )
    AS
    DECLARE VARIABLE id GUID;
    DECLARE VARIABLE type_id GUID;
    BEGIN
        SELECT t.ID 
        FROM ACTIVITY_TYPE t 
        JOIN ACTIVITY a ON a.ID = :activity_id
        JOIN ACTIVITY_TYPE t2 ON t2.ID = a.ACTIVITY_TYPE_ID
        WHERE t.NAME = :activity_type_name
        AND t.ATTRIBUTE <> 0
        AND (t.KIND = t2.KIND)
        INTO :type_id;
        
        IF (type_id IS NULL) THEN EXIT;
        IF (content_blob IS NULL AND content_string IS NULL AND content_datetime IS NULL AND content_numeric IS NULL) THEN EXIT;
        
        /* Make sure this attribute has not already been associated with the given activity */
        id = NULL;
        SELECT ah.ID
        FROM ACTIVITY_HIERARCHY ah
        JOIN ACTIVITY a ON a.ID = ah.CHILD_ACTIVITY_ID
        JOIN ACTIVITY a2 ON a2.ID = ah.PARENT_ACTIVITY_ID
        WHERE ah.PARENT_ACTIVITY_ID = :activity_id
        AND a.ACTIVITY_TYPE_ID = :type_id
        AND a2.IS_TEMPLATE = ''0''
        INTO :id;
        IF (id IS NOT NULL) THEN EXIT;
        
        /* create the parent/container activity */
        attribute_id = GEN_UUID();
        INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:attribute_id, ''ACTIVITY'');
        INSERT INTO ACTIVITY (ID, ACTIVITY_TYPE_ID, CONTENT_BLOB, CONTENT_STRING, CONTENT_DATETIME, CONTENT_NUMERIC)
        VALUES( :attribute_id, :type_id, :content_blob, :content_string, :content_datetime, :content_numeric );
        
        id = GEN_UUID();
        INSERT INTO ENTITY_BASE (ID, TABLE_NAME) VALUES (:id, ''ACTIVITY_HIERARCHY'');
        INSERT INTO ACTIVITY_HIERARCHY (ID, PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID)
        VALUES (:id, :activity_id, :attribute_id);
    SUSPEND;
    END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('ED3E7320-C021-4EE4-91FC-6A725ADBDC00');
    
    
    /* getChildren */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('F8F01599-1C8F-479D-A26D-A7FFCA39B9DE'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('F8F01599-1C8F-479D-A26D-A7FFCA39B9DE')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('F8F01599-1C8F-479D-A26D-A7FFCA39B9DE') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('F8F01599-1C8F-479D-A26D-A7FFCA39B9DE')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'getChildren', 
        DESCRIPTION = 'Fetches all children hierarchically related to the referenced existing activity record. NOTE: The result set will also include the given parent activity, as it is an activity.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_ID:GUID', 
        OUTPUTS = 'PARENT_ACTIVITY_ID:GUID,CHILD_ACTIVITY_ID:GUID,ACTIVITY_TYPE_ID:GUID,ACTIVITY_TYPE_NAME:Varchar(35),ACTIVITY_TYPE_KIND:Integer',
        IS_READ_ONLY = '1', 
        CONTENT_BLOB = CAST('/* ACTIVITY.getChildren 
    Description: Fetches all children hierarchically related to the input parent :activity_id. NOTE: The result set will also include the given parent activity, as it is an activity.
    */
    WITH RECURSIVE tree (PARENT_ACTIVITY_ID, CHILD_ACTIVITY_ID, LEVEL, ACTIVITY_TYPE_ID, ACTIVITY_TYPE_NAME, ACTIVITY_TYPE_KIND)
    AS (
        SELECT a.ID, a.ID, 0 AS LEVEL, t.ID, t.NAME, t.KIND
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        LEFT JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a.ID
        WHERE ah.PARENT_ACTIVITY_ID IS NULL
        AND a.ID = :activity_id
        
        UNION ALL
        
        SELECT ah.PARENT_ACTIVITY_ID, a.ID, tt.LEVEL + 1, t.ID, t.NAME, t.KIND
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        LEFT JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a.ID
            AND ah.CHILD_ACTIVITY_ID <> ah.PARENT_ACTIVITY_ID
        JOIN tree tt ON tt.CHILD_ACTIVITY_ID = ah.PARENT_ACTIVITY_ID
        WHERE t.ATTRIBUTE = 0
    )
    SELECT * FROM tree
    ORDER BY LEVEL' AS BLOB)
    WHERE ID = CHAR_TO_UUID('F8F01599-1C8F-479D-A26D-A7FFCA39B9DE');
    
    /* getAttributes */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('AE1B29B0-4908-473A-BB7B-DF9343BDFB8D'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('AE1B29B0-4908-473A-BB7B-DF9343BDFB8D')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('AE1B29B0-4908-473A-BB7B-DF9343BDFB8D') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('AE1B29B0-4908-473A-BB7B-DF9343BDFB8D')
    WHERE m.ID IS NULL;

    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'getAttributes', 
        DESCRIPTION = 'Fetches all attributes related to the referenced existing activity record. NOTE: The result set will NOT include the parent activity, as it is not an attribute.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_ID:GUID', 
        OUTPUTS = 'CHILD_ACTIVITY_ID:GUID,ACTIVITY_TYPE_ID:GUID,ACTIVITY_TYPE_NAME:Varchar(35),ACTIVITY_TYPE_ATTRIBUTE:Integer,CONTENT_STRING:Varchar(255),CONTENT_DATETIME:Timestamp,CONTENT_NUMERIC:Decimal(10,8),CONTENT_BLOB:BLOB',
        IS_READ_ONLY = '1', 
        CONTENT_BLOB = CAST('/* ACTIVITY.getAttributes 
    Description: Fetches all attributes related to the referenced existing activity record. NOTE: The result set will NOT include the parent activity, as it is not an attribute.
    */
    SELECT a.ID AS CHILD_ACTIVITY_ID, 
    t.ID AS ACTIVITY_TYPE_ID, 
    t.NAME AS ACTIVITY_TYPE_NAME, 
    t.ATTRIBUTE AS ACTIVITY_TYPE_ATTRIBUTE, 
    a.CONTENT_STRING, 
    a.CONTENT_DATETIME, 
    a.CONTENT_NUMERIC, 
    a.CONTENT_BLOB
    FROM ACTIVITY a
    JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
    LEFT JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a.ID
        AND ah.CHILD_ACTIVITY_ID <> ah.PARENT_ACTIVITY_ID
    WHERE t.ATTRIBUTE <> 0
    AND ah.PARENT_ACTIVITY_ID = :activity_id' AS BLOB)
    WHERE ID = CHAR_TO_UUID('AE1B29B0-4908-473A-BB7B-DF9343BDFB8D');
    
        /* delete */
    INSERT INTO ENTITY_BASE (ID,TABLE_NAME)
    SELECT CHAR_TO_UUID('D772FAF0-63FB-4559-9042-4E916CC3862E'), 'METHOD' FROM RDB$DATABASE
    LEFT JOIN ENTITY_BASE eb ON eb.ID = CHAR_TO_UUID('D772FAF0-63FB-4559-9042-4E916CC3862E')
    WHERE eb.ID IS NULL;

    INSERT INTO METHOD (ID)
    SELECT CHAR_TO_UUID('D772FAF0-63FB-4559-9042-4E916CC3862E') FROM RDB$DATABASE
    LEFT JOIN METHOD m ON m.ID = CHAR_TO_UUID('D772FAF0-63FB-4559-9042-4E916CC3862E')
    WHERE m.ID IS NULL;
    
    UPDATE METHOD SET 
        TABLE_NAME = 'ACTIVITY', 
        ROW_ID = NULL, 
        NAME = 'delete', 
        DESCRIPTION = 'Deletes the ACTIVITY record matching the input ID, and any attributes associated with it via the ACTIVITY_HIERARCHY table. The delete_children boolean flag indicates whether or not to cascade the delete down to any child activity records that are related to the one being deleted.', 
        LANGUAGE = 'SQL', 
        INPUTS = 'ACTIVITY_ID:GUID,DELETE_CHILDREN:BOOLEAN', 
        OUTPUTS = 'RESULT:BOOLEAN', 
        IS_READ_ONLY = '0', 
        CONTENT_BLOB = CAST('/* ACTIVITY.delete
    Sequencing:
    - Check to make sure the given :activity_id corresponds to an activity record 
    - If the delete children flag is true, then recursively call this function for each child
    - Fetch the attributes related to the container activity:
        - Delete the ACTIVITY_HIERARCHY record relating each attribute to the container activity
        - Delete the ACTIVITY record representing each attribute
    - Finally, delete the input container activity from the ACTIVITY table
    */
    EXECUTE BLOCK (
        ACTIVITY_ID GUID = :activity_id,
        DELETE_CHILDREN BOOLEAN = :delete_children
        )
    RETURNS (
        RESULT BOOLEAN )
    AS
    DECLARE VARIABLE id GUID;
    DECLARE VARIABLE stmt_recurse BLOB:
    DECLARE VARIABLE child_id GUID;
    DECLARE VARIABLE type_id GUID;
    BEGIN
        /* Call method recursively if cascading delete is specified */
        IF (delete_children = ''1'') THEN BEGIN
            SELECT m.CONTENT_BLOB FROM METHOD m WHERE m.TABLE_NAME = ''ACTIVITY'' AND m.NAME = ''delete'' AND m.ROW_ID IS NULL
            INTO stmt_recurse;
            
            FOR
            SELECT a.ID
            FROM ACTIVITY a
            JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
            JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a.ID
            WHERE t.ATTRIBUTE = 0
            AND ah.PARENT_ACTIVITY_ID = :activity_id
            INTO :child_id
            DO BEGIN
                EXECUTE STATEMENT (stmt_recurse) (ACTIVITY_ID := child_id, DELETE_CHILDREN := delete_children)
                INTO :result;
                /* If deleting one of the children fails, then exit and return the failure */
                IF (result = ''0'') THEN BEGIN
                    SUSPEND;
                    EXIT;
                END
            END
        END
        
        FOR
        SELECT ah.ID, a.ID
        FROM ACTIVITY a
        JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
        JOIN ACTIVITY_HIERARCHY ah ON ah.CHILD_ACTIVITY_ID = a.ID
        WHERE t.ATTRIBUTE <> 0
        AND ah.PARENT_ACTIVITY_ID = :activity_id
        INTO :id, :child_id
        DO BEGIN
            DELETE FROM ACTIVITY_HIERARCHY ah WHERE ah.ID = :id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :id;
            
            DELETE FROM ACTIVITY a WHERE a.ID = :child_id;
            DELETE FROM ENTITY_BASE eb WHERE eb.ID = :child_id;
        END
        
        DELETE FROM ACTIVITY a WHERE a.ID = :activity_id;
        DELETE FROM ENTITY_BASE eb WHERE eb.ID = :activity_id;
    RESULT = ''1'';
    SUSPEND;
    END' AS BLOB)
    WHERE ID = CHAR_TO_UUID('D772FAF0-63FB-4559-9042-4E916CC3862E');



    
    
    
    COMMIT;