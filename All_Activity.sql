/* --------------------------------------------------------------
PURPOSE:
	
	All Activities
	(Lists all activities grouped by the party they are associated
    with, as well as the center location there are assigned to,
    if any.)
	
	References:
	Stack Overflow having the same issue as we are:
		http://stackoverflow.com/questions/28175328/tfdupdatesql-not-posting-updates-on-datasnap-server
	Embarcadero documentation of what we want to do:
		http://docwiki.embarcadero.com/CodeExamples/Tokyo/en/FireDAC.TFDQuery.OnUpdateRecord_Sample
	
	So, to summarize today's understanding:
	- Each table that you want to update/insert/delete needs a TFDUpdateSQL component
	- Tables that you do not want to update/insert/delete do NOT need a TFDUpdateSQL component
	- You need to write your own frigging plumbing to sequence your TFDUpdateSQL statements in order that you want with code written in the .pas file of the project
	- When writing sequencing for a form (e.g. order of statement calls) make sure it is topped with a heading that describes what is going on (see embarcadero example)
	
CREATED: 2017-05-29
UPDATED: 2017-05-20 with changed schema
*/

	SELECT
		a.ID,
		t2.NAME,
		a2.CONTENT_BLOB,
		a2.CONTENT_STRING,
		a2.CONTENT_DATETIME,
		a2.CONTENT_NUMERIC,
		ah.POSITION_INDEX,
		a2.ID AS DETAIL_ID
	FROM ACTIVITY a
	JOIN ACTIVITY_TYPE t ON t.ID = a.ACTIVITY_TYPE_ID
	JOIN ACTIVITY_HIERARCHY ah ON ah.PARENT_ACTIVITY_ID = a.ID 
	JOIN ACTIVITY a2 ON a2.ID = ah.CHILD_ACTIVITY_ID
	JOIN ACTIVITY_TYPE t2 ON t2.ID = a2.ACTIVITY_TYPE_ID
	WHERE t.NAME = 'SCHEDULE_CHANGE'
	ORDER BY a.ID, ah.POSITION_INDEX

/*
UNDER CONSTRUCTION:

Summary of SQL sequencing necessary to perform CRUD operations on activities:

INSERT:
    - Create ENTITY_BASE record
    - Create ACTIVITY record
    - Create ACTIVITY_TASK or ACTIVITY_EVENT subtype record (if necessary?)
    - Create PARTICIPANT record (to tie the ACTIVITY to the PARTY)
    
UPDATE:
    - If the center is updated, don't change the name of a center location! Change the PARTY_ROLE_LEARNING_CENTER_ID value in the ACTIVITY table
    - If the party ID is updated, don't change the ID of the party! Change the PARTY_ID value in the ACTIVITY table
    - ...
    
DELETE:
    - Delete the PARTICIPANT record
    - Delete the subtype ACTIVITY_TASK or ACTIVITY_EVENT record
    - Delete the ACTIVITY record
    - Delete the ENTITY_BASE record
    
*/

